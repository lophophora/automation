#!/usr/bin/python3
# coding=utf-8

"""
Alexander Robinson - 2017-06-26

NI 9214 - 16-Channel, 68 S/s Aggregate, ±78 mV, Isothermal C Series Temperature Input Module
http://www.ni.com/en-us/support/model.ni-9214.html

Acquisition settings:
High Speed - 8 Samples, 1 Hz
High Resolution - 130 Samples, 80 Hz

Prior to calibration, set the calibrator to 2.0V and lock the output range to 3.3V

"""

from PyDAQmx import *
from PyDAQmx.DAQmxTypes import *
import numpy
import time

## I'm really glad I wrote this..
from selectaroo import menu

import visa

# Set up the visa session with the calibrator.
rm = visa.ResourceManager()
print( "\nPlease select a calibrator from the list of available devices.")
## Here's the menu in all of it's glory.
calibrator = rm.open_resource(menu(rm.list_resources()))


## We will forego this so as not to remove the range lock required for calibration
# Reset the calibrator
# calibrator.write("*RST;*WAI")

# Declarations galore
device = "cDAQ1Mod1"
channel = "/ai0:15"
channels = 16
read = int32()
taskHandle = TaskHandle(0)

highResVolts = numpy.zeros((16,8), dtype=numpy.float64)
highSpeedVolts = numpy.zeros((16,130), dtype=numpy.float64)

highResVoltsAverage = []
highSpeedVoltsAverage = []

voltageSetpoints = ["-0.07", "-0.035", "0.0", "0.035", "0.07"]
inputRange = 0.078125

input("Connect the NI-9214 for voltage measurements and press enter to begin.")

for setpoint in voltageSetpoints :

    print( "Setting calibrator to", setpoint, "volts." )

    ## This seems fine to leave here because the setpoint voltages are below the
    ## maximum inputRange of the unit. 
    calibrator.write("OUT "+setpoint+"V;*WAI;OPER")

    ## Voltage High Resolution
    print("Acquiring High Resolution Voltage Readings")
    time.sleep(4)
    sampleCount = 8
    sampleRate = 1

    try:
        DAQmxCreateTask("",byref(taskHandle))
        DAQmxCreateAIVoltageChan(taskHandle, str(device+channel), "", DAQmx_Val_Cfg_Default, -inputRange, inputRange, DAQmx_Val_Volts,None)
        DAQmxCfgSampClkTiming(taskHandle, "", sampleRate, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, sampleCount)
        DAQmxSetAIADCTimingMode(taskHandle, str(device+channel), DAQmx_Val_HighResolution)
        DAQmxStartTask(taskHandle)
        DAQmxReadAnalogF64(taskHandle, -1, -1, DAQmx_Val_GroupByChannel, highResVolts, sampleCount*channels, byref(read), None)
        DAQmxWaitUntilTaskDone(taskHandle, -1)

    except DAQError as err:
        print ("DAQmx Error: %s", err )

    finally:
        if taskHandle:
            DAQmxStopTask(taskHandle)
            DAQmxClearTask(taskHandle)

    for j in range( channels ):
        highResVoltsAverage.append(numpy.average(highResVolts[j]))
        #print( "Channel", j, ":", numpy.average(highResVolts[j]))

    #print(highResVoltsAverage)
    

    ## Voltage High Speed
    print("Acquiring High Speed Voltage Readings")
    time.sleep(4)
    sampleCount = 130
    sampleRate = 80

    try:
        DAQmxCreateTask("",byref(taskHandle))
        DAQmxCreateAIVoltageChan(taskHandle, str(device+channel), "", DAQmx_Val_Cfg_Default, -inputRange, inputRange, DAQmx_Val_Volts,None)
        DAQmxCfgSampClkTiming(taskHandle, "", sampleRate, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, sampleCount)
        DAQmxSetAIADCTimingMode(taskHandle, str(device+channel), DAQmx_Val_HighSpeed)
        DAQmxStartTask(taskHandle)
        DAQmxReadAnalogF64(taskHandle, -1, -1, DAQmx_Val_GroupByChannel, highSpeedVolts, sampleCount*channels, byref(read), None)
        DAQmxWaitUntilTaskDone(taskHandle, -1)

    except DAQError as err:
        print ("DAQmx Error: %s", err )

    finally:
        if taskHandle:
            DAQmxStopTask(taskHandle)
            DAQmxClearTask(taskHandle)

    for j in range( channels ):
        highSpeedVoltsAverage.append(numpy.average(highSpeedVolts[j]))
        print( "Channel", j, ":", numpy.average(highSpeedVolts[j]))

    #print(highSpeedVoltsAverage)

# Shut it off
calibrator.write("STBY")


## Let the data dump begin..

outputFile = open('NI-9214 output.csv', 'w')

print("")

outputFile.write("High Resolution:\n")
for x in range( channels ) :
    ch = "Channel " + str(x + 1) + ","
    #print(highResVoltsAverage[x], "\n", highResVoltsAverage[x+16], "\n", highResVoltsAverage[x+32], "\n", highResVoltsAverage[x+48], "\n", highResVoltsAverage[x+64], "\n",sep="")
    output = ch + str(highResVoltsAverage[x]*1000) + "\n" + ch + str(highResVoltsAverage[x+16]*1000) + "\n" + ch + str(highResVoltsAverage[x+32]*1000) + "\n" + ch + str(highResVoltsAverage[x+48]*1000) + "\n" + ch + str(highResVoltsAverage[x+64]*1000) + "\n"
    outputFile.write(output)

    ch = "Channel " + str(x + 1) + ","
outputFile.write("High Speed:\n")
for x in range( channels ) :    
    #print(highSpeedVoltsAverage[x], "\n", highSpeedVoltsAverage[x+16], "\n", highSpeedVoltsAverage[x+32], "\n", highSpeedVoltsAverage[x+48], "\n", highSpeedVoltsAverage[x+64], "\n",sep="")
    output = ch +  str(highSpeedVoltsAverage[x]*1000) + "\n" + ch + str(highSpeedVoltsAverage[x+16]*1000) + "\n" + ch + str(highSpeedVoltsAverage[x+32]*1000) + "\n" + ch + str(highSpeedVoltsAverage[x+48]*1000) + "\n" + ch + str(highSpeedVoltsAverage[x+64]*1000) + "\n"
    outputFile.write(output)

# Session cleanup
outputFile.close()
calibrator.close()
#EOF
