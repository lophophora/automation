


read = int32()

#taskHandle  TaskHandle 
taskHandle = TaskHandle(0)
#physicalChannel  const char [] 
physicalChannel = 'cDAQ1Mod1'
#nameToAssignToChannel  const char []
nameToAssignToChannel
#minVal  float64 
minVal = 0
#maxVal  float64 
maxVal = 100
#units  int32
'''
DAQmx_Val_DegC   degrees Celsius 
DAQmx_Val_DegF   degrees Fahrenheit 
DAQmx_Val_Kelvins   kelvins 
DAQmx_Val_DegR   degrees Rankine 
'''
units
#thermocoupleType  int32
'''
DAQmx_Val_J_Type_TC   J-type thermocouple. 
DAQmx_Val_K_Type_TC   K-type thermocouple. 
DAQmx_Val_N_Type_TC   N-type thermocouple. 
DAQmx_Val_R_Type_TC   R-type thermocouple. 
DAQmx_Val_S_Type_TC   S-type thermocouple. 
DAQmx_Val_T_Type_TC   T-type thermocouple. 
DAQmx_Val_B_Type_TC   B-type thermocouple. 
DAQmx_Val_E_Type_TC   E-type thermocouple. 
'''
thermocoupleType
#cjcSource  int32 
'''
DAQmx_Val_BuiltIn   Use a cold-junction compensation channel built into the terminal block. 
DAQmx_Val_ConstVal   You must specify the cold-junction temperature. 
DAQmx_Val_Chan   Use a channel for cold-junction compensation. 
'''
cjcSource = 'DAQmx_Val_BuiltIn'
#cjcVal  float64 
cjcVal = ""
#cjcChannel  const char []  
cjcChannel = ""

DAQmxCreateAIThrmcplChan(
    taskHandle,
    physicalChannel,
    nameToAssignToChannel,
    minVal,
    maxVal,
    units,
    thermocoupleType,
    cjcSource,
    cjcVal,
    cjcChannel)
    
