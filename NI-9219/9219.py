

import visa
import numpy

from PyDAQmx import *
from PyDAQmx.DAQmxTypes import *




rm = visa.ResourceManager()

cal = rm.open_resource('visa://172.27.0.11/GPIB0::4::INSTR')


cal.write("*RST;*WAI")




read = int32()

highResVolts = numpy.zeros((4,20), dtype=numpy.float64)


taskHandle = TaskHandle(0)

DAQmxCreateTask("",byref(taskHandle))


DAQmxCreateAIVoltageChan(taskHandle, \
                         'cDAQ1Mod1/ai0:3', \
                         "", \
                         DAQmx_Val_Cfg_Default, \
                         -60, \
                         60, \
                         DAQmx_Val_Volts, None)


DAQmxCfgSampClkTiming(taskHandle, "", 1.95, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, 20)


DAQmxSetAIADCTimingMode(taskHandle, 'cDAQ1Mod1/ai0:3', DAQmx_Val_HighResolution)

DAQmxStartTask(taskHandle)

DAQmxReadAnalogF64(taskHandle, -1, -1, \
                   DAQmx_Val_GroupByChannel, \
                   highResVolts, \
                   80, \
                   byref(read), None)

DAQmxWaitUntilTaskDone(taskHandle, -1)

DAQmxStopTask(taskHandle)

DAQmxClearTask(taskHandle)





