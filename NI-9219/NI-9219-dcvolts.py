#!/usr/bin/python3
# coding=utf-8

"""
Alexander Robinson - 2017-06-08

NI 9219 4-Channel Universal Analog Input


"""


import time
startTime = time.time()


import sys

import PyDAQmx
from PyDAQmx import *
from PyDAQmx.DAQmxTypes import *

import json
import configparser
from datetime import datetime

import numpy
import time
from selectaroo import menu
import visa


# Get calibrator object
rm = visa.ResourceManager()
print( "\nPlease select a calibrator from the list of available devices.")
## Here's the menu in all of it's glory.


menuSelection = menu(rm.list_resources())

if (menuSelection == "q") :
    print( "\nQuit selected, exiting." )
    print( "Execution Time: ", time.time() - startTime, "seconds.\n")
    sys.exit("10")
else :
    calibrator = rm.open_resource(menuSelection)

#calibrator = rm.open_resource(menu(rm.list_resources()))

# Reset the calibrator
calibrator.write("*RST;*WAI")

# Declarations galore
device = "cDAQ1Mod1"

'''
#Voltage Accuracy
'''

dcVoltsAverage = []
voltageSetpoints = {60.0:[48.0,0.0,-48.0],
                    15.0:[3.2,0.0,-3.2],
                    1.0:[0.8,0.0,-0.8],
                    0.125:[0.1,0.0,-0.1]}

## number of samples per channel
sampleCount = 20
channels = 4

#ADC timing mode - high res

ranges = list(sorted(voltageSetpoints.keys(), reverse=True))



print("\nAcquiring High Resolution Voltage Readings")
input("\nConnect all 9219 channels to the 5520 and press enter to continue...")

## do this 5 times, once for each range
for voltRange in ranges:
    print("\nCommencing acquisition for",voltRange,"volt range")

    ## do this three times for each setpoint
    for setpoint in voltageSetpoints[voltRange]:
        try:

            read = int32()
            taskHandle = TaskHandle(0)
            #dcVolts = numpy.zeros((4,20), dtype=numpy.float64)
            dcVolts = numpy.zeros((channels,sampleCount), dtype=numpy.float64)
            
            DAQmxCreateTask("",byref(taskHandle))
            ## Set the DAQ module to the appropriate setting
            ## channel is cDAQ1Mod1/ai'+channel (starts at 0), max range is voltRange, and min range is -voltRange
            DAQmxCreateAIVoltageChan(taskHandle, 'cDAQ1Mod1/ai0:3', "", DAQmx_Val_Cfg_Default, \
                                     -float(voltRange), float(voltRange), DAQmx_Val_Volts, None)

            # 1.95 Hz, for sampleCount number of samples
            DAQmxCfgSampClkTiming(taskHandle, "", 1.95, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, sampleCount)
            DAQmxSetAIADCTimingMode(taskHandle, 'cDAQ1Mod1/ai0:3', DAQmx_Val_HighResolution)


            DAQmxTaskControl(taskHandle, DAQmx_Val_Task_Verify)

            DAQmxTaskControl(taskHandle, DAQmx_Val_Task_Reserve)

            DAQmxTaskControl(taskHandle, DAQmx_Val_Task_Commit)

            ## Begin task
            DAQmxStartTask(taskHandle)

            DAQmxWaitUntilTaskDone(taskHandle, -1)

            ## Set the calibrator to the appropriate setting
            print( "--Setting calibrator to", setpoint, "volts." )
            calibrator.write("OUT "+setpoint+"V;*WAI;OPER")
            
            if (abs(float(setpoint)) == 48.0) :
                print("--Setpoint is ", setpoint, "-", end=" ")
                input("Press OPER on the calibrator, then enter to continue")
            else :
                print("--Setpoint is ", setpoint)

            time.sleep(5)

            # dcVolts is the array                
            DAQmxReadAnalogF64(taskHandle, sampleCount, 0, DAQmx_Val_GroupByChannel, \
                               dcVolts, \
                               sampleCount*channels, \
                               byref(read), None)
            
            time.sleep(40)

            DAQmxWaitUntilTaskDone(taskHandle, -1)

            DAQmxTaskControl(taskHandle, DAQmx_Val_Task_Unreserve)

            print("\n***Debug***\n")
            print(dcVolts)
            print("")            
        
            print("")
            for j in range(channels) :
                dcVoltsAverage.append(numpy.average(dcVolts[j]))
                print("Channel",j,": ",dcVoltsAverage[-1])
            print("")
                
        except DAQError as err:
            print ("DAQmx Error: %s", err )

        finally:
            if taskHandle:
                DAQmxStopTask(taskHandle)
                DAQmxClearTask(taskHandle)
                #time.sleep(5)
                calibrator.write("STBY")



####
'''
Cleanup
'''

calibrator.close()

print( "\nExecution Time: ", time.time() - startTime, "seconds.\n")
