#!/usr/bin/python3
# coding=utf-8

"""
Alexander Robinson - 2017-06-08

NI 9219 4-Channel Universal Analog Input


"""



from PyDAQmx import *
from PyDAQmx.DAQmxTypes import *
import numpy
import time
from selectaroo import menu
import visa
import sys


startTime = time.time()

# Get calibrator object
rm = visa.ResourceManager()
print( "\nPlease select a calibrator from the list of available devices.")



## Here's the menu in all of it's glory.


menuSelection = menu(rm.list_resources())

if (menuSelection == "q") :
    print( "\nQuit selected, exiting." )
    print( "Execution Time: ", time.time() - startTime, "seconds.\n")
    sys.exit("10")
else :
    calibrator = rm.open_resource(menuSelection)

#calibrator = rm.open_resource(menu(rm.list_resources()))



# Reset the calibrator
calibrator.write("*RST;*WAI")

# Declarations galore
device = "cDAQ1Mod1"

'''
#Voltage Accuracy
'''
read = int32()
taskHandle = TaskHandle(0)
highResVolts = numpy.zeros((20), dtype=numpy.float64)
highResVoltsAverage = []
voltageSetpoints = {"60":["48.0","0.0","-48.0"], "15":["12.0","0.0","-12.0"], "4":["3.2","0.0","-3.2"], "1":["0.8","0.0","-0.8"], "0.125":["0.1","0.0","-0.1"]}                                                   
## number of samples per channel, one channel at a time
sampleCount = 20

# Start with channel 1 - 20 readings averaged.

#ADC timing mode - high res
'''
ranges = list(voltageSetpoints.keys())


print("Acquiring High Resolution Voltage Readings")
## do this 4 times for each channel
for channel in range(4):
    print("Now Calibrating channel",channel)
    input("Connect the terminal connector from the 9219 to the 5520 and press enter to continue")

    ## do this 5 times for each range
    for voltRange in ranges:
        print("Commencing acquisition for",voltRange,"volt range")

        ## do this three times for each setpoint
        for setpoint in voltageSetpoints[voltRange]:
            try:
                DAQmxCreateTask("",byref(taskHandle))
                ## Set the DAQ module to the appropriate setting
                ## channel is cDAQ1Mod1/ai'+channel (starts at 0), max range is voltRange, and min range is -voltRange
                DAQmxCreateAIVoltageChan(taskHandle, 'cDAQ1Mod1/ai'+str(channel), "", DAQmx_Val_Cfg_Default, -float(voltRange), float(voltRange), DAQmx_Val_Volts,None)

                ## Set the calibrator to the appropriate setting
                print( "Setting calibrator to", setpoint, "volts." )
                calibrator.write("OUT "+setpoint+"V;*WAI;OPER")
                print("Setpoint is ", setpoint)
                if (voltRange == "60") :
                    input("Please press OPER on the calibrator and press enter to continue")
                time.sleep(5)
                # 1.95 Hz, for sampleCount number of samples
                DAQmxCfgSampClkTiming(taskHandle, "", 1.95, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, sampleCount)
                DAQmxSetAIADCTimingMode(taskHandle, 'cDAQ1Mod1/ai'+str(channel), DAQmx_Val_HighResolution)
                DAQmxStartTask(taskHandle)
                # highResVolts is the array                
                DAQmxReadAnalogF64(taskHandle, -1, -1, DAQmx_Val_GroupByChannel, highResVolts, sampleCount, byref(read), None)
                DAQmxWaitUntilTaskDone(taskHandle, -1)
                calibrator.write("STBY")
                highResVoltsAverage.append(numpy.average(highResVolts))

            except DAQError as err:
                print ("DAQmx Error: %s", err )

            finally:
                if taskHandle:
                    DAQmxStopTask(taskHandle)
                    DAQmxClearTask(taskHandle)



            
'''
'''
Current Accuracy
'''
currentSetpoints = ["0.019","0.0","-0.019"]
highResAmps = numpy.zeros((20), dtype=numpy.float64)
highResAmpsAverage = []

print("Acquiring High Resolution Current Readings")
## do this 4 times for each channel
for channel in range(4):
    print("Now Calibrating channel",channel)
    input("Connect the terminal connector from the 9219 to the 5520 and press enter to continue")

    print("Commencing acquisition for current range")

    ## do this three times for each setpoint
    for setpoint in currentSetpoints:
        try:
            DAQmxCreateTask("",byref(taskHandle))
            ## Set the DAQ module to the appropriate setting
            ## channel is cDAQ1Mod1/ai'+channel (starts at 0), max range is voltRange, and min range is -voltRange
            #DAQmxCreateAICurrentChan(taskHandle, 'cDAQ1Mod1/ai'+str(channel), "", DAQmx_Val_Cfg_Default, -0.025, 0.025, DAQmx_Val_Volts,None)
            DAQmxCreateAICurrentChan(taskHandle, 'cDAQ1Mod1/ai'+str(channel), "", DAQmx_Val_Diff, -0.025, 0.025, DAQmx_Val_Amps, DAQmx_Val_Internal, 0.0, None)
        
            ## Set the calibrator to the appropriate setting
            #print( "Setting calibrator to", setpoint, "amps." )
            #calibrator.write("OUT "+setpoint+"A;*WAI;OPER")

            time.sleep(3)
            # 1.95 Hz, for sampleCount number of samples
            DAQmxCfgSampClkTiming(taskHandle, "", 1.95, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, sampleCount)
            DAQmxSetAIADCTimingMode(taskHandle, 'cDAQ1Mod1/ai'+str(channel), DAQmx_Val_HighResolution)
            DAQmxStartTask(taskHandle)
            print( "Setting calibrator to", setpoint, "amps." )
            calibrator.write("OUT "+setpoint+"A;*WAI;OPER")
            time.sleep(3)
            # highResVolts is the array                
            DAQmxReadAnalogF64(taskHandle, -1, -1, DAQmx_Val_GroupByChannel, highResAmps, sampleCount, byref(read), None)
            DAQmxWaitUntilTaskDone(taskHandle, -1)
            calibrator.write("STBY")
            highResAmpsAverage.append(numpy.average(highResAmps))

        except DAQError as err:
            print ("DAQmx Error: %s", err )

        finally:
            if taskHandle:
                DAQmxStopTask(taskHandle)
                DAQmxClearTask(taskHandle)



#outputFile = open('NI-9219.csv', 'w')
for x in highResAmpsAverage:
    print(x)

print( "\nExecution Time: ", time.time() - startTime, "seconds.\n")
