#!/usr/bin/python
#encoding=utf-8

def engToAbstract(eString) :
    """
    Converts e-notated numbers to a list containing the value
    in abstract representation as per
    Decimal Arithmetic Specification, version 1.07
    Copyright (c) IBM Corporation, 2002. All rights reserved. ©

    Engineering notation per IBM decimal notation conventions:
    "12.3E+7"      [0,123,6]
    "1.23E+3"      [0,123,1]
    "12.3E+7"      [0,123,6]
    "1234.5E-4"    [0,12345,-5]

    """
    abstract = []
    # Sign
    if eString[0] == "+" :
        abstract.append(0)
    elif eString[0] == "-" :
        abstract.append(1)
    else :
        print("This was unexpected. Illegal sign character in ", eString,".", sep="")
        return "Error"

    # Coefficient
    # Count the decimals first
    decimals = (eString.index("E") - eString.index(".")) - 1
    abstract.append(int(eString[1:eString.index("E")].replace(".","")))

    # Exponent
    if eString[-3] == "+" :
        abstract.append((int(eString[-2:]) - decimals))
    elif eString[-3] == "-" :
        abstract.append((int(eString[-2:]) + decimals) * -1 )
    else :
        print("This was unexpected. Illegal exponent character in ", eString,".",sep="")
        return "Error"

    # RETURN [sign, coefficient, exponent]
    return abstract

def engToFloat(eString) :
    """
    Converts abstract represented numbers to a floating point number.
    This is entirely dependent on the engToAbstract function.
    """
    # Send eString to engToAbstract
    # Multiply coefficient by 10 to the exponent
    abstract = engToAbstract(eString)
    #print(abstract)
    floatValue = float(abstract[1]) * 10 ** float(abstract[2])
    if abstract[0] == 1 :
        floatValue = (floatValue * -1)
    return floatValue

def metricPrefix(dataPoint, prefix) :
    """
    Given the value datapoint and the metric prefix (p n m k M G),
    multiply the value by the corresponding exponent, and return
    the floating point equivalent.  Assumes number is provided at
    the base unit (eg, volts. 10^0)
    If this module doesn't workout it's because it was ill planned
    and poorly conceived.
    """
    prefices = {"E":18,"P":15,"T":12,"G":9,"M":6,"k":3,"h":2,"d":1,"0":0,"d":-1,"c":-2,"m":-3,"u":-6,"n":-9,"p":-12,"f":-15,"a":-18}

    if prefix in prefices :
        convertedValue = 0.0
        #print(10 ** prefices[prefix])
        convertedValue = float(dataPoint) * 10 ** float(prefices[prefix] * -1)
    else:
        print("Invalid argument: Prefix.\nPlease select one of: ",
                list(prefices.keys()))
        return "Error"
    return convertedValue



'''

#readData = "-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,+2.81300000E+01,-8.79210000E+01,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,+2.89260000E+01,-9.90000000E+37"
readData= "-10.12345E-03,+1.2345678E+03"


dataList = readData.split(",")

for x in range( len( dataList ) ):
	print(engToFloat(dataList[x]))


print(metricPrefix(dataList[1], "k"))
'''
