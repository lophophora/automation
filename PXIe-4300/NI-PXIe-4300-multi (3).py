#!/usr/bin/python3
# coding=utf-8

"""
Alexander Robinson - 2017-11-04

NI PXIe-4300 


"""

import time
startTime = time.time()

from datetime import datetime
from datetime import datetime


import sys
import os

import sys
import os

import visa

from PyDAQmx import *
from PyDAQmx.DAQmxTypes import *

from selectaroo import menu


def getSONumber():
    SONumber = input( "Please enter the SO Number of the UUT: ")
    return SONumber
    





## Needed Assets,
#  Fluke 5520A
#  PXIe-4300

#   Device ID      Device Name 
#   0x7492C4C4     NI PXIe-4300 


rm = visa.ResourceManager()
#deviceList = list(rm.list_resources())
#deviceInfo = rm.list_resources_info()
#deviceList = list(deviceInfo)

print( "\nPlease select a calibrator from the list of available devices.")
#cal = menu(deviceList)

calibrator = rm.open_resource("GPIB0::4::INSTR")
calibrator.write("OUT 0V;*WAI;STBY")

#print( "\nPlease select the PXIe-4300 from the list of available devices.")
#pxi = menu(deviceList)

#pxi = "PXI39::0::INSTR"

## Set the physical channel variable to the physical address channel or alias as specified in MAX
#physicalChannel = deviceInfo[pxi][-1]

#physicalChannel = 'PXISlot3/ai0:7, '


physicalChannel = 'PXISlot3/ai0:7, PXISlot4/ai0:7'
'''

>>> devinfo = rm.list_resources_info()
>>> devinfo['PXI39::0::INSTR'][-1]
'PXISlot3'

'''


## Including leading zeros on the keys so the numbers sort correctly
setpoints = {
    "01.0":["0.0","0.998","-0.998"],
    "02.0":["0.0","1.996","-1.996"],
    "05.0":["0.0","4.990","-4.990"],
    "10.0":["0.0","9.980","-9.980"]}

## This has to be sorted in 3.4,
#  python 3.6 returns the keys in their stated order
voltageRanges = list(sorted(setpoints.keys()))


numberOfCards = 2
channelsPerCard = 8
channels = channelsPerCard * numberOfCards

samplesPerChannel = 10000
samplesTotal = channels * samplesPerChannel
sampleRate = 100000.0


readings = numpy.zeros((channels,samplesPerChannel), dtype=numpy.float64)
average = [[] for i in range(channels)]


for voltageRange in voltageRanges :

    # Remove leadings zeros from setpoints.
    minMax = voltageRange.lstrip("0")

    try:
        ## Initialize the variables
        read = int32()
        taskHandle = TaskHandle(0)
        readings = numpy.zeros((channels,samplesPerChannel), dtype=numpy.float64)

                    ## Initialize the task
        DAQmxCreateTask("",byref(taskHandle))

        DAQmxCreateAIVoltageChan(taskHandle, \
                                 physicalChannel, \
                                 "", \
                                 DAQmx_Val_Diff, \
                                 -float(minMax), \
                                 float(minMax), \
                                 DAQmx_Val_Volts, \
                                 None)

        DAQmxCfgSampClkTiming(taskHandle, \
                              "", \
                              sampleRate, \
                              DAQmx_Val_Rising, \
                              DAQmx_Val_FiniteSamps, \
                              samplesPerChannel)

        DAQmxTaskControl(taskHandle, DAQmx_Val_Task_Verify)
        DAQmxTaskControl(taskHandle, DAQmx_Val_Task_Reserve)
        DAQmxTaskControl(taskHandle, DAQmx_Val_Task_Commit)

        for setpoint in setpoints[voltageRange] :

            print("\nChannel:",physicalChannel)
            print("Voltage Range:",minMax,"V\tSetpoint:",setpoint,"V")
            calibrator.write("OUT "+setpoint+"V;*WAI;")
            calibrator.write("OPER")
            time.sleep(5)
            

            readStart = datetime.now()

            DAQmxReadAnalogF64(taskHandle,
                               samplesPerChannel,
                               20,
                               DAQmx_Val_GroupByChannel,
                               readings,
                               samplesTotal,
                               byref(read),
                               None)

            readStop = datetime.now()

            readTime = readStop - readStart
            
            print("")
            for channelIndex in range(channels):
                #averageReadings.append(numpy.average(readings[channelIndex]))
                #average[channelIndex].append(averageReadings[-1])
                average[channelIndex].append(numpy.average(readings[channelIndex]))
                print("\t\tChannel:",channelIndex,": ",average[channelIndex][-1])

            print("\nPerformance: ",samplesTotal,
                  " readings in ",readTime.microseconds,
                  " microseconds (",round(readTime.microseconds/samplesTotal, 4),
                  " samples per uS.)",sep="")
        
    except DAQError as err :
        print ("DAQmx Error: %s", err )
        calibrator.write("STBY")

    finally :
        #print("finally\n")
        if taskHandle:
            DAQmxStopTask(taskHandle)
            DAQmxClearTask(taskHandle)
            calibrator.write("STBY")     





SONumber = "test"
time.strftime("%Y-%m-%d", time.localtime())
fileName = time.strftime("%Y-%m-%d - %H-%M - ", time.localtime())+SONumber+".csv"
f = open( fileName, 'a' )
output = str(fileName) + "\n"
f.write(output)

for channelIndex in range(channels) :
    for readingIndex in range(len(average[channelIndex])) :
        output = str( average[channelIndex][readingIndex] ) + "\n"
        f.write(output)
f.close()



calibrator.close()
rm.close()
                                   
endTime = time.time()
executionTime = endTime - startTime
print( "\nExecution Time: ", executionTime, "seconds. (",executionTime / 60,"minutes)\n")            



