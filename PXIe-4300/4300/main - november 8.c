#include <stdio.h>
//#include <stdlib.h>     // probably don't need this one
//#include <stddef.h>     // probably don't need this either
#include <string.h>
#include <NIDAQmx.h>
#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else

// Output to file as comma separated values.
void outputData(float64 data[]);
void outputData(float64 avg[])
{
    int i = 0;

    FILE *outputFile;
    outputFile = fopen("./4300 Output.csv", "a");

    for ( i = 0 ; i < 8 ; i++ )
    {
        fprintf(outputFile, "%f,",avg[i]);
    }
    fprintf(outputFile, "\n");
    fclose(outputFile);
}

void printAverage(float64 data[]);
void printAverage(float64 data[])        // This was the function that sucks.
{
    float64 avg[8];
    float64 sum = 0.0;
    int i = 0;
    int j = 0;
    int k = 0;

    for ( i = 0 ; i < 8 ; i++ )
    {
        sum = 0.0;
        k = (j+10000);
        for ( ; j < k ; j++ )
        {
            sum += data[j];
        }
        avg[i] = ( sum / 10000 );
    }
    outputData(avg);
}


int main(void)
{
    // NI Example Variables
    int32       error=0;
    TaskHandle  taskHandle=0;
    int32       read;
    float64     data[80000];
    char        errBuff[2048]={'\0'};
    // My variables
    int     i = 0;
    char    cardAddress[50];

    char    *setpoint[3];
    setpoint[0]="first";
    setpoint[1]="second";
    setpoint[2]="third";

    float   signalRange[4] = {1.0, 2.0, 5.0, 10.0};

    printf("NI PXIe-4300 Isolated Analog Input Module Data Acquisition - Alex Robinson\n");
    printf("==========================================================================\n");
    printf("This program will output a file named \"4300 Output.csv\" in the current\ndirectory containing the test points as comma separated values.\n\n");
    printf("This program also comes with no warranty, enjoy!");

    printf("Please enter the Physical Address string of the NI-4300 card.\n\nExample: PXI1Slot2_1\n\nChannel Name: ");
    scanf("%s", cardAddress);

    strcat(cardAddress, "/ai0:7");

    const char * physicalChannel = cardAddress;

    printf("\"%s\"\n", physicalChannel);
    getchar();

    /*
    This is all NI junk, which reads analog input values from my PXI chassis into the array called data[]
    In this case 80k samples are read.  The first 10k samples are for channel 0, the next 10k samples are for channel 1, and so on.
    Then I want to pass the array to a function which then averages the first 10k indicies, then the next 10k, etc.
    These values will then get dumped to stdout for testing and later to a text file as comma separated values.
    */

    printf("\nVoltage range: 1 V\nSetpoints: 0.0, 0.998, -0.998 V\n\n");

    for ( i = 0 ; i < 3 ; i++ )
    {
        printf("Set calibrator to the %s set point, then press the enter key to continue", setpoint[i]);
        getchar(); // wait for enter
        DAQmxErrChk(DAQmxCreateTask("", &taskHandle));
        DAQmxErrChk(DAQmxCreateAIVoltageChan(taskHandle, physicalChannel, "", DAQmx_Val_Cfg_Default, -1.0, 1.0, DAQmx_Val_Volts, NULL));
        DAQmxErrChk(DAQmxCfgSampClkTiming(taskHandle, "", 100000.0, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, 80000));
        DAQmxErrChk(DAQmxStartTask(taskHandle));
        DAQmxErrChk(DAQmxReadAnalogF64(taskHandle, -1, 10.0, DAQmx_Val_GroupByChannel, data, 80000, &read, NULL));
        DAQmxErrChk(DAQmxStopTask(taskHandle));
        printAverage(data); // Calculate and print averaged data
    }

    printf("\nVoltage range: 2 V\nSetpoints: 0.0, 1.996, -1.996 V\n\n");

    for ( i = 0 ; i < 3 ; i++ )
    {
        printf("Set calibrator to the %s set point, then press the enter key to continue", setpoint[i]);
        getchar(); // wait for enter
        DAQmxErrChk(DAQmxCreateTask("", &taskHandle));
        DAQmxErrChk(DAQmxCreateAIVoltageChan(taskHandle, physicalChannel, "", DAQmx_Val_Cfg_Default, -2.0, 2.0, DAQmx_Val_Volts, NULL));
        DAQmxErrChk(DAQmxCfgSampClkTiming(taskHandle, "", 100000.0, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, 80000));
        DAQmxErrChk(DAQmxStartTask(taskHandle));
        DAQmxErrChk(DAQmxReadAnalogF64(taskHandle, -1, 10.0, DAQmx_Val_GroupByChannel, data, 80000, &read, NULL));
        DAQmxErrChk(DAQmxStopTask(taskHandle));
        printAverage(data); // Calculate and print averaged data
    }

    printf("\nVoltage range: 5 V\nSetpoints: 0.0, 4.99, -4.99 V\n\n");

    for ( i = 0 ; i < 3 ; i++ )
    {
        printf("Set calibrator to the %s set point, then press the enter key to continue", setpoint[i]);
        getchar(); // wait for enter
        DAQmxErrChk(DAQmxCreateTask("", &taskHandle));
        DAQmxErrChk(DAQmxCreateAIVoltageChan(taskHandle, physicalChannel, "", DAQmx_Val_Cfg_Default, -5.0, 5.0, DAQmx_Val_Volts, NULL));
        DAQmxErrChk(DAQmxCfgSampClkTiming(taskHandle, "", 100000.0, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, 80000));
        DAQmxErrChk(DAQmxStartTask(taskHandle));
        DAQmxErrChk(DAQmxReadAnalogF64(taskHandle, -1, 10.0, DAQmx_Val_GroupByChannel, data, 80000, &read, NULL));
        DAQmxErrChk(DAQmxStopTask(taskHandle));
        printAverage(data); // Calculate and print averaged data
    }

    printf("\nVoltage range: 10 V\nSetpoints: 0.0, 9.98, -9.98 V\n\n");

    for ( i = 0 ; i < 3 ; i++ )
    {
        printf("Set calibrator to the %s set point, then press the enter key to continue", setpoint[i]);
        getchar(); // wait for enter
        DAQmxErrChk(DAQmxCreateTask("", &taskHandle));
        DAQmxErrChk(DAQmxCreateAIVoltageChan(taskHandle, physicalChannel, "", DAQmx_Val_Cfg_Default, -10.0, 10.0, DAQmx_Val_Volts, NULL));
        DAQmxErrChk(DAQmxCfgSampClkTiming(taskHandle, "", 100000.0, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, 80000));
        DAQmxErrChk(DAQmxStartTask(taskHandle));
        DAQmxErrChk(DAQmxReadAnalogF64(taskHandle, -1, 10.0, DAQmx_Val_GroupByChannel, data, 80000, &read, NULL));
        DAQmxErrChk(DAQmxStopTask(taskHandle));
        printAverage(data); // Calculate and print averaged data
    }

// Just to keep the window persistent until closed
printf("\nAll done! Press the Enter key to return to the drudgery of work!");
getchar();

Error:
       if( DAQmxFailed(error) )
              DAQmxGetExtendedErrorInfo(errBuff,2048);
       if( taskHandle!=0 )  {
              DAQmxStopTask(taskHandle);
              DAQmxClearTask(taskHandle);
       }
       if( DAQmxFailed(error) )
              printf("DAQmx Error: %s\n",errBuff);
              return 0;
}

/*
// Test output to verify all channels are reading. This function works just dandy
void printData(float64 data[]);
void printData(float64 data[])
{
    printf("%f\n",data[0]);
    printf("%f\n",data[10000]);
    printf("%f\n",data[20000]);
    printf("%f\n",data[30000]);
    printf("%f\n",data[40000]);
    printf("%f\n",data[50000]);
    printf("%f\n",data[60000]);
    printf("%f\n",data[70000]);
    printf("\n");
}
*/


