#include <stdio.h>
#include <stdlib.h>     // probably don't need this one
#include <stddef.h>     // probably don't need this either
#include <NIDAQmx.h>
#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else



float64 printAverage(float64 data[]);
float64 printAverage(float64 data[])        // This is the function that sucks.
{
    int i,j,k,l = 0;
    int k = 10000;
    float64 avg[8];
    float64 sum = 0.0;

    for ( i = 0 ; i < 8 ; i++ )
    {
        sum = 0.0;
        for ( ; j < k ; j++ )     // The first 10k values
        {
            sum += data[j];
        }
        avg[i] = ( sum / 10000 );
        k = (j+10000)
    }

        /*
        // How can I make the variable J go from 0 to 10k, then 10k to 20k, etc.. within the confines of the for loop
        for ( j = 0 ; j < 10000 ; j++ )     // The first 10k values
            {
                sum += data[j];
            }
        avg[0] = ( sum / 10000 );           // The index of avg[] should be incremented by 1, using i
        sum = 0.0;
        for ( j = 10000 ; j < 20000 ; j++ ) // The next 10k values
            {
                sum += data[j];
            }
        avg[1] = ( sum / 10000 );
        sum = 0.0;
        for ( j = 20000 ; j < 30000 ; j++ )
            {
                sum += data[j];
            }
        avg[2] = ( sum / 10000 );
        sum = 0.0;
        for ( j = 30000 ; j < 40000 ; j++ )
            {
                sum += data[j];
            }
        avg[3] = ( sum / 10000 );
        sum = 0.0;
        for ( j = 40000 ; j < 50000 ; j++ )
            {
                sum += data[j];
            }
        avg[4] = ( sum / 10000 );
        sum = 0.0;
        for ( j = 50000 ; j < 60000 ; j++ )
            {
                sum += data[j];
            }
        avg[5] = ( sum / 10000 );
        sum = 0.0;
        for ( j = 60000 ; j < 70000 ; j++ )
            {
                sum += data[j];
            }
        avg[6] = ( sum / 10000 );
        sum = 0.0;
        for ( j = 70000 ; j < 80000 ; j++ )
            {
                sum += data[j];
            }
        avg[7] = ( sum / 10000 );
        sum = 0.0;
   // }
   */
    for ( k = 0 ; k < 8 ; k++)  // print the value for each index of the avg array to stdout, separated by commas
    {
        printf("%f,",avg[k]);
    }
}

// test output, not averaged.  This function works just dandy
void printData(float64 data[]);
void printData(float64 data[])
{
    printf("%f\n",data[0]);
    printf("%f\n",data[10000]);
    printf("%f\n",data[20000]);
    printf("%f\n",data[30000]);
    printf("%f\n",data[40000]);
    printf("%f\n",data[50000]);
    printf("%f\n",data[60000]);
    printf("%f\n",data[70000]);
    printf("\n");
}

// test output to file, not averaged.  This function works just dandy as well.
void outputData(float64 data[]);
/*
void outputData(float64 data[])
{
    FILE *outputFile;
    outputFile = fopen("./4300 Output.txt", "a");
    fprintf(outputFile, "%f,",data[0]);
    fprintf(outputFile, "%f,",data[10000]);
    fprintf(outputFile, "%f,",data[20000]);
    fprintf(outputFile, "%f,",data[30000]);
    fprintf(outputFile, "%f,",data[40000]);
    fprintf(outputFile, "%f,",data[50000]);
    fprintf(outputFile, "%f,",data[60000]);
    fprintf(outputFile, "%f,",data[70000]);
    fclose(outputFile);
}
*/
void outputData(float64 data[])
{
    int i = 0;

    FILE *outputFile;
    outputFile = fopen("./4300 Output.txt", "a");

    for ( i = 0 ; i < 8 ; i++ )
    {
        fprintf(outputFile, "%f,",data[i]);
    }

    fclose(outputFile);
}




int main(void)
{
    // NI Example Variables
    int32       error=0;
    TaskHandle  taskHandle=0;
    int32       read;
    float64     data[80000];
    char        errBuff[2048]={'\0'};
    // My Variables
    float64 avg = 0.0;

    /*
    This is all NI junk, which reads analog input values from my PXI chassis into the array called data[]
    In this case 80k samples are read.  The first 10k samples are for channel 0, the next 10k samples are for channel 1, and so on.
    Then I want to pass the array to a function which then averages the first 10k indicies, then the next 10k, etc.
    These values will then get dumped to stdout for testing and later to a text file as comma separated values.
    */

// DAQmx analog voltage channel and timing parameters
    DAQmxErrChk (DAQmxCreateTask("", &taskHandle));
    DAQmxErrChk(DAQmxCreateAIVoltageChan(taskHandle, "PXI2Slot3_5/ai0:7", "", DAQmx_Val_Cfg_Default, -1.0, 1.0, DAQmx_Val_Volts, NULL));
    DAQmxErrChk(DAQmxCfgSampClkTiming(taskHandle, "", 100000.0, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, 80000));
// DAQmx Start Code
    DAQmxErrChk(DAQmxStartTask(taskHandle));
// DAQmx Read Code
    DAQmxErrChk(DAQmxReadAnalogF64(taskHandle, -1, 10.0, DAQmx_Val_GroupByChannel, data, 80000, &read, NULL));

// Let's try some standard output!
    printf("Samples Read Per Channel: %i\n\n",read);    // outputs the number of samples read per channel, should be 10000
//    printData(data);

// And now some file output!
//    outputData(data);

    printAverage(data); // print averaged values to stdout

// Stop and clear task

getchar(); // wait for enter key before closing

Error:
       if( DAQmxFailed(error) )
              DAQmxGetExtendedErrorInfo(errBuff,2048);
       if( taskHandle!=0 )  {
              DAQmxStopTask(taskHandle);
              DAQmxClearTask(taskHandle);
       }
       if( DAQmxFailed(error) )
              printf("DAQmx Error: %s\n",errBuff);
              return 0;
}



