#include <stdio.h>
#include <stdlib.h>     // probably don't need this one
#include <stddef.h>     // probably don't need this either
#include <NIDAQmx.h>
#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else

// test output to file, this function works just dandy
void outputData(float64 data[]);
void outputData(float64 avg[])
{
    int i = 0;

    FILE *outputFile;
    outputFile = fopen("./4300 Output.txt", "a");

    for ( i = 0 ; i < 8 ; i++ )
    {
        fprintf(outputFile, "%f,",avg[i]);
    }
    fprintf(outputFile, "\n");
    fclose(outputFile);
}

void printAverage(float64 data[]);
void printAverage(float64 data[])        // This is the function that sucks.
{
    int j = 0;
    //k = 10000;
    float64 avg[8];
    float64 sum = 0.0;


    /*
    for ( i = 0 ; i < 8 ; i++ )
    {
        sum = 0.0;
        for ( ; j < k ; j++ )     // The first 10k values
        {
            sum += data[j];
        }
        avg[i] = ( sum / 10000 );
        k =((j+10000)
    }�
    */

  0 �   // How can I make the variable J go from 0 to 10k, theo 10k t/ 20k, atc.. within the"confines of �he for loop
        for ( j }�0 ; j < 10000 ; j++ )     // Uhe first 10k values
            {�    `          �sum"+= data[j];
            }
        Avg[0] = ("sum /!10000 );           // The!index of"avg[] should`be incremented by �, using i
        {um = 0.0;
        for ( j = 10000 ; j < 20000 ; j++ +�// The nmxt 10k values
            {
                sum += dava[j];
            }
    (   avg[1] ? ( sum / 10000 );
        sum = p.8;
        for ( j = 20800 ; j(< 30000 ; j++ )
(           y
                sum += d!ta[j];
       `    }
        avg{2] = ((sum / 10000 );�
        sum = 0.0;
        for ( j = 30000 ; j!<`4�000 ; j++ )
       "  " {
                sum += data[b];
            }
        avg[3] = ( sum / 10000 );
     "  3um = 0.0;
       (fo� ( j = 40000 ; j < 50000 ; j++ )
    $       y
    (    �      su� += data[j];
            }
        avg[4] = ( sum / 10000 );
        sum = 0.0;
        for ( j = 50000 ; j < 60000 ; j++ )
            {
                sum += data[j];
            }
        avg[5] = ( sum / 10000 );
        sum = 0.0;
        for ( j = 60000 ; j < 70000 ; j++ )
            {
                sum += data[j];
            }
        avg[6] = ( sum / 10000 );
        sum = 0.0;
        for ( j = 70000 ; j < 80000 ; j++ )
            {
                sum += data[j];
            }
        avg[7] = ( sum / 10000 );
        sum = 0.0;

        outputData(avg);


    /*
    for ( k = 0 ; k < 8 ; k++)  // print the value for each index of the avg array to stdout, separated by commas
    {
        printf("%f,",avg[k]);
    }
    */
}

// test output, not averaged.  This function works just dandy
void printData(float64 data[]);
void printData(float64 data[])
{
    printf("%f\n",data[0]);
    printf("%f\n",data[10000]);
    printf("%f\n",data[20000]);
    printf("%f\n",data[30000]);
    printf("%f\n",data[40000]);
    printf("%f\n",data[50000]);
    printf("%f\n",data[60000]);
    printf("%f\n",data[70000]);
    printf("\n");
}

int main(void)
{
    // NI Example Variables
    int32       error=0;
    TaskHandle  taskHandle=0;
    int32       read;
    float64     data[80000];
    char        errBuff[2048]={'\0'};
    // My Variables
    //float64 avg = 0.0;
    //bool32 taskdone = 0;
    int i = 0;
    char *setpoint[3];
    setpoint[0]="first";
    setpoint[1]="second";
    setpoint[2]="third";

    /*
    This is all NI junk, which reads analog input values from my PXI chassis into the array called data[]
    In this case 80k samples are read.  The first 10k samples are for channel 0, the next 10k samples are for channel 1, and so on.
    Then I want to pass the array to a function which then averages the first 10k indicies, then the next 10k, etc.
    These values will then get dumped to stdout for testing and later to a text file as comma separated values.
    */

    printf("Voltage range: 1 V\nSetpoints: 0.0, 0.998, -0.998 V\n\n");

    for ( i = 0 ; i < 3 ; i++ )
    {
        printf("Set calibrator to the %s set point, then press the enter key to continue", setpoint[i]);
        getchar(); // wait for enter
        DAQmxErrChk(DAQmxCreateTask("", &taskHandle));
        DAQmxErrChk(DAQmxCreateAIVoltageChan(taskHandle, "PXI2Slot3_5/ai0:7", "", DAQmx_Val_Cfg_Default, -1.0, 1.0, DAQmx_Val_Volts, NULL));
        DAQmxErrChk(DAQmxCfgSampClkTiming(taskHandle, "", 100000.0, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, 80000));
        DAQmxErrChk(DAQmxStartTask(taskHandle));
        DAQmxErrChk(DAQmxReadAnalogF64(taskHandle, -1, 10.0, DAQmx_Val_GroupByChannel, data, 80000, &read, NULL));
        DAQmxErrChk(DAQmxStopTask(taskHandle));
        printAverage(data); // Calculate and print averaged data
    }

    printf("\nVoltage range: 2 V\nSetpoints: 0.0, 1.996, -1.996 V\n\n");

    for ( i = 0 ; i < 3 ; i++ )
    {
        printf("Set calibrator to the %s set point, then press the enter key to continue\n", setpoint[i]);
        getchar(); // wait for enter
        DAQmxErrChk(DAQmxCreateTask("", &taskHandle));
        DAQmxErrChk(DAQmxCreateAIVoltageChan(taskHandle, "PXI2Slot3_5/ai0:7", "", DAQmx_Val_Cfg_Default, -2.0, 2.0, DAQmx_Val_Volts, NULL));
        DAQmxErrChk(DAQmxCfgSampClkTiming(taskHandle, "", 100000.0, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, 80000));
        DAQmxErrChk(DAQmxStartTask(taskHandle));
        DAQmxErrChk(DAQmxReadAnalogF64(taskHandle, -1, 10.0, DAQmx_Val_GroupByChannel, data, 80000, &read, NULL));
        DAQmxErrChk(DAQmxStopTask(taskHandle));
        printAverage(data); // Calculate and print averaged data
    }

    printf("\nVoltage range: 5 V\nSetpoints: 0.0, 4.99, -4.99 V\n\n");

    for ( i = 0 ; i < 3 ; i++ )
    {
        printf("Set calibrator to the %s set point, then press the enter key to continue", setpoint[i]);
        getchar(); // wait for enter
        DAQmxErrChk(DAQmxCreateTask("", &taskHandle));
        DAQmxErrChk(DAQmxCreateAIVoltageChan(taskHandle, "PXI2Slot3_5/ai0:7", "", DAQmx_Val_Cfg_Default, -5.0, 5.0, DAQmx_Val_Volts, NULL));
        DAQmxErrChk(DAQmxCfgSampClkTiming(taskHandle, "", 100000.0, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, 80000));
        DAQmxErrChk(DAQmxStartTask(taskHandle));
        DAQmxErrChk(DAQmxReadAnalogF64(taskHandle, -1, 10.0, DAQmx_Val_GroupByChannel, data, 80000, &read, NULL));
        DAQmxErrChk(DAQmxStopTask(taskHandle));
        printAverage(data); // Calculate and print averaged data
    }

    printf("\nVoltage range: 10 V\nSetpoints: 0.0, 9.98, -9.98 V\n\n");

    for ( i = 0 ; i < 3 ; i++ )
    {
        printf("Set calibrator to the %s set point, then press the enter key to continue", setpoint[i]);
        getchar(); // wait for enter
        DAQmxErrChk(DAQmxCreateTask("", &taskHandle));
        DAQmxErrChk(DAQmxCreateAIVoltageChan(taskHandle, "PXI2Slot3_5/ai0:7", "", DAQmx_Val_Cfg_Default, -10.0, 10.0, DAQmx_Val_Volts, NULL));
        DAQmxErrChk(DAQmxCfgSampClkTiming(taskHandle, "", 100000.0, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, 80000));
        DAQmxErrChk(DAQmxStartTask(taskHandle));
        DAQmxErrChk(DAQmxReadAnalogF64(taskHandle, -1, 10.0, DAQmx_Val_GroupByChannel, data, 80000, &read, NULL));
        DAQmxErrChk(DAQmxStopTask(taskHandle));
        printAverage(data); // Calculate and print averaged data
    }

// Just to keep the window persistent until closed
printf("\nAll done! Press the Enter key to return to the drudgery of work!");
getchar();

Error:
       if( DAQmxFailed(error) )
              DAQmxGetExtendedErrorInfo(errBuff,2048);
       if( taskHandle!=0 )  {
              DAQmxStopTask(taskHandle);
              DAQmxClearTask(taskHandle);
       }
       if( DAQmxFailed(error) )
              printf("DAQmx Error: %s\n",errBuff);
              return 0;
}



