#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <NIDAQmx.h>

#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else

int main(void)

{

       int32       error=0;

       TaskHandle  taskHandle=0;

       int32       read;

       float64     data[80000];

       char        errBuff[2048]={'\0'};

// DAQmx analog voltage channel and timing parameters

  DAQmxErrChk (DAQmxCreateTask("", &taskHandle));

  DAQmxErrChk(DAQmxCreateAIVoltageChan(taskHandle, "PXI1Slot4/ai0", "", DAQmx_Val_Cfg_Default, -10.0, 10.0, DAQmx_Val_Volts, NULL));

DAQmxErrChk(DAQmxCfgSampClkTiming(taskHandle, "", 100000.0, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, 80000));

// DAQmx Start Code

 DAQmxErrChk(DAQmxStartTask(taskHandle));

// DAQmx Read Code
//    int32 DAQmxReadAnalogF64 (TaskHandle taskHandle, int32 numSampsPerChan, float64 timeout, bool32 fillMode, float64 readArray[], uInt32 arraySizeInSamps, int32 *sampsPerChanRead, bool32 *reserved);
 DAQmxErrChk(DAQmxReadAnalogF64(taskHandle, -1, 10.0, DAQmx_Val_GroupByChannel, data, 80000, &read, NULL));

// Let's try some output!


printf("Samples Read: %i\n\n",read);

/*
printf("%f\n",data[0]);
printf("%f\n",data[1000]);
printf("%f\n",data[2000]);
printf("%f\n",data[3000]);
printf("%f\n",data[4000]);
printf("%f\n",data[5000]);
printf("%f\n",data[6000]);
printf("%f\n",data[7000]);
*/

int i=0;
int j=0;
float channelsum=0;
float average[8];

// This prints a single reading for each of the channels, 0-7

/*
for ( i = 0; i < 80000; i = i+10000)
{
    printf("%f\n",data[i]);
}
*/

for (i = 0; i <8; i++)
{
    for( ; j < i * 1000 ; j++ )
    {
        //printf("%f\n",data[j]);
        printf("%i\n",j);
        channelsum = channelsum + data[j];
    }
    average[i] = channelsum/1000;


 /*
    for( j = 1000; j < 2000; j++ )
    {
        //printf("%f\n",data[j]);
        channelsum = channelsum + data[j];
    }
    average[1] = channelsum/1000;
*/
    //printf ("Channel %i sum: %i\n",i,channelsum);
    printf("Channel %i average: %f\n",i,average[i]);

}
/*


    average[1] = channelsum/1000;

    for( j = 2000; j < 3000; j++ )
    {
        //printf("%f\n",data[j]);
        channelsum = channelsum + data[j];
    }

    average[2] = channelsum/1000;
*/


//average[i] = channelsum/1000;
//printf ("%i %i\n",i,channelsum);
//printf ("%i\n",j);
//printf ("%f\n",channelsum[i]);
//printf("The average reading for channel 1 is: %f\n",average);

/*
for (i=0;i<8;i++)
{
    printf("Channel %i: %f\n",i,average[1]);
}
*/



// Stop and clear task

Error:

       if( DAQmxFailed(error) )

              DAQmxGetExtendedErrorInfo(errBuff,2048);

       if( taskHandle!=0 )  {

              DAQmxStopTask(taskHandle);

              DAQmxClearTask(taskHandle);

       }

       if( DAQmxFailed(error) )

              printf("DAQmx Error: %s\n",errBuff);

              return 0;


}



