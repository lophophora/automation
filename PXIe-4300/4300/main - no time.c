/*
2016 November 8 - Alex Robinson
NI PXIe-4300 Data Acquisition Program
This program reads samples from the user specified device and appends the values to a CSV file
*/

#include <stdio.h>
#include <string.h>
#include <NIDAQmx.h>
#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else

// Output to file as comma separated values.
void outputData(float64 data[]);
void outputData(float64 avg[])
{
    int i = 0;

    FILE *outputFile;
    outputFile = fopen("./4300 Output.csv", "a");

    for ( i = 0 ; i < 8 ; i++ )
    {
        fprintf(outputFile, "%f,",avg[i]);
    }
    fprintf(outputFile, "\n");
    fclose(outputFile);
}

void printAverage(float64 data[]);
void printAverage(float64 data[])        // This was the function that sucks.
{
    float64 avg[8];
    float64 sum = 0.0;
    int i = 0;
    int j = 0;
    int k = 0;

    for ( i = 0 ; i < 8 ; i++ )
    {
        sum = 0.0;
        k = (j+10000);
        for ( ; j < k ; j++ )
        {
            sum += data[j];
        }
        avg[i] = ( sum / 10000 );
    }
    outputData(avg);
}

int main(void)
{
    // NI Variables
    int32       error=0;                // For error handling.  Refer to NIDAQMX.h for detailed error codes
    TaskHandle  taskHandle=0;           // This could be named whatever
    int32       read;                   // This value records the number of samples read per channel
    float64     data[80000];            // This will hold our samples
    char        errBuff[2048]={'\0'};
    // My variables
    int     i = 0;
    int     j = 0;
    char    cardAddress[50];
    //char    *setpointtext[3] = {"first", "second", "third"};
    float   signalRange[4] = {1.0, 2.0, 5.0, 10.0};
    float   setpoint[4][3] = {{0.0, 0.998, -0.998},{0.0, 1.996, -1.996},{0.0, 4.99, -4.99},{0.0, 9.98, -9.98}};

    printf("NI PXIe-4300 Isolated Analog Input Module Data Acquisition - Alex Robinson\n");
    printf("==========================================================================\n");
    printf("This program will output a file named \"4300 Output.csv\" in the current\ndirectory containing the test points as comma separated values.\n\n");
    printf("This program also comes with no warranty, enjoy!\n\n");

    printf("Please enter the Physical Address of the NI-4300 card as shown in NI MAX.\n\nExample: PXI1Slot2_1\n\nChannel Name: ");
    scanf("%s", cardAddress);
    getchar();      // I could probably use fgets() or something similar,
    strcat(cardAddress, "/ai0:7");

    const char * physicalChannel = cardAddress; // I'm not sure if this is necessary, but the NI documentation specified const char[] for physical channel name

    for ( i = 0 ; i < 4 ; i++ )
    {
        printf("\nVoltage range: %.2f V\n\n", signalRange[i]);
        for ( j = 0 ; j < 3 ; j++ )
        {
            printf("Set calibrator to %.3f volts, then press the enter key to continue", setpoint[i][j]);
            getchar(); // wait for enter
            DAQmxErrChk(DAQmxCreateTask("", &taskHandle));
            DAQmxErrChk(DAQmxCreateAIVoltageChan(taskHandle, physicalChannel, "", DAQmx_Val_Cfg_Default, -signalRange[j], signalRange[i], DAQmx_Val_Volts, NULL));
            DAQmxErrChk(DAQmxCfgSampClkTiming(taskHandle, "", 100000.0, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, 80000));
            DAQmxErrChk(DAQmxStartTask(taskHandle));
            DAQmxErrChk(DAQmxReadAnalogF64(taskHandle, -1, 10.0, DAQmx_Val_GroupByChannel, data, 80000, &read, NULL));
            DAQmxErrChk(DAQmxStopTask(taskHandle));
            printAverage(data); // Calculate and print averaged data
        }
     }

// Just to keep the window persistent until closed
printf("\nAll done! Press the Enter key to return to the drudgery of work!");
getchar();

Error:
       if( DAQmxFailed(error) )
              DAQmxGetExtendedErrorInfo(errBuff,2048);
       if( taskHandle!=0 )  {
              DAQmxStopTask(taskHandle);
              DAQmxClearTask(taskHandle);
       }
       if( DAQmxFailed(error) )
              printf("DAQmx Error: %s\n",errBuff);
              return 0;
}
