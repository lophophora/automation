Python 3.4.4 (v3.4.4:737efcadf5a6, Dec 20 2015, 19:28:18) [MSC v.1600 32 bit (Intel)] on win32
Type "copyright", "credits" or "license()" for more information.
>>> 
============ RESTART: \\172.27.0.10\nishare\NI-PXIe-4300-multi.py ============
Traceback (most recent call last):
  File "\\172.27.0.10\nishare\NI-PXIe-4300-multi.py", line 23, in <module>
    from selectaroo import menu
ImportError: No module named 'selectaroo'
>>> 
===== RESTART: \\172.27.0.10\nishare\NI-PXIe-4300\NI-PXIe-4300-multi.py =====

Please select a calibrator from the list of available devices.

Voltage Range: 1.0 V	Setpoint: 0.0 V	Channel PXISlot3/ai0:7, PXISlot4/ai0:7, PXISlot5/ai0:7 /ai0:7

Channel 0 :  -0.000111199995115
Channel 1 :  -9.56778272193e-05
Channel 2 :  -0.00012343843349
Channel 3 :  -1.44743332484e-05
Channel 4 :  -8.16814576372e-05
Channel 5 :  -7.39102442199e-05
Channel 6 :  -0.000108201223268
Channel 7 :  -4.87017642735e-05
Channel 8 :  -0.00011685228173
Channel 9 :  -0.000105044921122
Channel 10 :  -1.88520005907e-05
Channel 11 :  -2.17612390485e-05
Channel 12 :  -7.84751073514e-05
Channel 13 :  -5.28381532171e-05
Channel 14 :  -3.78537298033e-05
Channel 15 :  -8.74678459395e-05
Channel 16 :  -8.29458295709e-05
Channel 17 :  -6.72565282524e-05
Channel 18 :  -0.000109592111717
Channel 19 :  -7.37525546716e-05
Channel 20 :  -4.46008433585e-05
Channel 21 :  -8.64204360772e-06
Channel 22 :  -0.000105479501123
Channel 23 :  -0.000106084624133

Voltage Range: 1.0 V	Setpoint: 0.998 V	Channel PXISlot3/ai0:7, PXISlot4/ai0:7, PXISlot5/ai0:7 /ai0:7

Channel 0 :  0.997795915747
Channel 1 :  0.997866539655
Channel 2 :  0.997858172948
Channel 3 :  0.997973386266
Channel 4 :  0.997883479156
Channel 5 :  0.997897708535
Channel 6 :  0.997905255489
Channel 7 :  0.997959316217
Channel 8 :  0.997887293029
Channel 9 :  0.997903586185
Channel 10 :  0.997969333272
Channel 11 :  0.997981287234
Channel 12 :  0.997877443024
Channel 13 :  0.997934406216
Channel 14 :  0.997974489832
Channel 15 :  0.997916649347
Channel 16 :  0.997852004475
Channel 17 :  0.997897751607
Channel 18 :  0.997859613319
Channel 19 :  0.99789417186
Channel 20 :  0.99793409273
Channel 21 :  0.997972892524
Channel 22 :  0.997845757085
Channel 23 :  0.997861683632

Voltage Range: 1.0 V	Setpoint: -0.998 V	Channel PXISlot3/ai0:7, PXISlot4/ai0:7, PXISlot5/ai0:7 /ai0:7

Channel 0 :  -0.998041196366
Channel 1 :  -0.998080977575
Channel 2 :  -0.998119643889
Channel 3 :  -0.998028826449
Channel 4 :  -0.998093320844
Channel 5 :  -0.998067863468
Channel 6 :  -0.998133110416
Channel 7 :  -0.998085606164
Channel 8 :  -0.998144350339
Channel 9 :  -0.998136338229
Channel 10 :  -0.99805054202
Channel 11 :  -0.998044133511
Channel 12 :  -0.998072952666
Channel 13 :  -0.998081849957
Channel 14 :  -0.998066835786
Channel 15 :  -0.99811515415
Channel 16 :  -0.998037203769
Channel 17 :  -0.998064181788
Channel 18 :  -0.998083567951
Channel 19 :  -0.99809113949
Channel 20 :  -0.998045880999
Channel 21 :  -0.998002383771
Channel 22 :  -0.998092956755
Channel 23 :  -0.998096833478

Voltage Range: 2.0 V	Setpoint: 0.0 V	Channel PXISlot3/ai0:7, PXISlot4/ai0:7, PXISlot5/ai0:7 /ai0:7

Channel 0 :  -0.000115254939876
Channel 1 :  -9.37735113231e-05
Channel 2 :  -0.000119139678743
Channel 3 :  -7.01318598066e-06
Channel 4 :  -6.42108888052e-05
Channel 5 :  -4.89986107249e-05
Channel 6 :  -0.000120766960813
Channel 7 :  -5.65384514797e-05
Channel 8 :  -0.000133978159305
Channel 9 :  -0.000100778425261
Channel 10 :  -4.05823435625e-06
Channel 11 :  -1.12353106849e-05
Channel 12 :  -8.67015973568e-05
Channel 13 :  -6.47760284457e-05
Channel 14 :  -4.81414268635e-05
Channel 15 :  -9.43613373337e-05
Channel 16 :  -8.69939404852e-05
Channel 17 :  -7.15473776843e-05
Channel 18 :  -9.44979318888e-05
Channel 19 :  -5.89893424582e-05
Channel 20 :  -5.0932734706e-05
Channel 21 :  1.49474365032e-05
Channel 22 :  -9.30711879385e-05
Channel 23 :  -0.000107507631247

Voltage Range: 2.0 V	Setpoint: 1.996 V	Channel PXISlot3/ai0:7, PXISlot4/ai0:7, PXISlot5/ai0:7 /ai0:7

Channel 0 :  1.99566474598
Channel 1 :  1.99581596525
Channel 2 :  1.99581471928
Channel 3 :  1.9959328796
Channel 4 :  1.99582555686
Channel 5 :  1.99586285527
Channel 6 :  1.99586255135
Channel 7 :  1.99589517124
Channel 8 :  1.99580369324
Channel 9 :  1.99584148566
Channel 10 :  1.99591021442
Channel 11 :  1.99594933621
Channel 12 :  1.99575699776
Channel 13 :  1.99586240281
Channel 14 :  1.99589909136
Channel 15 :  1.99583879994
Channel 16 :  1.99575111324
Channel 17 :  1.99581205774
Channel 18 :  1.99579829735
Channel 19 :  1.99584446839
Channel 20 :  1.99587322701
Channel 21 :  1.9959446308
Channel 22 :  1.995762332
Channel 23 :  1.99581981632

Voltage Range: 2.0 V	Setpoint: -1.996 V	Channel PXISlot3/ai0:7, PXISlot4/ai0:7, PXISlot5/ai0:7 /ai0:7

Channel 0 :  -1.99597264651
Channel 1 :  -1.99607310026
Channel 2 :  -1.99612452916
Channel 3 :  -1.99603429618
Channel 4 :  -1.99608090873
Channel 5 :  -1.99604543173
Channel 6 :  -1.99615414197
Channel 7 :  -1.99611738297
Channel 8 :  -1.99616954313
Channel 9 :  -1.99613655203
Channel 10 :  -1.99603649758
Channel 11 :  -1.99608404393
Channel 12 :  -1.99605503085
Channel 13 :  -1.9961030784
Channel 14 :  -1.99607686441
Channel 15 :  -1.99611620737
Channel 16 :  -1.99597645584
Channel 17 :  -1.99605773362
Channel 18 :  -1.9960408012
Channel 19 :  -1.99610608248
Channel 20 :  -1.9960577505
Channel 21 :  -1.99597753748
Channel 22 :  -1.99606468687
Channel 23 :  -1.996095811

Voltage Range: 5.0 V	Setpoint: 0.0 V	Channel PXISlot3/ai0:7, PXISlot4/ai0:7, PXISlot5/ai0:7 /ai0:7

Channel 0 :  -0.000177438227987
Channel 1 :  -0.000131209714147
Channel 2 :  -0.000162935236427
Channel 3 :  -2.41054101821e-05
Channel 4 :  -5.37849645505e-05
Channel 5 :  -1.79921118396e-05
Channel 6 :  -0.000225967666249
Channel 7 :  -0.00010003752658
Channel 8 :  -0.0002254518129
Channel 9 :  -0.000143248380275
Channel 10 :  6.85764721157e-06
Channel 11 :  -2.59680768102e-05
Channel 12 :  -0.000167299069984
Channel 13 :  -0.000134895582803
Channel 14 :  -0.000120757116802
Channel 15 :  -0.000160297425599
Channel 16 :  -0.000117830148505
Channel 17 :  -0.000123045984053
Channel 18 :  -0.000106020593367
Channel 19 :  -5.70347369889e-05
Channel 20 :  -0.000114919848109
Channel 21 :  1.14455502878e-05
Channel 22 :  -0.000100271249999
Channel 23 :  -0.000167598181415

Voltage Range: 5.0 V	Setpoint: 4.990 V	Channel PXISlot3/ai0:7, PXISlot4/ai0:7, PXISlot5/ai0:7 /ai0:7

Channel 0 :  4.98922431374
Channel 1 :  4.98956785771
Channel 2 :  4.98966071758
Channel 3 :  4.98982059667
Channel 4 :  4.98965226977
Channel 5 :  4.98976185346
Channel 6 :  4.9897155277
Channel 7 :  4.98973985291
Channel 8 :  4.98958234108
Channel 9 :  4.98970192109
Channel 10 :  4.98977133195
Channel 11 :  4.98987355399
Channel 12 :  4.98946567576
Channel 13 :  4.98966238962
Channel 14 :  4.98975112046
Channel 15 :  4.98966924936
Channel 16 :  4.98947772356
Channel 17 :  4.9896094771
Channel 18 :  4.98964761702
Channel 19 :  4.989666997
Channel 20 :  4.98970407887
Channel 21 :  4.98986218909
Channel 22 :  4.98950462808
Channel 23 :  4.98966692553

Voltage Range: 5.0 V	Setpoint: -4.990 V	Channel PXISlot3/ai0:7, PXISlot4/ai0:7, PXISlot5/ai0:7 /ai0:7

Channel 0 :  -4.98972996765
Channel 1 :  -4.99002324793
Channel 2 :  -4.99011174785
Channel 3 :  -4.99005546774
Channel 4 :  -4.99003811593
Channel 5 :  -4.98998391103
Channel 6 :  -4.99021096934
Channel 7 :  -4.99015433508
Channel 8 :  -4.99024262874
Channel 9 :  -4.99015280596
Channel 10 :  -4.99001687031
Channel 11 :  -4.99013677745
Channel 12 :  -4.99005973562
Channel 13 :  -4.99018478074
Channel 14 :  -4.99015897696
Channel 15 :  -4.99016112377
Channel 16 :  -4.98982276224
Channel 17 :  -4.99007645454
Channel 18 :  -4.98997006457
Channel 19 :  -4.99012300331
Channel 20 :  -4.99012681845
Channel 21 :  -4.98993773029
Channel 22 :  -4.99001488107
Channel 23 :  -4.99009588273

Voltage Range: 10.0 V	Setpoint: 0.0 V	Channel PXISlot3/ai0:7, PXISlot4/ai0:7, PXISlot5/ai0:7 /ai0:7

Channel 0 :  -0.000304776226503
Channel 1 :  -0.00023622108168
Channel 2 :  -0.000265323992933
Channel 3 :  -8.30509426454e-05
Channel 4 :  -2.52604898078e-05
Channel 5 :  3.01306705653e-05
Channel 6 :  -0.000396641725505
Channel 7 :  -0.000234158090469
Channel 8 :  -0.000423196058166
Channel 9 :  -0.00020601295921
Channel 10 :  -2.46454425861e-05
Channel 11 :  -5.68697495555e-05
Channel 12 :  -0.000325497656711
Channel 13 :  -0.000274691173919
Channel 14 :  -0.000286779037455
Channel 15 :  -0.000279811633909
Channel 16 :  -0.000179679321314
Channel 17 :  -0.000233733305396
Channel 18 :  -0.000148854599628
Channel 19 :  -3.62194598549e-05
Channel 20 :  -0.00025279784087
Channel 21 :  3.16378938778e-05
Channel 22 :  -0.000134436856794
Channel 23 :  -0.000270395306778

Voltage Range: 10.0 V	Setpoint: 9.980 V	Channel PXISlot3/ai0:7, PXISlot4/ai0:7, PXISlot5/ai0:7 /ai0:7

Channel 0 :  9.92001949799
Channel 1 :  9.9206765634
Channel 2 :  9.92099701511
Channel 3 :  9.92115640277
Channel 4 :  9.92090151645
Channel 5 :  9.92112108016
Channel 6 :  9.92095622058
Channel 7 :  9.92098172136
Channel 8 :  9.97925698806
Channel 9 :  9.97950610957
Channel 10 :  9.97958433689
Channel 11 :  9.9798223312
Channel 12 :  9.97905159425
Channel 13 :  9.97942177475
Channel 14 :  9.97955655865
Channel 15 :  9.97941578111
Channel 16 :  9.97911442316
Channel 17 :  9.97930222161
Channel 18 :  9.97945378626
Channel 19 :  9.97945469152
Channel 20 :  9.97949630724
Channel 21 :  9.97976674644
Channel 22 :  9.97911195982
Channel 23 :  9.97946378888

Voltage Range: 10.0 V	Setpoint: -9.980 V	Channel PXISlot3/ai0:7, PXISlot4/ai0:7, PXISlot5/ai0:7 /ai0:7

Channel 0 :  -9.97938030161
Channel 1 :  -9.98001822828
Channel 2 :  -9.98023981794
Channel 3 :  -9.98020160189
Channel 4 :  -9.98007558746
Channel 5 :  -9.97996734486
Channel 6 :  -9.98058427556
Channel 7 :  -9.9805385931
Channel 8 :  -9.98050142658
Channel 9 :  -9.98023467215
Channel 10 :  -9.98005280911
Channel 11 :  -9.9803596207
Channel 12 :  -9.98018852282
Channel 13 :  -9.9804097283
Channel 14 :  -9.98044259047
Channel 15 :  -9.9802972467
Channel 16 :  -9.97992764103
Channel 17 :  -9.98023006416
Channel 18 :  -9.97991553434
Channel 19 :  -9.98029804679
Channel 20 :  -9.98032208015
Channel 21 :  -9.97993747438
Channel 22 :  -9.97995604384
Channel 23 :  -9.98019054375

Execution Time:  54.015625 seconds. ( 0.9002604166666667 minutes)

>>> 
