Python 3.4.4 (v3.4.4:737efcadf5a6, Dec 20 2015, 19:28:18) [MSC v.1600 32 bit (Intel)] on win32
Type "copyright", "credits" or "license()" for more information.
>>> import * from PyDAQmx
SyntaxError: invalid syntax
>>> from PyDAQmx import *
>>> import nidmm
>>> import visa
>>> rm = visa.ResourceManager()
>>> dmm = nidmm.Session("ED-0053")
>>> dmm.function = nidmm.Function.DC_VOLTS
>>> dmm.range = 10
>>> dmm.configure_measurement_digits = 6.5
>>> print(dmm.read())
-0.013773069716989994
>>> from PyDAQmx.DAQmxTypes import *
>>> tH = TaskHandle(0)
>>> DAQmxCreateTask("",byref(tH))
0
>>> DAQmxCreateAIBridgeChan(tH, 'PXI1Slot3_2/ai1', '',-0.025,0.025,Volts/Volts,Volts/Volts,1.0,350, "")
Traceback (most recent call last):
  File "<pyshell#13>", line 1, in <module>
    DAQmxCreateAIBridgeChan(tH, 'PXI1Slot3_2/ai1', '',-0.025,0.025,Volts/Volts,Volts/Volts,1.0,350, "")
NameError: name 'Volts' is not defined
>>> DAQmxCreateAIBridgeChan(tH, 'PXI1Slot3_2/ai1', '',-0.025,0.025,DAQmx_Val_VoltsPerVolt,DAQmx_Val_FullBridge,DAQmx_Val_Internal,1,350.0, "")
0
>>> DAQmxStartTask(tH)
0
>>> dmm.read()
-0.016625426709651947
>>> dmm.read()
1.020141363143921
>>> dmm.read()
1.0202306509017944
>>> DAQmxStopTask()
Traceback (most recent call last):
  File "<pyshell#19>", line 1, in <module>
    DAQmxStopTask()
TypeError: function() missing 1 required positional argument: 'taskHandle'
>>> DAQmxStopTask(tH)
0
>>> DAQmxCreateAIBridgeChan(tH, 'PXI1Slot3_2/ai1', '',-0.025,0.025,DAQmx_Val_VoltsPerVolt,DAQmx_Val_FullBridge,DAQmx_Val_Internal,2,350.0, "")
Traceback (most recent call last):
  File "<pyshell#21>", line 1, in <module>
    DAQmxCreateAIBridgeChan(tH, 'PXI1Slot3_2/ai1', '',-0.025,0.025,DAQmx_Val_VoltsPerVolt,DAQmx_Val_FullBridge,DAQmx_Val_Internal,2,350.0, "")
  File "<string>", line 2, in function
  File "C:\Python34\lib\site-packages\PyDAQmx\DAQmxFunctions.py", line 62, in mafunction
    raise exception_class(errBuff.value.decode("utf-8"), f.__name__)
PyDAQmx.DAQmxFunctions.ChanAlreadyInTaskError: Specified channel cannot be added to the task, because a channel with the same name is already in the task.
Channel Name: PXI1Slot3_2/ai1

Task Name: _unnamedTask<0>

Status Code: -200489
 in function DAQmxCreateAIBridgeChan
>>> DAQmxSetAIExcitVal(tH, 'PXI1Slot3_2/ai1', 2.0)
0
>>> DAQmxStartTask(tH)
0
>>> dmm.read()
2.0175089836120605
>>> DAQmxStopTask(tH)
0
>>> DAQmxClearTask()
Traceback (most recent call last):
  File "<pyshell#26>", line 1, in <module>
    DAQmxClearTask()
TypeError: function() missing 1 required positional argument: 'taskHandle'
>>> DAQmxClearTask(tH)
0
>>> DAQmxCreateTask("",byref(tH))
0
>>> DAQmxCreateAIBridgeChan(tH, 'PXI1Slot4/ai0', '',-0.025,0.025,DAQmx_Val_VoltsPerVolt,DAQmx_Val_FullBridge,DAQmx_Val_Internal,0.625,350.0, "")
0
>>> DAQmxSetAIBridgeShuntCalEnable(tH, 'PXI1Slot4/ai0', True)
0
>>> DAQmxSetAIBridgeShuntCalSelect(tH, 'PXI1Slot4/ai0', DAQmx_Val_A)
0
>>> DAQmxSetAIBridgeShuntCalShuntCalAResistance(tH, 'PXI1Slot4/ai0', 50000)
0
>>> DAQmxStartTask(tH)
0
>>> DAQmxStopTask(tH)
0
>>> DAQmxStartTask(tH)
0
>>> dmm.function = nidmm.Function._4_WIRE_RES
>>> print(dmm.read())
50066.08984375
>>> print(dmm.read())
50110.1796875
>>> dmm.input_resistance = 10000000
>>> dmm.range = 100000
>>> dmm.configure_measurement_digits = 7.5
>>> dmm.auto_zero = True
Traceback (most recent call last):
  File "<pyshell#42>", line 1, in <module>
    dmm.auto_zero = True
  File "C:\Python34\lib\site-packages\nidmm\session.py", line 422, in __setattr__
    object.__setattr__(self, key, value)
  File "C:\Python34\lib\site-packages\nidmm\attributes.py", line 69, in __set__
    raise TypeError('must be ' + str(self._attribute_type.__name__) + ' not ' + str(type(value).__name__))
TypeError: must be AutoZero not bool
>>> dmm.auto_zero = AutoZero
Traceback (most recent call last):
  File "<pyshell#43>", line 1, in <module>
    dmm.auto_zero = AutoZero
NameError: name 'AutoZero' is not defined
>>> dmm.auto_zero = nidmm.AutoZero
Traceback (most recent call last):
  File "<pyshell#44>", line 1, in <module>
    dmm.auto_zero = nidmm.AutoZero
  File "C:\Python34\lib\site-packages\nidmm\session.py", line 422, in __setattr__
    object.__setattr__(self, key, value)
  File "C:\Python34\lib\site-packages\nidmm\attributes.py", line 69, in __set__
    raise TypeError('must be ' + str(self._attribute_type.__name__) + ' not ' + str(type(value).__name__))
TypeError: must be AutoZero not EnumMeta
>>> dmm.auto_zero = nidmm.AutoZero.ON
>>> dmm.read()
49994.71875112018
>>> DAQmxStopTask(tH)
0
>>> DAQmxSetAIBridgeShuntCalShuntCalAResistance(tH, 'PXI1Slot4/ai0', 100000)
0
>>> DAQmxStartTask(tH)
0
>>> dmm.read()
99957.50369199169
>>> DAQmxStopTask(tH)
0
>>> DAQmxClearTask(tH)
0
>>> DAQmxCreateTask("",byref(tH))
0
>>> DAQmxCreateAIBridgeChan(tH, 'PXI1Slot4/ai0', '',-0.025,0.025,DAQmx_Val_VoltsPerVolt,DAQmx_Val_FullBridge,DAQmx_Val_Internal,1.0,350.0, "")
0
>>> DAQmxStartTask(tH)
0
>>> DAQmxStopTask(tH)
0
>>> DAQmxClearTask(tH)
0
>>> tH = TaskHandle(0)
>>> DAQmxCreateTask("",byref(tH))
0
>>> DAQmxCreateAIBridgeChan(tH, 'PXI1Slot4/ai0', '',-0.025,0.025,DAQmx_Val_VoltsPerVolt,DAQmx_Val_FullBridge,DAQmx_Val_Internal,1.0,350.0, "")
0
>>> DAQmxSetAIExcitVoltageOrCurrent(th, 'PXI1Slot4/ai0', 1.0)
Traceback (most recent call last):
  File "<pyshell#61>", line 1, in <module>
    DAQmxSetAIExcitVoltageOrCurrent(th, 'PXI1Slot4/ai0', 1.0)
NameError: name 'th' is not defined
>>> DAQmxSetAIExcitVoltageOrCurrent(tH, 'PXI1Slot4/ai0', 1.0)
Traceback (most recent call last):
  File "<pyshell#62>", line 1, in <module>
    DAQmxSetAIExcitVoltageOrCurrent(tH, 'PXI1Slot4/ai0', 1.0)
  File "<string>", line 2, in function
  File "C:\Python34\lib\site-packages\PyDAQmx\DAQmxFunctions.py", line 57, in mafunction
    error = f(*arg)
ctypes.ArgumentError: argument 3: <class 'TypeError'>: wrong type
>>> DAQmxSetAIExcitVal(tH, 'PXI1Slot4/ai0', 1.0)
0
>>> DAQmxSetAIExcitVal(tH, 'PXI1Slot4/ai0', 2.0)
0
>>> DAQmxStartTask(tH)
0
>>> DAQmxSetAIExcitVal(tH, 'PXI1Slot4/ai0', 1.0)
Traceback (most recent call last):
  File "<pyshell#66>", line 1, in <module>
    DAQmxSetAIExcitVal(tH, 'PXI1Slot4/ai0', 1.0)
  File "<string>", line 2, in function
  File "C:\Python34\lib\site-packages\PyDAQmx\DAQmxFunctions.py", line 62, in mafunction
    raise exception_class(errBuff.value.decode("utf-8"), f.__name__)
PyDAQmx.DAQmxFunctions.CannotSetPropertyWhenTaskRunningError: Specified property cannot be set while the task is running.

Set the property prior to starting the task, or stop the task prior to setting the property.
Property: DAQmx_AI_Excit_Val
Channel Name: PXI1Slot4/ai0

Task Name: _unnamedTask<3>

Status Code: -200557
 in function DAQmxSetAIExcitVal
>>> DAQmxStopTask(tH)
0
>>> DAQmxCreateAIBridgeChan(tH, 'PXI1Slot4/ai0-7', '',-0.025,0.025,DAQmx_Val_VoltsPerVolt,DAQmx_Val_FullBridge,DAQmx_Val_Internal,1.0,350.0, "")
Traceback (most recent call last):
  File "<pyshell#68>", line 1, in <module>
    DAQmxCreateAIBridgeChan(tH, 'PXI1Slot4/ai0-7', '',-0.025,0.025,DAQmx_Val_VoltsPerVolt,DAQmx_Val_FullBridge,DAQmx_Val_Internal,1.0,350.0, "")
  File "<string>", line 2, in function
  File "C:\Python34\lib\site-packages\PyDAQmx\DAQmxFunctions.py", line 62, in mafunction
    raise exception_class(errBuff.value.decode("utf-8"), f.__name__)
PyDAQmx.DAQmxFunctions.PhysicalChanDoesNotExistError: Physical channel specified does not exist on this device.

Refer to the documentation for channels available on this device.
Device: PXI1Slot4
Physical Channel Name: ai0-7

Task Name: _unnamedTask<3>

Status Code: -200170
 in function DAQmxCreateAIBridgeChan
>>> DAQmxCreateAIBridgeChan(tH, 'PXI1Slot4/ai0:7', '',-0.025,0.025,DAQmx_Val_VoltsPerVolt,DAQmx_Val_FullBridge,DAQmx_Val_Internal,1.0,350.0, "")
Traceback (most recent call last):
  File "<pyshell#69>", line 1, in <module>
    DAQmxCreateAIBridgeChan(tH, 'PXI1Slot4/ai0:7', '',-0.025,0.025,DAQmx_Val_VoltsPerVolt,DAQmx_Val_FullBridge,DAQmx_Val_Internal,1.0,350.0, "")
  File "<string>", line 2, in function
  File "C:\Python34\lib\site-packages\PyDAQmx\DAQmxFunctions.py", line 62, in mafunction
    raise exception_class(errBuff.value.decode("utf-8"), f.__name__)
PyDAQmx.DAQmxFunctions.ChanAlreadyInTaskError: Specified channel cannot be added to the task, because a channel with the same name is already in the task.
Channel Name: PXI1Slot4/ai0

Task Name: _unnamedTask<3>

Status Code: -200489
 in function DAQmxCreateAIBridgeChan
>>> import numpy
>>> readings = numpy.zeros((8,4), dtype=numpy.float64)
>>> readings[4,2] = 55.5
>>> readings
array([[  0. ,   0. ,   0. ,   0. ],
       [  0. ,   0. ,   0. ,   0. ],
       [  0. ,   0. ,   0. ,   0. ],
       [  0. ,   0. ,   0. ,   0. ],
       [  0. ,   0. ,  55.5,   0. ],
       [  0. ,   0. ,   0. ,   0. ],
       [  0. ,   0. ,   0. ,   0. ],
       [  0. ,   0. ,   0. ,   0. ]])
>>> DAQmxStopTask(tH)
0
>>> test = ""
>>> yes = ""
>>> test == yes
True
>>> yes = "3"
>>> test == yes
False
>>> DAQmxClearTask(tH)
0
>>> 
====== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300.py ======

Channel Time:  4.28125 seconds. ( 0.07135416666666666 minutes)

Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300.py", line 68, in <module>
    input("Preparing channel", chan,", press Enter when ready")
TypeError: input expected at most 1 arguments, got 3
>>> 
====== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300.py ======
Preparing channel 0 , press Enter when ready


Channel Time:  8.140625 seconds. ( 0.13567708333333334 minutes)

Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300.py", line 77, in <module>
    350.0, "")
  File "<string>", line 2, in function
  File "C:\Python34\lib\site-packages\PyDAQmx\DAQmxFunctions.py", line 62, in mafunction
    raise exception_class(errBuff.value.decode("utf-8"), f.__name__)
PyDAQmx.DAQmxFunctions.InvalidAttributeValueError: Requested value is not a supported value for this property. The property value may be invalid because it conflicts with another property.
Property: DAQmx_AI_Excit_Val
Requested Value:  0.0
Maximum Value: Inf
Minimum Value:  100.000000e-9
Channel Name: PXI1Slot4/ai0

Task Name: _unnamedTask<0>

Status Code: -200077
 in function DAQmxCreateAIBridgeChan
>>> 
====== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300.py ======
Preparing channel 0 , press Enter when ready

Setpoint: 1.000000 Reading: 0.989411   Measurement Time:                       0.989411uS

Channel Time:  9.921875 seconds. ( 0.16536458333333334 minutes)

Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300.py", line 77, in <module>
    350.0, "")
  File "<string>", line 2, in function
  File "C:\Python34\lib\site-packages\PyDAQmx\DAQmxFunctions.py", line 62, in mafunction
    raise exception_class(errBuff.value.decode("utf-8"), f.__name__)
PyDAQmx.DAQmxFunctions.ChanAlreadyInTaskError: Specified channel cannot be added to the task, because a channel with the same name is already in the task.
Channel Name: PXI1Slot4/ai0

Task Name: _unnamedTask<0>

Status Code: -200489
 in function DAQmxCreateAIBridgeChan
>>> DAQmxStopTask(tH)
0
>>> DAQmxClearTask(tH)
0
>>> DAQmxClearTask(tH)
0
>>> DAQmxStopTask(tH)
Traceback (most recent call last):
  File "<pyshell#84>", line 1, in <module>
    DAQmxStopTask(tH)
  File "<string>", line 2, in function
  File "C:\Python34\lib\site-packages\PyDAQmx\DAQmxFunctions.py", line 62, in mafunction
    raise exception_class(errBuff.value.decode("utf-8"), f.__name__)
PyDAQmx.DAQmxFunctions.InvalidTaskError: Task specified is invalid or does not exist.
Status Code: -200088
 in function DAQmxStopTask
>>> DAQmxStopTask(tH)
Traceback (most recent call last):
  File "<pyshell#85>", line 1, in <module>
    DAQmxStopTask(tH)
  File "<string>", line 2, in function
  File "C:\Python34\lib\site-packages\PyDAQmx\DAQmxFunctions.py", line 62, in mafunction
    raise exception_class(errBuff.value.decode("utf-8"), f.__name__)
PyDAQmx.DAQmxFunctions.InvalidTaskError: Task specified is invalid or does not exist.
Status Code: -200088
 in function DAQmxStopTask
>>> DAQmxClearTask(tH)
0
>>> 
====== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300.py ======
Preparing channel 0 , press Enter when ready

Setpoint: 1.000000 Reading: 0.989661   Measurement Time:                       0.989661uS
DAQmx Error: %s Specified channel cannot be added to the task, because a channel with the same name is already in the task.
Channel Name: PXI1Slot4/ai0

Task Name: _unnamedTask<0>

Status Code: -200489
 in function DAQmxCreateAIBridgeChan

Channel Time:  8.15625 seconds. ( 0.1359375 minutes)

>>> 
====== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300.py ======
Preparing channel 0 , press Enter when ready

Setpoint: 1.000000 Reading: 0.989538   Measurement Time:                       0.989538uS
Setpoint: 2.000000 Reading: 1.999642   Measurement Time:                       1.999642uS
Setpoint: 5.000000 Reading: 4.981196   Measurement Time:                       4.981196uS
Setpoint: 10.000000 Reading: 10.007131  Measurement Time:                      10.007131uS
Press enter to continue, or any other character to test again.
Preparing channel 1 , press Enter when ready

Setpoint: 1.000000 Reading: 0.992713   Measurement Time:                       0.992713uS
Setpoint: 2.000000 Reading: 2.004112   Measurement Time:                       2.004112uS
Setpoint: 5.000000 Reading: 4.979811   Measurement Time:                       4.979811uS
Setpoint: 10.000000 Reading: 9.994394   Measurement Time:                       9.994394uS
Press enter to continue, or any other character to test again.
Preparing channel 2 , press Enter when ready

Setpoint: 1.000000 Reading: 1.021858   Measurement Time:                       1.021858uS
Setpoint: 2.000000 Reading: 1.986431   Measurement Time:                       1.986431uS
Setpoint: 5.000000 Reading: 5.003248   Measurement Time:                       5.003248uS
Setpoint: 10.000000 Reading: 9.976294   Measurement Time:                       9.976294uS
Press enter to continue, or any other character to test again.
Preparing channel 3 , press Enter when ready

Setpoint: 1.000000 Reading: 0.984664   Measurement Time:                       0.984664uS
Setpoint: 2.000000 Reading: 1.992360   Measurement Time:                       1.992360uS
Setpoint: 5.000000 Reading: 4.997252   Measurement Time:                       4.997252uS
Setpoint: 10.000000 Reading: 9.986510   Measurement Time:                       9.986510uS
Press enter to continue, or any other character to test again.
Preparing channel 4 , press Enter when ready

Setpoint: 1.000000 Reading: 1.015418   Measurement Time:                       1.015418uS
Setpoint: 2.000000 Reading: 2.014360   Measurement Time:                       2.014360uS
Setpoint: 5.000000 Reading: 4.981032   Measurement Time:                       4.981032uS
Setpoint: 10.000000 Reading: 10.006260  Measurement Time:                      10.006260uS
Press enter to continue, or any other character to test again.
Preparing channel 5 , press Enter when ready

Setpoint: 1.000000 Reading: 0.994095   Measurement Time:                       0.994095uS
Setpoint: 2.000000 Reading: 1.996073   Measurement Time:                       1.996073uS
Setpoint: 5.000000 Reading: 5.009675   Measurement Time:                       5.009675uS
Setpoint: 10.000000 Reading: 9.989340   Measurement Time:                       9.989340uS
Press enter to continue, or any other character to test again.
Preparing channel 6 , press Enter when ready

Setpoint: 1.000000 Reading: 1.015799   Measurement Time:                       1.015799uS
Setpoint: 2.000000 Reading: 2.018790   Measurement Time:                       2.018790uS
Setpoint: 5.000000 Reading: 5.017704   Measurement Time:                       5.017704uS
Setpoint: 10.000000 Reading: 9.993496   Measurement Time:                       9.993496uS
Press enter to continue, or any other character to test again.

Channel Time:  234.171875 seconds. ( 3.9028645833333333 minutes)

>>> print("Press \'y\' to re")
Press 'y' to re
>>> 
====== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300.py ======
Preparing channel 0 , press Enter when ready

Setpoint: 1.0 Reading: 4.998546                       Measurement Time: 15625.000000 uS
Setpoint: 2.0 Reading: 4.998637                       Measurement Time: 0.000000 uS
Setpoint: 5.0 Reading: 4.998611                       Measurement Time: 0.000000 uS
Setpoint: 10.0 Reading: 4.998579                       Measurement Time: 0.000000 uS

Channel Time:  14.96875 seconds. ( 0.24947916666666667 minutes)

Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300.py", line 97, in <module>
    channelTime = chanStop - chanStart
NameError: name 'chanStop' is not defined
>>> 
====== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300.py ======
Preparing channel 0 , press Enter when ready

Setpoint: 1.0 Reading: 0.989486                       Measurement Time: 15625.000000 uS
Setpoint: 2.0 Reading: 1.999681                       Measurement Time: 15625.000000 uS
Setpoint: 5.0 Reading: 4.981260                       Measurement Time: 0.000000 uS
Setpoint: 10.0 Reading: 10.007252                      Measurement Time: 0.000000 uS

Channel Time:  19.140625 seconds. ( 0.3190104166666667 minutes)

Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300.py", line 97, in <module>
    chanTime = chanStop - chanStart
NameError: name 'chanStart' is not defined
>>> 
====== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300.py ======
Preparing channel 0 , press Enter when ready

Setpoint: 1.0  Reading: 0.989456                       Measurement Time: 15625.000000 uS
Setpoint: 2.0  Reading: 1.999719                       Measurement Time: 0.000000 uS
Setpoint: 5.0  Reading: 4.981307                       Measurement Time: 0.000000 uS
Setpoint: 10.0 Reading: 10.007164                      Measurement Time: 0.000000 uS

 Channel 0 completed in 421875.000000 microseconds
Press 'y' to retest PXI1Slot4/ai0 ,or any other key to continue

Channel Time:  19.171875 seconds. ( 0.31953125 minutes)

Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300.py", line 102, in <module>
    response = input("")
  File "C:\Python34\lib\idlelib\PyShell.py", line 1386, in readline
    line = self._line_buffer or self.shell.readline()
KeyboardInterrupt
>>> 
====== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300.py ======
Preparing channel 0 , press Enter when ready

Setpoint: 1.0    Reading: 0.989528                       Measurement Time: 31250.000000 uS
Setpoint: 2.0    Reading: 1.999593                       Measurement Time: 15625.000000 uS
Setpoint: 5.0    Reading: 4.981188                       Measurement Time: 15625.000000 uS
Setpoint: 10.0   Reading: 10.007468                      Measurement Time: 15625.000000 uS

 Channel 0   completed in 203125.00 microseconds
Press 'y' to retest PXI1Slot4/ai0 ,or any other key to continue
====== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300.py ======
Preparing channel 0 , press Enter when ready
Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300.py", line 71, in <module>
    input("")
  File "C:\Python34\lib\idlelib\PyShell.py", line 1386, in readline
    line = self._line_buffer or self.shell.readline()
KeyboardInterrupt

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300.py", line 115, in <module>
    executionTime = endTime - startTime
NameError: name 'endTime' is not defined
>>> 
====== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300.py ======
Preparing channel0, press Enter when ready
Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300.py", line 71, in <module>
    input("")
  File "C:\Python34\lib\idlelib\PyShell.py", line 1386, in readline
    line = self._line_buffer or self.shell.readline()
KeyboardInterrupt

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300.py", line 116, in <module>
    executionTime = endTime - startTime
NameError: name 'endTime' is not defined
>>> 
====== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300.py ======
Preparing channel 0 , press Enter when ready
Setpoint: 1.0     Reading: 0.989516                       Measurement Time: 31250.00 uS
Setpoint: 2.0     Reading: 1.999622                       Measurement Time: 15625.00 uS
Setpoint: 5.0     Reading: 4.981273                       Measurement Time: 15625.00 uS
Setpoint: 10.0    Reading: 10.007496                      Measurement Time: 15625.00 uS

 Channel 0   completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot4/ai0 ,or any other key to continue
Preparing channel 1 , press Enter when ready
Setpoint: 1.0     Reading: 10.007368                      Measurement Time: 31250.00 uS
Setpoint: 2.0     Reading: 10.007691                      Measurement Time: 15625.00 uS
Setpoint: 5.0     Reading: 10.007368                      Measurement Time: 15625.00 uS
Setpoint: 10.0    Reading: 10.007326                      Measurement Time: 15625.00 uS

 Channel 1   completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot4/ai1 ,or any other key to continue
Preparing channel 2 , press Enter when ready
Setpoint: 1.0     Reading: 10.007430                      Measurement Time: 31250.00 uS
Setpoint: 2.0     Reading: 10.007447                      Measurement Time: 15625.00 uS
Setpoint: 5.0     Reading: 10.007457                      Measurement Time: 15625.00 uS
Setpoint: 10.0    Reading: 10.007456                      Measurement Time: 15625.00 uS

 Channel 2   completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot4/ai2 ,or any other key to continue
Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300.py", line 101, in <module>
    response = input("")
  File "C:\Python34\lib\idlelib\PyShell.py", line 1386, in readline
    line = self._line_buffer or self.shell.readline()
KeyboardInterrupt

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300.py", line 116, in <module>
    executionTime = endTime - startTime
NameError: name 'endTime' is not defined
>>> 
====== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300.py ======
Preparing channel 0 , press Enter when ready
Setpoint: 1.00 Reading: 0.989636                       Measurement Time: 31250.00 uS
Setpoint: 2.00 Reading: 1.999843                       Measurement Time: 15625.00 uS
Setpoint: 5.00 Reading: 4.981420                       Measurement Time: 15625.00 uS
Setpoint: 10.00 Reading: 10.007544                      Measurement Time: 15625.00 uS

 Channel 0 completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot4/ai0 , or any other key to continue
Preparing channel 1 , press Enter when ready
Setpoint: 1.00 Reading: 0.992834                       Measurement Time: 31250.00 uS
Setpoint: 2.00 Reading: 2.003839                       Measurement Time: 15625.00 uS
Setpoint: 5.00 Reading: 4.979725                       Measurement Time: 15625.00 uS
Setpoint: 10.00 Reading: 9.994493                       Measurement Time: 15625.00 uS

 Channel 1 completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot4/ai1 , or any other key to continue
Preparing channel 2 , press Enter when ready
Setpoint: 1.00 Reading: 1.021729                       Measurement Time: 31250.00 uS
Setpoint: 2.00 Reading: 1.986278                       Measurement Time: 15625.00 uS
Setpoint: 5.00 Reading: 5.003345                       Measurement Time: 31250.00 uS
Setpoint: 10.00 Reading: 9.976175                       Measurement Time: 15625.00 uS

 Channel 2 completed in 312500.00 microseconds
Press 'y' to retest PXI1Slot4/ai2 , or any other key to continue
Preparing channel 3 , press Enter when ready
Setpoint: 1.00 Reading: 0.984537                       Measurement Time: 15625.00 uS
Setpoint: 2.00 Reading: 1.991974                       Measurement Time: 15625.00 uS
Setpoint: 5.00 Reading: 4.997816                       Measurement Time: 31250.00 uS
Setpoint: 10.00 Reading: 9.987012                       Measurement Time: 15625.00 uS

 Channel 3 completed in 328125.00 microseconds
Press 'y' to retest PXI1Slot4/ai3 , or any other key to continue
Preparing channel 4 , press Enter when ready
Setpoint: 1.00 Reading: 1.015470                       Measurement Time: 31250.00 uS
Setpoint: 2.00 Reading: 2.014458                       Measurement Time: 15625.00 uS
Setpoint: 5.00 Reading: 4.981216                       Measurement Time: 15625.00 uS
Setpoint: 10.00 Reading: 10.006319                      Measurement Time: 15625.00 uS

 Channel 4 completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot4/ai4 , or any other key to continue
Preparing channel 5 , press Enter when ready
Setpoint: 1.00 Reading: 0.994381                       Measurement Time: 31250.00 uS
Setpoint: 2.00 Reading: 1.996033                       Measurement Time: 15625.00 uS
Setpoint: 5.00 Reading: 5.009850                       Measurement Time: 15625.00 uS
Setpoint: 10.00 Reading: 9.989298                       Measurement Time: 15625.00 uS

 Channel 5 completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot4/ai5 , or any other key to continue
Preparing channel 6 , press Enter when ready
Setpoint: 1.00 Reading: 1.016037                       Measurement Time: 15625.00 uS
Setpoint: 2.00 Reading: 2.019106                       Measurement Time: 15625.00 uS
Setpoint: 5.00 Reading: 5.017983                       Measurement Time: 15625.00 uS
Setpoint: 10.00 Reading: 9.993498                       Measurement Time: 15625.00 uS

 Channel 6 completed in 437500.00 microseconds
Press 'y' to retest PXI1Slot4/ai6 , or any other key to continue

Channel Time:  272.578125 seconds. ( 4.54296875 minutes)

>>> 
====== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300.py ======
Connect TB-4300 to channel 0, press Enter when ready
Setpoint: 1.00 Reading: 0.989443                       Measurement Time: 31250.00 uS
Setpoint: 2.00 Reading: 1.999604                       Measurement Time: 15625.00 uS
Setpoint: 5.00 Reading: 4.981308                       Measurement Time: 15625.00 uS
Setpoint: 10.00 Reading: 10.007576                      Measurement Time: 15625.00 uS

 Channel 0 completed in 437500.00 microseconds
Press 'y' to retest PXI1Slot4/ai0 , or any other key to continue
====== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300.py ======
Connect TB-4300 to channel 0, press Enter when ready
Setpoint: 1.00 Reading: 0.989546
Setpoint: 2.00 Reading: 1.999624
Setpoint: 5.00 Reading: 4.981251
Setpoint: 10.00 Reading: 10.007265

Channel 0 completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot4/ai0 , or any other key to continue
Connect TB-4300 to channel 1, press Enter when ready
Setpoint: 1.00 Reading: 10.007340
Setpoint: 2.00 Reading: 10.007336
Setpoint: 5.00 Reading: 10.007342
Setpoint: 10.00 Reading: 10.007392

Channel 1 completed in 375000.00 microseconds
Press 'y' to retest PXI1Slot4/ai1 , or any other key to continuey
Connect TB-4300 to channel 2, press Enter when ready
====== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300.py ======
Connect TB-4300 to channel 0, press Enter when ready... 
Setpoint: 1.00 Reading: 0.989699
Setpoint: 2.00 Reading: 1.999615
Setpoint: 5.00 Reading: 4.981645
Setpoint: 10.00 Reading: 10.007454

Channel 0 completed in 437500.00 microseconds
Press 'y' to retest PXI1Slot4/ai0 , or any other key to continue... 
Connect TB-4300 to channel 1, press Enter when ready... 
Setpoint: 1.00 Reading: 0.992714
Setpoint: 2.00 Reading: 2.003961
Setpoint: 5.00 Reading: 4.979749
Setpoint: 10.00 Reading: 9.994513

Channel 1 completed in 359375.00 microseconds
Press 'y' to retest PXI1Slot4/ai1 , or any other key to continue... 
Connect TB-4300 to channel 2, press Enter when ready... 
Setpoint: 1.00 Reading: 1.021951
Setpoint: 2.00 Reading: 1.986320
Setpoint: 5.00 Reading: 5.003169
Setpoint: 10.00 Reading: 9.976038

Channel 2 completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot4/ai2 , or any other key to continue... 
Connect TB-4300 to channel 3, press Enter when ready... 
Setpoint: 1.00 Reading: 0.984083
Setpoint: 2.00 Reading: 1.992447
Setpoint: 5.00 Reading: 4.997501
Setpoint: 10.00 Reading: 9.986898

Channel 3 completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot4/ai3 , or any other key to continue... 
Connect TB-4300 to channel 4, press Enter when ready... 
Setpoint: 1.00 Reading: 1.015121
Setpoint: 2.00 Reading: 2.013923
Setpoint: 5.00 Reading: 4.980745
Setpoint: 10.00 Reading: 10.006034

Channel 4 completed in 437500.00 microseconds
Press 'y' to retest PXI1Slot4/ai4 , or any other key to continue... 
Connect TB-4300 to channel 5, press Enter when ready... 
Setpoint: 1.00 Reading: 0.994344
Setpoint: 2.00 Reading: 1.996491
Setpoint: 5.00 Reading: 5.009985
Setpoint: 10.00 Reading: 9.989330

Channel 5 completed in 453125.00 microseconds
Press 'y' to retest PXI1Slot4/ai5 , or any other key to continue... 
Connect TB-4300 to channel 6, press Enter when ready... 
Setpoint: 1.00 Reading: 1.016064
Setpoint: 2.00 Reading: 2.019022
Setpoint: 5.00 Reading: 5.018278
Setpoint: 10.00 Reading: 9.993145

Channel 6 completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot4/ai6 , or any other key to continue... 
Connect TB-4300 to channel 7, press Enter when ready... 
Setpoint: 1.00 Reading: 0.986216
Setpoint: 2.00 Reading: 1.985004
Setpoint: 5.00 Reading: 4.998529
Setpoint: 10.00 Reading: 10.000360

Channel 7 completed in 437500.00 microseconds
Press 'y' to retest PXI1Slot4/ai7 , or any other key to continue... 

Channel Time:  203.21875 seconds. ( 3.386979166666667 minutes)

>>> readings
array([[  0.98969877,   1.99961483,   4.98164463,  10.00745392],
       [  0.99271357,   2.00396109,   4.9797492 ,   9.99451256],
       [  1.0219506 ,   1.9863199 ,   5.00316858,   9.97603798],
       [  0.98408318,   1.99244738,   4.99750137,   9.98689842],
       [  1.0151211 ,   2.01392293,   4.98074532,  10.0060339 ],
       [  0.99434406,   1.99649072,   5.00998545,   9.98933029],
       [  1.01606405,   2.01902223,   5.01827765,   9.99314499],
       [  0.98621649,   1.98500419,   4.99852943,  10.00035954]])
>>> len(readings)
8
>>> fileName = time.strftime("%Y-%m-%d - %H-%M - ", time.localtime())+device+".csv"
>>> f = open( fileName, 'a' )
Traceback (most recent call last):
  File "<pyshell#91>", line 1, in <module>
    f = open( fileName, 'a' )
FileNotFoundError: [Errno 2] No such file or directory: '2018-02-27 - 21-19 - PXI1Slot4/ai.csv'
>>> time.strftime("%Y-%m-%d - %H-%M - ", time.localtime())+dev+".csv"
'2018-02-27 - 21-20 - PXI1Slot4/ai7.csv'
>>> time.strftime("%Y-%m-%d - %H-%M - ", time.localtime())+".csv"
'2018-02-27 - 21-20 - .csv'
>>> fN = time.strftime("%Y-%m-%d - %H-%M - ", time.localtime())+".csv"
>>> f = open( fileName, 'a' )
Traceback (most recent call last):
  File "<pyshell#95>", line 1, in <module>
    f = open( fileName, 'a' )
FileNotFoundError: [Errno 2] No such file or directory: '2018-02-27 - 21-19 - PXI1Slot4/ai.csv'
>>> f = open( fN, 'a' )
>>> for c in len(readings):
	for v in len(readings[c]):
		output = str(readings[c][v])
		f.write(output)

		
Traceback (most recent call last):
  File "<pyshell#101>", line 1, in <module>
    for c in len(readings):
TypeError: 'int' object is not iterable
>>> for c in range(len(readings)):
	for v in range(len(readings[c])):
		output = str(readings[c][v])
		f.write(output)

		
14
13
13
13
14
13
13
13
13
13
13
13
14
13
13
13
13
13
13
13
14
13
13
13
13
13
13
13
12
13
12
13
>>>  f.close()
 
SyntaxError: unexpected indent
>>> f.close()
>>> for c in range(len(readings)):
	for v in range(len(readings[c])):
		output = str(readings[c][v])+"\n"
		f.write(output)

		
Traceback (most recent call last):
  File "<pyshell#107>", line 4, in <module>
    f.write(output)
ValueError: I/O operation on closed file.
>>> f = open( fileName, 'a' )
Traceback (most recent call last):
  File "<pyshell#108>", line 1, in <module>
    f = open( fileName, 'a' )
FileNotFoundError: [Errno 2] No such file or directory: '2018-02-27 - 21-19 - PXI1Slot4/ai.csv'
>>> f = open( fN, 'a' )
>>> for c in range(len(readings)):
	for v in range(len(readings[c])):
		output = str(readings[c][v])+"\n"
		f.write(output)
	f.write("\n")

	
15
14
14
14
1
15
14
14
14
1
14
14
14
14
1
15
14
14
14
1
14
14
14
14
1
15
14
14
14
1
14
14
14
14
1
13
14
13
14
1
>>> f.close
<built-in method close of _io.TextIOWrapper object at 0x0384DEB0>
>>> f.close()
>>> 
====== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300.py ======
Connect TB-4300 to channel 0, press Enter when ready... 
DAQmx Error: %s Device identifier is invalid.
Device Specified: empty string
Suggested Device(s): PXI1Slot3_2, PXI1Slot4, PXI1Slot5_3

Task Name: _unnamedTask<1>

Status Code: -200220
 in function DAQmxCreateAIBridgeChan

Channel Time:  6.90625 seconds. ( 0.1151 minutes)

>>> 
====== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300.py ======
Connect TB-4300 to channel 0, press Enter when ready... 
Setpoint: 1          Reading: 1.013514  
Setpoint: 2          Reading: 2.015090  
Setpoint: 5          Reading: 4.977113  
Setpoint: 10         Reading: 10.017169 

Channel 0 completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot3_2/ai0, or any other key to continue... 
Connect TB-4300 to channel 1, press Enter when ready... 
Setpoint: 1          Reading: 1.019965  
Setpoint: 2          Reading: 2.017194  
Setpoint: 5          Reading: 5.019849  
Setpoint: 10         Reading: 9.982804  

Channel 1 completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot3_2/ai1, or any other key to continue... 
Connect TB-4300 to channel 2, press Enter when ready... 
Setpoint: 1          Reading: 1.003312  
Setpoint: 2          Reading: 2.001589  
Setpoint: 5          Reading: 5.015054  
Setpoint: 10         Reading: 10.008355 

Channel 2 completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot3_2/ai2, or any other key to continue... 
Connect TB-4300 to channel 3, press Enter when ready... 
Setpoint: 1          Reading: 0.001172  
Setpoint: 2          Reading: 0.001146  
Setpoint: 5          Reading: 0.001161  
Setpoint: 10         Reading: 0.001144  

Channel 3 completed in 437500.00 microseconds
Press 'y' to retest PXI1Slot3_2/ai3, or any other key to continue... 
Connect TB-4300 to channel 4, press Enter when ready... 
Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300.py", line 71, in <module>
    input("")
  File "C:\Python34\lib\idlelib\PyShell.py", line 1386, in readline
    line = self._line_buffer or self.shell.readline()
KeyboardInterrupt

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300.py", line 120, in <module>
    executionTime = endTime - startTime
NameError: name 'endTime' is not defined
>>> 
====== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300.py ======
Connect TB-4300 to channel 0, press Enter when ready... 

Setpoint: 1.00       Reading: 1.013537  
Setpoint: 2.00       Reading: 2.015334  
Setpoint: 5.00       Reading: 4.977168  
Setpoint: 10.00      Reading: 10.017557 

Channel 0 completed in 437500.00 microseconds
Press 'y' to retest PXI1Slot3_2/ai0, or any other key to continue... 
Connect TB-4300 to channel 1, press Enter when ready... 

Setpoint: 1.00       Reading: 1.020100  
Setpoint: 2.00       Reading: 2.017470  
Setpoint: 5.00       Reading: 5.020061  
Setpoint: 10.00      Reading: 9.983132  

Channel 1 completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot3_2/ai1, or any other key to continue... 
Connect TB-4300 to channel 2, press Enter when ready... 

Setpoint: 1.00       Reading: 1.003289  
Setpoint: 2.00       Reading: 2.001520  
Setpoint: 5.00       Reading: 5.014563  
Setpoint: 10.00      Reading: 10.007979 

Channel 2 completed in 437500.00 microseconds
Press 'y' to retest PXI1Slot3_2/ai2, or any other key to continue... 
Connect TB-4300 to channel 3, press Enter when ready... 

Setpoint: 1.00       Reading: 1.020270  
Setpoint: 2.00       Reading: 1.984075  
Setpoint: 5.00       Reading: 5.012888  
Setpoint: 10.00      Reading: 9.986566  

Channel 3 completed in 437500.00 microseconds
Press 'y' to retest PXI1Slot3_2/ai3, or any other key to continue... 
Connect TB-4300 to channel 4, press Enter when ready... 

Setpoint: 1.00       Reading: 1.012331  
Setpoint: 2.00       Reading: 2.005683  
Setpoint: 5.00       Reading: 4.988719  
Setpoint: 10.00      Reading: 9.990263  

Channel 4 completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot3_2/ai4, or any other key to continue... 
Connect TB-4300 to channel 5, press Enter when ready... 

Setpoint: 1.00       Reading: 0.999746  
Setpoint: 2.00       Reading: 1.999914  
Setpoint: 5.00       Reading: 5.002486  
Setpoint: 10.00      Reading: 9.998694  

Channel 5 completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot3_2/ai5, or any other key to continue... 
Connect TB-4300 to channel 6, press Enter when ready... 

Setpoint: 1.00       Reading: 0.977209  
Setpoint: 2.00       Reading: 2.017520  
Setpoint: 5.00       Reading: 5.008197  
Setpoint: 10.00      Reading: 10.004703 

Channel 6 completed in 375000.00 microseconds
Press 'y' to retest PXI1Slot3_2/ai6, or any other key to continue... 
Connect TB-4300 to channel 7, press Enter when ready... 

Setpoint: 1.00       Reading: 1.010565  
Setpoint: 2.00       Reading: 2.008688  
Setpoint: 5.00       Reading: 4.996536  
Setpoint: 10.00      Reading: 9.989985  

Channel 7 completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot3_2/ai7, or any other key to continue... 

Channel Time:  232.359375 seconds. ( 3.8727 minutes)

>>> f.close()
>>> f = open( fileName, 'a' )
>>> for c in range(len(readings)):
	for v in range(len(readings[c])):
		output = str(readings[c][v]) + "\n"
		f.write(output)
	f.write("\n")

	
14
14
14
14
1
14
13
14
14
1
14
14
14
13
1
14
13
14
13
1
14
14
14
14
1
15
14
14
14
1
15
14
14
14
1
14
14
14
12
1
>>> f.close()
>>> fileName
'2018-02-27 - 21:41 - PXI1Slot3_2.csv'
>>> fileName = "test"
>>> f = open( fileName, 'a' )
>>> for c in range(len(readings)):
	for v in range(len(readings[c])):
		output = str(readings[c][v]) + "\n"
		f.write(output)
	f.write("\n")

	
14
14
14
14
1
14
13
14
14
1
14
14
14
13
1
14
13
14
13
1
14
14
14
14
1
15
14
14
14
1
15
14
14
14
1
14
14
14
12
1
>>> f.close()
>>> f.close()
>>> 
====== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300.py ======
Connect TB-4300 to channel 0, press Enter when ready... 

Setpoint: 1.00       Reading: 0.046422  
Setpoint: 2.00       Reading: 0.046493  
Setpoint: 5.00       Reading: 0.046611  
Setpoint: 10.00      Reading: 0.046593  

Channel 0 completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot5_3/ai0, or any other key to continue... 
Connect TB-4300 to channel 1, press Enter when ready... 

Setpoint: 1.00       Reading: 0.046579  
Setpoint: 2.00       Reading: 0.046576  
Setpoint: 5.00       Reading: 0.046508  
Setpoint: 10.00      Reading: 0.046537  

Channel 1 completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot5_3/ai1, or any other key to continue... 
Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300.py", line 105, in <module>
    response = input("")
  File "C:\Python34\lib\idlelib\PyShell.py", line 1386, in readline
    line = self._line_buffer or self.shell.readline()
KeyboardInterrupt

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300.py", line 121, in <module>
    executionTime = endTime - startTime
NameError: name 'endTime' is not defined
>>> 
====== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300.py ======
Connect TB-4300 to channel 0, press Enter when ready... 

Setpoint: 1.00       Reading: 1.002842  
Setpoint: 2.00       Reading: 1.996268  
Setpoint: 5.00       Reading: 4.991208  
Setpoint: 10.00      Reading: 10.007868 

Channel 0 completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot5_3/ai0, or any other key to continue... 
Connect TB-4300 to channel 1, press Enter when ready... 

Setpoint: 1.00       Reading: 1.018401  
Setpoint: 2.00       Reading: 2.017048  
Setpoint: 5.00       Reading: 5.003818  
Setpoint: 10.00      Reading: 10.019721 

Channel 1 completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot5_3/ai1, or any other key to continue... 
Connect TB-4300 to channel 2, press Enter when ready... 

Setpoint: 1.00       Reading: 0.999913  
Setpoint: 2.00       Reading: 2.003283  
Setpoint: 5.00       Reading: 5.004023  
Setpoint: 10.00      Reading: 10.006097 

Channel 2 completed in 437500.00 microseconds
Press 'y' to retest PXI1Slot5_3/ai2, or any other key to continue... 
Connect TB-4300 to channel 3, press Enter when ready... 

Setpoint: 1.00       Reading: 0.984551  
Setpoint: 2.00       Reading: 1.978749  
Setpoint: 5.00       Reading: 5.014427  
Setpoint: 10.00      Reading: 10.019397 

Channel 3 completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot5_3/ai3, or any other key to continue... 
Connect TB-4300 to channel 4, press Enter when ready... 

Setpoint: 1.00       Reading: 1.000763  
Setpoint: 2.00       Reading: 2.006634  
Setpoint: 5.00       Reading: 5.011952  
Setpoint: 10.00      Reading: 10.013834 

Channel 4 completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot5_3/ai4, or any other key to continue... 
Connect TB-4300 to channel 5, press Enter when ready... 

Setpoint: 1.00       Reading: 0.992364  
Setpoint: 2.00       Reading: 1.993513  
Setpoint: 5.00       Reading: 5.005584  
Setpoint: 10.00      Reading: 9.993809  

Channel 5 completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot5_3/ai5, or any other key to continue... 
Connect TB-4300 to channel 6, press Enter when ready... 

Setpoint: 1.00       Reading: 1.018534  
Setpoint: 2.00       Reading: 2.014585  
Setpoint: 5.00       Reading: 5.018220  
Setpoint: 10.00      Reading: 10.009199 

Channel 6 completed in 421875.00 microseconds
Press 'y' to retest PXI1Slot5_3/ai6, or any other key to continue... 
Connect TB-4300 to channel 7, press Enter when ready... 

Setpoint: 1.00       Reading: 1.004356  
Setpoint: 2.00       Reading: 2.006090  
Setpoint: 5.00       Reading: 5.013195  
Setpoint: 10.00      Reading: 9.999557  

Channel 7 completed in 437500.00 microseconds
Press 'y' to retest PXI1Slot5_3/ai7, or any other key to continue... 

Channel Time:  167.625 seconds. ( 2.7938 minutes)

>>> fileName = time.strftime("%Y-%m-%d - %H-%M - ", time.localtime())+ device + ".csv"
>>> f = open( fileName, 'a' )
>>> for c in range(len(readings)):
	for v in range(len(readings[c])):
		output = str(readings[c][v]) + "\n"
		f.write(output)
	f.write("\n")

	
14
14
14
14
1
14
14
14
14
1
15
12
14
14
1
15
14
14
14
1
14
14
14
14
1
15
14
14
14
1
14
14
14
14
1
14
14
14
14
1
>>> f.close()
>>> f.close()
>>> f.close()
>>> f.close()
>>> 
=== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300-shunt.py ===
Connect TB-4300 to channel 0, press Enter when ready... 


Warning (from warnings module):
  File "C:\Python34\lib\site-packages\nidmm\errors.py", line 82
    warnings.warn(NidmmWarning(code, description))
NidmmWarning: Warning 1073356801 occurred.

Over range.
Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300-shunt.py", line 102, in <module>
    shunt[res][res], readings[chan,res]))
TypeError: 'int' object is not subscriptable

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300-shunt.py", line 130, in <module>
    executionTime = endTime - startTime
NameError: name 'endTime' is not defined
>>> 
=== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300-shunt.py ===
Connect TB-4300 to channel 0, press Enter when ready... 

Setpoint: 33333.00   Reading: 33417.683594
Setpoint: 50000.00   Reading: 49903.394531
Setpoint: 100000.00  Reading: 100191.156250

Channel 0 completed in 343750.00 microseconds
Press 'y' to retest PXI1Slot5_3/ai0, or any other key to continue... 
Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300-shunt.py", line 114, in <module>
    response = input("")
  File "C:\Python34\lib\idlelib\PyShell.py", line 1386, in readline
    line = self._line_buffer or self.shell.readline()
KeyboardInterrupt

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300-shunt.py", line 130, in <module>
    executionTime = endTime - startTime
NameError: name 'endTime' is not defined
>>> 33417 / 1000
33.417
>>> 
=== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300-shunt.py ===
Connect TB-4300 to channel 0, press Enter when ready... 

Setpoint: 33.33      Reading: 33.329437 
Setpoint: 50.00      Reading: 50.129242 
Setpoint: 100.00     Reading: 99.412352 

Channel 0 completed in 390625.00 microseconds
Press 'y' to retest PXI1Slot5_3/ai0, or any other key to continue... 
Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300-shunt.py", line 116, in <module>
    response = input("")
  File "C:\Python34\lib\idlelib\PyShell.py", line 1386, in readline
    line = self._line_buffer or self.shell.readline()
KeyboardInterrupt

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300-shunt.py", line 132, in <module>
    executionTime = endTime - startTime
NameError: name 'endTime' is not defined
>>> dmm.read
<bound method Session.read of <nidmm.session.Session object at 0x01F3DB90>>
>>> dmm.read()
100167.171875
>>> dmm.configure_ac_bandwidth
<bound method Session.configure_ac_bandwidth of <nidmm.session.Session object at 0x01F3DB90>>
>>> 
>>> dmm.get_aperture_time_info
<bound method Session.get_aperture_time_info of <nidmm.session.Session object at 0x01F3DB90>>
>>> dmm.get_aperture_time_info()
(0.0005, <ApertureTimeUnits.SECONDS: 0>)
>>> dmm.get_measurement_period
<bound method Session.get_measurement_period of <nidmm.session.Session object at 0x01F3DB90>>
\
>>> dmm.get_measurement_period()
0.031133333333333332
>>> dmm.settle_time()
Traceback (most recent call last):
  File "<pyshell#144>", line 1, in <module>
    dmm.settle_time()
TypeError: 'float' object is not callable
>>> dmm.settle_time
-1.0
>>> dmm.settle_time = 5.0
>>> dmm.read()
99646.421875
>>> dmm.settle_time = 3.0
>>> dmm.input_resistance
10000000.0
>>> dmm.powerline_freq = 60
>>> dmm.read
<bound method Session.read of <nidmm.session.Session object at 0x01F3DB90>>
>>> dmm.read()
100195.3671875
>>> dmm.close()
>>> dmm = nidmm.Session("ED-0053")
>>> dmm._get_attribute_vi_string
<bound method Session._get_attribute_vi_string of <nidmm.session.Session object at 0x01085D90>>
>>> dmm._get_attribute_vi_string()
Traceback (most recent call last):
  File "<pyshell#156>", line 1, in <module>
    dmm._get_attribute_vi_string()
TypeError: _get_attribute_vi_string() missing 1 required positional argument: 'attribute_id'
>>> dmm.read_status
<bound method Session.read_status of <nidmm.session.Session object at 0x01085D90>>
>>> dmm.read_status()
(0, <AcquisitionStatus.NO_ACQUISITION_IN_PROGRESS: 4>)
>>> dmm.range_check()
Traceback (most recent call last):
  File "<pyshell#159>", line 1, in <module>
    dmm.range_check()
TypeError: 'bool' object is not callable
>>> dmm.read()
0.0025826224591583014
>>> dmm.function = nidmm.Function._4_WIRE_RES
>>> dmm.input_resistance = 10000000
>>> dmm.range = 100000
>>> dmm.auto_zero = nidmm.AutoZero.ON
>>> dmm.configure_measurement_digits = 7.5
>>> dmm.powerline_freq = 60
>>> dmm.aperture_time
0.0005
>>> dmm.aperture_time_units
<ApertureTimeUnits.SECONDS: 0>
>>> dmm.aperture_time_units = nidmm.ApertureTimeUnits.
SyntaxError: invalid syntax
>>> dmm.aperture_time_units = nidmm.ApertureTimeUnits.POWER_LINE_CYCLES
>>> dmm.aperture_time_units
<ApertureTimeUnits.POWER_LINE_CYCLES: 1>
>>> dmm.aperture_time
0.03
>>> dmm.read()
99687.3984375
>>> dmm.read()
100176.390625
>>> dmm.aperture_time = 1.0
>>> dmm.aperture_time_units
<ApertureTimeUnits.POWER_LINE_CYCLES: 1>
>>> dmm.aperture_time
1.0
>>> dmm.read()
99947.35999350603
>>> dmm.aperture_time = 10.0
>>> dmm.read()
99947.26441239045
>>> 
=== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300-shunt.py ===
Connect TB-4300 to channel 0, press Enter when ready... 

Setpoint: 33.33      Reading: 33.325273 
Setpoint: 50.00      Reading: 49.996711 
Setpoint: 100.00     Reading: 99.946672 

Channel 0 completed in 359375.00 microseconds
Press 'y' to retest PXI1Slot5_3/ai0, or any other key to continue... 
Connect TB-4300 to channel 1, press Enter when ready... 

Setpoint: 33.33      Reading: 33.327355 
Setpoint: 50.00      Reading: 49.996235 
Setpoint: 100.00     Reading: 99.953571 

Channel 1 completed in 359375.00 microseconds
Press 'y' to retest PXI1Slot5_3/ai1, or any other key to continue... 
Connect TB-4300 to channel 2, press Enter when ready... 

Setpoint: 33.33      Reading: 33.322355 
Setpoint: 50.00      Reading: 49.989258 
Setpoint: 100.00     Reading: 99.945727 

Channel 2 completed in 312500.00 microseconds
Press 'y' to retest PXI1Slot5_3/ai2, or any other key to continue... 
Connect TB-4300 to channel 3, press Enter when ready... 

Setpoint: 33.33      Reading: 33.335953 
Setpoint: 50.00      Reading: 50.017543 
Setpoint: 100.00     Reading: 99.949399 

Channel 3 completed in 312500.00 microseconds
Press 'y' to retest PXI1Slot5_3/ai3, or any other key to continue... 
Connect TB-4300 to channel 4, press Enter when ready... 

Setpoint: 33.33      Reading: 33.323906 
Setpoint: 50.00      Reading: 49.993383 
Setpoint: 100.00     Reading: 99.950040 

Channel 4 completed in 359375.00 microseconds
Press 'y' to retest PXI1Slot5_3/ai4, or any other key to continue... 
Connect TB-4300 to channel 5, press Enter when ready... 

Setpoint: 33.33      Reading: 33.325433 
Setpoint: 50.00      Reading: 49.997156 
Setpoint: 100.00     Reading: 99.945993 

Channel 5 completed in 296875.00 microseconds
Press 'y' to retest PXI1Slot5_3/ai5, or any other key to continue... 
Connect TB-4300 to channel 6, press Enter when ready... 

Setpoint: 33.33      Reading: 33.326343 
Setpoint: 50.00      Reading: 49.996059 
Setpoint: 100.00     Reading: 99.956868 

Channel 6 completed in 359375.00 microseconds
Press 'y' to retest PXI1Slot5_3/ai6, or any other key to continue... 
Connect TB-4300 to channel 7, press Enter when ready... 

Setpoint: 33.33      Reading: 33.329812 
Setpoint: 50.00      Reading: 49.998414 
Setpoint: 100.00     Reading: 99.968751 

Channel 7 completed in 296875.00 microseconds
Press 'y' to retest PXI1Slot5_3/ai7, or any other key to continue... 

Channel Time:  341.328125 seconds. ( 5.6888 minutes)

>>> 
=== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300-shunt.py ===
Connect TB-4300 to channel 0, press Enter when ready... 


Warning (from warnings module):
  File "C:\Python34\lib\site-packages\nidmm\errors.py", line 82
    warnings.warn(NidmmWarning(code, description))
NidmmWarning: Warning 1073356801 occurred.

Over range.
Setpoint: 33.33      Reading: nan       

Warning (from warnings module):
  File "C:\Python34\lib\site-packages\nidmm\errors.py", line 82
    warnings.warn(NidmmWarning(code, description))
NidmmWarning: Warning 1073356801 occurred.

Over range.
Setpoint: 50.00      Reading: nan       

Warning (from warnings module):
  File "C:\Python34\lib\site-packages\nidmm\errors.py", line 82
    warnings.warn(NidmmWarning(code, description))
NidmmWarning: Warning 1073356801 occurred.

Over range.
Setpoint: 100.00     Reading: nan       

Channel 0 completed in 375000.00 microseconds
Press 'y' to retest PXI1Slot3_2/ai0, or any other key to continue... 
Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300-shunt.py", line 117, in <module>
    response = input("")
  File "C:\Python34\lib\idlelib\PyShell.py", line 1386, in readline
    line = self._line_buffer or self.shell.readline()
KeyboardInterrupt

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300-shunt.py", line 133, in <module>
    executionTime = endTime - startTime
NameError: name 'endTime' is not defined
>>> DAQmxClearTask(tH)
0
>>> DAQmxCreateTask("",byref(tH))
0
>>> DAQmxCreateAIBridgeChan(tH, dev,\
                                        '',-0.025,0.025,\
                                        DAQmx_Val_VoltsPerVolt,DAQmx_Val_FullBridge,\
                                        DAQmx_Val_Internal,\
                                        0.625,\
                                        350.0, "")
0
>>> DAQmxSetAIBridgeShuntCalEnable(tH, dev, True)
0
>>> dev
'PXI1Slot3_2/ai0'
>>> DAQmxSetAIBridgeShuntCalShuntCalAResistance(tH, dev, 33333)
0
>>> DAQmxGetAIExcitVal(tH, dev, "")
Traceback (most recent call last):
  File "<pyshell#187>", line 1, in <module>
    DAQmxGetAIExcitVal(tH, dev, "")
  File "<string>", line 2, in function
  File "C:\Python34\lib\site-packages\PyDAQmx\DAQmxFunctions.py", line 57, in mafunction
    error = f(*arg)
ctypes.ArgumentError: argument 3: <class 'TypeError'>: expected LP_c_double instance instead of str
>>> DAQmxGetAIExcitVal(th, dev, readings[0][0])
Traceback (most recent call last):
  File "<pyshell#188>", line 1, in <module>
    DAQmxGetAIExcitVal(th, dev, readings[0][0])
NameError: name 'th' is not defined
>>> DAQmxGetAIExcitVal(tH, dev, readings[0][0])
Traceback (most recent call last):
  File "<pyshell#189>", line 1, in <module>
    DAQmxGetAIExcitVal(tH, dev, readings[0][0])
  File "<string>", line 2, in function
  File "C:\Python34\lib\site-packages\PyDAQmx\DAQmxFunctions.py", line 57, in mafunction
    error = f(*arg)
ctypes.ArgumentError: argument 3: <class 'TypeError'>: expected LP_c_double instance instead of numpy.float64
>>> DAQmxSetAIExcitVal(tH, dev, 0.625)
0
>>> dmm.read()

Warning (from warnings module):
  File "C:\Python34\lib\site-packages\nidmm\errors.py", line 82
    warnings.warn(NidmmWarning(code, description))
NidmmWarning: Warning 1073356801 occurred.

Over range.
nan
>>> dmm.read()
99955.1021859435
>>> 
=== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300-shunt.py ===
Connect TB-4300 to channel 0, press Enter when ready... 

Setpoint: 33.33      Reading: 33.337300 
Setpoint: 50.00      Reading: 50.015031 
Setpoint: 100.00     Reading: 99.955719 

Channel 0 completed in 312500.00 microseconds
Press 'y' to retest PXI1Slot3_2/ai0, or any other key to continue... 
Connect TB-4300 to channel 1, press Enter when ready... 

Setpoint: 33.33      Reading: 33.327414 
Setpoint: 50.00      Reading: 49.996883 
Setpoint: 100.00     Reading: 99.953922 

Channel 1 completed in 296875.00 microseconds
Press 'y' to retest PXI1Slot3_2/ai1, or any other key to continue... 
Connect TB-4300 to channel 2, press Enter when ready... 

Setpoint: 33.33      Reading: 33.326582 
Setpoint: 50.00      Reading: 49.995715 
Setpoint: 100.00     Reading: 99.952829 

Channel 2 completed in 296875.00 microseconds
Press 'y' to retest PXI1Slot3_2/ai2, or any other key to continue... 
Connect TB-4300 to channel 3, press Enter when ready... 

Setpoint: 33.33      Reading: 33.325047 
Setpoint: 50.00      Reading: 49.992750 
Setpoint: 100.00     Reading: 99.951891 

Channel 3 completed in 359375.00 microseconds
Press 'y' to retest PXI1Slot3_2/ai3, or any other key to continue... 
Connect TB-4300 to channel 4, press Enter when ready... 

Setpoint: 33.33      Reading: 33.325500 
Setpoint: 50.00      Reading: 49.997141 
Setpoint: 100.00     Reading: 99.938735 

Channel 4 completed in 359375.00 microseconds
Press 'y' to retest PXI1Slot3_2/ai4, or any other key to continue... 
Connect TB-4300 to channel 5, press Enter when ready... 

Setpoint: 33.33      Reading: 33.328543 
Setpoint: 50.00      Reading: 49.996949 
Setpoint: 100.00     Reading: 99.959212 

Channel 5 completed in 343750.00 microseconds
Press 'y' to retest PXI1Slot3_2/ai5, or any other key to continue... 
Connect TB-4300 to channel 6, press Enter when ready... 

Setpoint: 33.33      Reading: 33.331754 
Setpoint: 50.00      Reading: 50.005254 
Setpoint: 100.00     Reading: 99.958915 

Channel 6 completed in 359375.00 microseconds
Press 'y' to retest PXI1Slot3_2/ai6, or any other key to continue... 
Connect TB-4300 to channel 7, press Enter when ready... 

Setpoint: 33.33      Reading: 33.327277 
Setpoint: 50.00      Reading: 49.995363 
Setpoint: 100.00     Reading: 99.957094 

Channel 7 completed in 296875.00 microseconds
Press 'y' to retest PXI1Slot3_2/ai7, or any other key to continue... 

Channel Time:  307.9375 seconds. ( 5.1323 minutes)

>>> 
=== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300-shunt.py ===
Traceback (most recent call last):
  File "C:/Documents and Settings/NI/Desktop/New Folder/4300-shunt.py", line 41, in <module>
    dmm = nidmm.Session("ED-0053")
  File "C:\Python34\lib\site-packages\nidmm\session.py", line 858, in __init__
    self._vi = self._init_with_options(resource_name, id_query, reset_device, option_string)
  File "C:\Python34\lib\site-packages\nidmm\session.py", line 1849, in _init_with_options
    error_code = self._library.niDMM_InitWithOptions(resource_name_ctype, id_query_ctype, reset_device_ctype, option_string_ctype, ctypes.pointer(vi_ctype))
  File "C:\Python34\lib\site-packages\nidmm\library.py", line 325, in niDMM_InitWithOptions
    return self.niDMM_InitWithOptions_cfunc(resource_name, id_query, reset_device, option_string, vi)
KeyboardInterrupt
>>> 
=== RESTART: C:/Documents and Settings/NI/Desktop/New Folder/4300-shunt.py ===
Connect TB-4300 to channel 0, press Enter when ready... 

Setpoint: 33.33      Reading: 33.326425 
Setpoint: 50.00      Reading: 49.994633 
Setpoint: 100.00     Reading: 99.956688 

Channel 0 completed in 359375.00 microseconds
Press 'y' to retest PXI1Slot4/ai0, or any other key to continue... 
Connect TB-4300 to channel 1, press Enter when ready... 

Setpoint: 33.33      Reading: 33.335172 
Setpoint: 50.00      Reading: 50.015207 
Setpoint: 100.00     Reading: 99.953594 

Channel 1 completed in 312500.00 microseconds
Press 'y' to retest PXI1Slot4/ai1, or any other key to continue... 
Connect TB-4300 to channel 2, press Enter when ready... 

Setpoint: 33.33      Reading: 33.328105 
Setpoint: 50.00      Reading: 49.997219 
Setpoint: 100.00     Reading: 99.959048 

Channel 2 completed in 359375.00 microseconds
Press 'y' to retest PXI1Slot4/ai2, or any other key to continue... 
Connect TB-4300 to channel 3, press Enter when ready... 

Setpoint: 33.33      Reading: 33.325707 
Setpoint: 50.00      Reading: 49.994699 
Setpoint: 100.00     Reading: 99.953290 

Channel 3 completed in 359375.00 microseconds
Press 'y' to retest PXI1Slot4/ai3, or any other key to continue... 
Connect TB-4300 to channel 4, press Enter when ready... 

Setpoint: 33.33      Reading: 33.328632 
Setpoint: 50.00      Reading: 49.995992 
Setpoint: 100.00     Reading: 99.961212 

Channel 4 completed in 359375.00 microseconds
Press 'y' to retest PXI1Slot4/ai4, or any other key to continue... 
Connect TB-4300 to channel 5, press Enter when ready... 

Setpoint: 33.33      Reading: 33.329660 
Setpoint: 50.00      Reading: 49.995371 
Setpoint: 100.00     Reading: 99.981071 

Channel 5 completed in 296875.00 microseconds
Press 'y' to retest PXI1Slot4/ai5, or any other key to continue... 
Connect TB-4300 to channel 6, press Enter when ready... 

Setpoint: 33.33      Reading: 33.327226 
Setpoint: 50.00      Reading: 49.996817 
Setpoint: 100.00     Reading: 99.957172 

Channel 6 completed in 359375.00 microseconds
Press 'y' to retest PXI1Slot4/ai6, or any other key to continue... 
Connect TB-4300 to channel 7, press Enter when ready... 

Setpoint: 33.33      Reading: 49.990320 
Setpoint: 50.00      Reading: 49.989953 

Warning (from warnings module):
  File "C:\Python34\lib\site-packages\nidmm\errors.py", line 82
    warnings.warn(NidmmWarning(code, description))
NidmmWarning: Warning 1073356801 occurred.

Over range.
Setpoint: 100.00     Reading: nan       

Channel 7 completed in 296875.00 microseconds
Press 'y' to retest PXI1Slot4/ai7, or any other key to continue... y

Retesting...

Press 'y' to retest PXI1Slot4/ai7, or any other key to continue... 

Channel Time:  304.34375 seconds. ( 5.0724 minutes)

>>> readings
array([[ 33.32642542,  49.99463297,  99.95668812],
       [ 33.33517151,  50.0152072 ,  99.95359437],
       [ 33.32810511,  49.99721891,  99.9590475 ],
       [ 33.32570667,  49.99469938,  99.95328968],
       [ 33.32863245,  49.99599235,  99.96121156],
       [ 33.32965979,  49.99537126,  99.98107095],
       [ 33.3272262 ,  49.99681657,  99.9571725 ],
       [ 49.99032047,  49.98995328,          nan]])
>>> read()
Traceback (most recent call last):
  File "<pyshell#194>", line 1, in <module>
    read()
NameError: name 'read' is not defined
>>> dmm.read()

Warning (from warnings module):
  File "C:\Python34\lib\site-packages\nidmm\errors.py", line 82
    warnings.warn(NidmmWarning(code, description))
NidmmWarning: Warning 1073356801 occurred.

Over range.
nan
>>> 
