
#dmm.configure_measurement_digits(nidmm.Function.DC_VOLTS, 10, 5.5)
#dmm.read()
#cal.write("OPER")
#cal.write("STBY")
#cal.query("ISR?")

#10 MOHM
#dmm.input_resistance = 10000000
#10 GOHM
#dmm.input_resistance = 10000000000

import nidmm
import visa
import time
import os
import sys
from datetime import datetime
startTime = time.time()
from string import Template
#dmm = nidmm.Session("dmm")
dmm = nidmm.Session("dmm")
rm = visa.ResourceManager()
cal = rm.open_resource("GPIB0::15::INSTR")
dmm.settle_time = 5


meas = []
read = 0

'''
ascii = Template('OUT $amp V; *WAI; OPER')

try:
    res = [ 10, 7 ]
    vdc = [ 0, 1, -1 ]
    exp = [ 0.1, 1.0, 10.0 ]
    
    ## Take readings for both 10G and 10M ranges
    for r in res :
        dmm.input_resistance = 10 ** r
        print("Input Resistance: ", 10 ** r)
        for e in exp :
            dmm.function = nidmm.Function.DC_VOLTS
            dmm.range = e
            dmm.configure_measurement_digits = 6.5
            for v in vdc :
                #msg = "OUT " + str(v*e) + " V;*WAI;OPER;"
                cal.write(ascii.substitute(amp=v*e))
                time.sleep(10)
                read = dmm.read()
                print('Setpoint: {0:>10f} Range: {1:>10} Reading: {2:>10f}'.format(v*e, e, read))
                meas.append(read)

    exp = [100,300]
    for e in exp :
        dmm.function = nidmm.Function.DC_VOLTS
        dmm.range = e
        dmm.configure_measurement_digits = 6.5
        for v in vdc :
            #msg = "OUT " + str(v*e) + " V;*WAI;OPER;"
            cal.write(ascii.substitute(amp=v*e))
            input("Confirm calibrator is in OPER mode and press enter..")
            time.sleep(5)
            read = dmm.read()
            print('Setpoint: {0:>10f} Range: {1:>10} Reading: {2:>10f}'.format(v*e, e, read))
            meas.append(read)
    
    
    endTime = time.time()
    
except:
    print("Oops, error..")
    cal.write("STBY")
    endTime = time.time()

finally:
    executionTime = endTime - startTime
    cal.write("STBY")
    print( "\nExecution Time: ", executionTime, "seconds. (",executionTime / 60,"minutes)\n")

'''

acfunc = [nidmm.Function.AC_VOLTS, nidmm.Function.AC_VOLTS_DC_COUPLED]
acfreq = [1,0.05,1,20,50,100,300]
acvolt = [0.05,0.5,5,50]
#acvolt= [50]
acthree = [300]

dmm.function = nidmm.Function.AC_VOLTS_DC_COUPLED
dmm.range = 0.05
dmm.configure_measurement_digits = 7.5
dmm.powerline_freq = 60
dmm.aperture_time_units = nidmm.ApertureTimeUnits.POWER_LINE_CYCLES
dmm.aperture_time = 10.0
dmm.settle_time = 5.0
v = 0.5
ascii = Template('OUT $amp V, $freq HZ; *WAI; OPER')
'''
for x in range(4) :
    dmm.function = nidmm.Function.AC_VOLTS
    dmm.range = v
    dmm.configure_measurement_digits = 6.5
    for f in range(0,len(acfreq)) :
        volts = v/10 if (f == 0) else v
        cal.write(ascii.substitute(amp=volts, freq=f))
   '''

'''
for r in acvolt:
    dmm.function = nidmm.Function.AC_VOLTS
    dmm.range = r
    for f in range(len(acfreq)):
        if (f == 0):
            volts = r / 10
        else:
            volts = r
        hz = acfreq[f]*1000
        
        cal.write(ascii.substitute(amp=volts, freq=hz))
        if volts > 5:
            input("start calibrator")
        reading = dmm.read()
        print('Range: {0:>3f} Set: {1:>10f} Frequency: {2:>10f} Reading: {3:>10f}'.format(r,volts,hz,reading))
        meas.append(reading)
            
    '''
dmm.aperture_time = 10.0
dmm.function = nidmm.Function.AC_VOLTS        
dmm.range = 300
print("Enter For Next reading, to quit")
while(input() != "y"):
    reading = dmm.read()
    print(reading)
    meas.append(reading)
    
    
        
    
