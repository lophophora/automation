import nidmm
import visa
import time
import os
import sys
from datetime import datetime
startTime = time.time()
from string import Template
#dmm = nidmm.Session("dmm")
dmm = nidmm.Session("dmm")
rm = visa.ResourceManager()
cal = rm.open_resource("GPIB0::15::INSTR")
dmm.settle_time = 3


meas = []
read = 0

rng =[0.02,0.2,1.0]
point = [[0.0,0.018,-0.018],[0,0.18,-0.18],[0,0.9,-0.9]]

rng= [.01,0.1,1.0]
ac = [[0.001,0.01],[0.01,0.1],[0.1,1.0]]



ascii = Template('OUT $set A, $hz HZ; *WAI; OPER')

dmm.function = nidmm.Function.AC_CURRENT
#dmm.function = nidmm.Function.DC_CURRENT
dmm.range = 0.01
dmm.configure_measurement_digits = 7.5
dmm.powerline_freq = 60
dmm.aperture_time_units = nidmm.ApertureTimeUnits.POWER_LINE_CYCLES
dmm.aperture_time = 10.0
dmm.settle_time = 5.0
'''
for r in range(len(rng)):
    dmm.range = rng[r]
    for x in range(len(point[r])):
        cal.write(ascii.substitute(set=point[r][x]))
        reading = dmm.read()
        print('Range: {0:>3f} Setpoint: {1:>10f} Reading: {2:>10f}'.format(rng[r],point[r][x], reading))
        meas.append(reading)
 '''       


for r in range(len(rng)):
    dmm.range = rng[r]
    for x in range(len(ac[r])):
        if (r == 2 and x == 0):
            freq = 5000
        else :
            freq = 1000
        cal.write(ascii.substitute(set=ac[r][x],hz=freq))
        reading = dmm.read()
        print('Range: {0:>3f} Setpoint: {1:>10f} Reading: {2:>10f}'.format(rng[r],ac[r][x], reading))
        meas.append(reading)
cal.write("STBY")
