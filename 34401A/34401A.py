# -*- coding: utf-8 -*-
"""



d.query("conf?")
d.query("zero:auto?")
d.query("volt:ac:res?")


d = rm.open_resource('visa://172.27.0.11/GPIB0::6::INSTR')
d.query('system:error?')
d.write('volt:ac:res max')

"""

import visa
import configparser
import time
from string import Template
from datetime import datetime

import menu

import sys
import os

startTime = time.time()


rm = visa.ResourceManager()

'''

'''



readDict = {}
readings = []

def getConfig(device):
    conf = configparser.ConfigParser(delimiters='=', comment_prefixes="#")
    conf.read(device)
    return conf

conf = getConfig('34401.ini')

def deviceList(rm=visa.ResourceManager(),intf="GPIB"):
    deviceList = []
    try:
        query = intf + "?*::INSTR"
        print("Now querying: ", query)
        devices = rm.list_resources(query)
        for n, d in enumerate(devices):
            try:
                dev = rm.open_resource(d)
                deviceList.append((d, dev.query('*IDN?').strip('\n').split(',')))
                print(deviceList[n])
                dev.close()
            except visa.VisaIOError as err:
                print("Resource {0} failed with Visa IO error: {1}".format(d,err))
                pass
    except visa.Error as err:
        print("Device Listing failed with Visa error: {1}".format(err))
    except:
        print("Unexplicable Error")
    finally:
        return deviceList
    
    """
    def zeroAmpsDC():
        # d.query("conf?")
        # Out[60]: '"CURR +1.000000E-01,+1.000000E-05"\n'
        r = [0.01,0.1,1.0,3.0]
        
    def zeroVoltsDC():
        r = [0.100]
        #for n in range(5):
    """

class UUT:
    """ A class wrapper for visa resource objects """
    
    sonumber = ""
    #inst = visa.Resource
    TO = 30000
    readings = {}
    
    
    def __init__(self,name):
        self.inst = visa.Resource
        self.inst = rm.open_resource(name)
        self.inst.timeout = self.TO
        ## Get rid of excess text.
        self.name = name.split('/')[-1]
        self.sonumber = name
        self.readings = {}
        
        self.setSO(self)
        
    def setSO(self,so=""):
        if (so == ""):
            self.prompt = "{}: Please enter a service order number: ".format(self.name)
            self.sonumber = input(self.prompt)
        else:
            self.sonumber = so
    
    def getSO(self):
        if (self.sonumber == ""):
            self.setSO()
        print("{}: SO Number: {}".format(self.name,self.sonumber))
    
    def query(self,string):
        #self.inst.write(string)
        read = self.inst.query(string).strip('\n')
        #print(read)
        return read
    
    def write(self,string):
        self.inst.write(string)
        
    def read(self):
        r = self.inst.read()
        print("{}: {}".format(self.name,r))
        return(r)
    
    def measure(self,func,command):
        value = self.query(command)
        print("{}: {}".format(self.name,float(value)))
        if func not in self.readings.keys():
            self.readings[func] = []
        self.readings[func].append(value)
    
    def record(self,func):
        writeValues(self.name,self.readings[func])
        
    def sonumber(self):
        return str(self.sonumber)
    
    
    
def findDevice(dL, dev):
    matches = []
    for n, d in enumerate(dL):
        dID = d[1]
        if (dID[1].find(dev) != -1):
            print("Found {0} {1} (sn: {2}) at resource {3}".format(dID[0],dID[1],dID[2],d[0]))
            matches.append([d[0], dID[0] + " " + dID[1]])
    count = len(matches)
    if (count == 1):
        print("Using {0} at {1}".format(dev,matches[0][0]))
        return matches[0][0]
    if (count > 1):
        print("Found {0} {1} devices..".format(len(matches),dev))
        return matches
    else:
        return None


conf = configparser.ConfigParser(delimiters='=', comment_prefixes="#")
conf.read('34401A.ini')
conf.sections()
uut = []
matches = findDevice(deviceList(),'34401')
for m in matches:    uut.append(UUT(m[0]))
#objZero(uut,conf['DC Current Zero'])


#d = rm.open_resource('visa://172.27.0.11/GPIB0::6::INSTR')
c = rm.open_resource('visa://172.27.0.11/GPIB0::15::INSTR')
#d.timeout = 30000
c.timeout = 10000
        

def terminalCheck(terminal="FRONT"):
    while (True):
        prompt = ("Connect the UUT " + terminal + "terminals, and verify selector switch. [Enter] ")
        if (input(prompt) == "q"):
            return False;
        t = d.query("ROUTe:TERMinals?").strip('\n')
        if (t in terminal):
            return True;
'''
def uutConf(confList):
    print("UUT Configuration")
    for cmd in confList:
        d.write(cmd)
        print("Command: ",cmd)
        time.sleep(1)
    dconf = d.query("conf?")
    print(dconf)
'''
def calConf(confList):
    print("Calibrator Configuration")
    for cmd in confList:
        c.write(cmd)
        print("Command: ",cmd)
        time.sleep(1)
    #cconf = c.query("conf?")
    #print(cconf)


def uutFunc(section):
    funcTime = time.time()
    s = section
    unit = s['unit']
    
    read = []
    read.append(str(s))

    uutConf(s['dmmconf'].split(','))
    
    
    dmmOut = Template("SENS:$FUNC:RANGE $RANGE")
    calOut = Template("OUT $SETPOINT $UNIT;*WAI;OPER")
    
    
    rng = getDict(s['range'])
    

    for u in uut:
        calConf(s['calconf'].split(';'))
        for x in range(3):u.write("syst:beep")
        u.write("DISP:TEXT 'READY'")
        input(s['desc'])
        u.write("DISP:TEXT:CLE")
        for n,x in enumerate(rng.keys()):
            
            ## Set UUT to the correct function and range
            u.write(dmmOut.substitute(FUNC=s['func'], RANGE=x))
    
            print('({0}/{1}): Verifying {2} {3} range'.format(n,len(rng),float(x),unit))
            for sp in rng[x]:
                
                ## 5700 Four-wire range
                if (s['func'] == "FRES" and sp == "1.0E+08"):
                    for x in range(3):uut[0].write("syst:beep")
                    input( "External sense does not work on this range. Please connect both channels to the Output terminals and press enter to continue." )
                    c.write("EXTSENSE OFF")
                    
                ## 5700 Four-wire range
                if (s['func'] == "RES" and sp == "1.0E+05"):
                    for x in range(3):uut[0].write("syst:beep")
                    input( "2-Wire compensation does not work on this range. Please connect both channels to the Output terminals and press enter to continue." )
                    c.write("RCOMP OFF")
                
                ## Initialize the calibrator
                c.write(calOut.substitute(SETPOINT=sp, UNIT=unit))
                
                calOutput = c.query("OUT?").strip("\n").split(",")[0]
                
                if s['unit'] == 'V' and float(sp) > 50 or float(sp) < -50:
                    #statusByte = int(calibrator.query("*WAI;ISR?",delay=2).strip("\n"))
                    #if (statusByte != 6145) :
                    for x in range(3):uut[0].write("syst:beep")
                    input( "Verify that the calibrator is Operating and press enter" )
                    time.sleep(0)
                else:
                    time.sleep(3)
    
                print('Setpoint: {0:>10}'.format(float(sp)),end=" ")
                measStart = datetime.now()
                u.measure(s['name'],"READ?")
                m = u.query("READ?").strip("\n")
                measStop = datetime.now()
                measTime = measStop - measStart
                print('Reading: {0:>10} Time: {1:>10} uS'.format(float(m),round(measTime.microseconds, 4)))
                
                read.append(m)
        u.record(s['name'])
        c.write("STBY")
    print( "\nFunction Time: ", time.time() - funcTime, "seconds.\n")
    return read





def uutFuncAC(section):
    """
    Execution block for AC functions, or any 
    """
    try:
        funcTime = time.time()
        s = section
        unit = s['unit']
    
        read = []
        read.append(str(s))

        uutConf(s['dmmconf'].split(','))
        
    
        dmmOut = Template("SENS:$FUNC:RANGE $RANGE")
        calOut = Template("OUT $SETPOINT $UNIT, $FREQ HZ;*WAI;OPER")        

        rDict = getIndexedDict(s['range'])
        fDict = getIndexedDict(s['freq'])
        
        for u in uut:
            calConf(s['calconf'].split(';'))
            for x in range(3):u.write("syst:beep")
            u.write("DISP:TEXT 'READY'")
            input(s['desc'])
            u.write("DISP:TEXT:CLE")
    
            ## Iterate through the dictionary of dictionaries
            for dIndex, r in enumerate(rDict.keys()):
                
                # Each value is a dictionary with the setpoint:range key:values
                spDict = rDict[dIndex]
                
                funcRange = list(rDict[dIndex].keys())[0]
                
                ## Set UUT range
                u.write(dmmOut.substitute(FUNC=s['func'], RANGE=funcRange))
                
                #print('({0}/{1}): Verifying {2} range'.format(r,len(rDict),funcRange))
                printValue(r,len(rDict),funcRange)
                
                ## For each key (even though there is only one)
                for i, sp in enumerate(spDict[funcRange]):
                    
                    fq = fDict[dIndex][funcRange][i]
    
                    #print('Setpoint: {0:<10} @ {1:<10}'.format(str(sp) + " " + unit,str(fq) + " Hz"),end=" ")
                    printValue("s",sp,unit,fq)              
                    u.write("INIT")
                    c.write(calOut.substitute(SETPOINT=sp, UNIT=unit, FREQ=fq))
                    
                    if float(sp) > 50.0 :
                        input( "Veri/fy that the calibrator is Operating and press enter" )
                        time.sleep(7)
                    else:
                        time.sleep(7)
    
                    
                    
                    measStart = datetime.now()
                    u.write("*TRG")
                    u.measure(s['name'],"FETCH?")
                    #m = d.query("FETCH?").strip("\n")
                    measStop = datetime.now()
                    
                    measTime = measStop - measStart
                    #print('Reading: {0:<10} Time: {1:>10} uS'.format(str(float(m)) + " " + unit,round(measTime.microseconds, 4)))
                    #printValue("m",m,unit)
                    printValue("t",measTime.microseconds,'uS')
                    #read.append(m)
            u.record(s['name'])

    except: 
        print("An error oroccured:",sys.exc_info()[0])
        read = "Error"
    finally:
        c.write("STBY")
        print( "\nFunction Time: ", time.time() - funcTime, "seconds.\n")
        return read


def uutFuncZero(section):
    funcTime = time.time()
    s = section
    unit = s['unit']

    read = []
    read.append(str(s))
    
    uutConf(s['dmmconf'].split(','))
    
    input(s['desc'])

    rng = getDict(s['range'])
    for n,x in enumerate(rng.keys(),1):
        
        #print('({0}/{1}): Verifying {2} range'.format(n+1,len(rng),float(x)))
        printValue('r',x,unit,index=n,total=len(rng))
        for y in rng[x]:

            dmmOut = Template("SENS:$FUNC:RANGE $RANGE")
            d.write(dmmOut.substitute(FUNC=s['func'], RANGE=y))
            time.sleep(3)
            
            print('Setpoint: {0:>10} {1}'.format(float(y),unit),end=" ")
            measStart = datetime.now()
            u.measure(s['name'],"READ?")
            #m = d.query("READ?",3).strip("\n")
            measStop = datetime.now()
            measTime = measStop - measStart
            #printValue('m',m,unit)
            printValue('t',measTime.microseconds, 'uS')
            #print('Reading: {0:>10} {1} Time: {2:>10} uS'.format(float(m),unit,round(measTime.microseconds, 4)))
        #read.append(m)
    print( "\nFunction Time: ", time.time() - funcTime, "seconds.\n")
    return read


def uutConf(confList):
    print("UUT Configuration")
    for u in uut:
        for cmd in confList:
            u.write(cmd)
            print(u.name,": Command: ",cmd)
            time.sleep(0.5)
        dconf = u.query("conf?")
        print(dconf)

def objZero(section):
    '''
    objZero(uut,conf['DC Current Zero'])
    
    
    '''
    funcTime = time.time()
    s = section
    unit = s['unit']

    dmmOut = Template("SENS:$FUNC:RANGE $RANGE")
    
    read = []
    read.append(str(s))
    
    uutConf(s['dmmconf'].split(','))
    
    #input(s['desc'])

    rng = getDict(s['range'])
    for u in uut:
        for x in range(3):u.write("syst:beep")
        u.write("DISP:TEXT 'READY'")
        input(s['desc'])
        u.write("DISP:TEXT:CLE")
        for n,x in enumerate(rng.keys(),1):
        
            #print('({0}/{1}): Verifying {2} range'.format(n+1,len(rng),float(x)))
            printValue('r',float(x),unit,index=n,total=len(rng))
    
            for y in rng[x]:
            
                u.write(dmmOut.substitute(FUNC=s['func'], RANGE=y))
                time.sleep(3)
            
                printValue("s",y,unit)
                #print('Setpoint: {0:>10} {1}'.format(float(y),unit),end=" ")
                measStart = datetime.now()
                u.measure(s['name'],"READ?")
                measStop = datetime.now()
                measTime = measStop - measStart
                #printValue('m',m,unit)
                printValue('t',measTime.microseconds, 'uS')
            #print('Reading: {0:>10} {1} Time: {2:>10} uS'.format(float(m),unit,round(measTime.microseconds, 4)))
                #uut.read.append(m)
    for u in uut: u.record(s['name'])
    print( "\nFunction Time: ", time.time() - funcTime, "seconds.\n")
    return read

def verify():
    verifyTime = time.time()

    for n,s in enumerate(conf.sections()):
        while(True):
            print('({0}/{1}) - Function: {2}'.format(n+1,len(conf.sections()),s))
            #print(conf['desc'])
            prompt = input("Press [Enter] to proceed.. ([S]kip, [A]bort, [Q]uit) ")
            
            if prompt == "s":
                print("Skipping Function")
                continue
            elif prompt == "q":
                print("Quitting")
                break
            elif prompt == "a":
                print ("Aborting Verification")
                break
            
            

            
            if (s.startswith("AC")):
                funcReadings = uutFuncAC(conf[s])
            elif (s.endswith("Zero")):
                funcReadings = objZero(conf[s])
            else:
                funcReadings = uutFunc(conf[s])
                
            if (input("Press 'r' to retry, or any other key to continue") != "r"):
                readings.append(funcReadings)
                readDict[str(s)] = funcReadings
                break

    print( "\nVerification Execution Time: ", time.time() - verifyTime, "seconds.\n")
    fileOutput()

def listSetpoints(section):
    
    rng = section['range']
    frq = section['freq']
    rng = getIndexedDict(conf['DC Volts Gain']['range'])
    conf.has_option('DC Volts Gain','freq')
    
    for i, k in enumerate(rng.keys()):
        for k in rng[i]:
            rr = k
            for n,v in enumerate(rng[i][k]):
                ss = rng[i][k][n]
                ff = [frq[i][k][n]]
                print(rr,ss,ff)

def verifyMenu():

    sections = conf.sections()
    
    while(True):
        s = menu.menu(sections,title="UUT Functions")
        
        if (s.startswith("AC")):
            funcReadings = uutFuncAC(conf[s])
        elif (s.endswith("Zero")):
            funcReadings = uutFuncZero(conf[s])
        else:
            funcReadings = uutFunc(conf[s])
    
        
        prompt = input("Press [Enter] to save readings and proceed or [Q]uit) ")
        
        if prompt == "q":
            print("Quitting")
            readings.append(funcReadings)
            break
        else:
            readings.append(funcReadings)


def getDict(section):
    """
    Return a dictionary consisting of the function range value and a list of
    setpoints as key->value respectively.
    """
    setPoints = {}
    funcRange = section.split(' ')
    for x in funcRange:
        r = x.split(':')
        try:
            r[1]
            setPoints[r[0]] = [float(s) for s in r[1].split(',')]
        except IndexError:
            setPoints[r[0]] = [r[0]]
    return setPoints


def printValue(desc, value, unit, freq=None, index=None, total=None, width=20):
    
    descDict = {'r':'Range',
          's':'\nSetpoint',
          'm':'Reading',
          't':'Time',
          'e':'Tolerance'}
    
    desc = descDict[desc]
    
    sp = "{}: {:>10f} {}".format(desc, float(value), unit)
    
    if freq != None:
        sp = "{} @ {:,} Hz".format(sp,freq)
        
    if index != None or total != None:
        if index != None:
            count = '({})'.format(index)
        if total != None:
            count = '({}/{}) '.format(index,total)
        if desc == 'Range':
            sp = "\n{}: Verifying {} {} {}\n".format(count, value, unit, desc)

    print('{:{width}}'.format(sp,width=width),end=" ")
  
def getIndexedDict(section):
    """
    Return a dictionary consisting of the function range value and a list of
    setpoints as key->value respectively.
    """
    indexed = {}
    setPoints = {}
    funcRange = section.split(' ')
    for n, x in enumerate(funcRange):
        setPoints = {}
        r = x.split(':')
        try:
            r[1]
            setPoints[r[0]] = [float(s) for s in r[1].split(',')]
        except IndexError:
            setPoints[r[0]] = [r[0]]
        indexed[n] = setPoints
    return indexed


def fileOutput(readings=readings, sonumber=""):
    
    #sonumber = input("Please enter a service order number")
    if (sonumber == ""):
        sonumber = input("Please enter a service order number: ")
        
    #fileName = '{0} - {1} - 34401a.csv'.format(sonumber,time.strftime("%Y-%m-%d"))
    fileName = sonumber + ".csv"
    with open(fileName, 'a') as f:
        print("Opening ",fileName," for writing... ")
        f.write('{0} - {1} - 34401a.csv'.format(sonumber,time.strftime("%Y-%m-%d")))
        for index, function in enumerate(readings):
            print("Writing ", function[0], " values...")
            for r in function:
                f.write(str(r) + "\n")

    if (f.closed):
        print("Closed output file ",fileName)
        
        
def fOut(readings, fileName):

    with open(fileName, 'a') as f:
        print("Opening ",fileName," for writing... ")

        for index, function in enumerate(readings.keys()):
            print("Writing ", function, " values...")
            f.write(str(function) + '\n')
            for r in readings[function]:
                f.write(str(r) + "\n")

    if (f.closed):
        print("Closed output file ",fileName)


def writeValues(sonumber,values):
    fileName = sonumber + ".csv"
    with open(fileName, 'a') as f:
        for i,v in enumerate(values):
            f.write(str(v) + '\n')

def printTable(uut, desc, value, unit, freq=None, index=None, total=None, width=20):
    print('{15}{15}{15}{15}{15}'.format(uut.sonumber,setpoint,unit,reading))