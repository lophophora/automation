#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Cal-o-matic

Created on Thu May  3 15:13:41 2018

@author: slushy
"""

## Import external modules

## Parse command line arguments

## Welcome screen

## Load previous options if exist

## Get UUT (Unit under test) make and model (can be provided as an argument)

## Check for previous calibration records (write all values to temp file)

## Establish communication with and pairings of devices

## Read configuration file for device

## Parse all test points into dictionaries

## Execute verification procedures

## Validate data with user (save/discard)