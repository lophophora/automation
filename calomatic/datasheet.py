#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 10 20:08:54 2018

@author: slushy
"""

import xml.etree.ElementTree as ET

def xmlRead(fileName):
    """
    Parse the specified fileName as an XML datasheet exported from calTrak.
    Returned is a dictionary of dictionaries indexed by function name, with a 
    dictionary for each range, containing a list of dictionaries for each setpoint,
    with key->values for setpoint, unit, frequency, min, max, etc.. where
    applicable.
    """    
    #tree = ET.parse('Fluke2620A.xml')
    tree = ET.parse(fileName)
    root = tree.getroot()
    func = ""
    rng = ""
    d = {}
    ## Iterator object for each <Section> tag containing a <Field> tag.
    ## The datasheets tend to have many unused sections.
    ifind = root.iterfind(".//Section/[Field]")
    
    for i in ifind:
        
        ## Check if section is a Function name heading
        if (i.find("./Field/[@Name='FunctionName1']") != None):
            f = i.find("./Field/[@Name='FunctionName1']/FormattedValue")
            if f != None:
                func = f.text
                print("Function: ",func)
                d[func]={}
                continue
        
        # Check if the section is the corresponding frequency section for setpoint1
        # If so, append it to the setpoint dictionary
        if (i.find("./Field/[@Name='Setpoint2']") != None):
            fq = i.find("./Field/[@Name='Setpoint2']/FormattedValue")
            fu = i.find("./Field/[@Name='SetpointUnits2']/FormattedValue")
            if fq != None: freq = fq.text
            if fu != None: funit = fu.text
            ## Add values to current dictionary key
            d[func][rng][-1]['Frequency'] = freq
            d[func][rng][-1]['Frequency Unit'] = funit
            continue
        
        
        if (i.find("./Field/[@Name='Setpoint1']") != None):
            r = i.find("./Field/[@Name='ReadingName1']/FormattedValue")
            s = i.find("./Field/[@Name='Setpoint1']/FormattedValue")
            su = i.find("./Field/[@Name='SetpointUnits1']/FormattedValue")
            mn = i.find("./Field/[@Name='Min1']/FormattedValue")
            mx = i.find("./Field/[@Name='Max1']/FormattedValue")
            ru = i.find("./Field/[@Name='ReadingUnits1']/FormattedValue")
            
            ## Establish if the range has changed.  Instances in which the range
            ## has already been specified, the setpoint dictionary will be added
            ## to the same range list.  Otherwise a new range list will be
            ## instantiated with the ReadingName as the key.
            if r != None: 
                if r.text != None:
                    rng = r.text
                    d[func][rng] = []
                    
            if s != None: sp = s.text
            if su != None: sunit = su.text
            if mn != None: smn = mn.text
            if mx != None: smx = mx.text
            if ru != None: runit = ru.text
            spd = {'Setpoint':sp,'Unit':sunit,'Min':smn,'Max':smx, 'ReadingUnit':runit}
            d[func][rng].append(spd)
    
    return d

'''
tree = ET.parse('Fluke2620A.xml')
root = tree.getroot()

fieldList = ["FunctionName1","Setpoint1","SetpointUnits1","ReadingName1","SetpointAdj1","Accuracy1","Min1","Max1","ReadingUnits1"]

for i, tag in enumerate(root.iter('Section')):
    for f in tag.findall('Field'):
        func = f.find("[@Name='FunctionName1']")
        if func != None: 
            function = f.find('FormattedValue').text
            print("Function: ",function)
            break
        
        print("\t",f.attrib)
        
        
        
        sp = f.find("[@Name='Setpoint1']")
        if sp != None:
            print(sp.find('FormattedValue').text)
'''



            
'''
spd = {}
uutFunction = ""
uutRange = ""

for i,n in enumerate(root.iter('Field')):
    
    field = n.find("[@Name='FunctionName1']")
    if field != None:
        function = field.find('FormattedValue').text
        print("Function: ",function)
    
    for f in fieldList:
        value = ""
        field = n.find("[@Name='{}']".format(f))
        if field != None:
            value = field.find('FormattedValue').text
            if "Function" in f: 
                function = field.find('FormattedValue').text
                spd[function] = []
                print("Function: ",value)
                break
            
            spd[function].append(value)
'''

'''
fieldList = ["FunctionName","Setpoint","SetpointUnits","ReadingName","SetpointAdj","Accuracy","Min","Max","ReadingUnits"]

rng = ""
dd = {}
td = {}
for i, tag in enumerate(root.iter('Section')):
    for f in tag.findall('Field'):
        func = f.find("[@Name='FunctionName1']")
        if func != None: 
            function = f.find('FormattedValue').text
            print("Function: ",function)
            continue
            
        print("no break")
        td = {}
        for fl in fieldList:
            p = f.find("[@Name='{}1']".format(fl))
            if p != None:
                value = p.find('FormattedValue').text
                if fl in "ReadingName":
                    rng = value
                if value != None:
                    p
                    td[fl] = value
                    print(fl, value)
        #print("\nRange {}, Setpoint {}, Units {}, Min {}, Max {}".format(rng,td['Setpoint'],td['SetpointUnits'],td['Min'],td['Max']))
        #print(td)
'''        
        
'''
import visa
rm = visa.ResourceManager()
rm.list_resources()
uut = rm.open_resource("ASRL5::INSTR")
uut.query("*IDN?")
uut.read_termination
'''



