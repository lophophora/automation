#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 30 15:55:18 2018

@author: slushy
"""

import visa
import time


rm = visa.ResourceManager()




cal = rm.open_resource('visa://172.27.0.12/GPIB0::15::INSTR')
uut = rm.open_resource('visa://172.27.0.12/GPIB0::1::INSTR')

#import devices


readings = []
setpoints = {0.1:[0],1:[0],10:[0],100:[0],1000:[0]}

def dcVz(setpoints):
    print("{:<15} | {:<15} | {:<15}".format("Range","Setpoint","Reading"))
    for rng in setpoints.keys():
        uut.write("CONF:VOLT:DC {}".format(rng))
        time.sleep(4)
        readings.append(uut.query("READ?").strip("\n"))
        print("{:<15} | {:<15} | {:<15}".format(rng,0,readings[-1]))

def ohmz(function,setpoints):
    cal.write("OUT 0 OHM")
    if function in "FRES": cal.write("EXTSENSE ON")
    if function == "RES": cal.write("EXTSENSE OFF; RCOMP ON")
    slowprint(table(["Range","Setpoint","Reading"]))
    for rng in setpoints.keys():
        if function in "RES" and rng == 100000:
            input( "2-Wire compensation does not work on this range. Please connect both channels to the Output terminals and press enter to continue." )
            cal.write("RCOMP OFF")
        uut.write("CONF:{} {}".format(function,rng))
        cal.write("OUT {} OHM; OPER".format(rng))
        output = cal.query("OUT?").split(",")[0]
        readings.append(measure(uut))
        result = table([rng,output,float(readings[-1])])
        slowprint(result)
    
def measure(uut):
    time.sleep(5)
    uut.write("INIT")
    uut.write("FETC?")
    meas = uut.read().strip("\n")
    return meas

def printTable(table):
    print("")
    for x in table:
        print("{:<15} | ".format(x),end="")


def table(column):
    """
    Returns a neatly formatted table row
    """
    row = ""
    for x in column:
        row += "{:<15} | ".format(x)
    return row
    
def slowprint(string,delay=0.1):
    """
    Slowly print a string, character by character to the screen.
    This serves little purpose other than to annoy.  Delay can be
    adjusted by providing the delay argument.
    """
    try:
        for c in string:
            print(c,end="")
            time.sleep(delay)
    except:
        print(string)
    finally:
        print("")



def testDC(setpoints):
    printTable(["Range","Setpoint","Reading"])
    for rng in setpoints.keys():
        uut.write("CONF:VOLT:DC {}".format(rng))
        time.sleep(4)
        readings.append(measure(uut))
        printTable([rng,0,readings[-1]])



def ohms():
    resistance = {'FRES':{100:[0],1000:[0],10000:[0],100000:[0]},
                 'RES':{100:[0],1000:[0],10000:[0],100000:[0],1000000:[0],10000000:[0],100000000:[0]}}
    
    for uutFunc in resistance.keys():
        setpoints = resistance[uutFunc]
        ohmz(uutFunc,setpoints)


def dcA():
    ## Zero - DC Amps
    print( "Zero - DC Amps" )
        
    setpoints = [0.0001,0.001,0.01,0.1,0.4,1.0,3.0,10]
    
    print("\n{:<15} | {:<15} | {:<15}".format("Range","Setpoint","Reading"))
    for rng in setpoints:
        uut.write("CONF:CURR:DC {}".format(rng))
        time.sleep(4)
        readings.append(uut.query("READ?").strip("\n"))
        print("{:<15} | {:<15} | {:<15}".format(rng,0,readings[-1]))
        
        
        
zeroFunc = ["VOLT:DC","CURR:DC"]

def dcV():
    ## DC Volts Verification
    print("DC Volts Verification")
        
    setpoints = {"0.1":["0.01","-0.01"],
              "1":["1.0","-1.0"],
              "10":["5.0","-5.0","10.0","-10.0"],
              "100":["100.0","-100.0"],
              "1000":["1000.0","-1000.0"]}
    

    
    for rng in setpoints.keys():
        uut.write("CONF:VOLT:DC {}".format(rng))
        printTable(["Range","Setpoint","Reading"])
        for setpoint in setpoints[rng]:
            cal.write("OUT {} V, 0 HZ;*WAI;OPER".format(setpoint))
            if rng in [ "100","1000" ] :
                input( "Verify that the calibrator is operating and press enter." )
            time.sleep(4)
            readings.append(measure(uut))
            printTable([rng,setpoint,readings[-1]])
            
            
def acV():
        ## AC Volts Verification
    print("AC Volts Verification")
    
    acVolt = ["0.1", "1", "10", "100", "750", "320"]        
    acFreq = {"0.1":["10 HZ", "20 KHZ","50 KHZ","100 KHZ","300 KHZ"],
              "1":["10 HZ", "20 KHZ","50 KHZ","100 KHZ","300 KHZ"],
              "10":["10 HZ", "20 KHZ","50 KHZ","100 KHZ","300 KHZ"],
              "100":["45 HZ", "20 KHZ","50 KHZ","100 KHZ"],
              "750":["45 HZ", "1 KHZ","10 KHZ"],
              "320":["20 KHZ","50 KHZ","100 KHZ"]}
    
    
    setpoint = list(acFreq.keys())
    
    for x in acVolt:
        if x != "320":
            uut.write("CONF:VOLT:AC "+x)
        uut.write("SENS:VOLT:AC:BAND 20")
        freq = acFreq[x]
        for y in freq:
            cal.write("OUT "+x+" V, "+y+";*WAI;OPER")
            if x == "10" and y == "300 KHZ":
                cal.write("OUT 3 V, "+y+";*WAI;OPER")
            
            if x in [ "100","1000","320" ] :
                input( "Verify that the calibrator is operating and press enter." )
            else:
                time.sleep(5)
            time.sleep(2)
            readings.append(measure(uut))
            printTable([x,x,y,readings[-1]])
            
            
            
def Adc():
    ## DC Amps Verification
    print("DC Amps Verification")
    
    input( "Connect the UUT 100mA and LO terminals to the 5700A HI and LO terminals.\nPress enter to continue." )
    
    ranges = ["0.0001","0.001","0.01","0.1","1.0","3.0","10"]
    setpoints = {"0.0001":["0.0001","-0.0001"],
              "0.001":["0.001","-0.001"],
              "0.01":["0.01","-0.01"],
              "0.1":["0.1","-0.1"],
              "0.4":["0.4","-0.4"],
              "1.0":["1.0","-1.0"],
              "3.0":["1.9","-1.9"],
              "10":["10","-10"]}
    
    for x in ranges:
        uut.write("CONF:CURR:DC "+x)
        point = setpoints[x]
        if x in ["1.0","3.0","10"]:
            input("Connect calibrator to 10A connection on UUT")
        for y in point:
            if x == "1.0":
                input( "Connect the UUT 10A and LO terminals to the 5700A HI and LO terminals.\nPress enter to continue." )
            cal.write("OUT "+y+" A, 0 HZ;*WAI;OPER")
            time.sleep(4)
            if x == "10":
                cal.write("STBY")
                input( "Verify that the DMM is connected to the correct terminals and in operation, then press enter." )
            readings.append(measure(uut))
            printTable([x,x,y,readings[-1]])
    
    cal.write("STBY")
    
'''
AC Volts
"10 Hz","20 kHz","50 kHz","100 kHz","300 kHz"
"45 Hz",
"45 Hz","1 kHz"
'''


def Aac():
    """
    AC Amp verification function
    """
    ## AC Amps Verification
    setpoints = {"0.01":{"0.01":["1 KHZ"]},
              "0.1":{"0.1":["1 KHZ"]},
              "0.4":{"0.4":["1 KHZ"]},
              "1.0":{"1.0":["1 KHZ"]},
              "3.0":{"1.9":["1 KHZ"]},
              "10":{"10":["1 KHZ"]}}
    
    slowprint(table(["Range","Setpoint","Frequency","Reading"]))
    # Iterate through dictionary of setpoints
    for r in setpoints.keys():
        # Set range
        uut.write("CONF:CURR:AC {}".format(r))
        uut.write("SENS:CURR:AC:BAND 20")
        if r in ["1.0","3.0","10"]:
            input("Verify UUT 10A connection is connected to the calibrator and press Enter... ")
        # Iterate through dictionary of setpoints
        for s in setpoints[r].keys():
            
            #Iterate through list of 
            for f in setpoints[r][s]:
                
                cal.write("OUT {} A, {};*WAI;OPER".format(s,f))
                if s in ["1.0","3.0","10"]:
                    input("Verify that the calibrator is operational and pres Enter... ")
                readings.append(measure(uut))
                result = table([r,s,f,float(readings[-1])])
                slowprint(result)
        
    cal.write("STBY")

def verify():
    readings = []
    input("DC Volt Zero")
    dcVz({0.1:[0],1:[0],10:[0],100:[0],1000:[0]})
    input("Ohms Zero")
    ohms()
    input("DC Amps Zero")
    dcA()
    input("DC Volts")
    dcV()
    input("AC Volts")
    acV()
    input("DC Current")
    Adc()
    input("AC Current")
    Aac()
    input("Resistance")
    ohms()