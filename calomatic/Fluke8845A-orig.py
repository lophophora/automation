<?xml version="1.0" encoding="UTF-8" ?>
<CrystalReport xmlns="urn:crystal-reports:schemas:report-detail"  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:crystal-reports:schemas:report-detail http://www.businessobjects.com/products/xml/CR2008Schema.xsd">
<ReportHeader>
<Section SectionNumber="0">
</Section>
</ReportHeader>
<Group Level="1">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="FunctionName1" FieldName="{@FunctionName}"><FormattedValue>Function Check</FormattedValue><Value>Function Check</Value></Field>
</Section>
</GroupHeader>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>1 Hour Warm Up Time</FormattedValue><Value>1 Hour Warm Up Time</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>2.5</FormattedValue><Value>2.5</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>P</FormattedValue><Value>P</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>P</FormattedValue><Value>P</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>P/F</FormattedValue><Value>P/F</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="1">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="FunctionName1" FieldName="{@FunctionName}"><FormattedValue>Zero Offset Test Front Input: DCV</FormattedValue><Value>Zero Offset Test Front Input: DCV</Value></Field>
</Section>
</GroupHeader>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>0.0000</FormattedValue><Value>0.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>mV</FormattedValue><Value>mV</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>Zero Offset Front Input 100mV Range</FormattedValue><Value>Zero Offset Front Input 100mV Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.0037% Rdg + 0.0035 mV)</FormattedValue><Value>±( 0.0037% Rdg + 0.0035 mV)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-0.0035</FormattedValue><Value>-0.0035</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>0.0035</FormattedValue><Value>0.0035</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>mV</FormattedValue><Value>mV</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>0.000000</FormattedValue><Value>0.000000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>1V Range</FormattedValue><Value>1V Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.0025% Rdg + 0.000007 V)</FormattedValue><Value>±( 0.0025% Rdg + 0.000007 V)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-0.000007</FormattedValue><Value>-0.000007</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>0.000007</FormattedValue><Value>0.000007</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>0.00000</FormattedValue><Value>0.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>10V Range</FormattedValue><Value>10V Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.0024% Rdg + 0.00005 V)</FormattedValue><Value>±( 0.0024% Rdg + 0.00005 V)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-0.00005</FormattedValue><Value>-0.00005</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>0.00005</FormattedValue><Value>0.00005</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>0.0000</FormattedValue><Value>0.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>100V Range</FormattedValue><Value>100V Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.0038% Rdg + 0.0006 V)</FormattedValue><Value>±( 0.0038% Rdg + 0.0006 V)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-0.0006</FormattedValue><Value>-0.0006</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>0.0006</FormattedValue><Value>0.0006</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>0.000</FormattedValue><Value>0.000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>1000V Range</FormattedValue><Value>1000V Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.0045% Rdg + 0.01 V)</FormattedValue><Value>±( 0.0045% Rdg + 0.01 V)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-0.010</FormattedValue><Value>-0.010</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>0.010</FormattedValue><Value>0.010</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="1">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="FunctionName1" FieldName="{@FunctionName}"><FormattedValue>Zero Offset Test: 4-Wire</FormattedValue><Value>Zero Offset Test: 4-Wire</Value></Field>
</Section>
</GroupHeader>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>0.0000</FormattedValue><Value>0.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Ohm</FormattedValue><Value>Ohm</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>Zero Offset Test 100 Ohm Range</FormattedValue><Value>Zero Offset Test 100 Ohm Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.01% Rdg + 0.004 Ohm)</FormattedValue><Value>±( 0.01% Rdg + 0.004 Ohm)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-0.0040</FormattedValue><Value>-0.0040</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>0.0040</FormattedValue><Value>0.0040</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Ohm</FormattedValue><Value>Ohm</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>0.000000</FormattedValue><Value>0.000000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>1 kOhm Range</FormattedValue><Value>1 kOhm Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.01% Rdg + 0.00001 kOhm)</FormattedValue><Value>±( 0.01% Rdg + 0.00001 kOhm)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-0.000010</FormattedValue><Value>-0.000010</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>0.000010</FormattedValue><Value>0.000010</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>0.00000</FormattedValue><Value>0.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>10 kOhm Range</FormattedValue><Value>10 kOhm Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.01% Rdg + 0.0001 kOhm)</FormattedValue><Value>±( 0.01% Rdg + 0.0001 kOhm)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-0.00010</FormattedValue><Value>-0.00010</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>0.00010</FormattedValue><Value>0.00010</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>0.0000</FormattedValue><Value>0.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>100 kOhm Range</FormattedValue><Value>100 kOhm Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.01% Rdg + 0.001 kOhm)</FormattedValue><Value>±( 0.01% Rdg + 0.001 kOhm)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-0.0010</FormattedValue><Value>-0.0010</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>0.0010</FormattedValue><Value>0.0010</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="1">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="FunctionName1" FieldName="{@FunctionName}"><FormattedValue>Zero Offset Test: 2-Wire</FormattedValue><Value>Zero Offset Test: 2-Wire</Value></Field>
</Section>
</GroupHeader>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>0.0000</FormattedValue><Value>0.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Ohm</FormattedValue><Value>Ohm</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>Zero Offset Test 100 Ohm Range</FormattedValue><Value>Zero Offset Test 100 Ohm Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.01% Rdg + 0.004 Ohm)</FormattedValue><Value>±( 0.01% Rdg + 0.004 Ohm)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-0.0040</FormattedValue><Value>-0.0040</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>0.0040</FormattedValue><Value>0.0040</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Ohm</FormattedValue><Value>Ohm</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>0.000000</FormattedValue><Value>0.000000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>1 kOhm Range</FormattedValue><Value>1 kOhm Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.01% Rdg + 0.00001 kOhm)</FormattedValue><Value>±( 0.01% Rdg + 0.00001 kOhm)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-0.000010</FormattedValue><Value>-0.000010</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>0.000010</FormattedValue><Value>0.000010</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>0.00000</FormattedValue><Value>0.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>10 kOhm Range</FormattedValue><Value>10 kOhm Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.01% Rdg + 0.0001 kOhm)</FormattedValue><Value>±( 0.01% Rdg + 0.0001 kOhm)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-0.00010</FormattedValue><Value>-0.00010</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>0.00010</FormattedValue><Value>0.00010</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>0.0000</FormattedValue><Value>0.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>100 kOhm Range</FormattedValue><Value>100 kOhm Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.01% Rdg + 0.001 kOhm)</FormattedValue><Value>±( 0.01% Rdg + 0.001 kOhm)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-0.0010</FormattedValue><Value>-0.0010</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>0.0010</FormattedValue><Value>0.0010</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>0.000000</FormattedValue><Value>0.000000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>MOhm</FormattedValue><Value>MOhm</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>1 MOhm Range</FormattedValue><Value>1 MOhm Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.01% Rdg + 0.00001 MOhm)</FormattedValue><Value>±( 0.01% Rdg + 0.00001 MOhm)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-0.000010</FormattedValue><Value>-0.000010</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>0.000010</FormattedValue><Value>0.000010</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>MOhm</FormattedValue><Value>MOhm</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>0.00000</FormattedValue><Value>0.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>MOhm</FormattedValue><Value>MOhm</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>10 MOhm Range</FormattedValue><Value>10 MOhm Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.04% Rdg + 0.0001 MOhm)</FormattedValue><Value>±( 0.04% Rdg + 0.0001 MOhm)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-0.00010</FormattedValue><Value>-0.00010</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>0.00010</FormattedValue><Value>0.00010</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>MOhm</FormattedValue><Value>MOhm</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>0.0000</FormattedValue><Value>0.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>MOhm</FormattedValue><Value>MOhm</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>100 MOhm Range</FormattedValue><Value>100 MOhm Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.8% Rdg + 0.01 MOhm)</FormattedValue><Value>±( 0.8% Rdg + 0.01 MOhm)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-0.0100</FormattedValue><Value>-0.0100</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>0.0100</FormattedValue><Value>0.0100</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>MOhm</FormattedValue><Value>MOhm</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="1">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="FunctionName1" FieldName="{@FunctionName}"><FormattedValue>DC Current Measure: DCA</FormattedValue><Value>DC Current Measure: DCA</Value></Field>
</Section>
</GroupHeader>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>0.0000</FormattedValue><Value>0.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>µA</FormattedValue><Value>µA</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>Zero Offset Test 100 µA Range</FormattedValue><Value>Zero Offset Test 100 µA Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.05% Rdg + 0.025 µA)</FormattedValue><Value>±( 0.05% Rdg + 0.025 µA)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-0.0250</FormattedValue><Value>-0.0250</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>0.0250</FormattedValue><Value>0.0250</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>µA</FormattedValue><Value>µA</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>0.000000</FormattedValue><Value>0.000000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>mA</FormattedValue><Value>mA</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>1 mA Range</FormattedValue><Value>1 mA Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.05% Rdg + 0.00005 mA)</FormattedValue><Value>±( 0.05% Rdg + 0.00005 mA)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-0.000050</FormattedValue><Value>-0.000050</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>0.000050</FormattedValue><Value>0.000050</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>mA</FormattedValue><Value>mA</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>0.00000</FormattedValue><Value>0.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>mA</FormattedValue><Value>mA</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>10 mA Range</FormattedValue><Value>10 mA Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.05% Rdg + 0.002 mA)</FormattedValue><Value>±( 0.05% Rdg + 0.002 mA)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-0.00200</FormattedValue><Value>-0.00200</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>0.00200</FormattedValue><Value>0.00200</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>mA</FormattedValue><Value>mA</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>0.0000</FormattedValue><Value>0.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>mA</FormattedValue><Value>mA</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>100 mA Range</FormattedValue><Value>100 mA Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25</FormattedValue><Value>25</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.05% Rdg + 0.005 mA)</FormattedValue><Value>±( 0.05% Rdg + 0.005 mA)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-0.0050</FormattedValue><Value>-0.0050</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>0.0050</FormattedValue><Value>0.0050</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>mA</FormattedValue><Value>mA</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>0.000000</FormattedValue><Value>0.000000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>A</FormattedValue><Value>A</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>1 A Range</FormattedValue><Value>1 A Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.05% Rdg + 0.0002 A)</FormattedValue><Value>±( 0.05% Rdg + 0.0002 A)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-0.000200</FormattedValue><Value>-0.000200</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>0.000200</FormattedValue><Value>0.000200</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>A</FormattedValue><Value>A</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>0.00000</FormattedValue><Value>0.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>A</FormattedValue><Value>A</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>3 A Range</FormattedValue><Value>3 A Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.1% Rdg + 0.0006 A)</FormattedValue><Value>±( 0.1% Rdg + 0.0006 A)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-0.00060</FormattedValue><Value>-0.00060</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>0.00060</FormattedValue><Value>0.00060</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>A</FormattedValue><Value>A</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>0.00000</FormattedValue><Value>0.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>A</FormattedValue><Value>A</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>10 A Range</FormattedValue><Value>10 A Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.15% Rdg + 0.0008 A)</FormattedValue><Value>±( 0.15% Rdg + 0.0008 A)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-0.00080</FormattedValue><Value>-0.00080</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>0.00080</FormattedValue><Value>0.00080</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>A</FormattedValue><Value>A</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="1">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="FunctionName1" FieldName="{@FunctionName}"><FormattedValue>Direct Volts Verification</FormattedValue><Value>Direct Volts Verification</Value></Field>
</Section>
</GroupHeader>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>100.0000</FormattedValue><Value>100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>mV</FormattedValue><Value>mV</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>100 mV Range</FormattedValue><Value>100 mV Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.005% Rdg + 0.0035 mV)</FormattedValue><Value>±( 0.005% Rdg + 0.0035 mV)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>99.9915</FormattedValue><Value>99.9915</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>100.0085</FormattedValue><Value>100.0085</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>mV</FormattedValue><Value>mV</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>-100.0000</FormattedValue><Value>-100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>mV</FormattedValue><Value>mV</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.005% Rdg + 0.0035 mV)</FormattedValue><Value>±( 0.005% Rdg + 0.0035 mV)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-100.0085</FormattedValue><Value>-100.0085</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>-99.9915</FormattedValue><Value>-99.9915</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>mV</FormattedValue><Value>mV</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>1.000000</FormattedValue><Value>1.000000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>1 V Range</FormattedValue><Value>1 V Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.004% Rdg + 0.000007 V)</FormattedValue><Value>±( 0.004% Rdg + 0.000007 V)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>0.999953</FormattedValue><Value>0.999953</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>1.000047</FormattedValue><Value>1.000047</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>-1.000000</FormattedValue><Value>-1.000000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.004% Rdg + 0.000007 V)</FormattedValue><Value>±( 0.004% Rdg + 0.000007 V)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-1.000047</FormattedValue><Value>-1.000047</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>-0.999953</FormattedValue><Value>-0.999953</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>5.00000</FormattedValue><Value>5.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>10 V Range</FormattedValue><Value>10 V Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.0035% Rdg + 0.00005 V)</FormattedValue><Value>±( 0.0035% Rdg + 0.00005 V)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>4.99977</FormattedValue><Value>4.99977</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>5.00023</FormattedValue><Value>5.00023</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>-5.00000</FormattedValue><Value>-5.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.0035% Rdg + 0.00005 V)</FormattedValue><Value>±( 0.0035% Rdg + 0.00005 V)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-5.00023</FormattedValue><Value>-5.00023</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>-4.99977</FormattedValue><Value>-4.99977</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>10.00000</FormattedValue><Value>10.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.0035% Rdg + 0.00005 V)</FormattedValue><Value>±( 0.0035% Rdg + 0.00005 V)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>9.99960</FormattedValue><Value>9.99960</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>10.00040</FormattedValue><Value>10.00040</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>-10.00000</FormattedValue><Value>-10.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.0035% Rdg + 0.00005 V)</FormattedValue><Value>±( 0.0035% Rdg + 0.00005 V)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-10.00040</FormattedValue><Value>-10.00040</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>-9.99960</FormattedValue><Value>-9.99960</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>100.0000</FormattedValue><Value>100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>100 V Range</FormattedValue><Value>100 V Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.0045% Rdg + 0.0006 V)</FormattedValue><Value>±( 0.0045% Rdg + 0.0006 V)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>99.9949</FormattedValue><Value>99.9949</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>100.0051</FormattedValue><Value>100.0051</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>-100.0000</FormattedValue><Value>-100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.0045% Rdg + 0.0006 V)</FormattedValue><Value>±( 0.0045% Rdg + 0.0006 V)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-100.0051</FormattedValue><Value>-100.0051</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>-99.9949</FormattedValue><Value>-99.9949</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>1000.000</FormattedValue><Value>1000.000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>1000 V Range</FormattedValue><Value>1000 V Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.0045% Rdg + 0.01 V)</FormattedValue><Value>±( 0.0045% Rdg + 0.01 V)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>999.945</FormattedValue><Value>999.945</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>1000.055</FormattedValue><Value>1000.055</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>-1000.000</FormattedValue><Value>-1000.000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.0045% Rdg + 0.01 V)</FormattedValue><Value>±( 0.0045% Rdg + 0.01 V)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-1000.055</FormattedValue><Value>-1000.055</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>-999.945</FormattedValue><Value>-999.945</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="1">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="FunctionName1" FieldName="{@FunctionName}"><FormattedValue>Alternating Volts Verification</FormattedValue><Value>Alternating Volts Verification</Value></Field>
</Section>
</GroupHeader>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>100.0000</FormattedValue><Value>100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>mVrms</FormattedValue><Value>mVrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>100 mV Range</FormattedValue><Value>100 mV Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.06% Rdg + 0.04 mVrms)</FormattedValue><Value>±( 0.06% Rdg + 0.04 mVrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>99.9000</FormattedValue><Value>99.9000</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>100.1000</FormattedValue><Value>100.1000</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>mVrms</FormattedValue><Value>mVrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>10</FormattedValue><Value>10</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>Hz</FormattedValue><Value>Hz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>100.0000</FormattedValue><Value>100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>mVrms</FormattedValue><Value>mVrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.06% Rdg + 0.04 mVrms)</FormattedValue><Value>±( 0.06% Rdg + 0.04 mVrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>99.9000</FormattedValue><Value>99.9000</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>100.1000</FormattedValue><Value>100.1000</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>mVrms</FormattedValue><Value>mVrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>20</FormattedValue><Value>20</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>100.0000</FormattedValue><Value>100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>mVrms</FormattedValue><Value>mVrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.12% Rdg + 0.05 mVrms)</FormattedValue><Value>±( 0.12% Rdg + 0.05 mVrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>99.8300</FormattedValue><Value>99.8300</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>100.1700</FormattedValue><Value>100.1700</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>mVrms</FormattedValue><Value>mVrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>50</FormattedValue><Value>50</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>100.0000</FormattedValue><Value>100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>mVrms</FormattedValue><Value>mVrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.6% Rdg + 0.08 mVrms)</FormattedValue><Value>±( 0.6% Rdg + 0.08 mVrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>99.3200</FormattedValue><Value>99.3200</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>100.6800</FormattedValue><Value>100.6800</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>mVrms</FormattedValue><Value>mVrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>100</FormattedValue><Value>100</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>100.0000</FormattedValue><Value>100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>mVrms</FormattedValue><Value>mVrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 4% Rdg + 0.5 mVrms)</FormattedValue><Value>±( 4% Rdg + 0.5 mVrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>95.5000</FormattedValue><Value>95.5000</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>104.5000</FormattedValue><Value>104.5000</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>mVrms</FormattedValue><Value>mVrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>300</FormattedValue><Value>300</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>1.000000</FormattedValue><Value>1.000000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>1 V Range</FormattedValue><Value>1 V Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.06% Rdg + 0.0003 Vrms)</FormattedValue><Value>±( 0.06% Rdg + 0.0003 Vrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>0.999100</FormattedValue><Value>0.999100</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>1.000900</FormattedValue><Value>1.000900</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>10</FormattedValue><Value>10</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>Hz</FormattedValue><Value>Hz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>1.000000</FormattedValue><Value>1.000000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.06% Rdg + 0.0003 Vrms)</FormattedValue><Value>±( 0.06% Rdg + 0.0003 Vrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>0.999100</FormattedValue><Value>0.999100</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>1.000900</FormattedValue><Value>1.000900</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>20</FormattedValue><Value>20</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>1.000000</FormattedValue><Value>1.000000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.12% Rdg + 0.0005 Vrms)</FormattedValue><Value>±( 0.12% Rdg + 0.0005 Vrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>0.998300</FormattedValue><Value>0.998300</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>1.001700</FormattedValue><Value>1.001700</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>50</FormattedValue><Value>50</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>1.000000</FormattedValue><Value>1.000000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.6% Rdg + 0.0008 Vrms)</FormattedValue><Value>±( 0.6% Rdg + 0.0008 Vrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>0.993200</FormattedValue><Value>0.993200</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>1.006800</FormattedValue><Value>1.006800</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>100</FormattedValue><Value>100</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>1.000000</FormattedValue><Value>1.000000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 4% Rdg + 0.005 Vrms)</FormattedValue><Value>±( 4% Rdg + 0.005 Vrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>0.955000</FormattedValue><Value>0.955000</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>1.045000</FormattedValue><Value>1.045000</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>300</FormattedValue><Value>300</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>10.00000</FormattedValue><Value>10.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>10 V Range</FormattedValue><Value>10 V Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.06% Rdg + 0.003 Vrms)</FormattedValue><Value>±( 0.06% Rdg + 0.003 Vrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>9.99100</FormattedValue><Value>9.99100</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>10.00900</FormattedValue><Value>10.00900</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>10</FormattedValue><Value>10</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>Hz</FormattedValue><Value>Hz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>10.00000</FormattedValue><Value>10.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.06% Rdg + 0.003 Vrms)</FormattedValue><Value>±( 0.06% Rdg + 0.003 Vrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>9.99100</FormattedValue><Value>9.99100</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>10.00900</FormattedValue><Value>10.00900</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>20</FormattedValue><Value>20</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>10.00000</FormattedValue><Value>10.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.12% Rdg + 0.005 Vrms)</FormattedValue><Value>±( 0.12% Rdg + 0.005 Vrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>9.98300</FormattedValue><Value>9.98300</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>10.01700</FormattedValue><Value>10.01700</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>50</FormattedValue><Value>50</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>10.00000</FormattedValue><Value>10.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.6% Rdg + 0.008 Vrms)</FormattedValue><Value>±( 0.6% Rdg + 0.008 Vrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>9.93200</FormattedValue><Value>9.93200</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>10.06800</FormattedValue><Value>10.06800</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>100</FormattedValue><Value>100</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>3.00000</FormattedValue><Value>3.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 4% Rdg + 0.05 Vrms)</FormattedValue><Value>±( 4% Rdg + 0.05 Vrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>2.83000</FormattedValue><Value>2.83000</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>3.17000</FormattedValue><Value>3.17000</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>300</FormattedValue><Value>300</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>100.0000</FormattedValue><Value>100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>100 V Range</FormattedValue><Value>100 V Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.09 Vrms)</FormattedValue><Value>±( 0.09 Vrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>99.9100</FormattedValue><Value>99.9100</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>100.0900</FormattedValue><Value>100.0900</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>45</FormattedValue><Value>45</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>Hz</FormattedValue><Value>Hz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>100.0000</FormattedValue><Value>100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.09 Vrms)</FormattedValue><Value>±( 0.09 Vrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>99.9100</FormattedValue><Value>99.9100</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>100.0900</FormattedValue><Value>100.0900</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>20</FormattedValue><Value>20</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>100.0000</FormattedValue><Value>100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.17 Vrms)</FormattedValue><Value>±( 0.17 Vrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>99.8300</FormattedValue><Value>99.8300</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>100.1700</FormattedValue><Value>100.1700</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>50</FormattedValue><Value>50</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>100.0000</FormattedValue><Value>100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 4.5 Vrms)</FormattedValue><Value>±( 4.5 Vrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>95.5000</FormattedValue><Value>95.5000</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>104.5000</FormattedValue><Value>104.5000</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>100</FormattedValue><Value>100</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>750.000</FormattedValue><Value>750.000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>750 V Range</FormattedValue><Value>750 V Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.06% Rdg + 0.225 Vrms)</FormattedValue><Value>±( 0.06% Rdg + 0.225 Vrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>749.325</FormattedValue><Value>749.325</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>750.675</FormattedValue><Value>750.675</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>45</FormattedValue><Value>45</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>Hz</FormattedValue><Value>Hz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>750.000</FormattedValue><Value>750.000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.06% Rdg + 0.225 Vrms)</FormattedValue><Value>±( 0.06% Rdg + 0.225 Vrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>749.325</FormattedValue><Value>749.325</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>750.675</FormattedValue><Value>750.675</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>1</FormattedValue><Value>1</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>750.000</FormattedValue><Value>750.000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.06% Rdg + 0.225 Vrms)</FormattedValue><Value>±( 0.06% Rdg + 0.225 Vrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>749.325</FormattedValue><Value>749.325</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>750.675</FormattedValue><Value>750.675</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>10</FormattedValue><Value>10</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>320.000</FormattedValue><Value>320.000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.06% Rdg + 0.225 Vrms)</FormattedValue><Value>±( 0.06% Rdg + 0.225 Vrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>319.583</FormattedValue><Value>319.583</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>320.417</FormattedValue><Value>320.417</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>20</FormattedValue><Value>20</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>320.000</FormattedValue><Value>320.000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.12% Rdg + 0.375 Vrms)</FormattedValue><Value>±( 0.12% Rdg + 0.375 Vrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>319.241</FormattedValue><Value>319.241</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>320.759</FormattedValue><Value>320.759</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>50</FormattedValue><Value>50</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>320.000</FormattedValue><Value>320.000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.6% Rdg + 0.6 Vrms)</FormattedValue><Value>±( 0.6% Rdg + 0.6 Vrms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>317.480</FormattedValue><Value>317.480</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>322.520</FormattedValue><Value>322.520</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>100</FormattedValue><Value>100</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="1">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="FunctionName1" FieldName="{@FunctionName}"><FormattedValue>Direct Current Verification</FormattedValue><Value>Direct Current Verification</Value></Field>
</Section>
</GroupHeader>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>100.0000</FormattedValue><Value>100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>µA</FormattedValue><Value>µA</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>100 µA Range</FormattedValue><Value>100 µA Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.05% Rdg + 0.025 µA)</FormattedValue><Value>±( 0.05% Rdg + 0.025 µA)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>99.9250</FormattedValue><Value>99.9250</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>100.0750</FormattedValue><Value>100.0750</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>µA</FormattedValue><Value>µA</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>-100.0000</FormattedValue><Value>-100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>µA</FormattedValue><Value>µA</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.05% Rdg + 0.025 µA)</FormattedValue><Value>±( 0.05% Rdg + 0.025 µA)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-100.0750</FormattedValue><Value>-100.0750</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>-99.9250</FormattedValue><Value>-99.9250</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>µA</FormattedValue><Value>µA</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>1.000000</FormattedValue><Value>1.000000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>mA</FormattedValue><Value>mA</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>1 mA Range</FormattedValue><Value>1 mA Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.05% Rdg + 0.00005 mA)</FormattedValue><Value>±( 0.05% Rdg + 0.00005 mA)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>0.999450</FormattedValue><Value>0.999450</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>1.000550</FormattedValue><Value>1.000550</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>mA</FormattedValue><Value>mA</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>-1.000000</FormattedValue><Value>-1.000000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>mA</FormattedValue><Value>mA</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.05% Rdg + 0.00005 mA)</FormattedValue><Value>±( 0.05% Rdg + 0.00005 mA)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-1.000550</FormattedValue><Value>-1.000550</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>-0.999450</FormattedValue><Value>-0.999450</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>mA</FormattedValue><Value>mA</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>10.00000</FormattedValue><Value>10.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>mA</FormattedValue><Value>mA</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>10 mA Range</FormattedValue><Value>10 mA Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.05% Rdg + 0.002 mA)</FormattedValue><Value>±( 0.05% Rdg + 0.002 mA)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>9.99300</FormattedValue><Value>9.99300</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>10.00700</FormattedValue><Value>10.00700</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>mA</FormattedValue><Value>mA</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>-10.00000</FormattedValue><Value>-10.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>mA</FormattedValue><Value>mA</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.05% Rdg + 0.002 mA)</FormattedValue><Value>±( 0.05% Rdg + 0.002 mA)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-10.00700</FormattedValue><Value>-10.00700</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>-9.99300</FormattedValue><Value>-9.99300</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>mA</FormattedValue><Value>mA</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>100.0000</FormattedValue><Value>100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>mA</FormattedValue><Value>mA</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>100 mA Range</FormattedValue><Value>100 mA Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25</FormattedValue><Value>25</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.05% Rdg + 0.005 mA)</FormattedValue><Value>±( 0.05% Rdg + 0.005 mA)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>99.9450</FormattedValue><Value>99.9450</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>100.0550</FormattedValue><Value>100.0550</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>mA</FormattedValue><Value>mA</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>-100.0000</FormattedValue><Value>-100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>mA</FormattedValue><Value>mA</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25</FormattedValue><Value>25</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.05% Rdg + 0.005 mA)</FormattedValue><Value>±( 0.05% Rdg + 0.005 mA)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-100.0550</FormattedValue><Value>-100.0550</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>-99.9450</FormattedValue><Value>-99.9450</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>mA</FormattedValue><Value>mA</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>1.000000</FormattedValue><Value>1.000000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>A</FormattedValue><Value>A</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>1 A Range</FormattedValue><Value>1 A Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.05% Rdg + 0.0002 A)</FormattedValue><Value>±( 0.05% Rdg + 0.0002 A)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>0.999300</FormattedValue><Value>0.999300</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>1.000700</FormattedValue><Value>1.000700</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>A</FormattedValue><Value>A</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>-1.000000</FormattedValue><Value>-1.000000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>A</FormattedValue><Value>A</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.05% Rdg + 0.0002 A)</FormattedValue><Value>±( 0.05% Rdg + 0.0002 A)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-1.000700</FormattedValue><Value>-1.000700</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>-0.999300</FormattedValue><Value>-0.999300</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>A</FormattedValue><Value>A</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>1.90000</FormattedValue><Value>1.90000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>A</FormattedValue><Value>A</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>3 A Range</FormattedValue><Value>3 A Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.1% Rdg + 0.0006 A)</FormattedValue><Value>±( 0.1% Rdg + 0.0006 A)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>1.89750</FormattedValue><Value>1.89750</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>1.90250</FormattedValue><Value>1.90250</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>A</FormattedValue><Value>A</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>-1.90000</FormattedValue><Value>-1.90000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>A</FormattedValue><Value>A</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.1% Rdg + 0.0006 A)</FormattedValue><Value>±( 0.1% Rdg + 0.0006 A)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-1.90250</FormattedValue><Value>-1.90250</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>-1.89750</FormattedValue><Value>-1.89750</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>A</FormattedValue><Value>A</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>10.00000</FormattedValue><Value>10.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>A</FormattedValue><Value>A</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>10 A Range</FormattedValue><Value>10 A Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.15% Rdg + 0.0008 A)</FormattedValue><Value>±( 0.15% Rdg + 0.0008 A)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>9.98420</FormattedValue><Value>9.98420</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>10.01580</FormattedValue><Value>10.01580</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>A</FormattedValue><Value>A</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>-10.00000</FormattedValue><Value>-10.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>A</FormattedValue><Value>A</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.15% Rdg + 0.0008 A)</FormattedValue><Value>±( 0.15% Rdg + 0.0008 A)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>-10.01580</FormattedValue><Value>-10.01580</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>-9.98420</FormattedValue><Value>-9.98420</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>A</FormattedValue><Value>A</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="1">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="FunctionName1" FieldName="{@FunctionName}"><FormattedValue>Alternating Current Verification</FormattedValue><Value>Alternating Current Verification</Value></Field>
</Section>
</GroupHeader>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>10.00000</FormattedValue><Value>10.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>mArms</FormattedValue><Value>mArms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>10 mA Range</FormattedValue><Value>10 mA Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.15% Rdg + 0.006 mArms)</FormattedValue><Value>±( 0.15% Rdg + 0.006 mArms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>9.97900</FormattedValue><Value>9.97900</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>10.02100</FormattedValue><Value>10.02100</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>mArms</FormattedValue><Value>mArms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>1</FormattedValue><Value>1</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>100.0000</FormattedValue><Value>100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>mArms</FormattedValue><Value>mArms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>100 mA Range</FormattedValue><Value>100 mA Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25</FormattedValue><Value>25</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.1% Rdg + 0.04 mArms)</FormattedValue><Value>±( 0.1% Rdg + 0.04 mArms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>99.8600</FormattedValue><Value>99.8600</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>100.1400</FormattedValue><Value>100.1400</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>mArms</FormattedValue><Value>mArms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>1</FormattedValue><Value>1</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25</FormattedValue><Value>25</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>1.000000</FormattedValue><Value>1.000000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Arms</FormattedValue><Value>Arms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>1 A Range</FormattedValue><Value>1 A Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.1% Rdg + 0.0004 Arms)</FormattedValue><Value>±( 0.1% Rdg + 0.0004 Arms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>0.998600</FormattedValue><Value>0.998600</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>1.001400</FormattedValue><Value>1.001400</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Arms</FormattedValue><Value>Arms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>1</FormattedValue><Value>1</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>1.90000</FormattedValue><Value>1.90000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Arms</FormattedValue><Value>Arms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>3 A Range</FormattedValue><Value>3 A Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.1% Rdg + 0.0018 Arms)</FormattedValue><Value>±( 0.1% Rdg + 0.0018 Arms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>1.89630</FormattedValue><Value>1.89630</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>1.90370</FormattedValue><Value>1.90370</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Arms</FormattedValue><Value>Arms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>1</FormattedValue><Value>1</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>10.00000</FormattedValue><Value>10.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Arms</FormattedValue><Value>Arms</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>10 A Range</FormattedValue><Value>10 A Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.15% Rdg + 0.006 Arms)</FormattedValue><Value>±( 0.15% Rdg + 0.006 Arms)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>9.97900</FormattedValue><Value>9.97900</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>10.02100</FormattedValue><Value>10.02100</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Arms</FormattedValue><Value>Arms</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>1</FormattedValue><Value>1</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="1">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="FunctionName1" FieldName="{@FunctionName}"><FormattedValue>4-Wire Ohms Verification</FormattedValue><Value>4-Wire Ohms Verification</Value></Field>
</Section>
</GroupHeader>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>100.0000</FormattedValue><Value>100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Ohm</FormattedValue><Value>Ohm</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>100 Ohm Range</FormattedValue><Value>100 Ohm Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.01% Rdg + 0.004 Ohm)</FormattedValue><Value>±( 0.01% Rdg + 0.004 Ohm)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>99.9860</FormattedValue><Value>99.9860</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>100.0140</FormattedValue><Value>100.0140</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Ohm</FormattedValue><Value>Ohm</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>1.000000</FormattedValue><Value>1.000000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>1 kOhm Range</FormattedValue><Value>1 kOhm Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.01% Rdg + 0.00001 kOhm)</FormattedValue><Value>±( 0.01% Rdg + 0.00001 kOhm)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>0.999890</FormattedValue><Value>0.999890</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>1.000110</FormattedValue><Value>1.000110</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>10.00000</FormattedValue><Value>10.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>10 kOhm Range</FormattedValue><Value>10 kOhm Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.01% Rdg + 0.0001 kOhm)</FormattedValue><Value>±( 0.01% Rdg + 0.0001 kOhm)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>9.99890</FormattedValue><Value>9.99890</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>10.00110</FormattedValue><Value>10.00110</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>100.0000</FormattedValue><Value>100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>100 kOhm Range</FormattedValue><Value>100 kOhm Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.01% Rdg + 0.001 kOhm)</FormattedValue><Value>±( 0.01% Rdg + 0.001 kOhm)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>99.9890</FormattedValue><Value>99.9890</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>100.0110</FormattedValue><Value>100.0110</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="1">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="FunctionName1" FieldName="{@FunctionName}"><FormattedValue>2-Wire Ohms Verification</FormattedValue><Value>2-Wire Ohms Verification</Value></Field>
</Section>
</GroupHeader>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>100.0000</FormattedValue><Value>100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Ohm</FormattedValue><Value>Ohm</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>100 Ohm Range</FormattedValue><Value>100 Ohm Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25</FormattedValue><Value>25</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.01% Rdg + 0.004 Ohm)</FormattedValue><Value>±( 0.01% Rdg + 0.004 Ohm)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>99.9860</FormattedValue><Value>99.9860</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>100.0140</FormattedValue><Value>100.0140</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Ohm</FormattedValue><Value>Ohm</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>1.000000</FormattedValue><Value>1.000000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>1 kOhm Range</FormattedValue><Value>1 kOhm Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.01% Rdg + 0.00001 kOhm)</FormattedValue><Value>±( 0.01% Rdg + 0.00001 kOhm)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>0.999890</FormattedValue><Value>0.999890</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>1.000110</FormattedValue><Value>1.000110</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>10.00000</FormattedValue><Value>10.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>10 kOhm Range</FormattedValue><Value>10 kOhm Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.01% Rdg + 0.0001 kOhm)</FormattedValue><Value>±( 0.01% Rdg + 0.0001 kOhm)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>9.99890</FormattedValue><Value>9.99890</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>10.00110</FormattedValue><Value>10.00110</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>100.0000</FormattedValue><Value>100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>100 kOhm Range</FormattedValue><Value>100 kOhm Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.01% Rdg + 0.001 kOhm)</FormattedValue><Value>±( 0.01% Rdg + 0.001 kOhm)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>99.9890</FormattedValue><Value>99.9890</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>100.0110</FormattedValue><Value>100.0110</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>1.000000</FormattedValue><Value>1.000000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>MOhm</FormattedValue><Value>MOhm</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>1 MOhm Range</FormattedValue><Value>1 MOhm Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.01% Rdg + 0.00001 MOhm)</FormattedValue><Value>±( 0.01% Rdg + 0.00001 MOhm)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>0.999890</FormattedValue><Value>0.999890</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>1.000110</FormattedValue><Value>1.000110</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>MOhm</FormattedValue><Value>MOhm</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>10.00000</FormattedValue><Value>10.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>MOhm</FormattedValue><Value>MOhm</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>10 MOhm Range</FormattedValue><Value>10 MOhm Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.04% Rdg + 0.0001 MOhm)</FormattedValue><Value>±( 0.04% Rdg + 0.0001 MOhm)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>9.99590</FormattedValue><Value>9.99590</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>10.00410</FormattedValue><Value>10.00410</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>MOhm</FormattedValue><Value>MOhm</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>100.0000</FormattedValue><Value>100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>MOhm</FormattedValue><Value>MOhm</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>100 MOhm Range</FormattedValue><Value>100 MOhm Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.8% Rdg + 0.01 MOhm)</FormattedValue><Value>±( 0.8% Rdg + 0.01 MOhm)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>99.1900</FormattedValue><Value>99.1900</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>100.8100</FormattedValue><Value>100.8100</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>MOhm</FormattedValue><Value>MOhm</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="1">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="FunctionName1" FieldName="{@FunctionName}"><FormattedValue>Frequency Verification</FormattedValue><Value>Frequency Verification</Value></Field>
</Section>
</GroupHeader>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>300.000</FormattedValue><Value>300.000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>100mV Range</FormattedValue><Value>100mV Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.01% Rdg)</FormattedValue><Value>±( 0.01% Rdg)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>299.970</FormattedValue><Value>299.970</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>300.030</FormattedValue><Value>300.030</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>kHz</FormattedValue><Value>kHz</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>100</FormattedValue><Value>100</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>mVrms</FormattedValue><Value>mVrms</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>10.00000</FormattedValue><Value>10.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Hz</FormattedValue><Value>Hz</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>1V Range</FormattedValue><Value>1V Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.03% Rdg)</FormattedValue><Value>±( 0.03% Rdg)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>9.99700</FormattedValue><Value>9.99700</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>10.00300</FormattedValue><Value>10.00300</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Hz</FormattedValue><Value>Hz</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>1</FormattedValue><Value>1</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>40.0000</FormattedValue><Value>40.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>Hz</FormattedValue><Value>Hz</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue></FormattedValue><Value></Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.01% Rdg)</FormattedValue><Value>±( 0.01% Rdg)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>39.9960</FormattedValue><Value>39.9960</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>40.0040</FormattedValue><Value>40.0040</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>Hz</FormattedValue><Value>Hz</Value></Field>
</Section>
</GroupHeader>
<Details Level="3">
<Section SectionNumber="0">
<Field Name="Setpoint2" FieldName="{@Setpoint}"><FormattedValue>1</FormattedValue><Value>1</Value></Field>
<Field Name="SetpointUnits2" FieldName="{@SetpointUnits}"><FormattedValue>Vrms</FormattedValue><Value>Vrms</Value></Field>
<Field Name="SetpointAdj2" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
</Section>
</Details>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="1">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="FunctionName1" FieldName="{@FunctionName}"><FormattedValue>Direct Volts Verification (Rear Input)</FormattedValue><Value>Direct Volts Verification (Rear Input)</Value></Field>
</Section>
</GroupHeader>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>10.00000</FormattedValue><Value>10.00000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>10 V Range Rear Input</FormattedValue><Value>10 V Range Rear Input</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.0035% Rdg + 0.00005 V)</FormattedValue><Value>±( 0.0035% Rdg + 0.00005 V)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>9.99960</FormattedValue><Value>9.99960</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>10.00040</FormattedValue><Value>10.00040</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>V</FormattedValue><Value>V</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="1">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="FunctionName1" FieldName="{@FunctionName}"><FormattedValue>4-Wire Ohms Verification (Rear Input)</FormattedValue><Value>4-Wire Ohms Verification (Rear Input)</Value></Field>
</Section>
</GroupHeader>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>1.000000</FormattedValue><Value>1.000000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>1 kOhm Range Rear Input</FormattedValue><Value>1 kOhm Range Rear Input</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.01% Rdg + 0.00001 kOhm)</FormattedValue><Value>±( 0.01% Rdg + 0.00001 kOhm)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>0.999890</FormattedValue><Value>0.999890</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>1.000110</FormattedValue><Value>1.000110</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>kOhm</FormattedValue><Value>kOhm</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<Group Level="1">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="FunctionName1" FieldName="{@FunctionName}"><FormattedValue>Direct Current Verification (Rear Input)</FormattedValue><Value>Direct Current Verification (Rear Input)</Value></Field>
</Section>
</GroupHeader>
<Group Level="2">
<GroupHeader>
<Section SectionNumber="0">
<Field Name="Setpoint1" FieldName="{@Setpoint}"><FormattedValue>100.0000</FormattedValue><Value>100.0000</Value></Field>
<Field Name="SetpointUnits1" FieldName="{@SetpointUnits}"><FormattedValue>mA</FormattedValue><Value>mA</Value></Field>
<Field Name="ReadingName1" FieldName="{@ReadingName}"><FormattedValue>100 mA Range</FormattedValue><Value>100 mA Range</Value></Field>
<Field Name="SetpointAdj1" FieldName="{@SetpointAdj}"><FormattedValue>25.0</FormattedValue><Value>25.0</Value></Field>
<Field Name="Accuracy1" FieldName="{@Accuracy}"><FormattedValue>±( 0.05% Rdg + 0.005 mA)</FormattedValue><Value>±( 0.05% Rdg + 0.005 mA)</Value></Field>
<Field Name="Min1" FieldName="{@Min}"><FormattedValue>99.9450</FormattedValue><Value>99.9450</Value></Field>
<Field Name="Max1" FieldName="{@Max}"><FormattedValue>100.0550</FormattedValue><Value>100.0550</Value></Field>
<Field Name="ReadingUnits1" FieldName="{@ReadingUnits}"><FormattedValue>mA</FormattedValue><Value>mA</Value></Field>
</Section>
</GroupHeader>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
<GroupFooter>
<Section SectionNumber="0">
</Section>
</GroupFooter>
</Group>
</CrystalReport>
