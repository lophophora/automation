#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 12 16:29:41 2018

@author: slushy
"""
import time
import visa
from string import Template
#import datetime


rm = visa.ResourceManager()


#rm.list_resources()
uut = rm.open_resource("ASRL5::INSTR")
#uut.query("*IDN?")
#uut.read_termination
cal = rm.open_resource('visa://172.27.0.12/GPIB0::15::INSTR')




# RS-232 Prompts returned by device
cmdSuccess = '=>\r\n'
cmdUnknown = '?>\r\n'
cmdFailure = '!>\r=n'
cmdStatus = ['=','?','!']
# UUT Functions
uutFunc = {""}

#def monitor(chan):
    

def query(str,delay=2):
    """
    Query handler for Fluke 2620 data logger over RS-232 connection
    """
    try:
        uut.write(str)
        data = uut.read()
        print(data)
        if data[0] in cmdStatus:
            return data
        time.sleep(delay)
        code = uut.read()
        print(code)
        while (code[0] not in cmdStatus):
            time.sleep(delay)
            code = uut.read()
            print(code)

        if code in cmdSuccess:
            print("Success: ", code)
            return data
        elif code in cmdUnknown:
            print ("Not Understood")
            return "error"
        elif code in cmdFailure:
            print ("Unable to execute comamnd")
            return "error"
        
    except visa.VisaIOError as visaerr:
        print ("Visa Error", visaerr)
    except:
        print("Inexplicable Error encountered")
    
def inTolerance(value,low,high):
    """
    Compare measured value against provided tolerances.  Returns true if value
    is within tolerance, otherwise returns false.
    """
    
'''
rEADINGS
readings
F
currentFunc = next(functions)
currentFunc
d[currentFunc]
cRange = iter(currentFunc.keys())
cRange = iter(d[F].keys())
cRange
R
R = next(cRange)
R
func
func = d[currentFunc]
func
func.keys()
rng = iter(func.keys())
rng
r = next(rng)
r
func[r]
'''

# iterator object
functions = iter(d.keys())

currentFunc = next(functions)
        
readings.append(query("MON_VAL?").strip('\r\n'))

query("MON_VAL?").strip('\r\n')

# function, channel, uut function, range
uut.write("FUNC 0, VDC, 2")

uut.write("FUNC 0, VAC, 1")
uut.read()

for z in (calOut.substitute(V=x['Setpoint'], U=x['Unit'][0], F=x['Frequency'], FU=x['Frequency Unit']) for n, x in enumerate(func[r])):
    print(z)
    cal.write(z)
    cal.write("OPER")
    input("wait")
    print(query("MON_VAL?").strip('\r\n'))
    readings.append(query("MON_VAL?").strip('\r\n'))
    
frange = 4
uut.write("MON 0")
uut.write("FUNC {}, {}, {}".format("0","VAC",frange))
print(uut.query("MON 1, 0"))
print(uut.query("FUNC? 0"))
uut.read()


r = next(cRange)

with open('859WQ-80-1.csv', 'a') as f:
    f.write(F + "\n")
    for r in readings: f.write(r + "\n")

uut.write("FUNC 1, OHMS, 1, 4")

calOut = Template("OUT $V $U")
calRead = cal.query("OUT?")

for z in (calOut.substitute(V=x['Setpoint'], U=x['Unit']) for n, x in enumerate(func[r])):
    print(z)
    cal.write(z)
    cal.write("OPER")
    calRead = cal.query("OUT?")
    input("wait")
    meas = query("MON_VAL?").strip('\r\n')
    print (meas, calRead)
    readings.append(meas)
    
    
# 4 wire ohms, connected to channels 1 and 11

for i,rng in enumerate(func.keys(),1):
    ## Turn off channel monitoring
    query("MON 0")
    print(rng, " setting range: ",i)
    ## FUNC <channel>, <function>, <range>, <terminals>
    query("FUNC 1, OHMS, {}, 4".format(i))
    ## Enable monitoring for <channel>
    query("MON 1, 1")
    for z in (calOut.substitute(V=x['Setpoint'], U=x['Unit']) for n, x in enumerate(func[rng])):
        print(z)
        cal.write(z)
        cal.write("OPER")
        calRead = cal.query("OUT?")
        input("wait")
        meas = query("MON_VAL?").strip('\r\n')
        print (meas, calRead)
        readings.append(meas)
    