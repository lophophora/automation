#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  3 16:17:43 2018

@author: slushy
"""

import visa

def deviceList(rm=visa.ResourceManager(),intf="GPIB"):
    deviceList = []
    try:
        query = intf + "?*::INSTR"
        print("Now querying: ", query)
        devices = rm.list_resources(query)
        for n, d in enumerate(devices):
            try:
                dev = rm.open_resource(d)
                deviceList.append((d, dev.query('*IDN?').strip('\n').split(',')))
                print(deviceList[n])
                dev.close()
            except visa.VisaIOError as err:
                print("Resource {0} failed with Visa IO error: {1}".format(d,err))
                pass
    except visa.Error as err:
        print("Device Listing failed with Visa error: {1}".format(err))
    except:
        print("Unexplicable Error")
    finally:
        return deviceList
    
    
    
def findDevice(dL, dev):
    matches = []
    for n, d in enumerate(dL):
        dID = d[1]
        if (dID[1].find(dev) != -1):
            print("Found {} {} (sn: {}) at resource {}".format(dID[0],dID[1],dID[2],d[0]))
            matches.append([d[0], dID[0] + " " + dID[1]])
    count = len(matches)
    if (count == 1):
        print("Using {0} at {1}".format(dev,matches[0][0]))
        return matches[0][0]
    if (count > 1):
        print("Found {0} {1} devices..".format(len(matches),dev))
        return matches
    else:
        return None
