#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  3 16:23:44 2018

@author: slushy
"""

def getIndexedDict(section):
    """
    Return a dictionary consisting of the function range value and a list of
    setpoints as key->value respectively.
    """
    indexed = {}
    setPoints = {}
    funcRange = section.split(' ')
    for n, x in enumerate(funcRange):
        setPoints = {}
        r = x.split(':')
        try:
            r[1]
            setPoints[r[0]] = [float(s) for s in r[1].split(',')]
        except IndexError:
            setPoints[r[0]] = [r[0]]
        indexed[n] = setPoints
    return indexed


