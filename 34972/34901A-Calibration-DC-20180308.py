#!/usr/bin/python
#coding=utf-8

## agilent-thermocouple.py
##
## Alex Robinson - 2017-04-08

# some much needed libraries
import visa
import sys
import os
import time

# import some home-grown libraries
from selectaroo import menu
from engToFloat import *

rm = visa.ResourceManager()

#thermocoupleSetting = { 'J':["-100", "0", "100", "500", "1000"], 'K':["-100", "0", "100", "500", "1000"], 'T':["-100","0","100","200","390"] }
dcVolts = {"0.1":["0.0","0.1"], "1.0":["0.0","1.0"], "10.0":["0.0","10.0","-10.0"], "100.0":["0.0","100.0"], "300":["0.0","300.0"]}



# GET SONumber - We'll use this for the ouput filename and worksheet name
SONumber = input( "Please enter a Service Order number: " )

# bind resources
print( "\nPlease select a datalogger from the list of available devices." )
#dataLogger = rm.open_resource(menu(rm.list_resources()))
dataLogger = rm.open_resource('GPIB1::9::INSTR')

print( "\nPlease select a calibrator from the list of available devices." )
#calibrator = rm.open_resource(menu(rm.list_resources()))
calibrator = rm.open_resource('GPIB1::4::INSTR')


calibrator.write("*RST")
readings = []

    
while (True):
    print( "Please connect all datalogger channels to the 5520... ", end="" )
    input( "" )
    for i, x in enumerate(sorted((list(dcVolts.keys())))) :
        print("Verifying", x, "volt range.")
        # Configure the datalogger.  10 volts reading is taken on every channel.
        while(True):
            dataList = []
            dataLogger.write("CONF:VOLT:DC "+x+", (@211:214)")
            setpoints = dcVolts[x]
            for p, s in enumerate(dcVolts[x]) :
                calibrator.write("OUT "+s+" V")
                                 
                if s in [ "100.0","300.0" ] :
                    input( "Verify that the calibrator is operating and press enter" )
                #if s == "-10.0" :
                #    dataLogger.write("CONF:VOLT:DC "+x+", (@211:214)")
                else:
                    calibrator.write("OPER;")
                    while(calibrator.query("ISR?", 2).strip("\n") != '6145'): time.sleep(1)
                #if s == "10.0" :
                #   dataLogger.write("CONF:VOLT:DC "+x+", (@201:220)")
                             
                
                dataList.append(dataLogger.query("READ?").strip("\n,"))
                print( "Setpoint:",x,"Readings:",dataList[-1])
            if (input("Press enter to continue or r to retry range.. ") != "r"):
                readings += dataList
                break


# Turn off the juice        
calibrator.write("STBY")

# Modify the 10 volt readings into a list
dataList[5] = dataList[5].split(",")


    
## Output our data
f = open( SONumber+".csv", 'a' )
output = str( SONumber ) +" - " + time.asctime() + "\n"
f.write(output)

"""
11 readings for channel 201,
20 readings in readings[5] for 10 volt measurement
"""

# Channel 1 readings
for z in range(len(dataList)):
    # convert to milliamps
    if z == 0 or z == 1 :
        output = str(engToFloat(dataList[z]*1000))+",\n"
    if z == 5 :
        output = str(engToFloat(dataList[z][0]))+",\n"
    else :                 
        output = str(engToFloat(dataList[z]))+",\n"
    f.write(output)

# 10 volt readings
for w in range( 1, len(dataList[5]) ) :
    output = str(engToFloat(dataList[5][w]))+",\n"
    f.write(output)
    
f.close()
dataLogger.close()
calibrator.close()






"""
print( "\nPlease select the desired thermocouple type.")
thermocouple = menu(list(thermocoupleSetting.keys()))

temperature = thermocoupleSetting[ thermocouple ]

# Make certain the calibrator is in thermocouple mode
if calibrator.query("TC_TYPE?").strip('\n') != thermocouple :
    calibrator.write( "TC_TYPE " + thermocouple )

# Configure the datalogger
dataLogger.write("CONF:TEMP TCouple, "+thermocouple+", 10, (@201:220)")
print( "Please connect all datalogger channels to the 5520", end="" )
input( "" )

# Take a reading for each temperature
for x in range( len( temperature) ) : 
    calibrator.write("OUT "+str(temperature[x])+" CEL;OPER")
    time.sleep(6)
    dataList.append(dataLogger.query("READ?").strip('\n').split(","))
    print( "\nSetpoint:", temperature[x], "C" )
    for y in range( len( dataList[x] ) ) :
        print("Reading ", y, ": ", engToFloat(dataList[x][y]), sep="")

## set calibrator to standby
calibrator.write("STBY")

## Output our data
f = open( SONumber+".csv", 'a' )
output = str( SONumber ) +" - " + time.asctime() + "\n"
f.write(output)
## For each channel
for x in range(20) :
    # For each setpoint
    for y in range(5) :
        output = str(engToFloat(dataList[y][x]))+",\n"
        f.write(output)
f.close()

## End VISA session gracefully
dataLogger.close()
calibrator.close()
"""
