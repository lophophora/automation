#!/usr/bin/python
#coding=utf-8

## agilentAutomator.py
##
## Alex Robinson - 2017-04-08


import time
startTime = time.time()
from string import Template

# some much needed libraries
import sys
import os
#from selectaroo import menu
#from engToFloat import *
#import time

# VISA stuff
import visa
rm = visa.ResourceManager()

dataLogger = ""
calibrator = ""
functionGenerator = ""
option = ""
setup = False
calRes = []

def menu (choiceList) :
    """ The menu module takes a list of options as an argument, itemizes the
        items in the list, prompts the user for a selection, and returns the
        values of original list as a new list.

        The list indexes at zero and the selection menu indexes at one, which
        requires a bit of tricky thinking but is relatively straightforward to
        follow.
    """
    while (True) :
        for n,c in enumerate(choiceList):
            print( "[",n,"] ",c, sep="" )
        menuSelection = ""
        menuSelection = int(input( "\nPlease enter your choice or 'q' to quit: " ))
        if (menuSelection < len(choiceList) and menuSelection >= 0) :
            return str(choiceList[menuSelection])
        elif (menuSelection == "q") :
            print( "\nQuit selected, exiting." )
            print( "Execution Time: ", time.time() - startTime, "seconds.\n")
            sys.exit("10")
        else:
            print( "I'm sorry, that is not a valid selection." )
    

# Insantiate some blanks
#for x in [cal, dlog, fgen]:
#    x = none


#def isCal(idn):
#    if (idn in ['5500A','5520A','5522A','5700A','5720A']):


"""
Also consider:
http://zone.ni.com/reference/en-XX/help/370131S-01/ni-visa/findingvisaresourcesusingregularexpressions/
deviceList = rm.list_resources('(GPIB|USB)?*INSTR')
# With a 5520A
>>> calibrator.query("*IDN?")
'FLUKE,5520A,7380202,3.9+1.3+1.8\n'

"""

def scaninstr(rm):
    
    print("Scanning for available local VISA resources...")
    #deviceList = rm.list_resources('(visa)?*INSTR')
    #deviceList = list(rm.list_resources())
    deviceList = list(rm.list_resources('(GPIB|USB)?*INSTR'))

    idn = ""
    dList = []

    print("Scanning available GPIB and USB devices...")
    for n, dev in enumerate(deviceList):
        if ("GPIB" in dev or "USB" in dev):
            try:
                print("Retrieving device information for ", dev)
                info = rm.open_resource(dev)
                info.timeout = 5000
                idn = info.query("*IDN?",2).strip("\n")
                print("Found ", idn," at ", dev)
                dList.append([dev,idn])
                info.close()
                break
            except:
                print("Error opening device ", dev)
    return deviceList


'''
while (cal and  dlog and fgen = None):



print("Scanning available VISA devices...")
print("Scanning available VISA devices...")
print("Scanning available VISA devices...")
print("Scanning available VISA devices...")
'''
"""
while (setup == False) :
    option = input("Choose 1 for default device options, or 2 for device configuration: ")
    if (option == "2") :
        while(True):
            print( "\nPlease select a datalogger from the list of available devices.")
            dataLogger = rm.open_resource(menu(deviceList))

            dataLogger.write("DISP:TEXT 'Here We Go'")

            print( "\nPlease select a calibrator from the list of available devices.")
            calibrator = rm.open_resource(menu(deviceList))

            print( "\nPlease select a function generator from the list of available devices.")
            functionGenerator = rm.open_resource(menu(deviceList))

            print ("Found the following devices: ")
            for x in [dataLogger, calibrator, functionGenerator]:
                idn = x.query("*IDN?").strip("\n")
                print(idn)



            if (input("Press 'r' to reselect, or any other key to continue") != "r"):
                setup = True
                break



    elif (option == "1") :
        dataLogger = rm.open_resource("GPIB0::9::INSTR")
        calibrator = rm.open_resource("GPIB0::4::INSTR")
        functionGenerator = rm.open_resource("GPIB0::10::INSTR")
        setup = True
    else :
        print("Invalid selection.\n")

"""
dataLogger = rm.open_resource('visa://172.27.0.11/USB0::0x0957::0x2007::MY49011493::INSTR')
#dataLogger = rm.open_resource("visa://172.27.0.11/GPIB0::9::INSTR")
calibrator = rm.open_resource("visa://172.27.0.11/GPIB0::15::INSTR")
functionGenerator = rm.open_resource("visa://172.27.0.11/GPIB0::10::INSTR")

dataLogger.timeout = 45000
calibrator.timeout = 15000

currentRange = {0.01:[0.01],
                0.1:[0.1],
                1.0:[0.1,1.0]}


#currentRange = {"0.01":["0.01"],"0.1":["0.1"],"1.0":["0.1", "1.0"]}
#dcCurrentRange = {"0.01":["0.01","-0.01"],"0.1":["0.1","-0.1"],"1.0":["1.0", "-1.0"]}

voltageRange = ["0.100","-0.100","1.0","-1.0","10.0","1.0","-1.0","-10.0","100.0","-100.0","300.0","-300.0"]
acVolts = {"0.1":["1000 HZ","50 KHZ"],"1.0":["1000 HZ","50 KHZ","300 KHZ"],"10.0":["1000 HZ","50 KHZ","10 HZ"],"100.0":["1000 HZ","50 KHZ"],"300.0":["1000 HZ"],"195.0":["50 KHZ"]}
#acVolts = {"0.1":["1000 HZ","50 KHZ"],"1":["1000 HZ","50 KHZ","300 KHZ"],"10":["10 HZ","1000 HZ"],"1":["1000HZ"],"10":["50 KHZ"],"100":["1000 HZ","50 KHZ"],"300":["1000 HZ"],"195":["50 KHZ"]}
readEng = []
readings = []
calRes = []
################################################################################
def calDCV ():
    """
    DCV = [0.1,1.0,10.0,100.0,300.0]
    """
    while (True):

        print( "\nCalibrating DCV Gain" )
        logOut = Template("CONF:VOLT:DC $VOLTS,(@210)")
        logCal = Template("CAL:VAL $VOLTS")
        calOut = Template("OUT $VOLTS V;*WAI;OPER")
        #input( "Please connect the 210 connection to the calibrator output terminals and press enter." )
        if (input("Please connect the 210 connection to the calibrator output terminals \
                  and press enter to set the new calibration value or 'x' quit") == "x"):
                    return
        for x in [0.1,1.0,10.0,100.0,300.0]:
            while(True):
                print("Now calibrating",x," V range...")
                dataLogger.write(logOut.substitute(VOLTS=x))
                dataLogger.write("ROUT:MON (@210)")
                dataLogger.write("ROUT:MON:STAT ON")
                time.sleep(3)
                calibrator.write(calOut.substitute(VOLTS=x))
                if (x in [100.0,300.0]):
                    #statusByte = int(calibrator.query("*WAI;ISR?",delay=2).strip("\n"))
                    #if (statusByte != 6145) :
                    input( "Verify that the calibrator is Operating and press enter" )
                    time.sleep(4)
                else :
                    stableCal("6145")
                dataLogger.write(logCal.substitute(VOLTS=x))
                calVal = float(dataLogger.query("CAL:VAL?",2).strip('\n'))
                print("New calibration value for range ",x,": ",calVal)
                input("verify output")
                print(dataLogger.query("CAL?"))
                if (input("Verify the 34970A reading. Press 'r' to re-calibrate, or enter continue") != "r"):
                    break
                #if (input("Verify the 34970A reading. Press 'r' to re-calibrate, or enter continue") != "r"):
                #    return

        calibrator.write("STBY")

        if (input("Press 'r' to retry, or any other key to continue") != "r"):
            break
    ### End calDCV
        
    
def calZero ():
    """
    DCV = [0.1,1.0,10.0,100.0,300.0]
    
    """
    
    
 #   channels = {"CURR:DC":[[0.01,0.1,1.0],"CONF:CURR:DC ","(@219)"],
    channels = {"VOLT:DC":[[0.1,1.0,10.0,100.0,300.0],"CONF:VOLT:DC ","(@209)"],
                "FRES":[[100,1000,10000,100000,1000000,10000000,100000000],"CONF:FRES ","(@209)"]}
    dataLogger.timeout = 30000
    print( "\nCalibrating DCA Zero" )
    for k in iter(channels):
        for s in channels[k][0]:
            print(k," at ",s)
            dataLogger.write(channels[k][1]+str(s)+" ,"+channels[k][2])
            dataLogger.write("ROUT:SCAN "+channels[k][2])
            dataLogger.write(k+":NPLC 20")
            dataLogger.write("ROUT:CHAN:DEL:AUTO ON, "+channels[k][2])
            dataLogger.write("ROUT:MON "+channels[k][2])
            dataLogger.write(k+":RES MAX")
            dataLogger.write("CAL:VAL 0")
            print(dataLogger.query("CAL?;*WAI"))
            #print(dataLogger.query("CONF?"))
            #print(k," at ",s)
            time.sleep(15)
        dataLogger.write("ROUT:SCAN (@)")
        
    
    while (True):

        print( "\nCalibrating DCV Gain" )
        logOut = Template("CONF:VOLT:DC $VOLTS,(@210)")
        logCal = Template("CAL:VAL $VOLTS")
        calOut = Template("OUT $VOLTS V;*WAI;OPER")
        #input( "Please connect the 210 connection to the calibrator output terminals and press enter." )
        if (input("Please connect the 210 connection to the calibrator output terminals \
                  and press enter to set the new calibration value or 'x' quit") == "x"):
                    return
        for x in [0.1,1.0,10.0,100.0,300.0]:
            while(True):
                print("Now calibrating",x," V range...")
                dataLogger.write(logOut.substitute(VOLTS=x))
                dataLogger.write("ROUT:MON (@210)")
                dataLogger.write("ROUT:MON:STAT ON")
                time.sleep(3)
                calibrator.write(calOut.substitute(VOLTS=x))
                if (x in [100.0,300.0]):
                    #statusByte = int(calibrator.query("*WAI;ISR?",delay=2).strip("\n"))
                    #if (statusByte != 6145) :
                    input( "Verify that the calibrator is Operating and press enter" )
                    time.sleep(4)
                else :
                    #stableCal("6145")
                    time.sleep(4)
                dataLogger.write(logCal.substitute(VOLTS=x))
                calVal = float(dataLogger.query("CAL:VAL?",2).strip('\n'))
                print("New calibration value for range ",x,": ",calVal)
                if (input("Press enter to set the new calibration value or 'x' quit") == "x"):
                    return
                print(dataLogger.query("CAL?"))
                if (input("Verify the 34970A reading. Press 'r' to re-calibrate, or enter continue") != "r"):
                    break
                #if (input("Verify the 34970A reading. Press 'r' to re-calibrate, or enter continue") != "r"):
                #    return

        calibrator.write("STBY")

        if (input("Press 'r' to retry, or any other key to continue") != "r"):
            break
    ### End calDCV
        
def calOhms():
   
    logCal = Template("CAL:VAL $OHMS")
    print( "\nCalibrating 4W OHMS" )
    input( "Connect plugs 210 and 220 to the calibrator outputs, and press enter.")
    calModel = calibrator.query("*IDN?")[6:10]
    calibrator.write("OUT 100 OHM; *WAI")
    if (calModel in ["5700","5720"]):
        calibrator.write("EXTSENSE ON")
    if (calModel in ["5500","5520","5522"]):
        calibrator.write("ZCOMP WIRE4")
    calibrator.write("OPER")
    calRes = []
    for e in range(2,8) :
        while(True):
            ohms = 10 ** e
            resolution = round(3e-7 * ohms,6)
            print("Now calibrating",ohms," V range...")
            dataLogger.write("CONF:FRES "+str(ohms)+", "+str(resolution)+", (@210)")
            dataLogger.write("ROUT:SCAN (@210)")
            dataLogger.write("ROUT:CHAN:DEL:AUTO ON, (@210)")
            dataLogger.write("ROUT:MON (@210)")
            dataLogger.write("ROUT:MON:STAT ON")

            ## 5520
            if (ohms == 1000000 and calModel in ["5500","5520","5522"]):
                input( "Compensation sense does not work on this range. Please connect both channels to the Output terminals and press enter to continue." )
                calibrator.write("ZCOMP NONE")
            ## 5700
            if (ohms == 100000000 and calModel in ["5700","5720"]):
                input( "External sense does not work on this range. Please connect both channels to the Output terminals and press enter to continue." )
                calibrator.write("EXTSENSE OFF")
            calibrator.write("OUT "+str(ohms)+" OHM")
            calibrator.write("OPER")
            calRes.append(float(calibrator.query("OUT?").strip("\n").split(",")[0]))
            dataLogger.write(logCal.substitute(OHMS=calRes[-1]))
            calVal = float(dataLogger.query("CAL:VAL?",2).strip('\n'))
            print("New calibration value for range ",calRes[-1],": ",calVal)
            
            print(dataLogger.query("CAL?"))
            if (input("Verify the 34970A reading. Press 'r' to re-calibrate, or enter continue") != "r"):
                break
    calibrator.write("STBY")
    
    
def calACV():
    acVolts = {"0.1":["1000 HZ","50 KHZ"],
               "1.0":["1000 HZ","50 KHZ"],
               "10.0":["1000 HZ","50 KHZ","10 HZ"],
               "0.01":["1000 HZ"],
               "100.0":["1000 HZ","50 KHZ"],
               "300.0":["1000 HZ","50 KHZ"]}

    print( "\nCalibrating ACV Gain" )
    input( "Please connect the 210 plug to the calibrator output and press enter.")

    for v,volts in enumerate(acVolts.keys()):
            voltrange = volts
            if volts == "0.01":
                voltrange = "0.1"
            dataLogger.write("CONF:VOLT:AC "+voltrange+",(@210)")
            dataLogger.write("VOLT:AC:BAND 3, (@210)")
            dataLogger.write("ROUT:SCAN (@210)")
            dataLogger.write("ROUT:CHAN:DEL:AUTO ON,(@210)")
            dataLogger.write("ROUT:MON (@210)")
            for f,freq in enumerate(acVolts[volts]):
                while(True):
                    calibrator.write("OUT "+volts+" V, "+freq+";*WAI;OPER")
                    #if amplitude[x] not in ["0.1","0.01"] and int(amplitude[x]) >= 100 :
                    if volts in ["100.0","300.0"]:
                        input( "Please verify that calibrator is in OPER state, and press enter." )
                        time.sleep(4)
                    dataLogger.write("CAL:VAL "+volts)
                    calVal = float(dataLogger.query("CAL:VAL?",2).strip('\n'))
                    print("New calibration value for range ",volts,": ",calVal)
                    print("Calibrator output: ",float(calibrator.query("OUT?").strip("\n").split(",")[0]))
                    if (input("Press enter to set the new calibration value or 'x' quit") == "x"):
                        return
                    print("Applying calibration value")
                    print(dataLogger.query("CAL?"))
                    if (input("Verify the 34970A reading. Press 'r' to re-calibrate, or enter continue") != "r"):
                        break
    calibrator.write("STBY")

def calAAC():
    """
    ## AC Current, Gain
    """
    currentRange = {0.01:[0.01],
                0.1:[0.1],
                1.0:[0.01,1.0]}
    print( "\nCalibrating AC Current Gain" )
    input( "\nConnect plug 221 to the calibrator output, and press enter.")
    for i, a in enumerate(sorted(currentRange.keys())):
        dataLogger.write("CONF:CURR:AC "+str(a)+",(@221)")
        dataLogger.write("CURR:AC:BAND 3,(@221)")
        dataLogger.write("ROUT:SCAN (@221)")
        dataLogger.write("ROUT:CHAN:DEL:AUTO ON,(@221)")
        dataLogger.write("ROUT:MON (@221)")
        time.sleep(3)
        for b, s in enumerate(currentRange[a]):
            while(True):
                calibrator.write("OUT "+str(s)+" A, 1000HZ;*WAI;OPER")
                print("Waiting for calibrator to stabilize.",end="")

                dataLogger.write("CAL:VAL "+str(s))

                calVal = float(dataLogger.query("CAL:VAL?",2).strip('\n'))
                print("Range:", a,"\tSetpoint: ",s,"\tNew Calibration value: ",calVal)
                print("Before: ",float(dataLogger.query("READ?",1).strip("\n,")))
                if (input("Press enter to set the new calibration value or 'x' quit") == "x"):
                    return
                print(dataLogger.query("CAL?"))
                time.sleep(10)
                print("After: ",float(dataLogger.query("READ?",1).strip("\n,")))
                if (input("Verify the 34970A reading. Press 'r' to re-calibrate, or enter continue") != "r"):
                    break
        if (input("Press enter to continue") != "r"):
            break
        
    calibrator.write("STBY")
    
def calroutines():
    calDCV()
    calOhms()
    calACV()

    calAAC()
    
def calLock():
    if (dataLogger.query("CAL:SEC:STAT?").strip('\n') == "1"):
        dataLogger.write("CAL:SEC:STAT OFF,AT034972")
    elif (dataLogger.query("CAL:SEC:STAT?").strip('\n') == "0"):
        dataLogger.write("CAL:SEC:STAT OFF,HP034970")
    return dataLogger.query("CAL:SEC:STAT?").strip('\n')

def zeroADC():
    funcTime = time.time()
    """
    DC Current - Zero Offset - channel 221
    """
    zeroAmp = []
    zeroAmp.append("DCA Zero")
    print( "\nVerifying DCA zeros" )
    dataLogger.write("DISP:TEXT 'DCA Zeros'")
    for i, a in enumerate(currentRange.keys()) :
        dataLogger.write("CONF:CURR:DC "+str(a)+",(@221)")
        dataLogger.write("ROUT:CHAN:DEL:AUTO ON,(@221)")
        dataLogger.write("ROUT:SCAN (@221)")
        zeroAmp.append(float(dataLogger.query("READ?",3).strip("\n,")))
        print( "Range:",a,"\tReading:",zeroAmp[-1])
    
    #e2dataLogger.write("*RST")
    print( "\nFunction Time: ", time.time() - funcTime, "seconds.\n")
    return zeroAmp


def zeroDCV():
    zeroVoltageRange = ["0.100","1.0","10.0","100.0","300.0"]
    funcTime = time.time()
    read = []
    read.append("DCV Zero")
    print( "\nVerifying DCV zeros" )
    ### DC Volts, ch 209
    dataLogger.write("DISP:TEXT 'DCV Zeros'")
    for x in range(len(zeroVoltageRange)) :
        dataLogger.write("CONF:VOLT:DC "+zeroVoltageRange[x]+",(@209)")
        dataLogger.write("ROUT:CHAN:DEL:AUTO ON,(@209)")
        dataLogger.write("SENS:VOLT:DC:NPLC 100,(@209)")
        dataLogger.write("ROUT:SCAN (@209)")
        #time.sleep(4)
        meas = dataLogger.query("READ?",2).strip("\n,")
        read.append(float(meas))
        
        #dcvZ.append(float(dataLogger.query("READ?",2).strip("\n,")))
        #print("Using Meas: ",float(dataLogger.query("MEAS?",1).strip("\n,")))
        print( "Range:",zeroVoltageRange[x],"\tReading:",read[-1])
    dataLogger.write("*RST")
    print( "\nFunction Time: ", time.time() - funcTime, "seconds.\n")
    return read

## Add a space

def resZeros(w):
    funcTime = time.time()
    print( "\nFunction Time: ", time.time() - startTime, "seconds.\n")
    if (w == 4):
        func = "FRES"
    else:
        func = "RES"
    res = []
    res.append(str(w)+"Wire Ohm Zero")
    print( "\nVerifying ",w,"W OHMS" )
    dataLogger.write("DISP:TEXT '4W OHMs'")
    dataLogger.write("CONF:"+func+" 100,(@209)")
    dataLogger.write("ROUT:SCAN (@209)")
    for e in range(2,9) :
        ohms = 10 ** e
        resolution = round(3e-7 * ohms,6)
        dataLogger.write("CONF:"+func+" "+str(ohms)+", "+str(resolution)+", (@209)")
        dataLogger.write("ROUT:SCAN (@209)")
        dataLogger.write("ROUT:CHAN:DEL:AUTO ON,(@209)")
        time.sleep(2)
        res.append(float(dataLogger.query("READ?",4).strip("\n,")))
        #print("Using Meas: ",float(dataLogger.query("MEAS?",1).strip("\n,")))
        print( "Setpoint:",ohms,"\tReading:",res[-1])
    print( "\nFunction Time: ", time.time() - funcTime, "seconds.\n")
    return res



def gainDCV ():
    """
    ### Volts DC, gain
    +9.90000000E+37
    """
    funcTime = time.time()
    while (True):
        dcvG = []
        dcvG.append("DCV Gain")
        print( "\nVerifying DCV Gain" )
        dataLogger.write("DISP:TEXT 'DC Volts'")
        input( "Please connect the 210 connection to the calibrator output terminals and press enter." )
        for x in range(len(voltageRange)) :
            dataLogger.write("CONF:VOLT:DC "+voltageRange[x]+",(@210)")
            dataLogger.write("ROUT:MON (@210)")
            dataLogger.write("ROUT:MON:STAT ON")
            dataLogger.write("ROUT:SCAN (@210)")
            dataLogger.write("TRIG:SOUR TIMER")
            #dataLogger.write("TRIG:SOUR BUS")
            dataLogger.write("SENS:VOLT:DC:NPLC 100,(@210)")
            calibrator.write("OUT "+voltageRange[x]+" V, 0 HZ;")
            if voltageRange[x] in [ "100.0","300.0","-100.0","-300.0" ] :
                #statusByte = int(calibrator.query("*WAI;ISR?",delay=2).strip("\n"))
                #if (statusByte != 6145) :
                input( "Verify that the calibrator is Operating and press enter" )
                time.sleep(2)
            else :
                calibrator.write("OPER")
                #stableCal("6145")
                time.sleep(3)
            #dataLogger.write("INIT; *WAI;")
            dataLogger.write("INIT")
            #dataLogger.write("*TRG")

            #dataLogger.write("INIT; *TRG")
            #time.sleep(3)
            #readings.append(dataLogger.read("FETC?").strip("\n,"))
            dcvG.append(dataLogger.query("*WAI;FETC?",5).strip("\n,"))
            #print("Using Meas: ",float(dataLogger.query("MEAS?",1).strip("\n,")))
            print( "Setpoint:",voltageRange[x],"\tReading:",float(dcvG[-1]))
            ## append to file, readings

        calibrator.write("STBY")
        calibrator.write("*RST;*WAI")
        dataLogger.write("*RST")

        if (input("Press 'r' to retry, or any other key to continue") != "r"):
            print( "\nFunction Time: ", time.time() - funcTime, "seconds.\n")
            return dcvG




#acVolts = {"0.1":["1000 HZ","50 KHZ"],"1":["1000 HZ","50 KHZ","300 KHZ"],"10":["10 HZ","1000 HZ"],"1":["1000HZ"],"10":["50 KHZ"],"100":["1000 HZ","50 KHZ"],"300":["1000 HZ"],"195":["50 KHZ"]}
def gainACV():
    firstRange = {"0.1":["1000 HZ","50 KHZ"],"1.0":["1000 HZ","50 KHZ","300 KHZ"],"10.0":["10 HZ","1000 HZ"]}
    secondRange = {"1.0":["1000HZ"],"10.0":["50 KHZ"],"100":["1000 HZ","50 KHZ"],"300":["1000 HZ"]}
    thirdRange = {"195":["50 KHZ"]}

    """
    ### Volts AC, gain

    Here's the story, (2017-08-18)
    I had to change the calibration procedure because of the most recent changes to the datasheet.
    Because it is required to set the voltage to 1volts and 10volts more than once, I cannot have
    duplicate entries in teh dictionaries for the setpoints.  Now the setpoints are broken into
    two separate dictionaries and a for loop works through a list of the setpoint ranges within.
    enjoy!
    """
    funcTime = time.time()
    acvG = []
    acvG.append("ACV Gain")
    print( "\nVerifying ACV Gain" )
    dataLogger.write("DISP:TEXT 'AC Volts'")
    input( "Please connect the 210 plug to the calibrator output and press enter.")

    for voltRange in [firstRange, secondRange, thirdRange]:

        amplitude = list(sorted(voltRange.keys()))
        for x in range(len(amplitude)):
            frequency = voltRange[amplitude[x]]
            ## This should change the range for all but the last setting
            if amplitude != "195" :
                #print("CONF:VOLT:AC "+amplitude[x]+",(@210);*WAI")
                dataLogger.write("CONF:VOLT:AC "+amplitude[x]+",(@210)")
            dataLogger.write("VOLT:AC:BAND 3, (@210)")
            dataLogger.write("ROUT:SCAN (@210)")
            dataLogger.write("ROUT:CHAN:DEL:AUTO ON,(@210)")
            dataLogger.write("ROUT:MON (@210)")
            dataLogger.write("ROUT:MON:STAT ON")
            dataLogger.write("TRIG:SOUR TIMER")
            dataLogger.write("TRIG:TIM 5")
            for y in range(len(frequency)) :
                #print("OUT "+amplitude[x]+" V, "+frequency[0]+";*WAI;OPER")
                calibrator.write("OUT "+amplitude[x]+" V, "+frequency[y]+";*WAI;OPER")
                #if amplitude[x] not in ["0.1","0.01"] and int(amplitude[x]) >= 100 :
                if amplitude[x] not in ["0.1","0.01","1.0","10.0"] and int(amplitude[x]) >= 100 :
                    #statusByte = int(calibrator.query("ISR?", delay=2).strip("\n"))
                    #if (statusByte != 6145) :
                    input( "Please verify that calibrator is in OPER state, and press enter." )
                dataLogger.write("INIT")
                time.sleep(5)
                # For some reason READ? results in error -410, query interrupted.
                #dataLogger.write("*TRG")
                #time.sleep(1)

                acvG.append(dataLogger.query("*WAI;FETC?").strip("\n,"))
                #print("Using Meas: ",float(dataLogger.query("MEAS?",1).strip("\n,")))
                print( "Setpoint:",amplitude[x],"at",frequency[y],"\tReading:",float(acvG[-1]))
    calibrator.write("STBY")
    calibrator.write("*RST;*WAI")
    dataLogger.write("*RST")
    if (input("Press 'r' to retry, or any other key to continue") != "r"):
        print( "\nFunction Time: ", time.time() - funcTime, "seconds.\n")
        return acvG





def fourWire():
    """
    Resistance, 4w

    Note: external sense does not work on any values above 10 Mohm.  EX SNS must
    be disabled and the test leads repositined before taking last value.

    # With a 5520A
    >>> calibrator.query("*IDN?")
    'FLUKE,5520A,7380202,3.9+1.3+1.8\n'

    >>> calibrator.query("*IDN?")[6:10]
    '5520'

    >>> calibrator.query("*IDN?")[6:10]
    '5700'
    """
    funcTime = time.time()
    while (True):
        res = []
        res.append("4 Wire Ohm")
        print( "\nVerifying 4W OHMS" )
        dataLogger.write("DISP:TEXT '4W OHMs'")
        input( "Connect plugs 210 and 220 to the calibrator outputs, and press enter.")
        
        calModel = calibrator.query("*IDN?")[6:10]
        ## For some reason the first reading always comes up erroneous.
        calibrator.write("OUT 100 OHM; *WAI")
        if (calModel in ["5700","5720"]):
            ## 5700
            calibrator.write("EXTSENSE ON")
        if (calModel in ["5500","5520","5522"]):
            ## 5520
            calibrator.write("ZCOMP WIRE4")
        calibrator.write("OPER")
        dataLogger.write("CONF:FRES 100, 3e-5, (@210)")
        dataLogger.write("ROUT:SCAN (@210)")
        dataLogger.query("READ?",4)
        calRes = []
        for e in range(2,9) :
            ohms = 10 ** e
            resolution = round(3e-7 * ohms,6)
            dataLogger.write("CONF:FRES "+str(ohms)+", "+str(resolution)+", (@210)")
            dataLogger.write("ROUT:SCAN (@210)")
            dataLogger.write("ROUT:CHAN:DEL:AUTO ON, (@210)")
            ## 5520
            if (ohms == 1000000 and calModel in ["5500","5520","5522"]):
                input( "Compensation sense does not work on this range. Please connect both channels to the Output terminals and press enter to continue." )
                calibrator.write("ZCOMP NONE")
                calibrator.write("OUT "+str(ohms)+" OHM")
            ## 5700
            if (ohms == 100000000 and calModel in ["5700","5720"]):
                input( "External sense does not work on this range. Please connect both channels to the Output terminals and press enter to continue." )
                calibrator.write("EXTSENSE OFF")
                #calibrator.write("ZCOMP NONE")
                calibrator.write("OUT "+str(ohms)+" OHM")
            else :
               # calibrator.write("EXTSENSE ON")
                #calibrator.write("ZCOMP WIRE4")
                calibrator.write("OUT "+str(ohms)+" OHM")
            calibrator.write("OPER")
            time.sleep(3)
            calRes.append(float(calibrator.query("OUT?").strip("\n").split(",")[0]))
            res.append(float(dataLogger.query("READ?",4).strip("\n,")))
            if (e in [2]):
                tolerance = (calRes[-1] * 0.00004) + (calRes[-1] * 0.0001)
            if (e in [3,4,5,6]):
                 tolerance = (calRes[-1] * 0.00001) + (calRes[-1] * 0.0001)
            if (e in [7]):
                 tolerance = (calRes[-1] * 0.00001) + (calRes[-1] * 0.0004)
            if (e in [8]):
                 tolerance = (calRes[-1] * 0.0001) + (calRes[-1] * 0.008)
            #print("Using Meas: ",float(dataLogger.query("MEAS?",1).strip("\n,")))
            print( "\nSetpoint:",calRes[-1],"Reading:",res[-1],end="")
            if (res[-1] < calRes[-1] - tolerance and res[-1] < calRes[-1] + tolerance):
                print("\t - Out of tolerance",end="")
        calibrator.write("STBY")
        calibrator.write("*RST;*WAI")
        if (input("Press 'r' to retry, or any other key to continue") != "r"):
            print( "\nFunction Time: ", time.time() - funcTime, "seconds.\n")
            return res



def gainADC():
    """
    ### DC Current, Gain
    """
    funcTime = time.time()
    while(True):
        ampsDC = []
        ampsDC.append("ADC Gain")
        print( "\nVerifying DC Current Gain" )
        dataLogger.write("DISP:TEXT 'DC Current'")
        input( "Connect plug 221 to the calibrator output, and press enter.")
        for i, a in enumerate(sorted(currentRange.keys())) :
            dataLogger.write("CONF:CURR:DC "+str(a)+",(@221)")
            dataLogger.write("ROUT:SCAN (@221)")
            dataLogger.write("SENS:CURR:DC:NPLC 100,(@221)")
            dataLogger.write("ROUT:CHAN:DEL:AUTO ON,(@221)")
            for p in ["+","-"] :
                calibrator.write("OUT "+p+str(a)+" A;*WAI;OPER")
                #stableCal("6145")
                time.sleep(5)
                #dataLogger.write("INIT;*WAI")
                ampsDC.append(float(dataLogger.query("READ?",3).strip("\n,")))
                #ampsDC.append(s)
                print('Range:{0:<7.2f}Setpoint:{1:>1s}{2:<6.2f}Reading:{3:>10f}'.format(a,p,a,ampsDC[-1]))
        calibrator.write("STBY")
        calibrator.write("*RST;*WAI")
        if (input("Press 'r' to retry, or any other key to continue") != "r"):
            print( "\nFunction Time: ", time.time() - funcTime, "seconds.\n")
            return ampsDC



def calResistance():
   for e in range(2,9) :
       ohms = 10 ** e
       calibrator.write("OUT "+str(ohms)+" OHM")
       calRes.append(float(calibrator.query("OUT?").strip("\n").split(",")[0]))
"""
## AC Current, Gain


"""

def gainAAC():
    """
    ## AC Current, Gain
    """
    funcTime = time.time()
    while (True):
        ampsAC = []
        print( "\nVerifying AC Current Gain" )
        dataLogger.write("DISP:TEXT 'AC Current'")
        input( "\nConnect plug 221 to the calibrator output, and press enter.")
        for i, a in enumerate(sorted(currentRange.keys())) :
            for b, s in enumerate(currentRange[a]):
                dataLogger.write("CONF:CURR:AC "+str(a)+",(@221)")
                dataLogger.write("CURR:AC:BAND 3,(@221)")
                dataLogger.write("ROUT:SCAN (@221)")
                dataLogger.write("ROUT:CHAN:DEL:AUTO ON,(@221)")
                calibrator.write("OUT "+str(s)+" A, 1000HZ;*WAI;OPER")
                #stableCal("6145")
                time.sleep(7)
                ampsAC.append(float(dataLogger.query("READ?").strip("\n,")))
                #ampsAC.append(s)
                print('Range:{0:<7.2f}Setpoint:{1:<6.2f}Reading:{2:>10f}'.format(a,s,ampsAC[-1]))
        calibrator.write("STBY")
        calibrator.write("*RST;*WAI")
        if (input("Press 'r' to retry, or any other key to continue") != "r"):
            print( "\nFunction Time: ", time.time() - funcTime, "seconds.\n")
            return ampsAC



################################################################################

################################################################################
"""
## Frequency, Gain
# 10 mV, 100 Hz
"""

def gainHz():
    funcTime = time.time()
    hz = []
    freq = {0.100:100, 1.0:300000}
    while (True):
        print( "\nVerifying Frequency Gain" )
        dataLogger.write("DISP:TEXT 'Frequency'")
        input( "\nConnect plug 210 to the function generator output, and press enter.")
        dataLogger.write("CONF:FREQ (@210)")
        # In this case, MIN refers to 100 mV
        for v in sorted(list(freq.keys())):
            f = freq[v]
            dataLogger.write("FREQ:VOLT:RANG "+ str(v))
            # 3 Hz Scan rate
            dataLogger.write("FREQ:RANG:LOW 3")
            dataLogger.write("ROUT:SCAN (@210)")
            # 100 MVRMS overloads the unit, and 50mvPP is the lowest available voltage.
            # How about we settle for 100 mV.
            functionGenerator.write("VOLT:UNIT VPP")
            functionGenerator.write("APPL:SIN "+str(f)+", "+str(v))
            time.sleep(7)
            hz.append(float(dataLogger.query("READ?").strip('\n')))
            print( "Setpoint:",f," Reading:",hz[-1])
        print( "\nFunction Time: ", time.time() - funcTime, "seconds.\n")
        prompt = input("Press enter to save continue, 'r' to retry, 'x' to exit.. ")
        if (prompt == 'x'):
            sys.exit(10)
        elif (prompt == 'r'):
            continue
        else:
            return hz
    #'+9.99944350E+01'

    # 1 V, 100 KHz
    #dataLogger.write("FREQ:VOLT:RANG 1")
    #functionGenerator.write("APPL:SIN 300E+3, 1.0")
    #time.sleep(7)
    #hz.append(float(dataLogger.query("READ?").strip('\n')))
    #print( "Setpoint: 300000 Reading:",hz[-1])
    #return hz;

'''
def gainHz5520():
    print( "\nVerifying Frequency Gain" )
    dataLogger.write("DISP:TEXT 'Frequency'")
    input( "\nConnect plug 210 to the function generator output, and press enter.")

    dataLogger.write("CONF:FREQ (@210)")
    # In this case, MIN refers to 100 mV
    dataLogger.write("FREQ:VOLT:RANG MIN")
    # 3 Hz Scan rate
    dataLogger.write("FREQ:RANG:LOW 3")
    dataLogger.write("ROUT:SCAN (@210)")
    calibrator.write("OUT 100MV, 300KHZ")
    time.sleep(1)
    readings.append(dataLogger.query("READ?").strip('\n'))
    print( "Setpoint: 100 Reading:",engToFloat(readings[-1]))
    #'+9.99944350E+01'

    # 1 V, 100 KHz
    dataLogger.write("FREQ:VOLT:RANG 1")
    calibrator.write("OUT 100MV, 300KHZ")
    time.sleep(1)
    readings.append(dataLogger.query("READ?").strip('\n'))
    print( "Setpoint: 300000 Reading:",readings[-1])
'''

def stableCal(byte):
    if byte == "":
        byte = "6145"
    try:
        #if (calibrator.query("OPER?",0.5).strip("\n") != "1"):
        #    input("Please place calibrator in 'OPER' state, and press enter to continue... ")
        isr = calibrator.query("ISR?",0.5).strip("\n") != byte
        if (isr != byte):
            print("Waiting for calibrator to stabilize.",end="")
            while(isr != byte):
                isr = calibrator.query("ISR?",0.5).strip("\n")
                print(".",sep="",end="")
            print(" Ready! (status: ",byte,")")
        return
    except:
        print("Error reading querying calibrator! status byte: ",isr)
    finally:
        return

#'+1.00000370E+05'
################################################################################

def fileOutput():
    sonumber = input("Please enter a service order number")
    #dataLogger.write("DISP:TEXT 'All Done'")
    if (sonumber == ""):
        fileName = time.strftime("%Y-%m-%d - 34972")+ ".csv"
    else:   
        fileName = sonumber + ".csv"
    f = open( fileName, 'a' )
    print("Opening ",fileName," for writing... ")
    
    print("Writing data to  ",fileName)
    f.write( time.asctime() + "\n")
    for z in range( len( readings ) ) :
        output = str( readings[z]  ) + '\n'
        f.write(output)

    f.write("\nCalibrator resistance output readings:\n")
    for zz in range( len( calRes ) ) :
        output = str( calRes[zz] ) + '\n'
        f.write(output)

    ## Close our output file
    
    f.close()
    print("Closing output file ",fileName)
    
    
######
#   Cleanup section

def cleanup():

    dataLogger.write("DISP:TEXT 'Goodbye'")

    ## set calibrator to standby
    calibrator.write("STBY")

    ## End VISA session gracefully
    dataLogger.close()
    functionGenerator.close()
    calibrator.close()

'''
calDCV()
calOhms()
calACV()
calAAC()
'''


while (True):
    zero = []
    zero += zeroADC()
    zero += zeroDCV()
    zero += resZeros(2)
    zero += resZeros(4)
    if input("Press enter to continue, or anything else to retry.. ") != "q" :
        readings += zero
        break

readings += gainDCV()
readings += gainACV()
readings += fourWire()
readings += gainADC()
readings += gainAAC()
readings += gainHz()
gainHz()
#-gainHz5520()
#calLock()

"""
for i in range(len(readings)):
    funcTime = time.time()
    hz = []
    freq = {0.100:100, 1.0:300000}
    print( "\nVerifying Frequency Gain" )
    dataLogger.write("DISP:TEXT 'Frequency'")
    #input( "\nConnect plug 210 to the function generator output, and press enter.")
    dataLogger.write("CONF:FREQ (@210)")
    # In this case, MIN refers to 100 mV
    for v in sorted(list(freq.keys())):
        f = freq[v]
        dataLogger.write("FREQ:VOLT:RANG "+ str(v))
        # 3 Hz Scan rate
        dataLogger.write("FREQ:RANG:LOW 3")
        dataLogger.write("ROUT:SCAN (@210)")
        # 100 MVRMS overloads the unit, and 50mvPP is the lowest available voltage.
        # How about we settle for 100 mV.
        functionGenerator.write("VOLT:UNIT VPP")
        functionGenerator.write("APPL:SIN "+str(f)+", "+str(v))
        time.sleep(7)
        hz.append(float(dataLogger.query("READ?").strip('\n')))
        print( "Setpoint:",f," Reading:",hz[-1])
    print( "\nFunction Time: ", time.time() - funcTime, "seconds.\n")
    
    time.sleep(20)
    '''
    prompt = input("Press enter to save continue, 'r' to retry, 'x' to exit.. ")
    if (prompt == 'x'):
        sys.exit(10)
    elif (prompt == 'r'):
        continue
    else:
        return hz
    '''
    """
print( "\nExecution Time: ", time.time() - startTime, "seconds.\n")
