#!/usr/bin/python
#coding=utf-8

## agilentAutomator.py
##
## Alex Robinson - 2017-04-08

# some much needed libraries
import sys
import os
from selectaroo import menu
from engToFloat import *
import time

# VISA stuff
import visa
rm = visa.ResourceManager()

f = open( "34970 Calibration.csv", 'a' )

# Bind VISA resources ( Thank you menu module! )
print( "\nPlease select a datalogger from the list of available devices.")
dataLogger = rm.open_resource(menu(rm.list_resources()))
dataLogger.write("DISP:TEXT 'I Love You'")

print( "\nPlease select a calibrator from the list of available devices.")
calibrator = rm.open_resource(menu(rm.list_resources()))

print( "\nPlease select a function generator from the list of available devices.")
functionGenerator = rm.open_resource(menu(rm.list_resources()))

#dataLogger = rm.open_resource("USB0::0x0957::0x2007::MY49011493::INSTR")
#calibrator = rm.open_resource("GPIB0::15::INSTR")
#functionGenerator = rm.open_resource("GPIB0::10::INSTR")

currentRange = {"0.01":["0.01"],"0.1":["0.1"],"1.0":["0.1", "1.0"]}
dcCurrentRange = {"0.01":["0.01","-0.01"],"0.1":["0.1","-0.1"],"1.0":["1.0", "-1.0"]}
zeroVoltageRange = ["0.100","1.0","10.0","100.0","300.0"]
voltageRange = ["0.100","-0.100","1.0","-1.0","10.0","1.0","-10.0","100.0","-100.0","300.0","-300.0"]
ohmRange = ["100","1000","10000","100000","1000000","10000000","100000000"]
acVolts = {"0.1":["1000 HZ","50 KHZ"],"1":["1000 HZ","50 KHZ","300 KHZ"],"10":["1000 HZ","50 KHZ","10 HZ"],"100":["1000 HZ","50 KHZ"],"300":["1000 HZ"],"195":["50 KHZ"]}
#acVolts = {"0.1":["1000 HZ","50 KHZ"],"1":["1000 HZ","50 KHZ","300 KHZ"],"10":["10 HZ","1000 HZ"],"1":["1000HZ"],"10":["50 KHZ"],"100":["1000 HZ","50 KHZ"],"300":["1000 HZ"],"195":["50 KHZ"]}

readings = []
calibratorOut = []

################################################################################
"""
## Zero Offsets
### DC Current, ch 221



"""
print( "Verifying zero offsets" )
print( "\nVerifying DCA zeros" )
dataLogger.write("DISP:TEXT 'DCA Zeros'")
amplitude = list(currentRange.keys())
for x in range(len(amplitude)) :
    dataLogger.write("CONF:CURR:DC "+amplitude[x]+",(@221)")
    dataLogger.write("ROUT:SCAN (@221)")
    time.sleep(3)
    readings.append(dataLogger.query("READ?").strip("\n,"))
    print( "Range:",amplitude[x],"Reading:",engToFloat(readings[-1]))

## Add a space
readings.append("")

print( "\nVerifying DCV zeros" )
### DC Volts, ch 209
dataLogger.write("DISP:TEXT 'DCV Zeros'")
for x in range(len(zeroVoltageRange)) :
    dataLogger.write("CONF:VOLT:DC "+zeroVoltageRange[x]+",(@209)")
    dataLogger.write("ROUT:SCAN (@209)")
    time.sleep(3)
    readings.append(dataLogger.query("READ?").strip("\n,"))
    print( "Range:",zeroVoltageRange[x],"Reading:",engToFloat(readings[-1]))

## Add a space
readings.append("")

print( "\nVerifying 2W OHM zeros" )   
### Resistance, 2w and 4w, ch 209
dataLogger.write("DISP:TEXT 'OHM 2W Zeros'")
for x in range(len(ohmRange)) :
    dataLogger.write("CONF:RES "+ohmRange[x]+",(@209)")
    dataLogger.write("ROUT:SCAN (@209)")
    time.sleep(3)
    readings.append(dataLogger.query("READ?").strip("\n,"))
    print( "Range:",ohmRange[x],"Reading:",engToFloat(readings[-1]))

## Add a space
readings.append("")

print( "\nVerifying 4W OHM zeros" ) 
dataLogger.write("DISP:TEXT 'OHM 4W Zeros'")
for x in range(len(ohmRange)) :
    dataLogger.write("CONF:FRES "+ohmRange[x]+",(@209)")
    dataLogger.write("ROUT:SCAN (@209)")
    time.sleep(3)
    readings.append(dataLogger.query("READ?").strip("\n,"))
    print( "Range:",ohmRange[x],"Reading:",engToFloat(readings[-1]))

## Add a space
readings.append("")


## Now we get the calibrator involved
calibrator.write("*RST;*WAI")


"""
### Volts DC, gain
+9.90000000E+37
-1.25420200E-02
-1.28099000E-02
-6.65600000E-03
-1.41100000E-02

"""

print( "\nVerifying DCV Gain" )
dataLogger.write("DISP:TEXT 'DC Volts'")
input( "Please connect the 210 connection to the calibrator output terminals and press enter." )
for x in range(len(voltageRange)) :
    dataLogger.write("CONF:VOLT:DC "+voltageRange[x]+",(@210)")
    dataLogger.write("ROUT:SCAN (@210)")
    calibrator.write("OUT "+voltageRange[x]+" V;*WAI;OPER")
    if voltageRange[x] in [ "100.0","300.0","-100.0","-300.0" ] :
        input( "Verify that the calibrator is Operating and press enter" )
    else :
        time.sleep(5)
    readings.append(dataLogger.query("READ?").strip("\n,"))
    print( "Setpoint:",voltageRange[x],"Reading:",engToFloat(readings[-1]))
    ## append to file, readings

## Add a space
readings.append("")


calibrator.write("STBY")
calibrator.write("*RST;*WAI")


"""
### Volts AC, gain

+1.21400330E-02
+1.21377590E-02
+1.22186700E-02
+1.22122300E-02
+1.52460000E-03
+1.52020000E-03
+1.52900000E-03
+8.40000000E-05
+8.20000000E-05
+1.22585040E-02
+1.46900000E-02
+1.44900000E-02
"""

'''
Here's the story, (2017-08-18)
I had to change the calibration procedure because of the most recent changes to the datasheet.
Because it is required to set the voltage to 1volts and 10volts more than once, I cannot have
duplicate entries in teh dictionaries for the setpoints.  Now the setpoints are broken into
two separate dictionaries and a for loop works through a list of the setpoint ranges within.
enjoy!
'''

firstRange = {"0.1":["1000 HZ","50 KHZ"],"1":["1000 HZ","50 KHZ","300 KHZ"],"10":["10 HZ","1000 HZ"]}
secondRange = {"1":["1000HZ"],"10":["50 KHZ"],"100":["1000 HZ","50 KHZ"],"300":["1000 HZ"],"195":["50 KHZ"]}
#acVolts = {"0.1":["1000 HZ","50 KHZ"],"1":["1000 HZ","50 KHZ","300 KHZ"],"10":["10 HZ","1000 HZ"],"1":["1000HZ"],"10":["50 KHZ"],"100":["1000 HZ","50 KHZ"],"300":["1000 HZ"],"195":["50 KHZ"]}

print( "\nVerifying ACV Gain" )
dataLogger.write("DISP:TEXT 'AC Volts'")
input( "\nPlease connect the 210 plug to the calibrator output and press enter.")

for voltRange in [firstRange, secondRange]:

    amplitude = list(voltRange.keys())
    for x in range(len(amplitude)):
        frequency = voltRange[amplitude[x]]
        ## This should change the range for all but the last setting
        if amplitude != "195" :
            #print("CONF:VOLT:AC "+amplitude[x]+",(@210);*WAI")
            dataLogger.write("CONF:VOLT:AC "+amplitude[x]+",(@210);*WAI")
        dataLogger.write("VOLT:AC:BAND 3, (@210);*WAI")
        dataLogger.write("ROUT:SCAN (@210);*WAI") 
        dataLogger.write("ROUT:CHAN:DEL:AUTO ON,(@210)")
        for y in range(len(frequency)) :
            #print("OUT "+amplitude[x]+" V, "+frequency[0]+";*WAI;OPER")
            calibrator.write("OUT "+amplitude[x]+" V, "+frequency[y]+";*WAI;OPER")
            if amplitude[x] not in ["0.1","0.01"] and int(amplitude[x]) >= 100 :
                input( "Please verify that calibrator is in OPER state, and press enter." )
            else :
                time.sleep(5)
            dataLogger.write("INIT;*WAI")
            # For some reason READ? results in error -410, query interrupted.
            time.sleep(10)
            readings.append(dataLogger.query("FETC?;*WAI").strip("\n,"))
            print( "Setpoint:",amplitude[x],"Reading:",engToFloat(readings[-1]))

## Add a space
readings.append("")
        
calibrator.write("STBY")    
calibrator.write("*RST;*WAI")

"""
Resistance, 4w

Note: external sense does not work on any values above 10 Mohm.  EX SNS must
be disabled and the test leads repositined before taking last value.

# With a 5520A
>>> calibrator.query("*IDN?")
'FLUKE,5520A,7380202,3.9+1.3+1.8\n'

>>> calibrator.query("*IDN?")[6:10]
'5520'

>>> calibrator.query("*IDN?")[6:10]
'5700'

+9.99952390E+01
+9.99968060E+02
+9.99935890E+03
+9.99973010E+04
+9.99928850E+05
+1.00004490E+07
+9.99665000E+06

Calibrator Output:  1.000000E+02,OHM,0E+00,0,0.00E+00
Calibrator Output:  1.000000E+03,OHM,0E+00,0,0.00E+00
Calibrator Output:  1.000000E+04,OHM,0E+00,0,0.00E+00
Calibrator Output:  1.000000E+05,OHM,0E+00,0,0.00E+00
Calibrator Output:  1.000000E+06,OHM,0E+00,0,0.00E+00
Calibrator Output:  1.000000E+07,OHM,0E+00,0,0.00E+00
Calibrator Output:  1.000000E+08,OHM,0E+00,0,0.00E+00
"""
print( "\nVerifying 4W OHMS" )
dataLogger.write("DISP:TEXT '4W OHMs'")
input( "\nConnect plugs 210 and 220 to the calibrator outputs, and press enter.")

## For some reason the first reading always comes up erroneous.
calibrator.write("OUT 100 OHM")
calibrator.write("EXTSENSE ON")
calibrator.write("OPER")
dataLogger.write("CONF:FRES 100,(@210)")
dataLogger.write("ROUT:SCAN (@210)")
dataLogger.query("READ?")
dataLogger.query("READ?")

for x in range(len(ohmRange)) :
    dataLogger.write("CONF:FRES "+ohmRange[x]+",(@210)")
    dataLogger.write("ROUT:SCAN (@210)")
    if ohmRange[x] == "100000000" :
        input( "External sense does not work on this range. Please connect both channels to the Output terminals and press enter to continue." )
        calibrator.write("EXTSENSE OFF")
        calibrator.write("OUT "+ohmRange[x]+" OHM")
    else :
        calibrator.write("EXTSENSE ON")
        calibrator.write("OUT "+ohmRange[x]+" OHM")
    calibrator.write("OPER")
    time.sleep(5)
    readings.append(dataLogger.query("READ?").strip("\n,"))
    print( "Setpoint:",ohmRange[x],"Reading:",engToFloat(readings[-1]))
    calibratorOut.append(calibrator.query("OUT?").strip("\n"))
    print("Calibrator Output: ",calibratorOut[-1])

## Add a space
readings.append("")

calibrator.write("STBY")    
calibrator.write("*RST;*WAI")

"""
### DC Current, Gain

+1.00400000E-07
+0.00000000E+00
+5.13000000E-06

"""
print( "\nVerifying DC Current Gain" )
dataLogger.write("DISP:TEXT 'DC Current'")
input( "\nConnect plug 221 to the calibrator output, and press enter.")
amplitude = list(dcCurrentRange.keys())
for x in range(len(amplitude)) :
    dataLogger.write("CONF:CURR:DC "+amplitude[x]+",(@221)")
    dataLogger.write("ROUT:SCAN (@221)")
    frequency = dcCurrentRange[amplitude[x]]
    for y in range(len(frequency)) :  
        calibrator.write("OUT "+frequency[y]+"A;*WAI;OPER")
        time.sleep(5)
        readings.append(dataLogger.query("READ?").strip("\n,"))
        print( "Setpoint:",frequency[y],"Reading:",engToFloat(readings[-1]))

## Add a space
readings.append("")

calibrator.write("STBY")    
calibrator.write("*RST;*WAI")



"""
## AC Current, Gain

+8.33400000E-07
+1.33680000E-05
+1.22100000E-05
+1.41500000E-05

"""
print( "\nVerifying AC Current Gain" )
time.sleep(4)
dataLogger.write("DISP:TEXT 'AC Current'")
#input( "\nConnect plug 221 to the calibrator output, and press enter.")
amplitude = list(currentRange.keys())
for x in range(len(amplitude)) :
    ## set UUT to desired range
    dataLogger.write("CONF:CURR:AC "+amplitude[x]+",(@221)")
    #dataLogger.write("CURR:AC:BAND 3,(@221)")
    dataLogger.write("ROUT:SCAN (@221)")
    dataLogger.write("ROUT:CHAN:DEL:AUTO ON,(@210)")
    ## setpoints becomes list containing the value pair for the index amplitude
    setpoints = currentRange[amplitude[x]]
    for y in range(len(setpoints)):
        # set calibrator to setpoint
        calibrator.write("OUT "+setpoints[y]+" A, 1000HZ;*WAI;OPER")
        time.sleep(5)
        # take reading
        dataLogger.write("INIT;*WAI")
        # For some reason READ? results in error -410, query interrupted.
        readings.append(dataLogger.query("FETC?;*WAI").strip("\n,"))
        print( "Setpoint:",amplitude[x],"Reading:",engToFloat(readings[-1]))

calibrator.write("STBY")    
calibrator.write("*RST;*WAI")
    
################################################################################

################################################################################
"""
## Frequency, Gain
# 10 mV, 100 Hz
"""

print( "\nVerifying Frequency Gain" )
dataLogger.write("DISP:TEXT 'Frequency'")
input( "\nConnect plug 210 to the function generator output, and press enter.")
        
dataLogger.write("CONF:FREQ (@210)")
# In this case, MIN refers to 100 mV
dataLogger.write("FREQ:VOLT:RANG MIN")
# 3 Hz Scan rate
dataLogger.write("FREQ:RANG:LOW 3")
dataLogger.write("ROUT:SCAN (@210)")

# 100 MVRMS overloads the unit, and 50mvPP is the lowest available voltage.
# How about we settle for 100 mV.
functionGenerator.write("VOLT:UNIT VPP")
functionGenerator.write("APPL:SIN 100, 0.1")

readings.append(dataLogger.query("READ?").strip('\n'))
print( "Setpoint: 100 Reading:",engToFloat(readings[-1]))
#'+9.99944350E+01'        

# 1 V, 100 KHz
dataLogger.write("FREQ:VOLT:RANG 1")
functionGenerator.write("APPL:SIN 300E+3, 1.0")

readings.append(dataLogger.query("READ?").strip('\n'))
print( "Setpoint: 100000 Reading:",engToFloat(readings[-1]))
#'+1.00000370E+05'
################################################################################

dataLogger.write("DISP:TEXT 'All Done'")

f.write( time.asctime() + "\n" )
for z in range( len( readings ) ) :
    output = str( readings[z] + '\n' )
    f.write(output)

f.write("\nCalibrator resistance output readings:\n")
for zz in range( len( calibratorOut ) ) :
    output = str( calibratorOut[zz] + '\n' )
    f.write(output)

## Close our output file
f.close()

dataLogger.write("DISP:TEXT 'Goodbye'")

## set calibrator to standby
calibrator.write("STBY")
## End VISA session gracefully
dataLogger.close()
functionGenerator.close()
calibrator.close()
