#!/usr/bin/python
#coding=utf-8

## agilentAutomator.py
##
## Alex Robinson - 2017-04-08

# some much needed libraries
import sys
import os
from selectaroo import menu
from engToFloat import *
import time

# VISA stuff
import visa
rm = visa.ResourceManager()
#dataLogger = rm.open_resource(menu(rm.list_resources()))
dataLogger = rm.open_resource('USB0::0x0957::0x2007::MY49009128::INSTR')

## 13 characters


#dataLogger.write("DISP:TEXT '1234567890123'")
#dataLogger.write("DISP:TEXT 'I Love You'")

letter = 'a'
z=0
while (True):
    letter = str( 'I LOVE YOU' )
    string = letter
    for x in range( 12 ):

        string = " " + string
        dataLogger.write("DISP:TEXT '"+string+"'")
        time.sleep(0.25)

    z+=1
