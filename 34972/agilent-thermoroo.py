#!/usr/bin/python
#coding=utf-8

## agilent-thermocouple.py
##
## Alex Robinson - 2017-04-08

# some much needed libraries
import visa
import sys
import os
import time

# CSV is good enough, I gave up on this
#from openpyxl import Workbook

# import some home-grown libraries
from selectaroo import menu
#from engToFloat import *

rm = visa.ResourceManager()

thermocoupleSetting = { 'J':["-100", "0", "100", "500", "1000"],
                        'K':["-100", "0", "100", "500", "1000"],
                        'T':["-100","0","100","200","390"] }

dataList = []

# GET SONumber - We'll use this for the ouput filename and worksheet name
SONumber = input( "Please enter a Service Order number: " )


deviceList = rm.list_resources()

# bind resources
print( "\nPlease select a datalogger from the list of available devices." )
dataLogger = rm.open_resource(menu(deviceList))
#dataLogger = rm.open_resource('GPIB0::9::INSTR')

print( "\nPlease select a calibrator from the list of available devices." )
calibrator = rm.open_resource(menu(deviceList))
#calibrator = rm.open_resource('GPIB0::4::INSTR')

print( "\nPlease select the desired thermocouple type.")
thermocouple = menu(list(thermocoupleSetting.keys()))

temperature = thermocoupleSetting[ thermocouple ]

# Make certain the calibrator is in thermocouple mode
if calibrator.query("TC_TYPE?").strip('\n') != thermocouple :
    calibrator.write( "TC_TYPE " + thermocouple )

# Configure the datalogger
dataLogger.write("CONF:TEMP TCouple, "+thermocouple+", 10, (@201:220)")
print( "Please connect all datalogger channels to the 5520", end="" )
input( "" )

# Take a reading for each temperature
for x in iter(temperature) :
    #Loop range measurement indefinitely
    while (True):
        oot = False
        # Set calibrator
        calibrator.write("OUT "+x+" CEL;OPER")
        # Wait to stabilize
        print("Waiting for calibrator to stabilize",end="")
        while(calibrator.query("ISR?", 1).strip("\n") != '6145'):
            print(".",end="")
            time.sleep(0.5)
        time.sleep(2)    
        print( "\nSetpoint:", x, "C" )
        dataList.append(float(dataLogger.query("READ?").strip('\n').split(",")))
        for y in range( len( dataList[x] ) ) :
            print('Reading{0:>3d}: {1:>4.2f}'.format(y,float(dataList[x][y]),2))
            if ( float(y) >= float(temperature[x]) + 0.7 or float(y) <= float(temperature[x]) - 0.7):
                oot= True
    if (oot):
        if(input("Some readings exceed 70% of tolerance.\
                          Do you want to repeat the measurement? (y)") not in ["y","Y"]):
            break
    else:
        break

## set calibrator to standby
calibrator.write("STBY")

## Output our data

desktop = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop')
f = open( desktop+SONumber+".csv", 'a' )
output = str( SONumber ) +" - " + time.asctime() + "\n"
f.write(output)
## For each channel
for x in range(20) :
    # For each setpoint
    for y in range(5) :
        output = str(dataList[y][x])+",\n"
        f.write(output)
f.close()

## End VISA session gracefully
dataLogger.close()
calibrator.close()
