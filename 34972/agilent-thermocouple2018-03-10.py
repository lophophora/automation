#!/usr/bin/python
#coding=utf-8

## agilent-thermocouple.py
##
## Alex Robinson - 2017-04-08

# some much needed libraries
import visa
import sys
import os
import time

# CSV is good enough, I gave up on this
#from openpyxl import Workbook

# import some home-grown libraries
from selectaroo import menu
from engToFloat import *

rm = visa.ResourceManager()

thermocoupleSetting = { 'J':["-100", "0", "100", "500", "1000"], 'K':["-100", "0", "100", "500", "1000"], 'T':["-100","0","100","200","390"] }

dataList = []
deviceList = rm.list_resources()

# GET SONumber - We'll use this for the ouput filename and worksheet name
SONumber = input( "Please enter a Service Order number: " )

# bind resources
print( "\nPlease select a datalogger from the list of available devices." )
dataLogger = rm.open_resource(menu(deviceList))
#dataLogger = rm.open_resource('GPIB0::9::INSTR')




print( "\nPlease select a calibrator from the list of available devices." )
calibrator = rm.open_resource(menu(deviceList))
#calibrator = rm.open_resource('GPIB0::4::INSTR')

print( "\nPlease select the desired thermocouple type.")
thermocouple = menu(list(thermocoupleSetting.keys()))

temperature = thermocoupleSetting[ thermocouple ]

if calibrator.query("TC_TYPE?").strip('\n') != thermocouple :
    calibrator.write( "TC_TYPE " + thermocouple )

# Take a reading on each channel

for channel in range( 201, 219 ) :
    dataLogger.write("CONF:TEMP TCouple, "+thermocouple+", 10, (@"+str(channel)+")")
    print( "Please connect 5520 TC output to channel", channel, end="" )
    input( "" )
    #print( "Channel:",channel )
    # Take a reading for each temperature
    for x in range( len( temperature) ) :                  
        calibrator.write("OUT "+str(temperature[x])+" CEL;OPER")
        while(calibrator.query("ISR?", 2).strip("\n") != '6145'): time.sleep(1)
        time.sleep(6)
        dataList.append(float(dataLogger.query("READ?").strip('\n')))
        print( "Setpoint:",temperature[x]," Reading:", dataList[-1] )

## set calibrator to standby
calibrator.write("STBY")

## Output our data
#desktopFile = os.path.expanduser("~/Desktop/"+SONumber+".csv")
f = open( SONumber+".csv", 'a')
output = str(SONumber) +" - " + time.asctime() + "\n"
f.write(output)
for x in range( len( dataList ) ):
    output = str(dataList[x])+",\n"
    f.write(output)
f.close()

## End VISA session gracefully
dataLogger.close()
calibrator.close()
