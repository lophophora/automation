from openpyxl import Workbook
from engToFloat import *


# wb alias is the workbook function
wb = Workbook()
# activate the first worksheet
ws = wb.active
ws.title = "Imported Data"


readData = "-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,+2.81300000E+01,-8.79210000E+01,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,+2.89260000E+01,-9.90000000E+37"



dataList = readData.split(",")

for x in range( len( dataList ) ):
	print("Channel",x+1,":",engToFloat(dataList[x]))

for x in range( len( dataList ) ):
	print("Channel",x+1,":",metricPrefix(engToFloat(dataList[x]),"m"))



for y in range(len( dataList ) ):

	ws.cell(row=y+1, column=1, value="Channel ")
	ws.cell(row=y+1, column=2, value=dataList[y])

readData = "+12.234E+02,+12.1234E-02,-06.54321E+02,+17.345E+03"
dataList = readData.split(",")

wb.save('data.xlsx')
