Python 3.6.0 (v3.6.0:41df79263a11, Dec 23 2016, 07:18:10) [MSC v.1900 32 bit (Intel)] on win32
Type "copyright", "credits" or "license()" for more information.
>>> import visa
>>> rm = visa.ResourceManager()
>>> rm.list_resources()
('USB0::0x0957::0x2007::MY49011493::INSTR', 'ASRL1::INSTR', 'ASRL10::INSTR', 'GPIB0::10::INSTR', 'GPIB0::15::INSTR')
>>> dl = rm.open_resource('USB0::0x0957::0x2007::MY49011493::INSTR')
>>> cl = rm.open_resource('GPIB0::15::INSTR')
>>> fg = rm.open_resource('GPIB0::10::INSTR')
>>> dl.write("CONF:CURR:DC 0.01,(@221)")
(26, <StatusCode.success: 0>)
>>> dl.write("ROUT:SCAN (@221)")
(18, <StatusCode.success: 0>)
>>> dl.query("READ?")
'-1.97700000E-07\n'
>>> dl.write("CONF:CURR:DC 0.1,(@221)")
(25, <StatusCode.success: 0>)
>>> dl.write("ROUT:SCAN (@221)")
(18, <StatusCode.success: 0>)
>>> dl.query("READ?")
'-2.47000000E-07\n'
>>> dl.write("CONF:CURR:DC 1.0,(@221)")
(25, <StatusCode.success: 0>)
>>> dl.write("ROUT:SCAN (@221)")
(18, <StatusCode.success: 0>)
>>> dl.query("READ?")
'-1.27000000E-06\n'
>>> dl.query("READ?").strip('\n')
'-1.27000000E-06'
>>> voltageRange = ["0.100","1.0","10.0","100.0","300.0"]
>>> for x in range(len(voltageRange)) :
	dl.write("CONF:CURR:DC "+currentRange[x]+",(@221)")
	dl.write("ROUT:SCAN (@221)")
	dl.query("READ?")

	
Traceback (most recent call last):
  File "<pyshell#21>", line 2, in <module>
    dl.write("CONF:CURR:DC "+currentRange[x]+",(@221)")
NameError: name 'currentRange' is not defined
>>> for x in range(len(voltageRange)) :
	dl.write("CONF:VOLT:DC "+voltageRange[x]+",(@221)")
	dl.write("ROUT:SCAN (@221)")
	dl.query("READ?")

	
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'-5.06000000E-06\n'
(25, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'-2.53000000E-06\n'
(26, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+2.53000000E-06\n'
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+3.80000000E-06\n'
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'-6.33000000E-06\n'
>>> for x in range(len(voltageRange)) :
	dl.write("CONF:VOLT:DC "+voltageRange[x]+",(@209)")
	dl.write("ROUT:SCAN (@209)")
	dl.query("READ?")

	
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+3.80000000E-07\n'
(25, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+0.00000000E+00\n'
(26, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+0.00000000E+00\n'
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+0.00000000E+00\n'
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'-1.26000000E-03\n'
>>> for x in range(len(voltageRange)) :
	dl.write("CONF:VOLT:DC "+voltageRange[x]+",(@209)")
	dl.write("ROUT:SCAN (@209)")
	readings = dl.query("READ?")

	
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(25, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(26, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
>>> readings
'-1.26000000E-03\n'
>>> for x in range(len(voltageRange)) :
	dl.write("CONF:VOLT:DC "+voltageRange[x]+",(@209)")
	dl.write("ROUT:SCAN (@209)")
	readings = dl.query("READ?").split(",")

	
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(25, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(26, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
>>> readings
['+0.00000000E+00\n']
>>> readings = []
>>> for x in range(len(voltageRange)) :
	dl.write("CONF:VOLT:DC "+voltageRange[x]+",(@209)")
	dl.write("ROUT:SCAN (@209)")
	readings += dl.query("READ?")

	
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(25, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(26, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
>>> readings
['+', '0', '.', '0', '0', '0', '0', '0', '0', '0', '0', 'E', '+', '0', '0', '\n', '+', '0', '.', '0', '0', '0', '0', '0', '0', '0', '0', 'E', '+', '0', '0', '\n', '+', '0', '.', '0', '0', '0', '0', '0', '0', '0', '0', 'E', '+', '0', '0', '\n', '+', '0', '.', '0', '0', '0', '0', '0', '0', '0', '0', 'E', '+', '0', '0', '\n', '+', '0', '.', '0', '0', '0', '0', '0', '0', '0', '0', 'E', '+', '0', '0', '\n']
>>> for x in range(len(voltageRange)) :
	dl.write("CONF:VOLT:DC "+voltageRange[x]+",(@209)")
	dl.write("ROUT:SCAN (@209)")
	readings.append(dl.query("READ?"))

	
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(25, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(26, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
>>> readings
['+', '0', '.', '0', '0', '0', '0', '0', '0', '0', '0', 'E', '+', '0', '0', '\n', '+', '0', '.', '0', '0', '0', '0', '0', '0', '0', '0', 'E', '+', '0', '0', '\n', '+', '0', '.', '0', '0', '0', '0', '0', '0', '0', '0', 'E', '+', '0', '0', '\n', '+', '0', '.', '0', '0', '0', '0', '0', '0', '0', '0', 'E', '+', '0', '0', '\n', '+', '0', '.', '0', '0', '0', '0', '0', '0', '0', '0', 'E', '+', '0', '0', '\n', '+2.53000000E-07\n', '+1.27000000E-06\n', '-2.53000000E-05\n', '-1.26000000E-04\n', '-2.53000000E-03\n']
>>> readings = []
>>> for x in range(len(voltageRange)) :
	dl.write("CONF:VOLT:DC "+voltageRange[x]+",(@209)")
	dl.write("ROUT:SCAN (@209)")
	readings.append(dl.query("READ?").strip("\n"))

	
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(25, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(26, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
>>> readings
['+1.27000000E-07', '+1.27000000E-06', '+0.00000000E+00', '+0.00000000E+00', '+0.00000000E+00']
>>> ohmRange = ["100","1000","10000","100000","1000000","10000000","100000000"]
>>> for x in range(len(ohmRange)) :
	dl.write("CONF:RES "+ohmRange[x]+",(@209)")
	dl.write("ROUT:SCAN (@209)")
	dl.query("READ?")

	
(21, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+5.50000000E-03\n'
(22, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+3.75000000E-03\n'
(23, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+2.50000000E-02\n'
(24, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+0.00000000E+00\n'
(25, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+0.00000000E+00\n'
(26, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'-2.51000000E+01\n'
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+0.00000000E+00\n'
>>> for x in range(len(ohmRange)) :
	dl.write("CONF:RES "+ohmRange[x]+",(@209);*WAI")
	dl.write("ROUT:SCAN (@209)")
	dl.query("*WAI;READ?")

	
(26, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+4.75000000E-03\n'
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+3.75000000E-03\n'
(28, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+0.00000000E+00\n'
(29, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+0.00000000E+00\n'
(30, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+0.00000000E+00\n'
(31, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+0.00000000E+00\n'
(32, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+0.00000000E+00\n'
>>> for x in range(len(ohmRange)) :
	dl.write("CONF:RES "+ohmRange[x]+",(@209);*WAI")
	dl.write("ROUT:SCAN (@209)")
	dl.query("*WAI;READ?")

	
(26, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+4.62500000E-03\n'
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+5.00000000E-03\n'
(28, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+3.75000000E-02\n'
(29, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+0.00000000E+00\n'
(30, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'-2.51000000E+00\n'
(31, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+0.00000000E+00\n'
(32, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+0.00000000E+00\n'
>>> for x in range(len(ohmRange)) :
	dl.write("CONF:RES "+ohmRange[x]+",(@210);*WAI")
	dl.write("ROUT:SCAN (@210)")
	dl.query("*WAI;READ?")

	
(26, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+1.00493780E+02\n'
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+1.00488350E+02\n'
(28, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+1.00546000E+02\n'
(29, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+1.00525000E+02\n'
(30, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+1.00330000E+02\n'
(31, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+1.00400000E+02\n'
(32, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+0.00000000E+00\n'
>>> for x in range(len(ohmRange)) :
	dl.write("CONF:FRES "+ohmRange[x]+",(@210);*WAI")
	dl.write("ROUT:SCAN (@210)")
	dl.query("*WAI;READ?")

	
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+9.99972800E+01\n'
(28, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+9.99984300E+01\n'
(29, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+9.99964000E+01\n'
(30, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+1.00026000E+02\n'
(31, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+1.02840000E+02\n'
(32, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+7.53000000E+01\n'
(33, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
'+0.00000000E+00\n'
>>> for x in range(len(voltageRange)) :
	dl.write("CONF:VOLT:DC "+voltageRange[x]+",(@210)")
	dl.write("ROUT:SCAN (@210)")
	cl.write("OUT "+voltagerange[x]+"V;*WAI;OPER")
	dl.query("*WAI;READ?")
	c;.write("*WAI;STBY")
	
SyntaxError: invalid syntax
>>> for x in range(len(voltageRange)) :
	dl.write("CONF:VOLT:DC "+voltageRange[x]+",(@210)")
	dl.write("ROUT:SCAN (@210)")
	cl.write("OUT "+voltagerange[x]+"V;*WAI;OPER")
	dl.query("*WAI;READ?")
	cl.write("*WAI;STBY")

	
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
Traceback (most recent call last):
  File "<pyshell#64>", line 4, in <module>
    cl.write("OUT "+voltagerange[x]+"V;*WAI;OPER")
NameError: name 'voltagerange' is not defined
>>> for x in range(len(voltageRange)) :
	dl.write("CONF:VOLT:DC "+voltageRange[x]+",(@210)")
	dl.write("ROUT:SCAN (@210)")
	cl.write("OUT "+voltageRange[x]+"V;*WAI;OPER")
	dl.query("*WAI;READ?")
	cl.write("*WAI;STBY")

	
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(22, <StatusCode.success: 0>)
'+1.51900000E-06\n'
(11, <StatusCode.success: 0>)
(25, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(20, <StatusCode.success: 0>)
'-1.27000000E-06\n'
(11, <StatusCode.success: 0>)
(26, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(21, <StatusCode.success: 0>)
'-3.14523000E-02\n'
(11, <StatusCode.success: 0>)
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(22, <StatusCode.success: 0>)
'-1.43880000E-02\n'
(11, <StatusCode.success: 0>)
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
Traceback (most recent call last):
  File "<pyshell#66>", line 4, in <module>
    cl.write("OUT "+voltageRange[x]+"V;*WAI;OPER")
  File "H:\python\WinPython-32bit-3.6.0.1Qt5\python-3.6.0\lib\site-packages\pyvisa\resources\messagebased.py", line 207, in write
    count = self.write_raw(message.encode(enco))
  File "H:\python\WinPython-32bit-3.6.0.1Qt5\python-3.6.0\lib\site-packages\pyvisa\resources\messagebased.py", line 185, in write_raw
    return self.visalib.write(self.session, message)
  File "H:\python\WinPython-32bit-3.6.0.1Qt5\python-3.6.0\lib\site-packages\pyvisa\ctwrapper\functions.py", line 1864, in write
    ret = library.viWrite(session, data, len(data), byref(return_count))
  File "H:\python\WinPython-32bit-3.6.0.1Qt5\python-3.6.0\lib\site-packages\pyvisa\ctwrapper\highlevel.py", line 188, in _return_handler
    raise errors.VisaIOError(ret_value)
pyvisa.errors.VisaIOError: VI_ERROR_TMO (-1073807339): Timeout expired before operation completed.
>>> ## the datalogger takes all the readings instantaneously, and the calibrator sets itself after.  they aren't synced up
>>> for x in range(len(voltageRange)) :
	dl.write("CONF:VOLT:DC "+voltageRange[x]+",(@210)")
	dl.write("ROUT:SCAN (@210)")
	cl.write("OUT "+voltageRange[x]+"V;*WAI;OPER")
	wait = input("waiting")
	dl.query("*WAI;READ?")
	cl.write("*WAI;STBY")

	
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(22, <StatusCode.success: 0>)
waiting
'+9.90000000E+37\n'
(11, <StatusCode.success: 0>)
(25, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(20, <StatusCode.success: 0>)
waiting
'+9.99998530E-01\n'
(11, <StatusCode.success: 0>)
(26, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(21, <StatusCode.success: 0>)
waiting
'+9.99997580E+00\n'
(11, <StatusCode.success: 0>)
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(22, <StatusCode.success: 0>)
waiting
'-1.26000000E-04\n'
(11, <StatusCode.success: 0>)
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(22, <StatusCode.success: 0>)
waiting
'-1.26000000E-03\n'
(11, <StatusCode.success: 0>)
>>> for x in range(len(voltageRange)) :
	dl.write("CONF:VOLT:DC "+voltageRange[x]+",(@210)")
	dl.write("ROUT:SCAN (@210)")
	cl.write("OUT "+voltageRange[x]+"V;*WAI")
	cl.write("OPER")
	wait = input("waiting")
	dl.query("*WAI;READ?")
	cl.write("*WAI;STBY")

	
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(17, <StatusCode.success: 0>)
(6, <StatusCode.success: 0>)
waiting
'+9.99994350E-02\n'
(11, <StatusCode.success: 0>)
(25, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(15, <StatusCode.success: 0>)
(6, <StatusCode.success: 0>)
waiting
'+9.99998530E-01\n'
(11, <StatusCode.success: 0>)
(26, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(16, <StatusCode.success: 0>)
(6, <StatusCode.success: 0>)
waiting
'+9.99997580E+00\n'
(11, <StatusCode.success: 0>)
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(17, <StatusCode.success: 0>)
(6, <StatusCode.success: 0>)
waiting
'-2.52000000E-04\n'
(11, <StatusCode.success: 0>)
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(17, <StatusCode.success: 0>)
(6, <StatusCode.success: 0>)
waiting
'+1.26000000E-03\n'
(11, <StatusCode.success: 0>)
>>> # for some reason the unit doesnt auto operate above 100 V, hence the blank values for 100 and 300
>>> for x in range(len(voltageRange)) :
	dl.write("CONF:VOLT:DC "+voltageRange[x]+",(@210)")
	dl.write("ROUT:SCAN (@210)")
	cl.write("OUT "+voltageRange[x]+"V;*WAI")
	cl.write("OPER")
	wait = input("waiting")
	cl.write("OPER")
	wait = input("waiting")
	dl.query("*WAI;READ?")
	cl.write("*WAI;STBY")

	
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(17, <StatusCode.success: 0>)
(6, <StatusCode.success: 0>)
waiting
(6, <StatusCode.success: 0>)
waiting
'+9.99990550E-02\n'
(11, <StatusCode.success: 0>)
(25, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(15, <StatusCode.success: 0>)
(6, <StatusCode.success: 0>)
waiting
(6, <StatusCode.success: 0>)
waiting
'+9.99999790E-01\n'
(11, <StatusCode.success: 0>)
(26, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(16, <StatusCode.success: 0>)
(6, <StatusCode.success: 0>)
waiting
(6, <StatusCode.success: 0>)
waiting
'+9.99998840E+00\n'
(11, <StatusCode.success: 0>)
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(17, <StatusCode.success: 0>)
(6, <StatusCode.success: 0>)
waiting
(6, <StatusCode.success: 0>)
waiting
'+1.00000110E+02\n'
(11, <StatusCode.success: 0>)
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(17, <StatusCode.success: 0>)
(6, <StatusCode.success: 0>)
waiting
(6, <StatusCode.success: 0>)
waiting
'+3.00000620E+02\n'
(11, <StatusCode.success: 0>)
>>> ## i turned it on manually this time
>>> for x in range(len(voltageRange)) :
	dl.write("CONF:VOLT:DC "+voltageRange[x]+",(@210)")
	dl.write("ROUT:SCAN (@210)")
	cl.write("OUT "+voltageRange[x]+"V;*WAI")
	cl.write("OPER")
	wait = input("waiting")
	dl.query("*WAI;READ?")

	
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(17, <StatusCode.success: 0>)
(6, <StatusCode.success: 0>)
waiting
'+9.99986760E-02\n'
(25, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(15, <StatusCode.success: 0>)
(6, <StatusCode.success: 0>)
waiting
'+1.00000100E+00\n'
(26, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(16, <StatusCode.success: 0>)
(6, <StatusCode.success: 0>)
waiting
'+9.99997580E+00\n'
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(17, <StatusCode.success: 0>)
(6, <StatusCode.success: 0>)
waiting
'+1.00000360E+02\n'
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(17, <StatusCode.success: 0>)
(6, <StatusCode.success: 0>)
waiting
'+2.51389000E+01\n'
>>> for x in range(len(voltageRange)) :
	dl.write("CONF:VOLT:DC "+voltageRange[x]+",(@210)")
	dl.write("ROUT:SCAN (@210)")
	cl.write("OUT "+voltageRange[x]+"V;*WAI")
	cl.write("OPER")
	wait = input("waiting")
	dl.query("*WAI;READ?")

	
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(17, <StatusCode.success: 0>)
(6, <StatusCode.success: 0>)
waiting
'+9.99994350E-02\n'
(25, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(15, <StatusCode.success: 0>)
(6, <StatusCode.success: 0>)
waiting
'+9.99998530E-01\n'
(26, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(16, <StatusCode.success: 0>)
(6, <StatusCode.success: 0>)
waiting
'+9.99997580E+00\n'
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(17, <StatusCode.success: 0>)
(6, <StatusCode.success: 0>)
waiting
'+1.00000360E+02\n'
(27, <StatusCode.success: 0>)
(18, <StatusCode.success: 0>)
(17, <StatusCode.success: 0>)
(6, <StatusCode.success: 0>)
waiting
'+3.00001890E+02\n'
>>> ## it doesnt like switching to high voltage mode.
>>> cl.query("*STB?")
'8\n'
>>> cl.query("*FAULT?")
Traceback (most recent call last):
  File "<pyshell#82>", line 1, in <module>
    cl.query("*FAULT?")
  File "H:\python\WinPython-32bit-3.6.0.1Qt5\python-3.6.0\lib\site-packages\pyvisa\resources\messagebased.py", line 407, in query
    return self.read()
  File "H:\python\WinPython-32bit-3.6.0.1Qt5\python-3.6.0\lib\site-packages\pyvisa\resources\messagebased.py", line 332, in read
    message = self.read_raw().decode(enco)
  File "H:\python\WinPython-32bit-3.6.0.1Qt5\python-3.6.0\lib\site-packages\pyvisa\resources\messagebased.py", line 306, in read_raw
    chunk, status = self.visalib.read(self.session, size)
  File "H:\python\WinPython-32bit-3.6.0.1Qt5\python-3.6.0\lib\site-packages\pyvisa\ctwrapper\functions.py", line 1582, in read
    ret = library.viRead(session, buffer, count, byref(return_count))
  File "H:\python\WinPython-32bit-3.6.0.1Qt5\python-3.6.0\lib\site-packages\pyvisa\ctwrapper\highlevel.py", line 188, in _return_handler
    raise errors.VisaIOError(ret_value)
pyvisa.errors.VisaIOError: VI_ERROR_TMO (-1073807339): Timeout expired before operation completed.
>>> cl.query("FAULT?")
'2200\n'
>>> 
