#!/usr/bin/python
#coding=utf-8

## agilentAutomator.pytho
##
## Alex Robinson - 2017-04-08

# some much needed libraries
import visa
import sys
import os
import time

# import some home-grown libraries
from selectaroo import menu
from engToFloat import *

rm = visa.ResourceManager()

#thermocoupleSetting = { 'J':["-100", "0", "100", "500", "1000"], 'K':["-100", "0", "100", "500", "1000"], 'T':["-100","0","100","200","390"] }
voltageSetting = { "0.1":["0.0","0.1"], "1.0":["0.0","1.0"], "10.0":["0.0","10.0","-10.0"], "100.0":["0.0","100.0"], "300.0":["0.0","300.0"] }

dataList = []

# GET SONumber - We'll use this for the ouput filename and worksheet name
SONumber = input( "Please enter a Service Order number: " )

# bind resources
#print( "\nPlease select a datalogger from the list of available devices." )
#dataLogger = rm.open_resource(menu(rm.list_resources()))
dataLogger = rm.open_resource('GPIB0::9::INSTR')

#print( "\nPlease select a calibrator from the list of available devices." )
#calibrator = rm.open_resource(menu(rm.list_resources()))
calibrator = rm.open_resource('GPIB0::4::INSTR')

'''
print( "\nPlease select the desired thermocouple type.")
thermocouple = menu(list(thermocoupleSetting.keys()))
temperature = thermocoupleSetting[ thermocouple ]
if calibrator.query("TC_TYPE?").strip('\n') != thermocouple :
    calibrator.write( "TC_TYPE " + thermocouple )
'''

# Take a reading on each channel

print( "\nVerifying DCV Gain" )
calibrator.write("*RST;*WAI")
dataLogger.write("DISP:TEXT 'DC Volts'")

voltageRange = list(voltageSetting.keys())

for channel in range( 217, 221 ) :

    for z in range( len( voltageRange ) ) :
        # configure datalogger
        dataLogger.write("CONF:VOLT:DC "+voltageRange[z]+",(@"+str(channel)+")")
        dataLogger.write("ROUT:SCAN (@"+str(channel)+")")
        # configure calibrator
        setpoints = voltageSetting[voltageRange[z]]
        # for each value in range
        for y in range( len( setpoints ) ) :
            calibrator.write("OUT "+setpoints[y]+" V;*WAI;OPER")
            if setpoints[y] in ["100.0","300.0"] :
                input( "Please verify that the calibrator is in OPER state" )
            time.sleep(5)        
            dataList.append(dataLogger.query("READ?").strip("\n,"))
            print( "Setpoint:",setpoints[y],"Reading:",engToFloat(dataList[-1]))
        calibrator.write("STBY")

## Output our data
f = open( SONumber+".csv", 'a')
output = str(SONumber) +" - " + time.asctime() + "\n"
f.write(output)
for x in range( len( dataList ) ):
    output = str(dataList[x])+",\n"
    f.write(output)
f.close()

## End VISA session gracefully
dataLogger.close()
calibrator.close()
