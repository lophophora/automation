
def engToAbstract(eString) :
    """
    Converts e-notated numbers to a list containing the value
    in abstract representation as per
    Decimal Arithmetic Specification, version 1.07
    Copyright (c) IBM Corporation, 2002. All rights reserved. ©

    Engineering notation per IBM decimal notation conventions:
    "12.3E+7"      [0,123,6]
    "1.23E+3"      [0,123,1]
    "12.3E+7"      [0,123,6]
    "1234.5E-4"    [0,12345,-5]

    """
    abstract = []
    # Sign
    if eString[0] == "+" :
        abstract.append(0)
    elif eString[0] == "-" :
        abstract.append(1)
    else :
        print("This was unexpected.  Illegal character in", estring)
        return "error"

    # Coefficient
    # Count the decimals first
    decimals = (eString.index("E") - eString.index(".")) - 1
    abstract.append(int(eString[1:eString.index("E")].replace(".","")))

    # Exponent
    if eString[-3] == "+" :
        abstract.append((int(eString[-2:]) - decimals) * -1)
    elif eString[-3] == "-" :
        abstract.append((int(eString[-2:]) + decimals) * -1 )
    else :
        print("This was unexpected.  Illegal character in", estring)

    # RETURN [sign, coefficient, exponent]
    return abstract
	
def engToFloat(	

	
	


#readData = "-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,+2.81300000E+01,-8.79210000E+01,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,-9.90000000E+37,+2.89260000E+01,-9.90000000E+37"
readData= "+10.12345E-03,+1.2345678E+03"


dataList = readData.split(",")

for x in range( len( dataList ) ):
	print(engToAbstract(dataList[x]))
