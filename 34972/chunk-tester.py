#!/usr/bin/python
#coding=utf-8

## agilentAutomator.py
##
## Alex Robinson - 2017-04-08

# some much needed libraries
import sys
import os
from selectaroo import menu
from engToFloat import *
import time

# VISA stuff
import visa
rm = visa.ResourceManager()

#from win32com.shell import shell, shellcon
#savePath = shell.SHGetFolderPath(0, shellcon.CSIDL_PERSONAL, None, 0)
dataLogger = rm.open_resource("GPIB0::9::INSTR")
calibrator = rm.open_resource("GPIB0::15::INSTR")
functionGenerator = rm.open_resource("GPIB0::10::INSTR")

#print( "\nPlease select a datalogger from the list of available devices.")
#dataLogger = rm.open_resource(menu(rm.list_resources()))

currentRange = {"0.01":["0.01"],"0.1":["0.1"],"1.0":["0.01", "1.0"]}
voltageRange = ["0.100","1.0","10.0","100.0","300.0"]
ohmRange = ["100","1000","10000","100000","1000000","10000000","100000000"]
acVolts = {"0.1":["1000HZ","50KHZ"],"1":["1000HZ","50KHZ"],"10":["1000HZ","50KHZ","10HZ"],"100":["1000HZ","50KHZ"],"0.01":["1000HZ"],"100":["1000HZ","50KHZ"],"300":["1000HZ"],"195":["50KHZ"]}

readings = []
calibratorOut = []

################################################################################
print( "\nVerifying 4W OHMS" ) 
input( "\nConnect plugs 210 and 220 to the calibrator outputs, and press enter.")
for x in range(len(ohmRange)) :
    dataLogger.write("CONF:FRES "+ohmRange[x]+",(@210)")
    dataLogger.write("ROUT:SCAN (@210)")
    if ohmRange[x] == "100000000" :
        input( "External sense does not work on this range. Please connect both channels to the Output terminals and press enter to continue." )
        calibrator.write("EXTSENSE OFF")
        calibrator.write("OUT "+ohmRange[x]+" OHM")
    else :
        calibrator.write("EXTSENSE ON")
        calibrator.write("OUT "+ohmRange[x]+" OHM")
    calibrator.write("OPER")
    time.sleep(5)
    dataLogger.query("READ?")
    readings.append(dataLogger.query("READ?").strip("\n,"))
    print(engToFloat(readings[-1]))
    calibratorOut.append(calibrator.query("OUT?"))
    print("Calibrator Output: ",calibratorOut[-1])

calibrator.write("STBY")    
calibrator.write("*RST;*WAI")

################################################################################


dataLogger.write("DISP:TEXT 'I love you'")

outputFile = open('chunkoutput.csv', 'a')


outputFile.write( time.asctime() )
for x in readings :
    print( x )
    outputFile.write( x + '\n' )

outputFile.close()
