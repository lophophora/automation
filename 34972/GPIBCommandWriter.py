# 2017-04-04
#
# GPIB Command Writer

def menu ( choiceList ) :
    """ The menu module takes a list of options as an argument, itemizes the
        items in the list, prompts the user for a selection, and returns the
        values of original list as a string.

        The list indexes at zero and the selection menu indexes at one, which
        requires a bit of tricky thinking but is relatively straightforward to
        follow.
    """
    menuChoice = ""
    selectionCount = 0
    choiceCount = 0
    while menuChoice == "" :
        for choiceCount in range( len( choiceList ) ) :
            print( "[", choiceCount + 1, "] ", choiceList[ choiceCount ], sep="" )
        menuSelection = input( "\nPlease enter your selection number: " )
        if menuSelection.isdecimal() and int( menuSelection ) <= choiceCount + 1 and int( menuSelection ) > 0 :
            menuChoice = str( choiceList[ int( menuSelection ) - 1 ] )
        else:
            print( "I'm sorry, that is not a valid selection." )
    return menuChoice



import visa

rm = visa.ResourceManager()

print("Please select the device you would like to test: ")
device = rm.open_resource(menu(rm.list_resources()))

# rm.list_resources()

## This is kind of arbitrary, but works for now
#calibrator = rm.open_resource('GPIB0::4::INSTR')

# Keep it going
commandInput = ""

print("GPIB Command Tester\nType your command at the prompt or 'no more' to quit.")


while commandInput != "no more":
    commandInput = input("Please enter a GPIB command: ")
    print(device.write(commandInput))

# Exit gracefully
device.close()
