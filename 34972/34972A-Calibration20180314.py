#!/usr/bin/python
#coding=utf-8

## agilentAutomator.py
##
## Alex Robinson - 2017-04-08


import time
startTime = time.time()


# some much needed libraries
import sys
import os
#from selectaroo import menu
#from engToFloat import *
#import time

# VISA stuff
import visa
rm = visa.ResourceManager()

dataLogger = ""
calibrator = ""
functionGenerator = ""
option = ""
setup = False

def menu (choiceList) :
    """ The menu module takes a list of options as an argument, itemizes the
        items in the list, prompts the user for a selection, and returns the
        values of original list as a new list.

        The list indexes at zero and the selection menu indexes at one, which
        requires a bit of tricky thinking but is relatively straightforward to
        follow.
    """
    while (True) :
        for n,c in enumerate(choiceList):
            print( "[",n, "] ",c, sep="" )
        menuSelection = ""
        menuSelection = int(input( "\nPlease enter your choice or 'q' to quit: " ))
        if (menuSelection < len(choiceList) and menuSelection >= 0) :
            return str(choiceList[menuSelection])
        elif (menuSelection == "q") :
            print( "\nQuit selected, exiting." )
            print( "Execution Time: ", time.time() - startTime, "seconds.\n")
            sys.exit("10")
        else:
            print( "I'm sorry, that is not a valid selection." )
    

# Insantiate some blanks
#for x in [cal, dlog, fgen]:
#    x = none


#def isCal(idn):
#    if (idn in ['5500A','5520A','5522A','5700A','5720A']):


"""
Also consider:
http://zone.ni.com/reference/en-XX/help/370131S-01/ni-visa/findingvisaresourcesusingregularexpressions/
deviceList = rm.list_resources('(GPIB|USB)?*INSTR')
# With a 5520A
>>> calibrator.query("*IDN?")
'FLUKE,5520A,7380202,3.9+1.3+1.8\n'

"""

print("Scanning available local VISA resources...")
deviceList = list(rm.list_resources())
#deviceList = rm.list_resources('(GPIB|USB)?*INSTR')
#deviceList = rm.list_resources('(visa)?*INSTR')

'''
print("Scanning available GPIB devices...")
for n, dev in enumerate(deviceList):
    #if (dev.startswith("GPIB")):
    try:
        print("Retrieving device information for ", dev)
        info = rm.open_resource(dev)
        idn = info.query("*IDN?",1).strip("\n").split(",")
        print("Found ", idn[0], idn[1])
        dList[n].append([dev,idn[1]])



    except:
        print("Error opening device ", dev)

    finally:
        idn.close()


while (cal and  dlog and fgen = None):



print("Scanning available VISA devices...")
print("Scanning available VISA devices...")
print("Scanning available VISA devices...")
print("Scanning available VISA devices...")
'''
"""
while (setup == False) :
    option = input("Choose 1 for default device options, or 2 for device configuration: ")
    if (option == "2") :
        while(True):
            print( "\nPlease select a datalogger from the list of available devices.")
            dataLogger = rm.open_resource(menu(deviceList))

            dataLogger.write("DISP:TEXT 'Here We Go'")

            print( "\nPlease select a calibrator from the list of available devices.")
            calibrator = rm.open_resource(menu(deviceList))

            print( "\nPlease select a function generator from the list of available devices.")
            functionGenerator = rm.open_resource(menu(deviceList))

            print ("Found the following devices: ")
            for x in [dataLogger, calibrator, functionGenerator]:
                idn = x.query("*IDN?").strip("\n")
                print(idn)



            if (input("Press 'r' to reselect, or any other key to continue") != "r"):
                setup = True
                break



    elif (option == "1") :
        dataLogger = rm.open_resource("GPIB0::9::INSTR")
        calibrator = rm.open_resource("GPIB0::4::INSTR")
        functionGenerator = rm.open_resource("GPIB0::10::INSTR")
        setup = True
    else :
        print("Invalid selection.\n")

"""

dataLogger = rm.open_resource("visa://172.27.0.11/GPIB0::9::INSTR")
calibrator = rm.open_resource("visa://172.27.0.11/GPIB0::15::INSTR")
functionGenerator = rm.open_resource("visa://172.27.0.11/GPIB0::10::INSTR")

dataLogger.timeout = 15000
calibrator.timeout = 15000
currentRange = {0.01:[0.01],
                0.1:[0.1],
                1.0:[0.1,1.0]}


#currentRange = {"0.01":["0.01"],"0.1":["0.1"],"1.0":["0.1", "1.0"]}
#dcCurrentRange = {"0.01":["0.01","-0.01"],"0.1":["0.1","-0.1"],"1.0":["1.0", "-1.0"]}
zeroVoltageRange = ["0.100","1.0","10.0","100.0","300.0"]
voltageRange = ["0.100","-0.100","1.0","-1.0","10.0","1.0","-1.0","-10.0","100.0","-100.0","300.0","-300.0"]
acVolts = {"0.1":["1000 HZ","50 KHZ"],"1.0":["1000 HZ","50 KHZ","300 KHZ"],"10.0":["1000 HZ","50 KHZ","10 HZ"],"100.0":["1000 HZ","50 KHZ"],"300.0":["1000 HZ"],"195.0":["50 KHZ"]}
#acVolts = {"0.1":["1000 HZ","50 KHZ"],"1":["1000 HZ","50 KHZ","300 KHZ"],"10":["10 HZ","1000 HZ"],"1":["1000HZ"],"10":["50 KHZ"],"100":["1000 HZ","50 KHZ"],"300":["1000 HZ"],"195":["50 KHZ"]}

readings = []
calibratorOut = []

calibrator.write("STBY")
calibrator.write("*RST;*WAI")
dataLogger.write("*RST")

################################################################################


def zeroADC():
    """
    DC Current - Zero Offset - channel 221
    """
    zeroAmp = []
    zeroAmp.append("DCA Zero")
    print( "\nVerifying DCA zeros" )
    dataLogger.write("DISP:TEXT 'DCA Zeros'")
    for i, a in enumerate(currentRange.keys()) :
        dataLogger.write("CONF:CURR:DC "+str(a)+",(@221)")
        dataLogger.write("ROUT:CHAN:DEL:AUTO ON,(@221)")
        dataLogger.write("ROUT:SCAN (@221)")
        zeroAmp.append(float(dataLogger.query("READ?",3).strip("\n,")))
        print( "Range:",a,"\tReading:",zeroAmp[-1])
    
    #e2dataLogger.write("*RST")
    return zeroAmp


def zeroDCV():
    dcvZ = []
    dcvZ.append("DCV Zero")
    print( "\nVerifying DCV zeros" )
    ### DC Volts, ch 209
    dataLogger.write("DISP:TEXT 'DCV Zeros'")
    for x in range(len(zeroVoltageRange)) :
        dataLogger.write("CONF:VOLT:DC "+zeroVoltageRange[x]+",(@209)")
        dataLogger.write("ROUT:CHAN:DEL:AUTO ON,(@209)")
        dataLogger.write("ROUT:SCAN (@209)")
        time.sleep(4)
        dcvZ.append(float(dataLogger.query("READ?",2).strip("\n,")))
        #print("Using Meas: ",float(dataLogger.query("MEAS?",1).strip("\n,")))
        print( "Range:",zeroVoltageRange[x],"\tReading:",dcvZ[-1])
    dataLogger.write("*RST")
    return dcvZ

## Add a space

def resZeros(w):
    if (w == 4):
        func = "FRES"
    else:
        func = "RES"
    res = []
    print( "\nVerifying ",w,"W OHMS" )
    dataLogger.write("DISP:TEXT '4W OHMs'")
    dataLogger.write("CONF:"+func+" 100,(@209)")
    dataLogger.write("ROUT:SCAN (@209)")
    for e in range(2,9) :
        ohms = 10 ** e
        dataLogger.write("CONF:"+func+" "+str(ohms)+",(@209)")
        dataLogger.write("ROUT:SCAN (@209)")
        dataLogger.write("ROUT:CHAN:DEL:AUTO ON,(@209)")
        time.sleep(3)
        res.append(float(dataLogger.query("READ?",4).strip("\n,")))
        #print("Using Meas: ",float(dataLogger.query("MEAS?",1).strip("\n,")))
        print( "Setpoint:",e,"Reading:",res[-1])
    return res



def gainDCV ():
    """
    ### Volts DC, gain
    +9.90000000E+37
    """
    while (True):
        dcvG = []
        print( "\nVerifying DCV Gain" )
        dataLogger.write("DISP:TEXT 'DC Volts'")
        input( "Please connect the 210 connection to the calibrator output terminals and press enter." )
        for x in range(len(voltageRange)) :
            dataLogger.write("CONF:VOLT:DC "+voltageRange[x]+",(@210)")
            dataLogger.write("ROUT:MON (@210)")
            dataLogger.write("ROUT:MON:STAT ON")
            dataLogger.write("ROUT:SCAN (@210)")
            dataLogger.write("TRIG:SOUR BUS")
            calibrator.write("OUT "+voltageRange[x]+" V;*WAI;OPER")
            if voltageRange[x] in [ "100.0","300.0","-100.0","-300.0" ] :
                #statusByte = int(calibrator.query("*WAI;ISR?",delay=2).strip("\n"))
                #if (statusByte != 6145) :
                input( "Verify that the calibrator is Operating and press enter" )
                time.sleep(4)
            else :
                time.sleep(4)
            #dataLogger.write("INIT; *WAI;")
            #dataLogger.write("INIT")
            #dataLogger.write("*TRG")

            dataLogger.write("INIT; *TRG")
            #time.sleep(3)
            #readings.append(dataLogger.read("FETC?").strip("\n,"))
            dcvG.append(dataLogger.query("*WAI;FETC?").strip("\n,"))
            #print("Using Meas: ",float(dataLogger.query("MEAS?",1).strip("\n,")))
            print( "Setpoint:",voltageRange[x],"Reading:",float(dcvG[-1]))
            ## append to file, readings

        calibrator.write("STBY")
        calibrator.write("*RST;*WAI")
        dataLogger.write("*RST")

        if (input("Press 'r' to retry, or any other key to continue") != "r"):
            return dcvG




firstRange = {"0.1":["1000 HZ","50 KHZ"],"1.0":["1000 HZ","50 KHZ","300 KHZ"],"10.0":["10 HZ","1000 HZ"]}
secondRange = {"1.0":["1000HZ"],"10.0":["50 KHZ"],"100":["1000 HZ","50 KHZ"],"300":["1000 HZ"]}
thirdRange = {"195":["50 KHZ"]}
#acVolts = {"0.1":["1000 HZ","50 KHZ"],"1":["1000 HZ","50 KHZ","300 KHZ"],"10":["10 HZ","1000 HZ"],"1":["1000HZ"],"10":["50 KHZ"],"100":["1000 HZ","50 KHZ"],"300":["1000 HZ"],"195":["50 KHZ"]}
def gainACV():
    """
    ### Volts AC, gain

    Here's the story, (2017-08-18)
    I had to change the calibration procedure because of the most recent changes to the datasheet.
    Because it is required to set the voltage to 1volts and 10volts more than once, I cannot have
    duplicate entries in teh dictionaries for the setpoints.  Now the setpoints are broken into
    two separate dictionaries and a for loop works through a list of the setpoint ranges within.
    enjoy!
    """
    acvG = []
    print( "\nVerifying ACV Gain" )
    dataLogger.write("DISP:TEXT 'AC Volts'")
    input( "Please connect the 210 plug to the calibrator output and press enter.")

    for voltRange in [firstRange, secondRange, thirdRange]:

        amplitude = list(sorted(voltRange.keys()))
        for x in range(len(amplitude)):
            frequency = voltRange[amplitude[x]]
            ## This should change the range for all but the last setting
            if amplitude != "195" :
                #print("CONF:VOLT:AC "+amplitude[x]+",(@210);*WAI")
                dataLogger.write("CONF:VOLT:AC "+amplitude[x]+",(@210)")
            dataLogger.write("VOLT:AC:BAND 3, (@210)")
            dataLogger.write("ROUT:SCAN (@210)")
            dataLogger.write("ROUT:CHAN:DEL:AUTO ON,(@210)")
            dataLogger.write("ROUT:MON (@210)")
            dataLogger.write("ROUT:MON:STAT ON")
            dataLogger.write("TRIG:SOUR TIMER")
            dataLogger.write("TRIG:TIM 5")
            for y in range(len(frequency)) :
                #print("OUT "+amplitude[x]+" V, "+frequency[0]+";*WAI;OPER")
                calibrator.write("OUT "+amplitude[x]+" V, "+frequency[y]+";*WAI;OPER")
                #if amplitude[x] not in ["0.1","0.01"] and int(amplitude[x]) >= 100 :
                if amplitude[x] not in ["0.1","0.01","1.0","10.0"] and int(amplitude[x]) >= 100 :
                    #statusByte = int(calibrator.query("ISR?", delay=2).strip("\n"))
                    #if (statusByte != 6145) :
                    input( "Please verify that calibrator is in OPER state, and press enter." )
                    time.sleep(4)
                else:
                    #print("Waiting for calibrator to stabilize.",sep="",end="")
                    while(calibrator.query("ISR?").strip("\n") != '6145'):
                            #print(".",sep="",end="")
                            pass
                    time.sleep(2)
                dataLogger.write("INIT")
                #time.sleep(5)
                # For some reason READ? results in error -410, query interrupted.
                #dataLogger.write("*TRG")
                #time.sleep(1)

                acvG.append(dataLogger.query("*WAI;FETC?").strip("\n,"))
                #print("Using Meas: ",float(dataLogger.query("MEAS?",1).strip("\n,")))
                print( "Setpoint:",amplitude[x],"at",frequency[y],"\tReading:",float(acvG[-1]))
    calibrator.write("STBY")
    calibrator.write("*RST;*WAI")
    dataLogger.write("*RST")
    if (input("Press 'r' to retry, or any other key to continue") != "r"):
        return acvG





def fourWire():
    """
    Resistance, 4w

    Note: external sense does not work on any values above 10 Mohm.  EX SNS must
    be disabled and the test leads repositined before taking last value.

    # With a 5520A
    >>> calibrator.query("*IDN?")
    'FLUKE,5520A,7380202,3.9+1.3+1.8\n'

    >>> calibrator.query("*IDN?")[6:10]
    '5520'

    >>> calibrator.query("*IDN?")[6:10]
    '5700'
    """
    while (True):
        res = []
        print( "\nVerifying 4W OHMS" )
        dataLogger.write("DISP:TEXT '4W OHMs'")
        input( "Connect plugs 210 and 220 to the calibrator outputs, and press enter.")
        
        calModel = calibrator.query("*IDN?")[6:10]
        ## For some reason the first reading always comes up erroneous.
        calibrator.write("OUT 100 OHM; *WAI")
        if (calModel in ["5700","5720"]):
            ## 5700
            calibrator.write("EXTSENSE ON")
        if (calModel in ["5500","5520","5522"]):
            ## 5520
            calibrator.write("ZCOMP WIRE4")
        calibrator.write("OPER")
        dataLogger.write("CONF:FRES 100, 3e-5, (@210)")
        dataLogger.write("ROUT:SCAN (@210)")
        dataLogger.query("READ?",4)
        calRes = []
        for e in range(2,9) :
            ohms = 10 ** e
            resolution = round(3e-7 * ohms,6)
            dataLogger.write("CONF:FRES "+str(ohms)+", "+str(resolution)+", (@210)")
            dataLogger.write("ROUT:SCAN (@210)")
            dataLogger.write("ROUT:CHAN:DEL:AUTO ON, (@210)")
            ## 5520
            if (ohms == 1000000 and calModel in ["5500","5520","5522"]):
                input( "Compensation sense does not work on this range. Please connect both channels to the Output terminals and press enter to continue." )
                calibrator.write("ZCOMP NONE")
                calibrator.write("OUT "+str(ohms)+" OHM")
            ## 5700
            if (ohms == 100000000 and calModel in ["5700","5720"]):
                input( "External sense does not work on this range. Please connect both channels to the Output terminals and press enter to continue." )
                calibrator.write("EXTSENSE OFF")
                #calibrator.write("ZCOMP NONE")
                calibrator.write("OUT "+str(ohms)+" OHM")
            else :
               # calibrator.write("EXTSENSE ON")
                #calibrator.write("ZCOMP WIRE4")
                calibrator.write("OUT "+str(ohms)+" OHM")
            calibrator.write("OPER")
            time.sleep(3)
            calRes.append(float(calibrator.query("OUT?").strip("\n").split(",")[0]))
            res.append(float(dataLogger.query("READ?",4).strip("\n,")))
            if (e in [2]):
                tolerance = (calRes[-1] * 0.00004) + (calRes[-1] * 0.0001)
            if (e in [3,4,5,6]):
                 tolerance = (calRes[-1] * 0.00001) + (calRes[-1] * 0.0001)
            if (e in [7]):
                 tolerance = (calRes[-1] * 0.00001) + (calRes[-1] * 0.0004)
            if (e in [8]):
                 tolerance = (calRes[-1] * 0.0001) + (calRes[-1] * 0.008)
            #print("Using Meas: ",float(dataLogger.query("MEAS?",1).strip("\n,")))
            print( "\nSetpoint:",calRes[-1],"Reading:",res[-1],end="")
            if (res[-1] > calRes[-1] - tolerance or res[-1] < calRes[-1] + tolerance):
                print("\t - Out of tolerance",end="")
        calibrator.write("STBY")
        calibrator.write("*RST;*WAI")
        if (input("Press 'r' to retry, or any other key to continue") != "r"):
            return res



def gainADC():
    """
    ### DC Current, Gain
    """
    while(True):
        ampsDC = []
        print( "\nVerifying DC Current Gain" )
        dataLogger.write("DISP:TEXT 'DC Current'")
        input( "Connect plug 221 to the calibrator output, and press enter.")
        for i, a in enumerate(sorted(currentRange.keys())) :
            for b, s in enumerate(currentRange[a]):
                dataLogger.write("CONF:CURR:DC "+str(a)+",(@221)")
                dataLogger.write("ROUT:SCAN (@221)")
                dataLogger.write("ROUT:CHAN:DEL:AUTO ON,(@221)")
                for p in ["+","-"] :
                    calibrator.write("OUT "+p+str(s)+" A;*WAI;OPER")
                    #print("Waiting for calibrator to stabilize.",sep="",end="")
                    while(calibrator.query("ISR?", 2).strip("\n") != '6145'):
                        #print(".",sep="",end="")
                        pass
                    time.sleep(2)
                    #dataLogger.write("INIT;*WAI")
                    ampsDC.append(float(dataLogger.query("READ?",3).strip("\n,")))
                    #ampsDC.append(s)
                    print('Range:{0:<7.2f}Setpoint:{1:>1s}{2:<6.2f}Reading:{3:>10f}'.format(a,p,s,ampsDC[-1]))
        calibrator.write("STBY")
        calibrator.write("*RST;*WAI")
        if (input("Press 'r' to retry, or any other key to continue") != "r"):
            return ampsDC







"""
## AC Current, Gain


"""

def gainAAC():
    """
    ## AC Current, Gain
    """
    while (True):
        ampsAC = []
        print( "\nVerifying AC Current Gain" )
        dataLogger.write("DISP:TEXT 'AC Current'")
        input( "\nConnect plug 221 to the calibrator output, and press enter.")
        for i, a in enumerate(sorted(currentRange.keys())) :
            for b, s in enumerate(currentRange[a]):
                dataLogger.write("CONF:CURR:AC "+str(a)+",(@221)")
                dataLogger.write("CURR:AC:BAND 3,(@221)")
                dataLogger.write("ROUT:SCAN (@221)")
                dataLogger.write("ROUT:CHAN:DEL:AUTO ON,(@221)")
                calibrator.write("OUT "+str(s)+" A, 1000HZ;*WAI;OPER")
                #if (calibrator.query("OPER?", 2).strip("\n") != "1"):
                #    calibrator.write("OPER")
                #while(calibrator.query("ISR?", 2).strip("\n") != '6145'): time.sleep(2)
                #dataLogger.write("INIT;*WAI")
                ampsAC.append(float(dataLogger.query("READ?",4).strip("\n,")))
                #ampsAC.append(s)
                print('Range:{0:<7.2f}Setpoint:{1:<6.2f}Reading:{2:>10f}'.format(a,s,ampsAC[-1]))
        calibrator.write("STBY")
        calibrator.write("*RST;*WAI")
        if (input("Press 'r' to retry, or any other key to continue") != "r"):
            return ampsAC



################################################################################

################################################################################
"""
## Frequency, Gain
# 10 mV, 100 Hz
"""

def gainHz():
    hz = []
    print( "\nVerifying Frequency Gain" )
    dataLogger.write("DISP:TEXT 'Frequency'")
    input( "\nConnect plug 210 to the function generator output, and press enter.")

    dataLogger.write("CONF:FREQ (@210)")
    # In this case, MIN refers to 100 mV
    dataLogger.write("FREQ:VOLT:RANG MIN")
    # 3 Hz Scan rate
    dataLogger.write("FREQ:RANG:LOW 3")
    dataLogger.write("ROUT:SCAN (@210)")

    # 100 MVRMS overloads the unit, and 50mvPP is the lowest available voltage.
    # How about we settle for 100 mV.
    functionGenerator.write("VOLT:UNIT VPP")
    functionGenerator.write("APPL:SIN 100, 0.1")
    time.sleep(1)
    hz.append(float(dataLogger.query("READ?").strip('\n')))
    print( "Setpoint: 100 Reading:",hz[-1])
    #'+9.99944350E+01'

    # 1 V, 100 KHz
    dataLogger.write("FREQ:VOLT:RANG 1")
    functionGenerator.write("APPL:SIN 300E+3, 1.0")
    time.sleep(1)
    hz.append(dataLogger.query("READ?").strip('\n'))
    print( "Setpoint: 300000 Reading:",hz[-1])
    return hz;


def gainHz5520():
    print( "\nVerifying Frequency Gain" )
    dataLogger.write("DISP:TEXT 'Frequency'")
    input( "\nConnect plug 210 to the function generator output, and press enter.")

    dataLogger.write("CONF:FREQ (@210)")
    # In this case, MIN refers to 100 mV
    dataLogger.write("FREQ:VOLT:RANG MIN")
    # 3 Hz Scan rate
    dataLogger.write("FREQ:RANG:LOW 3")
    dataLogger.write("ROUT:SCAN (@210)")
    calibrator.write("OUT 100MV, 300KHZ")
    time.sleep(1)
    readings.append(dataLogger.query("READ?").strip('\n'))
    print( "Setpoint: 100 Reading:",engToFloat(readings[-1]))
    #'+9.99944350E+01'

    # 1 V, 100 KHz
    dataLogger.write("FREQ:VOLT:RANG 1")
    calibrator.write("OUT 100MV, 300KHZ")
    time.sleep(1)
    readings.append(dataLogger.query("READ?").strip('\n'))
    print( "Setpoint: 300000 Reading:",engToFloat(readings[-1]))



#'+1.00000370E+05'
################################################################################

def fileOutput():
    #dataLogger.write("DISP:TEXT 'All Done'")

    fileName = time.strftime("%Y-%m-%d - 34972")+ ".csv"
    f = open( fileName, 'a' )

    f.write( time.strftime("%Y-%m-%d") + "\n")
    for z in range( len( readings ) ) :
        output = str( readings[z]  )+ '\n'
        f.write(output)

    f.write("\nCalibrator resistance output readings:\n")
    for zz in range( len( calibratorOut ) ) :
        output = str( calibratorOut[zz] )+ '\n'
        f.write(output)

    ## Close our output file
    f.close()

######
#   Cleanup section

def cleanup():

    dataLogger.write("DISP:TEXT 'Goodbye'")

    ## set calibrator to standby
    calibrator.write("STBY")

    ## End VISA session gracefully
    dataLogger.close()
    functionGenerator.close()
    calibrator.close()


readings += zeroADC()
readings += zeroDCV()
readings += resZeros(2)
readings += resZeros(4)
readings += gainDCV()
readings += gainACV()
readings += fourWire()
readings += gainADC()
readings += gainAAC()
gainHz()
#-gainHz5520()



print( "\nExecution Time: ", time.time() - startTime, "seconds.\n")
