#!/usr/bin/python
#coding=utf-8

## 34902A-Calibration.py
##
## Alex Robinson - 2017-06-28

# some much needed libraries
import visa
import sys
import os
import time

# CSV is good enough, I gave up on this
#from openpyxl import Workbook

# import some home-grown libraries
from selectaroo import menu
from engToFloat import *

rm = visa.ResourceManager()

thermocoupleSetting = { 'J':["-100", "0", "100", "500", "1000"], 'K':["-100", "0", "100", "500", "1000"], 'T':["-100","0","100","200","390"] }

dataList = []

# GET SONumber - We'll use this for the ouput filename and worksheet name
SONumber = input( "Please enter a Service Order number: " )

# bind resources
print( "\nPlease select a datalogger from the list of available devices." )
dataLogger = rm.open_resource(menu(rm.list_resources()))
#dataLogger = rm.open_resource('GPIB0::9::INSTR')

print( "\nPlease select a calibrator from the list of available devices." )
calibrator = rm.open_resource(menu(rm.list_resources()))
#calibrator = rm.open_resource('GPIB0::4::INSTR')

print( "\nPlease select the desired thermocouple type.")
thermocouple = menu(list(thermocoupleSetting.keys()))

temperature = thermocoupleSetting[ thermocouple ]

# Make certain the calibrator is in thermocouple mode
if calibrator.query("TC_TYPE?").strip('\n') != thermocouple :
    calibrator.write( "TC_TYPE " + thermocouple )

# Configure the datalogger
dataLogger.write("CONF:TEMP TCouple, "+thermocouple+", 10, (@201:216)")
print( "Please connect all 34902A channels to the 5520", end="" )
input( "" )

# Take a reading for each temperature
for x in range( len( temperature) ) : 
    calibrator.write("OUT "+str(temperature[x])+" CEL;OPER")
    time.sleep(6)
    dataList.append(dataLogger.query("READ?").strip('\n').split(","))
    print( "\nSetpoint:", temperature[x], "C" )
    for y in range( len( dataList[x] ) ) :
        print("Reading ", y, ": ", engToFloat(dataList[x][y]), sep="")

## set calibrator to standby
calibrator.write("STBY")

## Output our data
f = open( SONumber+".csv", 'a' )
output = str( SONumber ) +" - " + time.asctime() + "\n"
f.write(output)
## For each channel
for x in range(16) :
    # For each setpoint
    for y in range(5) :
        output = str(engToFloat(dataList[y][x]))+",\n"
        f.write(output)
f.close()

## End VISA session gracefully
dataLogger.close()
calibrator.close()
