#!/usr/bin/python
#coding=utf-8

## agilentAutomator.pytho
##
## Alex Robinson - 2017-04-08

# some much needed libraries
import visa
import sys
import os
#from openpyxl import Workbook

# import some home-grown libraries
from selectaroo import menu

rm = visa.ResourceManager()


thermocoupleSetting = { 'J':[-100, 0 , 100, 500, 1000], 'K':[-100, 0 , 100, 500, 1000], 'T':[-100,0,100,200,390] }




# GET SONumber - We'll use this for the ouput filename and worksheet name
SONumber = input( "Please enter a Service Order number: " )

# bind resources
print( "\nPlease select a datalogger from the list of available devices.")
dataLogger = rm.open_resource(menu(rm.list_resources()))

print( "\nPlease select a calibrator from the list of available devices.")
calibrator = rm.open_resource(menu(rm.list_resources()))

print( "\nPlease select the desired thermocouple type.")
thermocouple = menu(list(thermocoupleSetting.keys()))

# configure data logger

dataLogger.write("UNIT:TEMP " + thermocouple + ",@(201:220)")
dataLogger.write("UNIT:TEMP C")

## configure calibrator
# set calibrator to desired output
# need to figure out how to pass a string to gpib commands for TC type.
calibrator.write("*RST;TC_TYPE J;OUT -100C;OPER;*WAI")
print("Thermocouple type: ", calibrator.query("TC_TYPE?"))
print("Status: ", calibrator.query("OUT?")

# set calibrator to operate

## wait

## read from data logger
dataList[x] = dataLogger.query("READ?").split(",")

## set calibrator to next setpoint

## set calibrator to standby
calibrator.write("STBY")

f = open( SONumber+".csv", 'a')
    output = str(SONumber)+"\n"
    f.write(output)
for x in range( len( dataList ) ):
    output = str(dataList[x])+","
    f.write(output)
f.close()


## End VISA session gracefully
dataLogger.close()
calibrator.close()
