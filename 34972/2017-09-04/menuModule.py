def menuModule (choiceList, maximumChoices, minimumChoices ) :
    """ The menu module takes a list of options as an argument, itemizes the
        items in the list, prompts the user for a selection, and returns the
        values of original list as a new list. The minimum and maximum
        arguments ensue that the user makes the correct number of selecitons.

        The list indexes at zero and the selection menu indexes at one, which
        requires a bit of tricky thinking but is relatively straightforward to
        follow.

        My super genius idea involved setting the first element of each list to
        it's appropriate category ( protien, wrapper, topping, sauce ) and then
        using the list[0] value to prompt the user for that type of selection
        which in turn eliminates the need for any math trickiness and results
        in the user selection index matching the choiceList index exactly.
        What a stroke of genius.

        What ultimately changed my mind on the matter is that it makes the
        menuModule needlessly more complex.  I can simply place a print
        statement before the main execution which calls the menuModule.  The
        idea was to make just the menuModule generic, not the entire script.
    """
    menuChoices = [ ]
    selectionCount = 0
    choiceCount = 0
    while selectionCount < maximumChoices :
        for choiceCount in range( len( choiceList ) ) :
            print( "[", choiceCount + 1, "] ", choiceList[ choiceCount ], sep="" )
        menuSelection = ""
        menuSelection = input( "\nPlease enter your choice or 'q' to quit: " )
        if menuSelection.isdecimal() and int( menuSelection ) <= choiceCount + 1 and int( menuSelection ) > 0 :
            menuChoices.append( str( choiceList[ int( menuSelection ) - 1 ] ) )
            selectionCount += 1
        elif menuSelection == 'q' :
            if selectionCount >= minimumChoices :
                return menuChoices
            else :
                print( "I'm sorry, you must choose at least", minimumChoices, "option(s)." )
        else:
            print( "I'm sorry, that is not a valid selection." )
    return menuChoices