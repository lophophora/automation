#!/usr/bin/python
#coding=utf-8

## agilentAutomator.py
##
## Alex Robinson - 2017-04-08

# some much needed libraries
import visa
import sys
import os
from openpyxl import Workbook

# import some home-grown libraries
from selectaroo import menu

rm = visa.ResourceManager()

'''
#thermocoupleSetting = { 'J':[-100, 0 , 100, 500, 1000], 'K':[-100, 0 , 100, 500, 1000], 'T':[-100,0,100,200,390] }
print( "\nPlease select the desired thermocouple type.")
thermocouple = menu(list(thermocoupleSetting.keys()))
'''

DISP:TEXT 'WAITING...'

# GET SONumber - We'll use this for the ouput filename and worksheet name
SONumber = input( "Please enter a Service Order number: " )

# bind resources
print( "\nPlease select a datalogger from the list of available devices.")
dataLogger = rm.open_resource(menu(rm.list_resources()))
dataLogger.write("DISP:TEXT 'WAITING FOR SELECTIONS'")

print( "\nPlease select a calibrator from the list of available devices.")
calibrator = rm.open_resource(menu(rm.list_resources()))

print( "\nPlease select a calibrator from the list of available devices.")
functionGenerator = rm.open_resource(menu(rm.list_resources()))


currentRange = ["0.01","0.1","1.0"]
voltageRange = ["0.100","1.0","10.0","100.0","300.0"]
ohmRange = ["100","1000","10000","100000","1000000","10000000","100000000"]
acVolts = {"0.1":["1000HZ","50KHZ"],"1":["1000HZ","50KHZ"],"10":["1000HZ","50KHZ","10HZ"],"100":["1000HZ","50KHZ"],"0.01":["1000HZ"],"100":["1000HZ","50KHZ"],"300":["1000HZ"],"195":["50KHZ"]}



## Zero Offsets
## DC Current, ch 221
for x in range(len(currentRange)) :
    dataLogger.write("CONF:CURR:DC "+currentRange[x]+",(@221)")
    dataLogger.write("ROUT:SCAN (@221)")
    readings = dataLogger.query("READ?").split(",")
    ## append to file, readings

## DC Volts, ch 209
for x in range(len(voltageRange)) :
    dataLogger.write("CONF:VOLT:DC "+voltageRange[x]+",{@209)")
    dataLogger.write("ROUT:SCAN (@209)")
    readings = dataLogger.query("READ?").split(",")
    ## append to file, readings

    
## Resistance, 2w and 4w, ch 209
for x in range(len(ohmRange)) :
    dataLogger.write("CONF:RES "+ohmRange[x]+",{@209)")
    dataLogger.write("ROUT:SCAN (@209)")
    readings = dataLogger.query("READ?").split(",")

for x in range(len(ohmRange)) :
    dataLogger.write("CONF:FRES "+ohmRange[x]+",{@209)")
    dataLogger.write("ROUT:SCAN (@209)")
    readings = dataLogger.query("READ?").split(",")

## Now we get the calibrator involved
calibrator.write("*RST;*WAI")


### Volts DC, gain
for x in range(len(voltageRange)) :
    dataLogger.write("CONF:VOLT:DC "+voltageRange[x]+",{@210)")
    dataLogger.write("ROUT:SCAN (@210)")
    calibrator.write("OUT "+voltagerange[x]+"V;*WAI;OPER")
    readings = dataLogger.query("READ?;*WAI").split(",")
    calibrator.write("STBY")                 
    ## append to file, readings

## Volts AC, gain
amplitude = list(acVolts.keys())
for x in range(len(amplitude)):
    frequency = acVolts[amplitude]
    ## This should change the range for all but the last setting
    if amplitude != "195" : dataLogger.write("CONF:VOLT:AC "+amplitude[x]+",(@210)")
    dataLogger.write("VOLT:AC:BAND 3,(@210)")
    dataLogger.write("ROUT:SCAN (@210)")  
    for y in range(len(frequency)) :
        calibrator.write("OUT "+amplitude[x]+"V, "+frequency+"HZ;*WAI;OPER")
        readings = dataLogger.query("READ?;*WAI").split(",")
    calibrator.write("STBY")
 

## Resistance, 4w


## DC Current, Gain

## AC Current, Gain

## Frequency, Gain



    
# configure data logger

dataLogger.write("UNIT:TEMP " + thermocouple + ",@(201:220)")
dataLogger.write("UNIT:TEMP C")

## configure calibrator
# set calibrator to desired output
# need to figure out how to pass a string to gpib commands for TC type.
calibrator.write("*RST;*WAI")
print("Thermocouple type: ", calibrator.query("TC_TYPE?"))
print("Status: ", calibrator.query("OUT?")

# set calibrator to operate

## wait

## read from data logger
dataList = dataLogger.query("READ?").split(",")

## set calibrator to next setpoint

## set calibrator to standby
calibrator.write("STBY")

## End VISA session gracefully
dataLogger.close()
calibrator.close()
