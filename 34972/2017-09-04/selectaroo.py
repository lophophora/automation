def menu ( choiceList ) :
    """ The menu module takes a list of options as an argument, itemizes the
        items in the list, prompts the user for a selection, and returns the
        values of original list as a new list.

        The list indexes at zero and the selection menu indexes at one, which
        requires a bit of tricky thinking but is relatively straightforward to
        follow.
    """
    menuChoice = " "
    while menuChoice = " " :
        for x in range( len( choiceList ) ) :
            print( "[", choiceCount + 1, "] ", choiceList[ x ], sep="" )
        menuSelection = ""
        menuSelection = input( "\nPlease enter your choice or 'q' to quit: " )
        if menuSelection.isdecimal() and int( menuSelection ) <= x + 1 and int( menuSelection ) > 0 :
            menuChoice = ( str( choiceList[ int( menuSelection ) - 1 ] ) )
        else:
            print( "I'm sorry, that is not a valid selection." )
    return menuChoice
