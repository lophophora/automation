#!/usr/bin/python
#coding=utf-8

## agilent-thermocouple.py
##
## Alex Robinson - 2017-04-08

# some much needed libraries
import visa
import sys
import os
import time

# import some home-grown libraries
from selectaroo import menu
from engToFloat import *

rm = visa.ResourceManager()

#thermocoupleSetting = { 'J':["-100", "0", "100", "500", "1000"], 'K':["-100", "0", "100", "500", "1000"], 'T':["-100","0","100","200","390"] }
#dcVolts = {"0.1":["0.0","0.1"], "1.0":["0.0","1.0"], "10.0":["0.0","10.0","-10.0"], "100.0":["0.0","100.0"], "300":["0.0","300.0"]}

acVoltRange = ["0.100", "1.0", "10.0", "100.0", "300.0"]
acVoltSetpoint = {"0.100":["0.01","0.100","0.100"], "1.0":["1.0","1.0","1.0","1.0","1.0","1.0"], "10.0":["0.100","1.0","10.0","10.0","10.0"], "100.0":["100.0","100.0"], "300.0":["195.0","300.0"]}
acVoltsFrequency = {"0.100":["1 KHZ","1 KHZ","50 KHZ"], "1.0":["20 HZ","1 KHZ","20 KHZ","50 KHZ","100 KHZ","300 KHZ"], "10.0":["1 KHZ","1 KHZ","10 HZ","1 KHZ","50 KHZ"], "100.0":["1 KHZ","50 KHZ"], "300.0":["50 KHZ","1 KHZ"]}

repeatIndex = 0

dataList = []

# GET SONumber - We'll use this for the ouput filename and worksheet name
SONumber = input( "Please enter a Service Order number: " )

# bind resources
print( "\nPlease select a datalogger from the list of available devices." )
dataLogger = rm.open_resource(menu(rm.list_resources()))
#dataLogger = rm.open_resource('GPIB0::9::INSTR')

print( "\nPlease select a calibrator from the list of available devices." )
calibrator = rm.open_resource(menu(rm.list_resources()))
#calibrator = rm.open_resource('GPIB0::4::INSTR')

print( "Please connect all datalogger channels to the 5520", end="" )
input( "" )

counter = 0;

for x in acVoltRange :
    print("Verifying", x, "volt range.")
    # Configure the datalogger.  100 volts reading is taken on every channel.

    # Using 'x'
    amplitude = list(acVoltSetpoint[x])
    frequency = list(acVoltsFrequency[x])

    #dataLogger.write("CONF:VOLT:AC "+x+", (@201)")


    # The amplitude and frequency dictionary value lists are of the same length.
    # By iterating through the indicies numerically, we can ensure the correct frequency is used.
    for y in range( len(amplitude) ) :
        dataLogger.write("CONF:VOLT:AC "+x+", (@201)")
        calibrator.write("OUT "+amplitude[y]+" V, "+frequency[y]+";")
        # verify not high voltage
        if x in [ "100.0","300.0" ] :
            input( "Verify that the calibrator is operating and press enter" )
            if amplitude[y] == "100.0" and frequency[y] == "1 KHZ" :
                repeatIndex = counter;
                dataLogger.write("CONF:VOLT:AC "+x+", (@201:220)")
            #print ( "this one", counter)
            calibrator.write("OPER")              
            # wait
            time.sleep(5)
            dataLogger.write("INIT;*WAI")
            # For some reason READ? results in error -410, query interrupted.
            time.sleep(10)
            dataList.append(dataLogger.query("FETC?;*WAI").strip("\n,"))
            time.sleep(10)
            
        else :
            calibrator.write("OPER")
            time.sleep(5)
            dataList.append(dataLogger.query("READ?").strip("\n,"))

              

        print( "Setpoint:",amplitude[y],"@",frequency[y]," Reading:",engToFloat(dataList[-1]))
        counter += 1
        
# Turn off the juice        
calibrator.write("STBY")
# Modify the repeated voltage readings into a list
dataList[repeatIndex] = dataList[repeatIndex].split(",")
print( repeatIndex )

'''  
## Output our data
f = open( SONumber+".csv", 'a' )
output = str( SONumber ) +" - " + time.asctime() + "\n"
f.write(output)

"""
11 readings for channel 201,
20 readings in readings[5] for 10 volt measurement
"""

# Channel 1 readings
for z in range(len(dataList)):
    # convert to milliamps
    if z < 3 :
        output = str(engToFloat(dataList[z]*1000))+",\n"
    if z == repeatIndex :
        output = str(engToFloat(dataList[z][0]))+",\n"
    else :                 
        output = str(engToFloat(dataList[z]))+",\n"
    f.write(output)

# 100 volt readings
for w in range( 1, len(dataList[repeatIndex]) ) :
    output = str(engToFloat(dataList[repeatIndex][w]))+",\n"
    f.write(output)
    
f.close()
dataLogger.close()
calibrator.close()

'''


