




import visa
rm = visa.ResourceManager()

cal = rm.open_resource('ASRL1::INSTR',\
                       read_termination='\n',\
                       write_termination='\n')


testpoint = [0.04,0.1,0.2,0.4,1,2,4,10,20,40,0.04,0.1,0.2,0.4,1,2,4,10,20,40,0.018,0.045,0.090,0.180,0.450,0.900,1.800,4.500,9.000,18.000,0.018,0.045,0.090]
voltrange = [0.180,0.450,0.900,1.800,4.500,9.000,18.000]

#This is looped through twice.  Perhaps positive and negative?
## dc offset
gain = [0.018,0.045,0.090,0.180,0.450,0.900,1.800,4.500,9.000,18.000]
pgain = [0.800,0.800,0.800,0.800,8.000,8.000,8.000,30.000,25.000,15.000]


cmd = ["oper",
       "out 0.02v, 50kHz",
       "out 0.02v, 50.1mHz",
       "out 0.02v, 50kHz",
       "out 0.02v, 100.1mHz",
       "out 0.2v, 50kHz",
       "out 0.2v, 50.1mHz",
       "out 0.2v, 50kHz",
       "out 0.2v, 100.1mHz",
       "out 1v, 50kHz",
       "out 1v, 50.1mHz",
       "out 1v, 50kHz",
       "out 1v, 125.1mHz",
       "out 5v, 50kHz",
       "out 5v, 50.1mHz",
       "out 5v, 50kHz",
       "out 5v, 125.1mHz",
       "out 0.5v, 50kHz",
       "out 0.5v, 20.1MHz",
       "out 0.5v, 50kHz",
       "out 0.5v, 25.1MHz",
       "stby"]


for n, c in enumerate(cmd):
    cal.write(c)
    print("\nNext value: ", cmd[n+1])
    if (input("Press enter or N to quit..") == "n"):
        cal.write(cmd[-1])
        break;


