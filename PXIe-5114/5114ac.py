



import time
import visa
rm = visa.ResourceManager()

cal = rm.open_resource('ASRL1::INSTR',\
                       read_termination='\n',\
                       write_termination='\n')


testpoint = [0.04,0.1,0.2,0.4,1,2,4,10,20,40,0.04,0.1,0.2,0.4,1,2,4,10,20,40,0.018,0.045,0.090,0.180,0.450,0.900,1.800,4.500,9.000,18.000,0.018,0.045,0.090]
voltrange = [0.180,0.450,0.900,1.800,4.500,9.000,18.000]

#This is looped through twice.  Perhaps positive and negative?
## dc offset
gain = [0.018,0.045,0.090,0.180,0.450,0.900,1.800,4.500,9.000,18.000]
pgain = [0.800,0.800,0.800,0.800,8.000,8.000,8.000,30.000,25.000,15.000]


def timedGain():
    gain = [0.018,0.045,0.090,0.180,0.450,0.900,1.800,4.500,9.000,18.000]
    for n, g in enumerate(gain):
        val = g
        for x in iter(["+","-"]):
            out = "OUT "+x+str(val)+"V"
            print ("\nSetpoint: ", out)
            cal.write(out)
            timer = time.time()
            t = 10
            while(time.time() < timer + 10.0):
                print(t,end="")
                t = t - 1
                time.sleep(1)
            print("")

def calAdjust():
    gain = [18,9,4.5,1.8,0.9,0.45,0.18,0.09,0.045,0.018]
    for x in iter(["+","-"]):
        for n, g in enumerate(gain):
            val = g
            out = "OUT "+x+str(val)+"V"
            print ("\nSetpoint: ", out)
            cal.write(out)
            timer = time.time()
            t = 10
            while(time.time() < timer + 10.0):
                print(t,end="")
                t = t - 1
                time.sleep(1)
            print("")
            
def verticalGain():
    gain = [0.018,0.045,0.090,0.180,0.450,0.900,1.800,4.500,9.000,18.000]
    for n, g in enumerate(gain):
        quitloop = False
        retry = True
        while(retry) :
            val = g
            for x in range(2):
                out = "OUT "+str(val)+"V"
                if ( x % 2 == 1):
                    out = "OUT -"+str(val)+"V"
                #print ("Setpoint ",n," Calibrator ", out)
                cal.write(out)
                print ("\nNext setpoint: ", out)
                userinput = input("\n(Enter) to proceed,(p)revious,(q)uit..")
                #if (input("\n(Enter) to proceed,(r)retry,(p)revious,(q)uit..") == "n"):
                if (userinput == ""):
                    retry = False
                elif (userinput.isletter()) :
                    if (userinput == "q"):
                        quitloop = True
                        break
                    elif (userinput == "p"):
                        val = pgain[n-1]
                    elif (userinput == "r") :
                        break
    
                elif (userinput.isnumeric()):
                    out = "OUT "+str(userinput)+"V"
                else:
                    retry = False
                    
                
            ## end for
            if (quitloop == True and input("Retry reading (y or n)? ") != "y"):
                retry = False
            else :
                quitloop = False
                retry = False
        ## End while
        if (quitloop == True):
            break
          
        ## end main for
        
#calAdjust()
timedGain()