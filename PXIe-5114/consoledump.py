Python 3.4.4 (v3.4.4:737efcadf5a6, Dec 20 2015, 19:28:18) [MSC v.1600 32 bit (Intel)] on win32
Type "copyright", "credits" or "license()" for more information.
>>> import visa
>>> rm = visa.ResourceManager()
>>> rm.list_resources()
('ASRL1::INSTR', 'ASRL10::INSTR')
>>> cal = rm.open_resource('ASRL1::INSTR')
>>> cal.query("*IDN?")
Traceback (most recent call last):
  File "<pyshell#4>", line 1, in <module>
    cal.query("*IDN?")
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 407, in query
    return self.read()
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 332, in read
    message = self.read_raw().decode(enco)
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 306, in read_raw
    chunk, status = self.visalib.read(self.session, size)
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\ctwrapper\functions.py", line 1582, in read
    ret = library.viRead(session, buffer, count, byref(return_count))
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\ctwrapper\highlevel.py", line 188, in _return_handler
    raise errors.VisaIOError(ret_value)
pyvisa.errors.VisaIOError: VI_ERROR_TMO (-1073807339): Timeout expired before operation completed.
>>> cal.query("*IDN?",3)
Traceback (most recent call last):
  File "<pyshell#5>", line 1, in <module>
    cal.query("*IDN?",3)
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 407, in query
    return self.read()
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 332, in read
    message = self.read_raw().decode(enco)
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 306, in read_raw
    chunk, status = self.visalib.read(self.session, size)
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\ctwrapper\functions.py", line 1582, in read
    ret = library.viRead(session, buffer, count, byref(return_count))
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\ctwrapper\highlevel.py", line 188, in _return_handler
    raise errors.VisaIOError(ret_value)
pyvisa.errors.VisaIOError: VI_ERROR_TMO (-1073807339): Timeout expired before operation completed.
>>> cal.close
<bound method SerialInstrument.close of <'SerialInstrument'('ASRL1::INSTR')>>
>>> cal.close()
>>> cal = rm.open_resource('ASRL1::INSTR')
>>> cal.query("*IDN?\n",1)
Traceback (most recent call last):
  File "<pyshell#9>", line 1, in <module>
    cal.query("*IDN?\n",1)
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 407, in query
    return self.read()
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 332, in read
    message = self.read_raw().decode(enco)
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 306, in read_raw
    chunk, status = self.visalib.read(self.session, size)
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\ctwrapper\functions.py", line 1582, in read
    ret = library.viRead(session, buffer, count, byref(return_count))
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\ctwrapper\highlevel.py", line 188, in _return_handler
    raise errors.VisaIOError(ret_value)
pyvisa.errors.VisaIOError: VI_ERROR_TMO (-1073807339): Timeout expired before operation completed.
>>> cal.read_termination
>>> cal.read_termination = "\r"
>>> cal.write_termination
'\r\n'
>>> cal.write_termination
'\r\n'
>>> cal.write_termination = '\n'
>>> cal.write_termination
'\n'
>>> cal.query("*IDN?",1)
Traceback (most recent call last):
  File "<pyshell#16>", line 1, in <module>
    cal.query("*IDN?",1)
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 407, in query
    return self.read()
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 332, in read
    message = self.read_raw().decode(enco)
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 306, in read_raw
    chunk, status = self.visalib.read(self.session, size)
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\ctwrapper\functions.py", line 1582, in read
    ret = library.viRead(session, buffer, count, byref(return_count))
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\ctwrapper\highlevel.py", line 188, in _return_handler
    raise errors.VisaIOError(ret_value)
pyvisa.errors.VisaIOError: VI_ERROR_TMO (-1073807339): Timeout expired before operation completed.
>>> cal.lock
<bound method SerialInstrument.lock of <'SerialInstrument'('ASRL1::INSTR')>>
>>> cal.unlock()
Traceback (most recent call last):
  File "<pyshell#18>", line 1, in <module>
    cal.unlock()
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\resource.py", line 371, in unlock
    self.visalib.unlock(self.session)
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\ctwrapper\functions.py", line 1727, in unlock
    return library.viUnlock(session)
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\ctwrapper\highlevel.py", line 188, in _return_handler
    raise errors.VisaIOError(ret_value)
pyvisa.errors.VisaIOError: VI_ERROR_SESN_NLOCKED (-1073807204): The current session did not have a lock on the resource.
>>> cal.close()
>>> cal = rm.open_resource('ASRL1::INSTR', read_termination='\n', write_termination='\n
		       
SyntaxError: EOL while scanning string literal
>>> cal = rm.open_resource('ASRL1::INSTR', read_termination='\n', write_termination='\n')
>>> cal.bau
Traceback (most recent call last):
  File "<pyshell#22>", line 1, in <module>
    cal.bau
AttributeError: 'SerialInstrument' object has no attribute 'bau'
>>> cal.baud_rate
9600
>>> cal.write_termination
'\n'
>>> cal.read_termination
'\n'
>>> cal.interface_type
<InterfaceType.asrl: 4>
>>> cal.query("OUT?")
'9.00000E-01,V,0E+00,0,0.00E+00'
>>> cal.query("OUT -")
Traceback (most recent call last):
  File "<pyshell#28>", line 1, in <module>
    cal.query("OUT -")
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 407, in query
    return self.read()
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 332, in read
    message = self.read_raw().decode(enco)
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 306, in read_raw
    chunk, status = self.visalib.read(self.session, size)
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\ctwrapper\functions.py", line 1582, in read
    ret = library.viRead(session, buffer, count, byref(return_count))
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\ctwrapper\highlevel.py", line 188, in _return_handler
    raise errors.VisaIOError(ret_value)
pyvisa.errors.VisaIOError: VI_ERROR_TMO (-1073807339): Timeout expired before operation completed.
>>> cal.write("MULT -1")
(8, <StatusCode.success: 0>)
>>> cal.write("OPER")
(5, <StatusCode.success: 0>)
>>> cal.write("MULT -1")
(8, <StatusCode.success: 0>)
>>> cal.write("OUT 1.8V")
(9, <StatusCode.success: 0>)
>>> gain = [0.018,0.045,0.090,0.180,0.450,0.900,1.800,4.500,9.000,18.000,0.018,0.045,0.090,0.180,0.450,0.900,1.800,4.500,9.000,18.000]
>>> cal.write("MULT -1")
(8, <StatusCode.success: 0>)
>>> cal.write("OUT 4.5V")
(9, <StatusCode.success: 0>)
>>> cal.write("MULT -1")
(8, <StatusCode.success: 0>)
>>> cal.write("OUT 9V")
(7, <StatusCode.success: 0>)
>>> cal.write("MULT -1")
(8, <StatusCode.success: 0>)
>>> cal.write("OUT 18V")cal.write("MULT -1")
SyntaxError: invalid syntax
>>> cal.write("OUT 18V")
(8, <StatusCode.success: 0>)
>>> cal.write("MULT -1")
(8, <StatusCode.success: 0>)
>>> cal.write("OUT 0.8V")
(9, <StatusCode.success: 0>)
>>> cal.write("OPER")
(5, <StatusCode.success: 0>)
>>> proggain = [0.800,0.800,0.800,0.800,8.000,8.000,8.000,30.000,25.000,15.000]
>>> while (input("next value? (n to quit)...: ") != "n") :
	val = 0
	c.write("OUT "+str(proggain[val])+"V")
	if (input("next value?" == "n") :
	    
SyntaxError: invalid syntax
>>> while (input("next value? (n to quit)...: ") != "n") :
	val = 0
	c.write("OUT "+str(proggain[val])+"V")
	if (input("next value?") == "n"):
		break
	val++;
	
SyntaxError: invalid syntax
>>> while (input("next value? (n to quit)...: ") != "n") :
	val = 0
	c.write("OUT "+str(proggain[val])+"V")
	if (input("next value?") == "n"):
		break
	val++
	
SyntaxError: invalid syntax
>>> while (input("next value? (n to quit)...: ") != "n") :
	val = 0
	c.write("OUT "+str(proggain[val])+"V")
	if (input("next value?") == "n"):
		break
	val = val + 1

	
next value? (n to quit)...: 
Traceback (most recent call last):
  File "<pyshell#55>", line 3, in <module>
    c.write("OUT "+str(proggain[val])+"V")
NameError: name 'c' is not defined
>>> while (input("next value? (n to quit)...: ") != "n") :
	val = 0
	cal.write("OUT "+str(proggain[val])+"V")
	if (input("next value?") == "n"):
		break
	val = val + 1

	
next value? (n to quit)...: 
(9, <StatusCode.success: 0>)
next value?n
>>> cal.write("MULT -1")
(8, <StatusCode.success: 0>)
>>> cal.write("MULT -1")
(8, <StatusCode.success: 0>)
>>> for x in range(2):
	if (x % 2 = 0):
		
SyntaxError: invalid syntax
>>> for x in range(2):
	if (x % 2 == 0):
		print(x," equals ", x % 2)
	else :
		print(x," equals ", x % 2)

		
0  equals  0
1  equals  1
>>> while (input("next value? (n to quit)...: ") != "n") :
	val = 0
	for x in range(2):
		out = str(proggain[val])
		if (x % 2 == 1):
			out = "-"+out
		cal.write("OUT "+out+"V")
		if (input("next value?") == "n"):
			break
	val = val + 1

	
next value? (n to quit)...: 
(9, <StatusCode.success: 0>)
next value?
(10, <StatusCode.success: 0>)
next value?n
next value? (n to quit)...: n
>>> while (input("next value? (n to quit)...: ") != "n") :
	val = 0
	for x in range(2):
		out = str(proggain[val])
		if (x % 2 == 1):
			out = "-"+out
		cal.write("OUT "+out+"V")
		if (input("next value?") == "n"):
			break
	val = val + 1
	print("next value ",val,end="")

	
next value? (n to quit)...: 
(9, <StatusCode.success: 0>)
next value?
(10, <StatusCode.success: 0>)
next value?
next value  1next value? (n to quit)...: n
>>> loop = True
>>> while (loop == True) :
	val = 0
	for x in range(2):
		out = str(proggain[val])
		if (x % 2 == 1):
			out = "-"+out
		cal.write("OUT "+out+"V")
		if (input("next value?") == "n"):
			loop = False
	val = val + 1
	print("next value ",val,end="")

	
(9, <StatusCode.success: 0>)
next value?
(10, <StatusCode.success: 0>)
next value?
next value  1(9, <StatusCode.success: 0>)
next value?n
(10, <StatusCode.success: 0>)
next value?n
next value  1
>>> while (loop == True) :
	val = 0
	for x in range(2):
		out = str(proggain[val])
		if (x % 2 == 1):
			out = "-"+out
		cal.write("OUT "+out+"V")
		if (input("next value?") == "n"):
			loop = False
			break
	val = val + 1
	print("next value ",proggain[val],end="")

	
>>> loop = True;while (loop == True) :
	val = 0
	for x in range(2):
		out = str(proggain[val])
		if (x % 2 == 1):
			out = "-"+out
		cal.write("OUT "+out+"V")
		if (input("next value?") == "n"):
			loop = False
			break
	val = val + 1
	print("next value ",proggain[val],end="")
	
SyntaxError: invalid syntax
>>> loop = True; while (loop == True) :
	val = 0
	for x in range(2):
		out = str(proggain[val])
		if (x % 2 == 1):
			out = "-"+out
		cal.write("OUT "+out+"V")
		if (input("next value?") == "n"):
			loop = False
			break
	val = val + 1
	print("next value ",proggain[val],end="")
	
SyntaxError: invalid syntax
>>> loop = True
>>> while (loop == True) :
	val = 0
	for x in range(2):
		out = str(proggain[val])
		if (x % 2 == 1):
			out = "-"+out
		cal.write("OUT "+out+"V")
		if (input("next value?") == "n"):
			loop = False
			break
	val = val + 1
	print("next value ",proggain[val],end="")

	
(9, <StatusCode.success: 0>)
next value?
(10, <StatusCode.success: 0>)
next value?
next value  0.8(9, <StatusCode.success: 0>)
next value?
(10, <StatusCode.success: 0>)
next value?n
next value  0.8
>>> loop = True
>>> while (loop == True) :
	val = 0
	for x in range(2):
		out = str(proggain[val])
		if (x % 2 == 1):
			out = "-"+out
		cal.write("OUT "+out+"V")
		if (input("next value?") == "n"):
			loop = False
			break
	val = val + 1
	print("next value ",proggain[val],end="")

	
(9, <StatusCode.success: 0>)
next value?
(10, <StatusCode.success: 0>)
next value?
next value  0.8(9, <StatusCode.success: 0>)
next value?
(10, <StatusCode.success: 0>)
next value?
next value  0.8(9, <StatusCode.success: 0>)
next value?
(10, <StatusCode.success: 0>)
next value?
next value  0.8(9, <StatusCode.success: 0>)
next value?n
next value  0.8
>>> cal.write("MULT -1")
(8, <StatusCode.success: 0>)
>>> cal.write("MULT -1")
(8, <StatusCode.success: 0>)
>>> cal.write("MULT -1")
(8, <StatusCode.success: 0>)
>>> cal.write("MULT -1")
(8, <StatusCode.success: 0>)
>>> cal.write("MULT -1")
(8, <StatusCode.success: 0>)
>>> cal.write("MULT -1")
(8, <StatusCode.success: 0>)
>>> cal.write("MULT -1")
(8, <StatusCode.success: 0>)
>>> cal.write("MULT -1")
(8, <StatusCode.success: 0>)
>>> cal.write("STBY")
(5, <StatusCode.success: 0>)
>>> cal.query("OUT?")
'2.000E-01,V,0E+00,0,5.0E+04'
>>> cal.query("UNCERT?")
'0E+00,0E+00,0,0E+00,0E+00,0'
>>> cal.query("scope?")
'LEVSINE'
>>> cal.write("*OPC")
(5, <StatusCode.success: 0>)
>>> cal.query("OPC?")
Traceback (most recent call last):
  File "<pyshell#98>", line 1, in <module>
    cal.query("OPC?")
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 407, in query
    return self.read()
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 332, in read
    message = self.read_raw().decode(enco)
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 306, in read_raw
    chunk, status = self.visalib.read(self.session, size)
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\ctwrapper\functions.py", line 1582, in read
    ret = library.viRead(session, buffer, count, byref(return_count))
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\ctwrapper\highlevel.py", line 188, in _return_handler
    raise errors.VisaIOError(ret_value)
pyvisa.errors.VisaIOError: VI_ERROR_TMO (-1073807339): Timeout expired before operation completed.
>>> cal.query("*OPC?")
'1'
>>> cal.query("*OPC?")
'1'
>>> cal.write("*OPC;OPER")
(10, <StatusCode.success: 0>)
>>> cal.query("*OPC?")
'1'
>>> cal.query("OUT?")
'2.000E-01,V,0E+00,0,5.010E+07'
>>> cal.write("EDIT PRI")
(9, <StatusCode.success: 0>)
>>> cal.query("EDIT PRI?")
Traceback (most recent call last):
  File "<pyshell#105>", line 1, in <module>
    cal.query("EDIT PRI?")
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 407, in query
    return self.read()
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 332, in read
    message = self.read_raw().decode(enco)
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 306, in read_raw
    chunk, status = self.visalib.read(self.session, size)
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\ctwrapper\functions.py", line 1582, in read
    ret = library.viRead(session, buffer, count, byref(return_count))
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\ctwrapper\highlevel.py", line 188, in _return_handler
    raise errors.VisaIOError(ret_value)
pyvisa.errors.VisaIOError: VI_ERROR_TMO (-1073807339): Timeout expired before operation completed.
>>> cal.query("EDIT?")
'OFF'
>>> cal.write("EDIT PRI")
(9, <StatusCode.success: 0>)
>>> cal.query("EDIT?")
'PRI'
>>> cal.query("ERR?")
'320,"Ohms zero needed every 12 hrs"'
>>> cal.query("*STB?")
'8'
>>> cal.query("*PUD?")
'#200'
>>> cal.query("*dbmz?")
Traceback (most recent call last):
  File "<pyshell#112>", line 1, in <module>
    cal.query("*dbmz?")
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 407, in query
    return self.read()
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 332, in read
    message = self.read_raw().decode(enco)
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\resources\messagebased.py", line 306, in read_raw
    chunk, status = self.visalib.read(self.session, size)
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\ctwrapper\functions.py", line 1582, in read
    ret = library.viRead(session, buffer, count, byref(return_count))
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\site-packages\pyvisa\ctwrapper\highlevel.py", line 188, in _return_handler
    raise errors.VisaIOError(ret_value)
pyvisa.errors.VisaIOError: VI_ERROR_TMO (-1073807339): Timeout expired before operation completed.
>>> cal.query("dbmz?")
'Z600'
>>> cal.query("zcomp?")
'NONE'
>>> cal.query("refout?")
'0E+00'
>>> cal.write("out 2.0e-1v, 5.0e-4 Hz")
(23, <StatusCode.success: 0>)
>>> cal.write("out 0.2v, 50kHz")
(16, <StatusCode.success: 0>)
>>> cal.write("out 0. 2v, 50kHz")
(17, <StatusCode.success: 0>)
>>> cal.query("OUT?")
'2.000E-01,V,0E+00,0,5.0E+04'
>>> cal.write("out 0.02v, 100.1mHz")
(20, <StatusCode.success: 0>)
>>> cal.write("out 0.02v, 100100000.00hz")
(26, <StatusCode.success: 0>)
>>> cal.write("out 0.2v, 50kHz")
(16, <StatusCode.success: 0>)
>>> cal.write("out 0.2v, 50.1mHz")
(18, <StatusCode.success: 0>)
>>> cal.write("out 0.2v, 50kHz")
(16, <StatusCode.success: 0>)
>>> cal.write("out 1v, 50kHz")
(14, <StatusCode.success: 0>)
>>> cal.write("out 1v, 5001kHz")
(16, <StatusCode.success: 0>)
>>> cal.write("out 1v, 50.1mHz")
(16, <StatusCode.success: 0>)
>>> cal.write("out 1v, 50kHz")
(14, <StatusCode.success: 0>)
>>> cal.write("out 1v, 125.1mHz")
(17, <StatusCode.success: 0>)
>>> cal.write("out 5v, 50kHz")
(14, <StatusCode.success: 0>)
>>> cal.write("out 5v, 50.1mHz")
(16, <StatusCode.success: 0>)
>>> cal.write("out 5v, 50kHz")
(14, <StatusCode.success: 0>)
>>> cal.write("out 5v, 125.1mHz")
(17, <StatusCode.success: 0>)
>>> cal.write("out 0.55v, 50kHz")
(17, <StatusCode.success: 0>)
>>> cal.write("out 0.5v, 50kHz")
(16, <StatusCode.success: 0>)
>>> cal.write("out 0.5v, 20.1MHz")
(18, <StatusCode.success: 0>)
>>> cal.write("out 0.5v, 50kHz")
(16, <StatusCode.success: 0>)
>>> cal.write("out 0.5v, 25.1MHz")
(18, <StatusCode.success: 0>)
>>> cal.write("OUT 0.018V")
(11, <StatusCode.success: 0>)
>>> gain = [0.018,0.045,0.090,0.180,0.450,0.900,1.800,4.500,9.000,18.000]
>>> enumerate(list(gain))
<enumerate object at 0x031F1030>
>>> gain
[0.018, 0.045, 0.09, 0.18, 0.45, 0.9, 1.8, 4.5, 9.0, 18.0]
>>> enumerate(gain)
<enumerate object at 0x031E6FD0>
>>> for i in enumerate(list(gain)):
	print (i)

	
(0, 0.018)
(1, 0.045)
(2, 0.09)
(3, 0.18)
(4, 0.45)
(5, 0.9)
(6, 1.8)
(7, 4.5)
(8, 9.0)
(9, 18.0)
>>> test = enumerate(gain))
SyntaxError: invalid syntax
>>> test = enumerate(gain)
>>> test
<enumerate object at 0x031E6BC0>
>>> tst[1]
Traceback (most recent call last):
  File "<pyshell#150>", line 1, in <module>
    tst[1]
NameError: name 'tst' is not defined
>>> test[1]
Traceback (most recent call last):
  File "<pyshell#151>", line 1, in <module>
    test[1]
TypeError: 'enumerate' object is not subscriptable
>>> test.__next__
<method-wrapper '__next__' of enumerate object at 0x031E6BC0>
>>> test.__str__
<method-wrapper '__str__' of enumerate object at 0x031E6BC0>
>>> print(test.__str__)
<method-wrapper '__str__' of enumerate object at 0x031E6BC0>
>>> for a, b in enumerate(gain):
	print (a, b)

	
0 0.018
1 0.045
2 0.09
3 0.18
4 0.45
5 0.9
6 1.8
7 4.5
8 9.0
9 18.0
>>> cal.close()
>>> rm.close)()
SyntaxError: invalid syntax
>>> rm.close()
>>> 
========== RESTART: C:/Users/arobinso.TRANSINC/Desktop/5114vgain.py ==========
Setpoint  0  Calibrator output:  0.018  voltsTraceback (most recent call last):
  File "C:/Users/arobinso.TRANSINC/Desktop/5114vgain.py", line 29, in <module>
    if (input("Enter to proceed, n to quit..",end="")):
TypeError: input() takes no keyword arguments
>>> 
========== RESTART: C:/Users/arobinso.TRANSINC/Desktop/5114vgain.py ==========
Setpoint  0  Calibrator output:  0.018  volts
Enter to proceed, n to quit..
Setpoint  0  Calibrator output:  0.018  volts
Enter to proceed, n to quit..
Setpoint  0  Calibrator output:  0.018  volts
Enter to proceed, n to quit..
Setpoint  0  Calibrator output:  0.018  volts
Enter to proceed, n to quit..
Setpoint  0  Calibrator output:  0.018  volts
Enter to proceed, n to quit..
Setpoint  0  Calibrator output:  0.018  volts
Enter to proceed, n to quit..
Setpoint  0  Calibrator output:  0.018  volts
Enter to proceed, n to quit..
========== RESTART: C:/Users/arobinso.TRANSINC/Desktop/5114vgain.py ==========
Setpoint  0  Calibrator output:  0.018  volts
Enter to proceed, n to quit..
Setpoint  0  Calibrator output:  0.018  volts
Enter to proceed, n to quit..
Setpoint  1  Calibrator output:  0.045  volts
Enter to proceed, n to quit..n
Setpoint  2  Calibrator output:  0.09  volts
Enter to proceed, n to quit..
Traceback (most recent call last):
  File "C:/Users/arobinso.TRANSINC/Desktop/5114vgain.py", line 29, in <module>
    if (input("Enter to proceed, n to quit..") == "n"):
  File "C:\Users\arobinso.TRANSINC\Desktop\Python34\lib\idlelib\PyShell.py", line 1386, in readline
    line = self._line_buffer or self.shell.readline()
KeyboardInterrupt
>>> 
========== RESTART: C:/Users/arobinso.TRANSINC/Desktop/5114vgain.py ==========
Setpoint  0  Calibrator output:  0.018  volts
Enter to proceed, n to quit..n
>>> 
========== RESTART: C:/Users/arobinso.TRANSINC/Desktop/5114vgain.py ==========
Setpoint  0  Calibrator output:  0.018  volts
Enter to proceed, n to quit..
Setpoint  0  Calibrator output:  0.018  volts
Enter to proceed, n to quit..n
>>> 
========== RESTART: C:/Users/arobinso.TRANSINC/Desktop/5114vgain.py ==========
Setpoint  0  Calibrator output:  0.018  volts
Enter to proceed, n to quit..
Setpoint  0  Calibrator output:  0.018  volts
Enter to proceed, n to quit..
========== RESTART: C:/Users/arobinso.TRANSINC/Desktop/5114vgain.py ==========
Setpoint  0  Calibrator output:  0.018  volts
Enter to proceed, n to quit..
Setpoint  0  Calibrator output:  0.018  volts
Enter to proceed, n to quit..
Setpoint  0  Calibrator output:  0.018  volts
Enter to proceed, n to quit..
Setpoint  0  Calibrator output:  0.018  volts
Enter to proceed, n to quit..
========== RESTART: C:/Users/arobinso.TRANSINC/Desktop/5114vgain.py ==========
Setpoint  0  Calibrator output:  0.018  volts
Enter to proceed, n to quit..n
Retry reading (y)?n
>>> 
========== RESTART: C:/Users/arobinso.TRANSINC/Desktop/5114vgain.py ==========
Setpoint  0  Calibrator output:  0.018  volts
Enter to proceed, n to quit..n
Retry reading (y)?y
Setpoint  0  Calibrator output:  0.018  volts
Enter to proceed, n to quit..
Setpoint  0  Calibrator output:  0.018  volts
Enter to proceed, n to quit..
Retry reading (y)?
========== RESTART: C:/Users/arobinso.TRANSINC/Desktop/5114vgain.py ==========
Setpoint  0  Calibrator  OUT 0.018V
Enter to proceed, n to quit..
Setpoint  0  Calibrator  OUT -0.018V
Enter to proceed, n to quit..
Setpoint  1  Calibrator  OUT 0.045V
Enter to proceed, n to quit..
Setpoint  1  Calibrator  OUT -0.045V
Enter to proceed, n to quit..
Setpoint  2  Calibrator  OUT 0.09V
Enter to proceed, n to quit..
Setpoint  2  Calibrator  OUT -0.09V
Enter to proceed, n to quit..
Setpoint  3  Calibrator  OUT 0.18V
Enter to proceed, n to quit..
Setpoint  3  Calibrator  OUT -0.18V
Enter to proceed, n to quit..
Setpoint  4  Calibrator  OUT 0.45V
Enter to proceed, n to quit..
Setpoint  4  Calibrator  OUT -0.45V
Enter to proceed, n to quit..
Setpoint  5  Calibrator  OUT 0.9V
Enter to proceed, n to quit..
Setpoint  5  Calibrator  OUT -0.9V
Enter to proceed, n to quit..
Setpoint  6  Calibrator  OUT 1.8V
Enter to proceed, n to quit..
Setpoint  6  Calibrator  OUT -1.8V
Enter to proceed, n to quit..
Setpoint  7  Calibrator  OUT 4.5V
Enter to proceed, n to quit..
Setpoint  7  Calibrator  OUT -4.5V
Enter to proceed, n to quit..
Setpoint  8  Calibrator  OUT 9.0V
Enter to proceed, n to quit..
Setpoint  8  Calibrator  OUT -9.0V
Enter to proceed, n to quit..
Setpoint  9  Calibrator  OUT 18.0V
Enter to proceed, n to quit..
Setpoint  9  Calibrator  OUT -18.0V
Enter to proceed, n to quit..
>>> 
========== RESTART: C:/Users/arobinso.TRANSINC/Desktop/5114vgain.py ==========
Setpoint  0  Calibrator  OUT 0.018V
Enter to proceed, n to quit..
Setpoint  0  Calibrator  OUT -0.018V
Enter to proceed, n to quit..
Setpoint  1  Calibrator  OUT 0.045V
Enter to proceed, n to quit..
Setpoint  1  Calibrator  OUT -0.045V
Enter to proceed, n to quit..
Setpoint  2  Calibrator  OUT 0.09V
Enter to proceed, n to quit..
Setpoint  2  Calibrator  OUT -0.09V
Enter to proceed, n to quit..
Setpoint  3  Calibrator  OUT 0.18V
Enter to proceed, n to quit..
Setpoint  3  Calibrator  OUT -0.18V
Enter to proceed, n to quit..
Setpoint  4  Calibrator  OUT 0.45V
Enter to proceed, n to quit..
Setpoint  4  Calibrator  OUT -0.45V
Enter to proceed, n to quit..
Setpoint  5  Calibrator  OUT 0.9V
Enter to proceed, n to quit..
Setpoint  5  Calibrator  OUT -0.9V
Enter to proceed, n to quit..
Setpoint  6  Calibrator  OUT 1.8V
Enter to proceed, n to quit..
Setpoint  6  Calibrator  OUT -1.8V
Enter to proceed, n to quit..
Setpoint  7  Calibrator  OUT 4.5V
Enter to proceed, n to quit..
Setpoint  7  Calibrator  OUT -4.5V
Enter to proceed, n to quit..
Setpoint  8  Calibrator  OUT 9.0V
Enter to proceed, n to quit..
Setpoint  8  Calibrator  OUT -9.0V
Enter to proceed, n to quit..
Setpoint  9  Calibrator  OUT 18.0V
Enter to proceed, n to quit..
Setpoint  9  Calibrator  OUT -18.0V
Enter to proceed, n to quit..
>>> 
========== RESTART: C:/Users/arobinso.TRANSINC/Desktop/5114vgain.py ==========

Next setpoint:  OUT 0.8V

(Enter) to proceed,(p)revious,(q)uit..
Traceback (most recent call last):
  File "C:/Users/arobinso.TRANSINC/Desktop/5114vgain.py", line 54, in <module>
    if (quitter == True and input("Retry reading (y or n)? ") != "y"):
NameError: name 'quitter' is not defined
>>> 
========== RESTART: C:/Users/arobinso.TRANSINC/Desktop/5114vgain.py ==========

Next setpoint:  OUT 0.8V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT -0.8V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT 0.8V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT -0.8V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT 0.8V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT -0.8V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT 0.8V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT -0.8V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT 8.0V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT -8.0V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT 8.0V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT -8.0V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT 8.0V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT -8.0V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT 30.0V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT -30.0V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT 25.0V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT -25.0V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT 15.0V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT -15.0V

(Enter) to proceed,(p)revious,(q)uit..
>>> 
>>> 
========== RESTART: C:/Users/arobinso.TRANSINC/Desktop/5114vgain.py ==========

Next setpoint:  OUT 0.8V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT -0.8V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT 0.8V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT -0.8V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT 0.8V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT -0.8V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT 0.8V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT -0.8V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT 8.0V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT -8.0V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT 8.0V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT -8.0V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT 8.0V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT -8.0V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT 30.0V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT -30.0V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT 25.0V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT -25.0V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT 15.0V

(Enter) to proceed,(p)revious,(q)uit..

Next setpoint:  OUT -15.0V

(Enter) to proceed,(p)revious,(q)uit..
>>> 
========== RESTART: C:/Users/arobinso.TRANSINC/Desktop/5114vgain.py ==========

Next value:  out 0. 2v, 50kHz
Press enter or N to quit..
========== RESTART: C:/Users/arobinso.TRANSINC/Desktop/5114vgain.py ==========

Next value:  out 0.02v, 50kHz
Press enter or N to quit..

Next value:  out 0.02v, 50.1mHz
Press enter or N to quit..

Next value:  out 0.02v, 50kHz
Press enter or N to quit..n
>>> 
========== RESTART: C:/Users/arobinso.TRANSINC/Desktop/5114vgain.py ==========

Next value:  out 0.02v, 50kHz
Press enter or N to quit..

Next value:  out 0.02v, 50.1mHz
Press enter or N to quit..

Next value:  out 0.02v, 50kHz
Press enter or N to quit..

Next value:  out 0.02v, 100.1mHz
Press enter or N to quit..

Next value:  out 0.2v, 50kHz
Press enter or N to quit..

Next value:  out 0.2v, 50.1mHz
Press enter or N to quit..

Next value:  out 0.2v, 50kHz
Press enter or N to quit..

Next value:  out 0.2v, 100.1mHz
Press enter or N to quit..

Next value:  out 1v, 50kHz
Press enter or N to quit..

Next value:  out 1v, 50.1mHz
Press enter or N to quit..

Next value:  out 1v, 50kHz
Press enter or N to quit..

Next value:  out 1v, 125.1mHz
Press enter or N to quit..

Next value:  out 5v, 50kHz
Press enter or N to quit..

Next value:  out 5v, 50.1mHz
Press enter or N to quit..

Next value:  out 5v, 50kHz
Press enter or N to quit..

Next value:  out 5v, 125.1mHz
Press enter or N to quit..

Next value:  out 0.5v, 50kHz
Press enter or N to quit..

Next value:  out 0.5v, 20.1MHz
Press enter or N to quit..

Next value:  out 0.5v, 50kHz
Press enter or N to quit..

Next value:  out 0.5v, 25.1MHz
Press enter or N to quit..

Next value:  stby
Press enter or N to quit..
Traceback (most recent call last):
  File "C:/Users/arobinso.TRANSINC/Desktop/5114vgain.py", line 49, in <module>
    print("\nNext value: ", cmd[n+1])
IndexError: list index out of range
>>> 
