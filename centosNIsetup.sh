#!/bin/bash
#!/bin/bash
# The following are the commands required before the NI-VISA drivers will install
# on scientific linux or centOS
yum install libXinerama.i686 mesa-libGL.i686 kernel-devel
yum upgrade libstdc++
yum install libstdc++.i686
yum upgrade zlib
yum install zlib.686
cd /var/lib/nikal/3.10.0-693.17.1.el7.x86_64/nikal
cd /var/lib/nikal/3.10.0-693.el7.x86_64/nikal/
vim nikal.c 
updateNIDrivers 

yum install pip36
yum install python36-devel
python3.6 -m ensurepip
python3.6 -m pip install psutil
python3.6 -m pip install spyder
python3.6 -m pip install numpy
python3.6 -m pip install pydaqmx
python3.6 -m pip install pyvisa
python3.6 -m pip install nidmm
