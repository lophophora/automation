
#!/usr/bin/python3
# coding=utf-8

"""
Alexander Robinson - 2017-04-26

NI 9207 24-bit Analog Input Module calibration verification routine

Wire a DB37F pins 1 to 8 in parallel (+) and pins 20 to 27 in parallel (-)
for voltage tests, with the calibrator guard conneced to any COM (9,10,28,29)

Use individual sockets on pins 11 to 18 (+) to any COM (9,10,28,29) for
current tests.
"""

from PyDAQmx import *
from PyDAQmx.DAQmxTypes import *
import numpy
import time

## I'm really glad I wrote this..
from selectaroo import menu

import visa

# Set up the visa session with the calibrator. I hope you're using a Fluke 5700A
# as specified by the calibration datasheet.
rm = visa.ResourceManager()
print( "\nPlease select a calibrator from the list of available devices.")
## Here's the menu in all of it's glory.
calibrator = rm.open_resource(menu(rm.list_resources()))

# Reset the calibrator
calibrator.write("*RST;*WAI")

# Declarations galore
device = "cDAQ1Mod1"
voltageSetpoints = ["10.0", "0.0", "-10.0"]
currentSetpoints = ["0.021", "0.0", "-0.021"]
read = int32()
highResVolts = numpy.zeros((8,5), dtype=numpy.float64)
highSpeedVolts = numpy.zeros((8,75), dtype=numpy.float64)
highResAmps = numpy.zeros((5,), dtype=numpy.float64)
highSpeedAmps = numpy.zeros((75,), dtype=numpy.float64)
taskHandle = TaskHandle(0)
highResVoltsAverage = []
highSpeedVoltsAverage = []
highResAmpsAverage = []
highSpeedAmpsAverage = []

input("Connect the NI-9207 for voltage measurements and press enter to begin.")

for i in range( len( voltageSetpoints ) ) :

    print( "Setting calibrator to", voltageSetpoints[i], "volts." )

    calibrator.write("OUT "+voltageSetpoints[i]+"V;*WAI;OPER")

    

    ## Voltage High Resolution
    print("Acquiring High Resolution Voltage Readings")
    time.sleep(3)
    sampleCount = 40

    try:
        DAQmxCreateTask("",byref(taskHandle))
        DAQmxCreateAIVoltageChan(taskHandle, 'cDAQ1Mod1/ai0:7', "", DAQmx_Val_Cfg_Default, -10, 10, DAQmx_Val_Volts,None)
        DAQmxCfgSampClkTiming(taskHandle, "", 2.0, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, sampleCount)
        DAQmxSetAIADCTimingMode(taskHandle, 'cDAQ1Mod1/ai0:7', DAQmx_Val_HighResolution)
        DAQmxStartTask(taskHandle)
        DAQmxReadAnalogF64(taskHandle, -1, -1, DAQmx_Val_GroupByChannel, highResVolts, sampleCount, byref(read), None)
        DAQmxWaitUntilTaskDone(taskHandle, -1)

    except DAQError as err:
        print ("DAQmx Error: %s", err )

    finally:
        if taskHandle:
            DAQmxStopTask(taskHandle)
            DAQmxClearTask(taskHandle)

    for j in range( 8 ):
        highResVoltsAverage.append(numpy.average(highResVolts[j]))
        #print( "Channel", j, ":", numpy.average(highResVolts[j]))

    #print(highResVoltsAverage)
    

    ## Voltage High Speed
    print("Acquiring High Speed Voltage Readings")
    time.sleep(3)
    sampleCount = 600

    try:
        DAQmxCreateTask("",byref(taskHandle))
        DAQmxCreateAIVoltageChan(taskHandle, 'cDAQ1Mod1/ai0:7', "", DAQmx_Val_Cfg_Default, -10, 10, DAQmx_Val_Volts,None)
        DAQmxCfgSampClkTiming(taskHandle, "", 20.0, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, sampleCount)
        DAQmxSetAIADCTimingMode(taskHandle, 'cDAQ1Mod1/ai0:7', DAQmx_Val_HighSpeed)
        DAQmxStartTask(taskHandle)
        DAQmxReadAnalogF64(taskHandle, -1, 30.0, DAQmx_Val_GroupByChannel, highSpeedVolts, sampleCount, byref(read), None)
        DAQmxWaitUntilTaskDone(taskHandle, -1)

    except DAQError as err:
        print ("DAQmx Error: %s", err )

    finally:
        if taskHandle:
            DAQmxStopTask(taskHandle)
            DAQmxClearTask(taskHandle)

    for j in range( 8 ):
        highSpeedVoltsAverage.append(numpy.average(highSpeedVolts[j]))
        #print( "Channel", j, ":", numpy.average(highSpeedVolts[j]))

calibrator.write("STBY")

## Current Measuremnets.
"""
6 readings per channel, 8 channels, starting from ai8 - ai15
75 readings per highspeed
5 readings per highres
"""

for k in range( 8, 16 ) :
    calibrator.write("STBY")
    print( "\nConnect the calibrator to ai", k, " (Pin ", k+3 , ") and COM", sep="" )
    input( "Press enter to continue." )

    for h in range( len( currentSetpoints ) ) :
        calibrator.write("OUT "+currentSetpoints[h]+"A;*WAI")
        calibrator.write("OPER")
		# Let the output signal stabilize.  Modify this to query the 
		# ESR register for stabilization instead of just a timed delay.
        time.sleep(2)

        print("Acquiring High Resolution Current Readings")
        sampleCount = 5

        channel = device + "/ai" + str(k)
        
        try:
            DAQmxCreateTask("",byref(taskHandle))
            DAQmxCreateAICurrentChan(taskHandle, channel, "", DAQmx_Val_RSE, -0.02, 0.02, DAQmx_Val_Amps, DAQmx_Val_Internal, 0.0, None)
            DAQmxCfgSampClkTiming(taskHandle, "", 2.0, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, sampleCount)
            DAQmxSetAIADCTimingMode(taskHandle, channel, DAQmx_Val_HighResolution)
            DAQmxStartTask(taskHandle)
            DAQmxReadAnalogF64(taskHandle, -1, -1, DAQmx_Val_GroupByChannel, highResAmps, sampleCount, byref(read), None)
            DAQmxWaitUntilTaskDone(taskHandle, -1)

        except DAQError as err:
            print ("DAQmx Error: %s", err )

        finally:
            if taskHandle:
                DAQmxStopTask(taskHandle)
                DAQmxClearTask(taskHandle)

        highResAmpsAverage.append(numpy.average(highResAmps))

        print("Acquiring High Speed Current Readings")
        sampleCount = 75
        
        try:
            DAQmxCreateTask("",byref(taskHandle))
            DAQmxCreateAICurrentChan(taskHandle, channel, "", DAQmx_Val_RSE, -0.02, 0.02, DAQmx_Val_Amps, DAQmx_Val_Internal, 0.0, None)
            DAQmxCfgSampClkTiming(taskHandle, "", 20.0, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, sampleCount)
            DAQmxSetAIADCTimingMode(taskHandle, channel, DAQmx_Val_HighSpeed)
            DAQmxStartTask(taskHandle)
            DAQmxReadAnalogF64(taskHandle, -1, -1, DAQmx_Val_GroupByChannel, highSpeedAmps, sampleCount, byref(read), None)
            DAQmxWaitUntilTaskDone(taskHandle, -1)

        except DAQError as err:
            print ("DAQmx Error: %s", err )

        finally:
            if taskHandle:
                DAQmxStopTask(taskHandle)
                DAQmxClearTask(taskHandle)

        highSpeedAmpsAverage.append(numpy.average(highSpeedAmps))

# Shut it off
calibrator.write("STBY")


## Let the data dump begin..

outputFile = open('NI-9207 output.csv', 'w')

print("")

for x in range( 8 ) :
    print(highResVoltsAverage[x], "\n", highResVoltsAverage[x+8], "\n", highResVoltsAverage[x+16],sep="")
    output = str(highResVoltsAverage[x]) + "\n" + str(highResVoltsAverage[x+8]) + "\n" + str(highResVoltsAverage[x+16]) + "\n"
    outputFile.write(output)
    print(highSpeedVoltsAverage[x], "\n", highSpeedVoltsAverage[x+8], "\n", highSpeedVoltsAverage[x+16],"\n",sep="")
    output = str(highSpeedVoltsAverage[x]) + "\n" + str(highSpeedVoltsAverage[x+8]) + "\n" + str(highSpeedVoltsAverage[x+16]) + "\n"
    outputFile.write(output)


for y in range( 0, len( highResAmpsAverage ), 3) :
    print(highResAmpsAverage[y], "\n", highResAmpsAverage[y+1], "\n", highResAmpsAverage[y+2],sep="")
    output = str(highResAmpsAverage[y]) + "\n" + str(highResAmpsAverage[y+1]) + "\n" + str(highResAmpsAverage[y+2]) + "\n"
    outputFile.write(output)
    print(highSpeedAmpsAverage[y], "\n", highSpeedAmpsAverage[y+1], "\n", highSpeedAmpsAverage[y+2],"\n",sep="")
    output = str(highSpeedAmpsAverage[y]) + "\n" + str(highSpeedAmpsAverage[y+1]) + "\n" + str(highSpeedAmpsAverage[y+2]) + "\n"
    outputFile.write(output)
   
# Session cleanup
outputFile.close()
calibrator.close()
#EOF
