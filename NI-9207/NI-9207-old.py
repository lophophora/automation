#!/usr/bin/python3
# coding=utf-8

#

import PyDAQmx

from PyDAQmx.DAQmxTypes import *
import numpy
import selectaroo
import visa

def channelAverage(array, sammples, channels) :
    averages = []
    ## easy solution using numpy and slices
    for x in range( channels ) :
        averages.append( numpy.average( array[ ( samples * x ) : ( samples * x ) + samples ] ) )
    return averages




read = int32()
highRes = numpy.zeros((40,), dtype=numpy.float64)
highSpeed = numpy.zeros((600,), dtype=numpy.float64)

sampleCount = 40


taskHandle = TaskHandle(0)

DAQmxCreateTask("",byref(taskHandle))

DAQmxCreateAIVoltageChan(taskHandle, 'cDAQ1Mod1/ai0:7', "", DAQmx_Val_Cfg_Default, -10, 10, DAQmx_Val_Volts,None)

DAQmxCfgSampClkTiming(taskHandle, "", 2.0, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, sampleCount)

DAQmxSetAIADCTimingMode(taskHandle, 'cDAQ1Mod1/ai0:7', DAQmx_Val_HighResolution)

DAQmxStartTask(taskHandle)

DAQmxReadAnalogF64(taskHandle, -1, 15.0, DAQmx_Val_GroupByChannel, highRes, sampleCount, byref(read), None)

DAQmxStopTask(taskHandle)

DAQmxClearTask(taskHandle)


channelCount = 8


averages = []

for x in range( len( array / channelCount )):
    averages[x] = numpy.average(data[ x *( sampleCount / channelCount):x *( sampleCount / channelCount) + ( sampleCount / channelCount)]


>>> averages = []
>>> for x in range( int(len( array) / int(channelCount)) - 2):
	averages.append(numpy.average(array[ int(x *( sampleCount / channelCount)):int(x *( sampleCount / channelCount) + ( sampleCount / channelCount))]))

	
>>> for x in averages:
	print(x)

	
0.968711787
0.972865411
0.971628734
0.985752515
0.969713276
0.946134214
0.915557229
0.922246444             


'''
array([ 0.78091922,  1.4800826 ,  2.06426289,  2.64188229,  1.73289538,
        0.89496255,  0.07625247,  0.00512648,  0.005336  ,  0.00539799,
        0.91639333,  1.497163  ,  2.13332342,  2.59776236,  1.67660332,
        0.83611036,  0.05537091,  0.00514507,  0.00532236,  0.00545998,
        1.05823123,  1.53855171,  2.20968994,  2.52156196,  1.60431194,
        0.7446855 ,  0.02334004,  0.00518971,  0.00534344,  0.00538187,
        1.16361472,  1.6337306 ,  2.29232973,  2.47333217,  1.56167354,
        0.68511919,  0.03156223,  0.00516491,  0.00545378,  0.00554428,
        1.1486593 ,  1.64498158,  2.30443739,  2.38892569,  1.51049556,
        0.64236053,  0.03878517,  0.00592117,  0.00609102,  0.00647535,
        1.14498336,  1.65297442,  2.31583838,  2.29194788,  1.43907079,
        0.57867073,  0.02190066,  0.00526161,  0.00542278,  0.00527153,
        1.17541492,  1.65677061,  2.31732363,  2.15902399,  1.33920306,
        0.48427785,  0.00808334,  0.005093  ,  0.00522442,  0.00515747,
        1.28970993,  1.76598874,  2.41261287,  2.08346332,  1.25165251,
        0.39570448,  0.0063365 ,  0.00553808,  0.00582075,  0.00563726])
"""

