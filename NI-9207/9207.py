### OMG DaqMX time

import numpy
from PyDAQmx import *
import visa


#Calibrator Stuff

rm = visa.ResourceManager()
cal = rm.open_resource('GPIB0::4::INSTR')

read = 0
data = numpy.zeros((1000,), dtype=numpy.float64)

taskHandle = Task()
taskHandle.DAQmxLoadTask(b'9207-HighRes')
taskHandle.DAQmxStartTask()

#5520
cal.write("OUT 10V;*WAI;OPER")

taskHandle.DAQmxReadAnalogF64 (5,10.0,DAQmx_Val_GroupByChannel,data,40,byref(read),None)
print ("Acquired %d points",read)

taskHandle.DAQmxStopTask()
taskHandle.DAQmxClearTask()
