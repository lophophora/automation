#!/usr/bin/python3
# coding=utf-8

#

from PyDAQmx import *
from PyDAQmx.DAQmxTypes import *
import numpy
import time

def channelAverage(array, samples, channels) :
    averages = []
    ## easy solution using numpy and slices
    for x in range( channels ) :
        averages.append( numpy.average( array[ ( samples * x ) : ( samples * x ) + samples ] ) )
    return averages

from selectaroo import menu
import visa

rm = visa.ResourceManager()

print( "\nPlease select a calibrator from the list of available devices.")
calibrator = rm.open_resource(menu(rm.list_resources()))

calibrator.write("*RST;*WAI")



voltageSetpoints = ["10.0", "0.0", "-10.0"]
currentSetpoints = ["0.021", "0.0", "-0.021"]

read = int32()
highResVolts = numpy.zeros((8,5), dtype=numpy.float64)
highSpeedVolts = numpy.zeros((600,), dtype=numpy.float64)
taskHandle = TaskHandle(0)


highResVoltsAverage = []
highSpeedVoltsAverage = []


for i in range( len( voltageSetpoints ) ) :

    print( "Setting calibrator to", voltageSetpoints[i], "volts." )

    calibrator.write("OUT "+voltageSetpoints[i]+"V;*WAI;OPER")

    wait = input("Calibrator ready?")

    ## Voltage High Resolution
    print("High Resolution")
    sampleCount = 40

    try:
        DAQmxCreateTask("",byref(taskHandle))
        DAQmxCreateAIVoltageChan(taskHandle, 'cDAQ1Mod1/ai0:7', "", DAQmx_Val_Cfg_Default, -10, 10, DAQmx_Val_Volts,None)
        DAQmxCfgSampClkTiming(taskHandle, "", 2.0, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, sampleCount)
        DAQmxSetAIADCTimingMode(taskHandle, 'cDAQ1Mod1/ai0:7', DAQmx_Val_HighResolution)
        DAQmxStartTask(taskHandle)
        DAQmxReadAnalogF64(taskHandle, -1, -1, DAQmx_Val_GroupByChannel, highResVolts, sampleCount, byref(read), None)
        DAQmxWaitUntilTaskDone(taskHandle, -1)

    except DAQError as err:
        print ("DAQmx Error: %s", err )

    finally:
        if taskHandle:
            DAQmxStopTask(taskHandle)
            DAQmxClearTask(taskHandle)

    for j in range( 8 ):
        highResVoltsAverage.append(numpy.average(highResVolts[j]))
        print( "Channel", j, ":", numpy.average(highResVolts[j]))

    print(highResVoltsAverage)
    
    print("High Speed")
    wait = input("Calibrator ready?")

    ## Voltage High Speed
    sampleCount = 600

    try:
        DAQmxCreateTask("",byref(taskHandle))
        DAQmxCreateAIVoltageChan(taskHandle, 'cDAQ1Mod1/ai0:7', "", DAQmx_Val_Cfg_Default, -10, 10, DAQmx_Val_Volts,None)
        DAQmxCfgSampClkTiming(taskHandle, "", 20.0, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, sampleCount)
        DAQmxSetAIADCTimingMode(taskHandle, 'cDAQ1Mod1/ai0:7', DAQmx_Val_HighSpeed)
        DAQmxStartTask(taskHandle)
        DAQmxReadAnalogF64(taskHandle, -1, 30.0, DAQmx_Val_GroupByChannel, highSpeedVolts, sampleCount, byref(read), None)
        DAQmxWaitUntilTaskDone(taskHandle, -1)

    except DAQError as err:
        print ("DAQmx Error: %s", err )

    finally:
        if taskHandle:
            DAQmxStopTask(taskHandle)
            DAQmxClearTask(taskHandle)

    for j in range( 8 ):
        highSpeedVoltsAverage.append(numpy.average(highSpeedVolts[j]))
        print( "Channel", j, ":", numpy.average(highSpeedVolts[j]))

calibrator.write("STBY")
calibrator.close()

## Lets dump this out now
'''
outputFile = open('NI-9207.csv', 'w')
output = str(x)
outputFile.write(output)
outputFile.close()
'''
for x in range(8):
    print(highResVoltsAverage[x], "\n", highResVoltsAverage[x+8], "\n", highResVoltsAverage[x+16],sep="")
    print(highSpeedVoltsAverage[x], "\n", highSpeedVoltsAverage[x+8], "\n", highSpeedVoltsAverage[x+16],"\n",sep="")

## Current Measuremnets.
