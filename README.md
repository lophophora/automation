# automation
A bunch of calibration automation scripts for various lab devices.  There's a lot of repeat work and leftover files that predate the adoption of a versioning system.

# Warranty

None, you're on your own.

# License

Completely free for personal and academic purposes. In the event that you're a corporation like Transcat, calibration reports must provide credit to the original author and scripts must be made available to customers upon request.
